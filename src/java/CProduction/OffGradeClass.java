package CProduction;

import java.util.*;

public class OffGradeClass
{
     String SCode,SName;

     double dEffy,dUOA,dUM,dUEOA,dUEM;
     double dSCAC,dSC,dShift,dMaint,dElect;

     Common common;
     Vector VStName,VValue,VStCode;

     OffGradeClass()
     {
     }
     OffGradeClass(String SCode,String SName)
     {
          this.SCode = SCode;
          this.SName = SName;

          common  = new Common();
          VStName = new Vector();
          VValue  = new Vector();
          VStCode = new Vector();
          dEffy   = 0;
          dUOA    = 0;
          dUM     = 0;
          dUEOA   = 0;
          dUEM    = 0;
          dSCAC   = 0;
          dSC     = 0;
          dShift  = 0;
          dMaint  = 0;
          dElect  = 0;
     }
     public void append1(String SEffy,String SUOA,String SUM,String SUEOA,String SUEM,String SSC,String SSCAC)
     {
          dEffy   = common.toDouble(SEffy);
          dUOA   += common.toDouble(SUOA);
          dUM    += common.toDouble(SUM);
          dUEOA  += common.toDouble(SUEOA);
          dUEM   += common.toDouble(SUEM);
          dSC    += common.toDouble(SSC);
          dSCAC  += common.toDouble(SSCAC);
     }
     public void append2(String SName,String SValue,String SCode)
     {
          VStName .addElement(SName);
          VValue  .addElement(SValue);
          VStCode .addElement(SCode);
     }
     public void append3(String SShift)
     {
          dShift += common.toDouble(SShift);
     }
     public void append4(String SMaint)
     {
          dMaint += common.toDouble(SMaint);
     }
     public void append5(String SElect)
     {
          dElect += common.toDouble(SElect);
     }
     public String getStopMnt(String SCode)
     {
          int iIndex = VStCode.indexOf(SCode);
          if(iIndex==-1)
               return "0";
          return common.getRound((String)VValue.elementAt(iIndex),0);
     }
}

