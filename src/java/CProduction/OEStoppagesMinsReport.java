/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CProduction;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Vector;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.sql.*;


import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;

import java.io.FileOutputStream;




/**
 *
 * @author root
 */
public class OEStoppagesMinsReport extends HttpServlet {

     HttpSession    session;
     RepVect repVect = new RepVect();
     Common common   = new Common();

     String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody,tbgBody;
     String SStDate,SEnDate,SUnit,SDept;
     String S1Array[][],S2Array[][],S3Array[][],STArray[][],STotArray[];
     String S1T[],S2T[],S3T[],STT[],SLoss[],SDiff[],SUpto[];
     int    iDateDiff=0;
     double dNetZ=0,dLossZ=0,dUptoZ=0,dTotSpdl=0,dSpdlShift=0;

     Vector VCode,VName,VMCode,VMName,VStd,VSpdl;
     Vector V1Code,V1Mnt,V2Code,V2Mnt,V3Code,V3Mnt;
     Vector VHDate;

     FileWriter  FW;
     int Lctr = 70,Pctr = 0;
     String SLine;
     Vector VHead;
     
         String   SFileName,SFileOpenPath,SFileWritePath;
    Document document;
    PdfPTable table,table1;
    PdfPCell c1;
    private static Font bigbold = FontFactory.getFont("TIMES_ROMAN", 10, Font.BOLD);
    private static Font mediumbold = FontFactory.getFont("TIMES_ROMAN", 9, Font.BOLD);
    private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN", 5, Font.NORMAL);
    private static Font smallbold = FontFactory.getFont("TIMES_ROMAN", 9, Font.BOLD);
    private static Font smallbold1 = FontFactory.getFont("TIMES_ROMAN", 5, Font.BOLD);
      
     int iWidth[] = {60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,
    60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60,60};//,
        int iTotalColumns = 53;
        
   

 int iWidth1[]     ={15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15};
 int iTotalColumns1 = 18;


     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);

          bgColor = common.bgColor;
          bgHead  = common.bgHead;
          bgUom   = common.bgUom;
          bgBody  = common.bgBody;
          fgHead  = common.fgHead;
          fgUom   = common.fgUom;
          fgBody  = common.fgBody;
          tbgBody = common.tbgBody;
     }
     
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /*
             * TODO output your page here. You may use following sample code.
             */
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet OEStoppagesMinsReport</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet OEStoppagesMinsReport at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }
    
      private void createPDFFile()
    {
        try 
        {
			document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(SFileWritePath));
            document.setPageSize(PageSize.LEGAL.rotate());
            document.open();
                             
            table = new PdfPTable(iTotalColumns);
            table.setWidths(iWidth);
            table.setWidthPercentage(100);
            
            table1 = new PdfPTable(iTotalColumns1);
            table1.setWidths(iWidth1);
            table1.setWidthPercentage(100);
            //table.setHeaderRows(3);
        } 
        catch (Exception ex) 
        {
            ex.printStackTrace();
            System.out.println(ex);
        }

    }
    

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
		response.setContentType("text/html");
          PrintWriter out  = response.getWriter();
		session = request.getSession(true);
		/*if(session.getValue("AppletTag")==null)
		{
			Control control = new Control();
			String SServer = control.getID("Select ProdTomHost from Server");
			response.sendRedirect(SServer + "/CProduction.SessionExpired");
			return;

		}*/
		setCriteria(request);
                
      //           SFileName = "d:\\CProduction\\OEStoppagesMinsReport.pdf";
      
      SFileName      = "pdfreports/C-UnitOEStoppagesMinsReport.pdf";
            SFileOpenPath  = "http://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/"+SFileName;
            SFileWritePath = request.getRealPath("/")+SFileName; 

            out.println("<p>");
            out.println(" <a href='"+SFileOpenPath+"' target='_blank'><b>View this PDF</b></a>");
            out.println("</p>"); 
                
                createPDFFile();

          setVectors();
          setSArray();

          try
          {
               FW      = new FileWriter("/software/C-Prod-Print/Reports/OEMinsStopc.prn");
//                  FW      = new FileWriter("D:\\OEMinsStopc.prn");
               Lctr    = 70;
               Pctr    =  0;
               Head(0,13);
               Body();
               FW.close();
               
          }catch(Exception ex){}

          out.println("<html>");
          out.println("<head>");
          out.println("<title>OE Stoppages Report - Period </title>");
          out.println("</head>");
          out.println("<body bgcolor='" + common.bgBody + "'>");
          //out.println(session.getValue("AppletTag"));
          out.println("<a href='Header'>Home</a>");
          out.println(common.getConsReportHeader("OE Stoppages Report - Period",SUnit,SStDate,SEnDate));
          out.println("<table border='1' width='361' height='303'>");
          
          	 

AddCellIntoTable("AMARJOTHI SPINNING MILLS LIMITED", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,20f,0,0,0,0, bigbold);
AddCellIntoTable("OE STOPPAGES MINUTES REPORT FROM " +common.parseDate(String.valueOf(SStDate))+"--"+common.parseDate(String.valueOf(SEnDate)), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,20f,0,0,0,0, bigbold);
AddCellIntoTable("UNIT       : "+(SUnit.equals("1")?"A":(SUnit.equals("2")?"B":(SUnit.equals("3")?"C":"S"))), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,20f,0,0,0,0, bigbold);      

AddCellIntoTable("Reason", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);


for(int k=0;k<13;k++)
          {
AddCellIntoTable((String)VName.elementAt(k), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 4,25f,4,1,8,2, smallbold);
          }
AddCellIntoTable("Shift", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
for(int k=0;k<13;k++)
{
AddCellIntoTable("I", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("II", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("III", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("Tot", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
}
AddCellIntoTable("MacNo", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
for(int k=0;k<13;k++)
{
AddCellIntoTable("Min", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("Min", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("Min", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("Min", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
}
  
  for(int i=0;i<VMName.size();i++)
          {
             
    AddCellIntoTable((String)VMName.elementAt(i), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);          
               for(int k=0;k<13;k++)
               {
AddCellIntoTable(S1Array[i][k], table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(S2Array[i][k], table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(S3Array[i][k], table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(STArray[i][k], table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
            }
            }
  
  AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,0,0,0,0, smallnormal);          
for(int k=0;k<13;k++)
          {
AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,0,0,0,0, smallnormal);          
AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,0,0,0,0, smallnormal);          
AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,0,0,0,0, smallnormal);          
AddCellIntoTable("", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,0,0,0,0, smallnormal);          
          }

AddCellIntoTable("Total", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);          
for(int k=0;k<13;k++)
          {
AddCellIntoTable(S1T[k], table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(S2T[k], table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(S3T[k], table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(STT[k], table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
          }

AddCellIntoTable("Loss %", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);          
for(int k=0;k<13;k++)
          {
AddCellIntoTable(".", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(".", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(".", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(SLoss[k], table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
          }

AddCellIntoTable("Upto Date Loss %", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);          
for(int k=0;k<13;k++)
          {
AddCellIntoTable(".", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(".", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(".", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(SUpto[k], table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
          }

AddCellIntoTable("Std %", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);          
for(int k=0;k<13;k++)
          {
AddCellIntoTable(".", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(".", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(".", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable((String)VStd.elementAt(k), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
          }
AddCellIntoTable("Diff %" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);          
for(int k=0;k<13;k++)
          {          
AddCellIntoTable( "." , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable("." , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable("." , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
if(common.toDouble(SDiff[k])<0)
AddCellIntoTable( SDiff[k] , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
else
AddCellIntoTable( SDiff[k] , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
          }   
               
   
// from this table 1 pdf


AddCellIntoTable("Reason", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);


for(int k=13;k<17;k++)
          {
AddCellIntoTable((String)VName.elementAt(k), table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 4,25f,4,1,8,2, smallbold);
          }
AddCellIntoTable("Net", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);

AddCellIntoTable("Shift", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
for(int k=13;k<17;k++)
{
AddCellIntoTable("I", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("II", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("III", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("Tot", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
}
AddCellIntoTable("", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);


AddCellIntoTable("MacNo", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
for(int k=13;k<17;k++)
{
AddCellIntoTable("Mins", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("Mins", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("Mins", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("Mins", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
}
AddCellIntoTable("Mins", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
  

  for(int i=0;i<VMName.size();i++)
          {
             
    AddCellIntoTable((String)VMName.elementAt(i), table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);          
               for(int k=13;k<17;k++)
               {
AddCellIntoTable(S1Array[i][k], table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(S2Array[i][k], table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(S3Array[i][k], table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(STArray[i][k], table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          

               }
// this is for net               
AddCellIntoTable(STotArray[i], table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
// till this  
               
            }
  
  AddCellIntoTable("", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,0,0,0,0, smallnormal);          
for(int k=13;k<17;k++)
          {
AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,0,0,0,0, smallnormal);          
AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,0,0,0,0, smallnormal);          
AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,0,0,0,0, smallnormal);          
AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,0,0,0,0, smallnormal);          
    
          }
AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,0,0,0,0, smallnormal);          


AddCellIntoTable("Total", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);          
{
for(int k=13;k<17;k++)
          {
AddCellIntoTable(S1T[k], table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(S2T[k], table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(S3T[k], table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(STT[k], table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
     }

AddCellIntoTable(common.getRound(dNetZ,0), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
}

AddCellIntoTable("Loss %", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);          
{
for(int k=13;k<17;k++)
          {
AddCellIntoTable(".", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(".", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(".", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(SLoss[k], table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          

         }
        
dLossZ   = dNetZ*100/(VMCode.size()*3*(iDateDiff*480));    //dTotSpdl
                           

AddCellIntoTable(common.getRound(dLossZ,3), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
}


AddCellIntoTable("Upto Date Loss %", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);          
{
 for(int k=0;k<13;k++) //k=0;
          {
AddCellIntoTable(".", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(".", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(".", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(SUpto[k], table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
            }
 
 	 dUptoZ   = dUptoZ*100/(dTotSpdl*3*(iDateDiff*480));
 
 AddCellIntoTable(common.getRound(dUptoZ,3), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
}


AddCellIntoTable("Std %", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);          
{
    for(int k=13;k<17;k++)
          {
AddCellIntoTable(".", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(".", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable(".", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable((String)VStd.elementAt(k), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          


          }
AddCellIntoTable(".", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
}

AddCellIntoTable("Diff %" , table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);          
{
for(int k=13;k<17;k++)
          {          
AddCellIntoTable( "." , table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable("." , table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
AddCellIntoTable("." , table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
if(common.toDouble(SDiff[k])<0)
AddCellIntoTable( SDiff[k] , table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
else
AddCellIntoTable( SDiff[k] , table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          

          }
AddCellIntoTable(".", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);          
          }   

         
          
          out.println("<tr>");
          out.println("<td width='121' height='61' rowspan='3' valign='top' bgcolor='"+bgHead+"'>");
          out.println("<p align='right'><font size='4' color='"+fgHead+"'>Reason</font></p>");
          out.println("<p align='right'><font size='4' color='"+fgHead+"'>Shift</font></p>");
          out.println("<p align='center'><font size='4' color='"+fgHead+"'>Mach Nº</font></td>");
          for(int k=0;k<VName.size();k++)
          {
               out.println("<td width='224' height='24' colspan='4' bgcolor='"+bgHead+"'><p align='center'><font size='4' color='"+fgHead+"'>"+(String)VName.elementAt(k)+"</font></td>");
          }
          out.println("</tr>");
          out.println("<tr>");
          for(int k=0;k<VName.size();k++)
          {
               out.println("<td width='70' height='19' valign='top' bgcolor='"+bgHead+"'><p align='center'><font color='"+fgHead+"'>I</font></td>");
               out.println("<td width='70' height='19' valign='top' bgcolor='"+bgHead+"'><p align='center'><font color='"+fgHead+"'>II</font></td>");
               out.println("<td width='70' height='19' valign='top' bgcolor='"+bgHead+"'><p align='center'><font color='"+fgHead+"'>III</font></td>");
               out.println("<td width='70' height='19' valign='top' bgcolor='"+bgHead+"'><p align='center'><font color='"+fgHead+"'>Total</font></td>");
          }
          out.println("</tr>");
          out.println("<tr>");
          for(int k=0;k<VName.size();k++)
          {
               out.println("<td width='70' height='18' valign='top' bgcolor='"+bgUom+"'><p align='center'><font color='"+fgUom+"'>Mins</font></td>");
               out.println("<td width='70' height='18' valign='top' bgcolor='"+bgUom+"'><p align='center'><font color='"+fgUom+"'>Mins</font></td>");
               out.println("<td width='70' height='18' valign='top' bgcolor='"+bgUom+"'><p align='center'><font color='"+fgUom+"'>Mins</font></td>");
               out.println("<td width='70' height='18' valign='top' bgcolor='"+bgUom+"'><p align='center'><font color='"+fgUom+"'>Mins</font></td>");
          }
          out.println("</tr>");

          for(int i=0;i<VMName.size();i++)
          {
               out.println("<tr>");
               out.println("<td width='121' height='31' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VMName.elementAt(i)+"</font></td>");
               for(int k=0;k<VName.size();k++)
               {
                    out.println("<td width='70' height='31' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S1Array[i][k]+"</font></td>");
                    out.println("<td width='70' height='31' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S2Array[i][k]+"</font></td>");
                    out.println("<td width='70' height='31' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S3Array[i][k]+"</font></td>");
                    out.println("<td width='70' height='31' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+STArray[i][k]+"</font></td>");
               }
               out.println("</tr>");
          }
          out.println("<tr>");
          out.println("<td width='121' height='34' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>Total</font></td>");
          for(int k=0;k<VName.size();k++)
          {
               out.println("<td width='70' height='34' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+S1T[k]+"</font></td>");
               out.println("<td width='70' height='34' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+S2T[k]+"</font></td>");
               out.println("<td width='70' height='34' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+S3T[k]+"</font></td>");
               out.println("<td width='70' height='34' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+STT[k]+"</font></td>");
          }
          out.println("</tr>");
          out.println("<tr>");
          out.println("<td width='121' height='31' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>Loss %</font></td>");
          for(int k=0;k<VName.size();k++)
          {
               out.println("<td width='70' height='31' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>.</font></td>");
               out.println("<td width='70' height='31' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>.</font></td>");
               out.println("<td width='70' height='31' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>.</font></td>");
               out.println("<td width='70' height='31' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SLoss[k]+"</font></td>");
          }
          out.println("</tr>");
          out.println("<tr>");
          out.println("<td width='121' height='25' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>Upto Date Loss %</font></td>");
          for(int k=0;k<VName.size();k++)
          {
               out.println("<td width='70' height='25' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>.</font></td>");
               out.println("<td width='70' height='25' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>.</font></td>");
               out.println("<td width='70' height='25' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>.</font></td>");
               out.println("<td width='70' height='25' bgcolor='"+bgBody+"'><font color='"+fgBody+"'>"+SUpto[k]+"</font></td>");
          }
          out.println("</tr>");
          out.println("<tr>");
          out.println("<td width='121' height='29' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>Std %</font></td>");
          for(int k=0;k<VName.size();k++)
          {
               out.println("<td width='70' height='29' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>.</font></td>");
               out.println("<td width='70' height='29' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>.</font></td>");
               out.println("<td width='70' height='29' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>.</font></td>");
               out.println("<td width='70' height='29' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+(String)VStd.elementAt(k)+"</font></td>");
          }
          out.println("</tr>");
          out.println("<tr>");
          out.println("<td width='121' height='31' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>Ex/Short</font></td>");
          for(int k=0;k<VName.size();k++)
          {
               out.println("<td width='70' height='31' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>.</font></td>");
               out.println("<td width='70' height='31' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>.</font></td>");
               out.println("<td width='70' height='31' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>.</font></td>");
               if(common.toDouble(SDiff[k])<0)
                    out.println("<td width='70' height='31' bgcolor='"+fgUom+"'><font color='"+bgUom+"'>"+SDiff[k]+"</font></td>");
               else
                    out.println("<td width='70' height='31' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SDiff[k]+"</font></td>");
          }
          out.println("</tr>");
          out.println("</table>");
          out.println("</div>");
          out.println("</body>");
          out.println("</html>");

          out.close();
          
          
	       try
            {
            document.add(table);
            document.newPage();
            document.add(table1);
            document.close();            
            }
            catch(Exception e)		 
            {
            e.printStackTrace();
            System.out.println(e);
            }
		
          
          
     }

     public void setVectors()
     {
          VCode    = new Vector();
          VName    = new Vector();
          VMCode   = new Vector();
          VMName   = new Vector();
          VSpdl    = new Vector();
          VStd     = new Vector();
          V1Code   = new Vector();
          V1Mnt    = new Vector();
          V2Code   = new Vector();
          V2Mnt    = new Vector();
          V3Code   = new Vector();
          V3Mnt    = new Vector();
          VHDate   = new Vector();

          dTotSpdl = 0;
          dUptoZ   = 0; 
          try
          {
                  Connection con = createConnection();
                  Statement stat = con.createStatement();
                  ResultSet res  = stat.executeQuery(getQSReasons(SDept));
                  while(res.next())
                  {
                         VCode .addElement(res.getString(1));
                         VName .addElement(res.getString(2));
                         VStd  .addElement(res.getString(3));
                         String SPri  = res.getString(4);
                  }
                  res.close();
                  res   = stat.executeQuery("Select * from ProdHoliday");
                  while(res.next())
                  {
                        VHDate.addElement(res.getString(1));
                  }

                  ResultSet res1 = stat.executeQuery(getQSMacs(SDept,SUnit));
                  while(res1.next())
                  {
                         VMCode .addElement(res1.getString(1));
                         VMName .addElement(res1.getString(2));
                         double dSpdl = res1.getDouble(3);
                         VSpdl  .addElement(common.getRound(dSpdl/480,4));
                         dTotSpdl += dSpdl;
                  }
                 try{stat.execute("Drop Table gskStop1");}catch(Exception ex){}//System.out.println("from : 209 "+ex);}
                  try{stat.execute("Drop Table gskStop2");}catch(Exception ex){}//System.out.println("from : 210 "+ex);}
                  try{stat.execute(getQS1());}catch(Exception ex){System.out.println("from : 211 "+ex);}
                  try{stat.execute(getQSMonth());}catch(Exception ex){System.out.println("from : 212 "+ex);}
                  ResultSet res10 = stat.executeQuery(getQS3());
                  SUpto   = new String[VCode.size()];  //for Total
                  int k=0;
                  double dNet = 3*8*60*common.toInt(SStDate.substring(6,8));
                  dSpdlShift = dTotSpdl/(VSpdl.size()*480);
                  while(res10.next())
                  {
                          String SDummy = res10.getString(1);
                          double dUptoX = res10.getDouble(2);
                          dUptoZ       += dUptoX;
                          SUpto[k]      = common.getRound(dUptoX*dSpdlShift/dNet,3);
                          k++;
                  }
                  con.close();
          }
          catch(Exception ex)
          {
               System.out.println("StopPeriod 226 : "+ex);
               ex.printStackTrace();
          }
     }
     public String getQSReasons(String SDept)
     {
          String QS = " SELECT stoppage.stop_code, stopreasons.stop_name,"+
                      " stoppage.MillStd,stoppage.RepPriority "+
                      " FROM stoppage INNER JOIN "+
                      " stopreasons ON stoppage.stop_code = stopreasons.STOP_CODE "+
                      " WHERE stoppage.dept_code="+SDept+
                      " ORDER BY Stoppage.stop_code,Stoppage.RepPriority";
          return QS;
     }
     public String getQSMacs(String SDept,String SUnit)
     {
          String QS = "SELECT machine.mach_code, machine.mach_name,machine.noofRotters FROM machine "+
                      " WHERE machine.dept_code="+SDept+" and machine.unit_code="+SUnit+
                      " ORDER BY to_number(machine.mach_name) ";
          return QS;
     }
     public String getQS1()
     {
          String QS = " create table gskstop1 as "+
                      "  (SELECT stoppage.stop_code "+
                      " FROM stoppage "+
                      " WHERE stoppage.dept_code="+SDept+")"+
                      " Order By stoppage.RepPriority,Stoppage.stop_Code";
          return QS;
     }
     public String getQS2(String SMac)
     {
          String QS = " create table gskStop2 "+ 
			       " as (SELECT Sum(StopTran.Stop_Mint) AS SumOfStop_Mint, "+
                      " gskstop1.stop_code AS Expr1 "+
                      " FROM gskstop1 INNER JOIN StopTran ON "+
                      " gskstop1.stop_code = StopTran.Stop_Code "+
                      " WHERE (((StopTran.Stop_Date)>='"+SStDate+"' And "+
                      " (StopTran.Stop_Date)<='"+SEnDate+"') AND "+
                      " ((StopTran.Stop_Shift)=1) AND "+
                      " ((StopTran.Unit_Code)="+SUnit+") AND "+
                      " ((StopTran.Dept_Code)="+SDept+") AND "+
                      " ((StopTran.Mach_Code)="+SMac+")) "+
                      " GROUP BY gskstop1.stop_code)"+
                      " order By gskstop1.stop_code";

          return QS;
     }
     public String getQS22(String SMac)
     {
          String QS = " create table gskStop2 "+
                      " as (SELECT Sum(StopTran.Stop_Mint) AS SumOfStop_Mint, "+
                      " gskstop1.stop_code AS Expr1 "+
                      " FROM gskstop1 INNER JOIN StopTran ON "+
                      " gskstop1.stop_code = StopTran.Stop_Code "+
                      " WHERE (((StopTran.Stop_Date)>='"+SStDate+"' And "+
                      " (StopTran.Stop_Date)<='"+SEnDate+"') AND "+
                      " ((StopTran.Stop_Shift)=2) AND "+
                      " ((StopTran.Unit_Code)="+SUnit+") AND "+
                      " ((StopTran.Dept_Code)="+SDept+") AND "+
                      " ((StopTran.Mach_Code)="+SMac+")) "+
                      " GROUP BY gskstop1.stop_code)"+
                      " order By gskstop1.stop_code";


          return QS;
     }
     public String getQS23(String SMac)
     {
          String QS = " create table gskStop2 "+
                      " as (SELECT Sum(StopTran.Stop_Mint) AS SumOfStop_Mint, "+
                      " gskstop1.stop_code AS Expr1 "+
                      " FROM gskstop1 INNER JOIN StopTran ON "+
                      " gskstop1.stop_code = StopTran.Stop_Code "+
                      " WHERE (((StopTran.Stop_Date)>='"+SStDate+"' And "+
                      " (StopTran.Stop_Date)<='"+SEnDate+"') AND "+
                      " ((StopTran.Stop_Shift)=3) AND "+
                      " ((StopTran.Unit_Code)="+SUnit+") AND "+
                      " ((StopTran.Dept_Code)="+SDept+") AND "+
                      " ((StopTran.Mach_Code)="+SMac+")) "+
                      " GROUP BY gskstop1.stop_code)"+
                      " order By gskstop1.stop_code";

          return QS;
     }
     public String getQSMonth()
     {
            String QS = " create table GskStop2 "+
                      " as (SELECT Sum(StopTran.Stop_Mint) AS SumOfStop_Mint, "+
                      " gskstop1.stop_code AS Expr1 "+
                      " FROM gskstop1 INNER JOIN StopTran ON "+
                      " gskstop1.stop_code=StopTran.Stop_Code "+
                      " WHERE stoptran.stop_date >= '"+common.getStartDate(SEnDate)+"'"+
                      " AND stoptran.stop_date <= '"+SEnDate+"'"+
                      " AND StopTran.Unit_Code="+SUnit+
                      " AND StopTran.Dept_Code="+SDept+
                      " GROUP BY gskstop1.stop_code)";
         
         /*    String QS=" SELECT StopTran.stop_code As Expr1,Sum(StopTran.Stop_Mint) AS SumOfStop_Mint "+
                      "   from StopTran "+
                      " WHERE stoptran.stop_date >= '"+common.getStartDate(SEnDate)+"'"+
                      " AND stoptran.stop_date <= '"+SEnDate+"'"+
                      " AND StopTran.Unit_Code="+SUnit+
                      " AND StopTran.Dept_Code="+SDept+
                      " GROUP BY StopTran.stop_code"; */
          return QS;

        //" WHERE (((substr([StopTran].[Stop_Date],5,2))="+SStDate.substring(4,6)+") And "+
     }
     public String getQS3()
     {
          String QS = " SELECT gskstop1.stop_code, gskstop2.SumOfStop_Mint "+
                      " FROM gskstop1 LEFT JOIN gskstop2 ON "+
                      " gskstop1.stop_code = gskstop2.Expr1 "+
                      " order by gskstop1.stop_Code";
          return QS;
     }
     public void setSArray()
     {
          dNetZ = 0;
          String SMac;
          double dTotal,d1T,d2T,d3T;
          double d11T,d22T,d33T;
          S1Array   = new String[VMCode.size()][VCode.size()];  //for I Shift
          S2Array   = new String[VMCode.size()][VCode.size()];  //for II Shift
          S3Array   = new String[VMCode.size()][VCode.size()];  //for III Shift
          STArray   = new String[VMCode.size()][VCode.size()];  //for Total
          STotArray = new String[VMCode.size()];  //for Net Total

          S1T     = new String[VCode.size()+1];  //for Total
          S2T     = new String[VCode.size()+1];  //for Total
          S3T     = new String[VCode.size()+1];  //for Total
          STT     = new String[VCode.size()+1];  //for Total
          SLoss   = new String[VCode.size()+1];  //for Total
          SDiff   = new String[VCode.size()+1];  //for Total

          //Row - Machines
          for(int i=0;i<VMCode.size();i++)
          {
               SMac = (String)VMCode.elementAt(i);

               getMntVectors(SMac);

               double dNet = 0;
               //Col - Stop Reasons
               for(int j=0;j<VCode.size();j++)
               {

                    dTotal = 0;

                    STArray[i][j] = "";


                    if((String)V1Mnt.elementAt(j)==null)
                         S1Array[i][j] = "-";
                    else
                    {
                         double dSpdl1 = common.toDouble((String)V1Mnt.elementAt(j));
                         S1Array[i][j] = common.getRound(dSpdl1,0);
                         dTotal       += dSpdl1;
                    }
                    if((String)V2Mnt.elementAt(j)==null)
                         S2Array[i][j] = "-";
                    else
                    {
                         double dSpdl2 = common.toDouble((String)V2Mnt.elementAt(j));
                         S2Array[i][j] = common.getRound(dSpdl2,0);
                         dTotal       += dSpdl2;
                    }
                    if((String)V3Mnt.elementAt(j)==null)
                         S3Array[i][j] = "-";
                    else
                    {
                         double dSpdl3 = common.toDouble((String)V3Mnt.elementAt(j));
                         S3Array[i][j] = common.getRound(dSpdl3,0);
                         dTotal       += dSpdl3;
                    }
                    if(dTotal<=0)
                         STArray[i][j] = ".";
                    else
                    STArray[i][j] = common.getRound(String.valueOf(dTotal),0);
                    dNet += common.toDouble(STArray[i][j]);               //dTotal;
               }
               STotArray[i] = common.getRound(dNet,0);
               dNetZ        += dNet;  

          }
          for(int i=0; i<VHDate.size(); i++)
          {
                int iHDate      = common.toInt((String)VHDate.elementAt(i));
                if(iHDate>=common.toInt(SStDate) && iHDate<=common.toInt(SEnDate))
                        iDateDiff=iDateDiff-1;
          }

          for(int j=0;j<VCode.size();j++)
          {
               d1T=0;d2T=0;d3T=0;                      
               d11T=0;d22T=0;d33T=0;
               for(int i=0;i<VMCode.size();i++)
               {                                            
                    d1T =d1T+common.toDouble(S1Array[i][j])*common.toDouble((String)VSpdl.elementAt(i));
                    d11T=d11T+common.toDouble(S1Array[i][j]);
                    d2T =d2T+common.toDouble(S2Array[i][j])*common.toDouble((String)VSpdl.elementAt(i)); 
                    d22T=d22T+common.toDouble(S2Array[i][j]);//*common.toDouble((String)VSpdl.elementAt(i));
                    d3T =d3T+common.toDouble(S3Array[i][j])*common.toDouble((String)VSpdl.elementAt(i));
                    d33T=d33T+common.toDouble(S3Array[i][j]);//*common.toDouble((String)VSpdl.elementAt(i));

               }
               S1T[j]   = (d11T<=0?".":common.getRound(String.valueOf(d11T),0));  //d1T
               S2T[j]   = (d22T<=0?".":common.getRound(String.valueOf(d22T),0));  //d2T
               S3T[j]   = (d33T<=0?".":common.getRound(String.valueOf(d33T),0));  //d3T


               STT[j]   = ((d11T+d22T+d33T)<=0?".":common.getRound(String.valueOf(d11T+d22T+d33T),0));


               SLoss[j] = common.getRound(String.valueOf((d11T+d22T+d33T)*100/(3*(VMCode.size())*(iDateDiff*480))),3);
               SLoss[j] = ((common.toDouble(SLoss[j])==0)?".":SLoss[j]);

               double dStd  = common.toDouble((String)VStd.elementAt(j));
               double dUpto = common.toDouble(SUpto[j]+" ");
               SDiff[j]     = common.getRound((dUpto-dStd),3);
               SDiff[j] = ((common.toDouble(SDiff[j])==0)?".":SDiff[j]);
          }
     }
     public void getMntVectors(String SMac)
     {
          V1Code.removeAllElements();
          V1Mnt.removeAllElements();
          V2Code.removeAllElements();
          V2Mnt.removeAllElements();
          V3Code.removeAllElements();
          V3Mnt.removeAllElements();

          try
          {
                  Connection con = createConnection();
                  Statement stat = con.createStatement();
/*                try{stat.execute("Drop Table gskStop1");}catch(Exception ex){}//System.out.println("from : 413 "+ex);}
                  try{stat.execute("Drop Table gskStop2");}catch(Exception ex){}//System.out.println("from : 414 "+ex);}
                  try{stat.execute(getQS1());}catch(Exception ex){System.out.println("from : 415 "+ex);}
                  try{stat.execute(getQS2(SMac));}catch(Exception ex){System.out.println("from : 416 "+ex);}
                    
                  ResultSet res2 = stat.executeQuery(getQS3());
                  while(res2.next())
                  {
                          V1Code .addElement(res2.getString(1));
                          V1Mnt  .addElement(res2.getString(2));
                  }
                  try{stat.execute("Drop Table gskStop2");}catch(Exception ex){}//System.out.println("from : 423 "+ex);}
                  try{stat.execute(getQS22(SMac));}catch(Exception ex){System.out.println("from : 424 "+ex);}
                  ResultSet res3 = stat.executeQuery(getQS3());
                  while(res3.next())
                  {
                          V2Code .addElement(res3.getString(1));
                          V2Mnt  .addElement(res3.getString(2));
                  }
                  try{stat.execute("Drop Table gskStop2");}catch(Exception ex){}//System.out.println("from : 431 "+ex);}
                  try{stat.execute(getQS23(SMac));}catch(Exception ex){System.out.println("from : 432 "+ex);}
                  ResultSet res4 = stat.executeQuery(getQS3());
                  while(res4.next())
                  {
                          V3Code .addElement(res4.getString(1));
                          V3Mnt  .addElement(res4.getString(2));
                  }*/
                  ResultSet rs = stat.executeQuery(getShiftWiseStoppageQS(SMac,1));
                  while(rs.next())
                  {
                      String SCode= rs.getString(1);
                      String SMint= rs.getString(2);
                      V1Code.addElement(SCode);
                      V1Mnt.addElement(SMint);                          
                  }
                  rs.close();
                  rs = stat.executeQuery(getShiftWiseStoppageQS(SMac,2));
                  while(rs.next())
                  {
                      String SCode= rs.getString(1);
                      String SMint= rs.getString(2);
                      V2Code.addElement(SCode);
                      V2Mnt.addElement(SMint);                          
                  }
                  rs.close();      
                                    rs = stat.executeQuery(getShiftWiseStoppageQS(SMac,3));
                  while(rs.next())
                  {
                      String SCode= rs.getString(1);
                      String SMint= rs.getString(2);
                      V3Code.addElement(SCode);
                      V3Mnt.addElement(SMint);                          
                  }
                  rs.close();      

                  stat.close();

                  con.close();
          }
          catch(Exception ex)
          {
               System.out.println("StopPeriod 443 : "+ex);
          }
     }
     public String getShiftWiseStoppageQS(String SMac,int iShift)
     {

        String QS= " Select StopReasons.stop_Code,t.StopMint,t.Shift from StopReasons"+
                   " left join (select Stop_Shift as Shift,stopTran.stop_Code as Stop_Code,sum(Stop_Mint) as StopMint from StopTran"+
                   " where StopTran.Dept_code="+SDept+" and Stop_date>='"+SStDate+"' and Stop_date<='"+SEnDate+"' and StopTran.Mach_Code="+SMac+
                   " and StopTran.Stop_shift="+iShift+
                   " and stoptran.stop_Date not in(select HDate from ProdHoliday where leaveday = 1)"+
                   " group by StopTran.Stop_Code,stop_Shift) t on StopReasons.stop_Code=t.stop_Code Inner join stoppage on Stoppage.stop_code = stopreasons.STOP_CODE and stoppage.dept_code="+SDept+
                   " order by 1 ";
        
        

        return QS;
     }

     public void Head(int iStartValue,int iEndValue)
     {
          if(Lctr < 60)
             return;
          if(Pctr > 0)
          {
             try
             {
               FW.write(SLine+"\n");
             }catch(Exception ex)
             {
               System.out.println(ex);
             }
          }
          Pctr++;

          String Str1    = common.Pad("Company    : AMARJOTHI SPINNING MILLS LIMITED",80);
          String Str2    = common.Pad("Document   : OE Minutes Stoppage Report During "+common.parseDate(SStDate)+" & "+common.parseDate(SEnDate),80);
          String Str3    = common.Pad("Unit       : "+(SUnit.equals("1")?"A":(SUnit.equals("2")?"B":"S")),70);
          VHead = Stop1Head(iStartValue,iEndValue);
          try
          {
               FW.write(Str1+"\n");
               FW.write(Str2+"\n");
               FW.write(Str3+"\n\nM");
               for(int i=0;i<VHead.size();i++)
               {
                    FW.write((String)VHead.elementAt(i));
               }
          }catch(Exception ex){}
          Lctr = 4+VHead.size();
     }
     public void Body()
     {
         dLossZ = 0 ;
         String Strl = "";
         for(int i=0;i<VMName.size();i++)
         {
               Strl = common.Pad((String)VMName.elementAt(i),6)+common.Space(2);
               for(int k=0;k<13;k++) // VName.size()
               {
                     Strl = Strl+common.Rad(S1Array[i][k],3)+" "+
                            common.Rad(S2Array[i][k],3)+" "+
                            common.Rad(S3Array[i][k],3)+" "+
                            common.Rad(STArray[i][k],4)+common.Space(2);
               }
               if(VName.size()==13)
                    Strl += common.Rad(STotArray[i],6);
                try
                {
                     Head(0,13);
                     FW.write(Strl+"\n");
                     Lctr++;
                }catch(Exception ex)
                {
                    System.out.println(ex);
                }
         }
          String ST = common.Pad("Total",6)+common.Space(2);
          String SL = common.Pad("Loss %",6)+common.Space(2);
          String SU = common.Pad("Upto %",6)+common.Space(2);
          String SD = common.Pad("Diff %",6)+common.Space(2);
          String SS = common.Pad("Std  %",6)+common.Space(2);
          for(int k=0;k<13;k++)      //VName.size()
          {
                ST = ST+common.Rad(" ",3)+" "+common.Rad(" ",3)+" "+common.Rad(STT[k],8)+common.Space(2);
                SL = SL+common.Rad(" ",3)+" "+common.Rad(" ",3)+" "+common.Rad(SLoss[k],8)+common.Space(2);
                SD = SD+common.Rad(" ",3)+" "+common.Rad(" ",3)+" "+common.Rad(SDiff[k],8)+common.Space(2);
                SU = SU+common.Rad(" ",3)+" "+common.Rad(" ",3)+" "+common.Rad(SUpto[k],8)+common.Space(2);
                SS = SS+common.Rad(" ",3)+" "+common.Rad(" ",3)+" "+common.Rad(common.getRound((String)VStd.elementAt(k),3),8)+common.Space(2);
          }
          if(VName.size()>13)
          {
               dLossZ   = dNetZ*100/(VMCode.size()*3*(iDateDiff*480));
               dUptoZ   = dUptoZ*100/(dTotSpdl*3*common.toInt(SEnDate.substring(6,8)));
                try
                {
                     FW.write(SLine+"\n");
                     FW.write(ST+"\n\n");
                     FW.write(SL+"\n\n");
                     FW.write(SU+"\n\n");
                     FW.write(SS+"\n\n");
                     FW.write(SD+"\n\n");
                     FW.write(SLine+"\n");
//                     FW.write("<End Of Report>");
                }catch(Exception ex){}
     
              Lctr=61; 
     
              Head(13,VName.size());
     
              for(int i=0;i<VMName.size();i++)
              {
                    Strl = common.Pad((String)VMName.elementAt(i),6)+common.Space(2);
                    for(int k=13;k<VName.size();k++)  // k=0
                    {
                          Strl = Strl+common.Rad(S1Array[i][k],3)+" "+
                                 common.Rad(S2Array[i][k],3)+" "+
                                 common.Rad(S3Array[i][k],3)+" "+
                                 common.Rad(STArray[i][k],4)+common.Space(2);
                    }
                    Strl += common.Rad(STotArray[i],6);
                     try
                     {
                          Head(13,VName.size());
                          FW.write(Strl+"\n");
                          Lctr++;
                     }catch(Exception ex){}
              }
               ST = common.Pad("Total",6)+common.Space(2);
               SL = common.Pad("Loss %",6)+common.Space(2);
               SU = common.Pad("Upto %",6)+common.Space(2);
               SD = common.Pad("Diff %",6)+common.Space(2);
               SS = common.Pad("Std  %",6)+common.Space(2);
               for(int k=13;k<VName.size();k++) //k=0;
               {
                     ST = ST+common.Rad(" ",3)+" "+common.Rad(" ",3)+" "+common.Rad(STT[k],8)+common.Space(2);
                     SL = SL+common.Rad(" ",3)+" "+common.Rad(" ",3)+" "+common.Rad(SLoss[k],8)+common.Space(2);
                     SD = SD+common.Rad(" ",3)+" "+common.Rad(" ",3)+" "+common.Rad(SDiff[k],8)+common.Space(2);
                     SU = SU+common.Rad(" ",3)+" "+common.Rad(" ",3)+" "+common.Rad(SUpto[k],8)+common.Space(2);
                     SS = SS+common.Rad(" ",3)+" "+common.Rad(" ",3)+" "+common.Rad(common.getRound((String)VStd.elementAt(k),3),8)+common.Space(2);
               }
               System.out.println(dNetZ+","+VMCode.size()+","+iDateDiff) ;
               dLossZ   = dNetZ*100/(VMCode.size()*3*(iDateDiff*480));    //dTotSpdl
               dUptoZ   = dUptoZ*100/(dTotSpdl*3*common.toInt(SEnDate.substring(6,8)));
                try
                {
                     FW.write(SLine+"\n");
                     FW.write(ST+common.Rad(common.getRound(dNetZ,0),6)+"\n\n");
                     FW.write(SL+common.Rad(common.getRound(dLossZ,3),6)+"\n\n");
                     FW.write(SU+common.Rad(common.getRound(dUptoZ,3),6)+"\n\n");
                     FW.write(SS+"\n\n");
                     FW.write(SD+"\n\n");
                     FW.write(SLine+"\n");
                     FW.write("<End Of Report>");
                }catch(Exception ex){}
           }
           else
           {

               dLossZ   = dNetZ*100/(VMCode.size()*3*(iDateDiff*480));    //dTotSpdl
               dUptoZ   = dUptoZ*100/(dTotSpdl*3*common.toInt(SEnDate.substring(6,8)));
                try
                {
                     FW.write(SLine+"\n");
                     FW.write(ST+common.Rad(common.getRound(dNetZ,0),6)+"\n\n");
                     FW.write(SL+common.Rad(common.getRound(dLossZ,3),6)+"\n\n");
                     FW.write(SU+common.Rad(common.getRound(dUptoZ,3),6)+"\n\n");
                     FW.write(SS+"\n\n");
                     FW.write(SD+"\n\n");
                     FW.write(SLine+"\n");
                     FW.write("<End Of Report>");
                }catch(Exception ex){}


           }
     }

     public Vector Stop1Head(int iStartValue,int iEndValue)
     {
          Vector VHead = new Vector();

          String Str1  = common.Pad("Reason",6)+common.Space(2);
          String Str2  = common.Pad(" ",6)+common.Space(2);
          String Str3  = common.Pad("Shift",6)+common.Space(2);
          String Str4  = common.Pad("Mac No",6)+common.Space(2);
          SLine = common.Replicate("-",6+2);

          for(int i=iStartValue;i<iEndValue;i++)
          {
               Str1  = Str1+common.Cad((String)VName.elementAt(i),16)+common.Space(2);
               Str2  = Str2+common.Replicate("-",16)+common.Space(2);
               Str3  = Str3+common.Pad("I  "+" "+"II "+" "+"III"+" "+" Tot",16)+common.Space(2);
               Str4  = Str4+common.Pad("Min"+" "+"Min"+" "+"Min"+" "+" Min",16)+common.Space(2);
               SLine = SLine+common.Replicate("-",16+2);
          }
          
          if(iEndValue==VName.size())
          {
               Str1  = Str1+common.Rad("Net",6)+"\n";
               Str2  = Str2+common.Space(6)+"\n";
               Str3  = Str3+common.Rad(" ",6)+"\n";
               Str4  = Str4+common.Rad("Min",6)+"\n";
               SLine = SLine+common.Replicate("-",6)+"\n";
          }
          else
          {
               Str1  = Str1+"\n";
               Str2  = Str2+"\n";
               Str3  = Str3+"\n";
               Str4  = Str4+"\n";
               SLine = SLine+"\n";

          }

          VHead.addElement(SLine);
          VHead.addElement(Str1);
          VHead.addElement(Str2);
          VHead.addElement(Str3);
          VHead.addElement(Str4);
          VHead.addElement(SLine);

          return VHead;
     }

	private void setCriteria(HttpServletRequest request)
	{
		SUnit            = request.getParameter("Unit");
		SStDate          = request.getParameter("Date");
		SEnDate          = request.getParameter("DateTo");		
                iDateDiff        = common.toInt(common.getDateDiff(SEnDate,SStDate))+1;
                SStDate          = common.pureDate(SStDate);
		SEnDate          = common.pureDate(SEnDate);
          SDept            = "43";          
	}

	private Connection createConnection()
	{
		Connection conn;
		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
			conn  = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
		}catch(Exception ee)
		{
			return null;
		}
		return conn;
	}
        
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>




public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        // c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str ));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        //  c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddColumnIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iColWidth, int iLeftBorder, int iTopBorder, int iRightBorder, Font DocFont, int iBottomBorder) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColWidth);
        // c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    } 
		  




}
