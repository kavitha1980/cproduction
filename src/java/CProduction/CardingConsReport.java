package CProduction;

import java.io.*;
import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import java.io.FileOutputStream;

public class CardingConsReport extends HttpServlet
{
     HttpSession    session;
     Common common;

     Vector VMis1,VMis2,VMis3,VMis4,VMis5,VMis6,VMis7,VMis8,VMis9,VMis10;
     Vector VMis11,VMis12,VMis13,VMis14,VMis15,VMis16,VMis17;
     Vector VMis18,VMis19,VMis20,VMis21,VMis22;

     String SStDate,SEnDate,SUnit;
     String bgColor,bgHead,bgUom,bgBody,tbgBody,fgHead,fgUom,fgBody;
     
     String SFile                     = "";
    String[]                         ColumnName,ColumnType,ColumnName1,ColumnType1,ColumnName2,ColumnType2,sBody;
    int[]                            iColumnWidth,iColumnWidth1,iColumnWidth2;

     private static Font catFont     = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
     private static Font redFont     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
     private static Font subFont     = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);

     private static Font bigBold     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
     private static Font bigNormal   = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);

     private static Font mediumBold  = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
     private static Font mediumNormal= new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font smallBold   = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.BOLD);
     private static Font smallNormal = new Font(Font.FontFamily.TIMES_ROMAN, 6, Font.NORMAL);

     private static Font tinyBold    = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
     private static Font tinyNormal  = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.NORMAL);

     private static Font underBold   = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.UNDERLINE);
    
     int    iWidth1[]                = {28,28,28,28,30,30,30,20,20,20,28,28,28,28,28,28,25,25,25,25,25,25};
    
     FileWriter  FW;
     int Lctr = 70,Pctr = 0;
     String SLine;
     Document   document;
     PdfPTable  table1;
     int        iUserCode;
     PdfWriter  writer      = null;

     
     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
          common = new Common();

          bgColor = common.bgColor;
          bgHead  = common.bgHead;
          bgUom   = common.bgUom;
          bgBody  = common.bgBody;
          fgHead  = common.fgHead;
          fgUom   = common.fgUom;
          fgBody  = common.fgBody;
		tbgBody = common.tbgBody;
          SLine   = common.Replicate("-",199)+"\n";
     }
     public void doGet(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
            doPost(request,response);
     }
     public void doPost(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
          response.setContentType("text/html");
          PrintWriter out  = response.getWriter();
          	session = request.getSession(true);
	/*	if(session.getValue("AppletTag")==null)
		{
			Control control = new Control();
			String SServer = control.getID("Select ProdHost from Server");
			response.sendRedirect(SServer + "/SessionExpired");
			return;

		}		
       */ 
           	setCriteria(request);
        //		System.out.println(SUnit + ":" + SStDate + ":" + SEnDate );
          out.println("<html>");
          out.println("<head>");
          out.println("<title>Carding Production Report </title>");
          out.println("</head>");
          out.println("<body bgcolor='" + common.bgBody + "'>");
		//out.println(session.getValue("AppletTag"));
          out.println("<a href='Header'>Home</a>");
          out.println(common.getConsReportHeader("Carding Report Consolidation (F CDG 2)",SUnit,SStDate,SEnDate));
          out.println("  <table border='2' cellspacing='1' width='1500' height='65' bgcolor='"+bgBody+"'>");
          out.println("    <tr>");
          out.println("      <td width='69' height='113' rowspan='3' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Mach No</font></td>");
          out.println("      <td width='207' height='32' colspan='3' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Hank");
          out.println("        Produced</font></td>");
          out.println("      <td width='204' height='32' colspan='3' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Production</font></td>");
          out.println("      <td width='204' height='32' colspan='3' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Actual");
          out.println("        Efficiency</font></td>");
          out.println("      <td width='204' height='32' colspan='3' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Target");
          out.println("        Vs Actual Variation</font></td>");
          out.println("      <td width='204' height='32' colspan='3' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Expected");
          out.println("        Vs Actual Variation</font></td>");
          out.println("      <td width='204' height='29' colspan='3' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Worked");
          out.println("        Hours</font></td>");
          out.println("      <td width='204' height='29' colspan='3' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Utilization</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='69' height='66' valign='top' align='center' bgcolor='"+bgHead+"' rowspan='2'><font size='4' color='"+fgHead+"'>I</font></td>");
          out.println("      <td width='69' height='66' valign='top' align='center' bgcolor='"+bgHead+"' rowspan='2'><font size='4' color='"+fgHead+"'>II</font></td>");
          out.println("      <td width='69' height='66' valign='top' align='center' bgcolor='"+bgHead+"' rowspan='2'><font size='4' color='"+fgHead+"'>III</font></td>");
          out.println("      <td width='68' height='41' valign='top' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>I</font></td>");
          out.println("      <td width='68' height='41' valign='top' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>II</font></td>");
          out.println("      <td width='68' height='41' valign='top' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>III</font></td>");
          out.println("      <td width='68' height='41' valign='top' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>I</font></td>");
          out.println("      <td width='68' height='41' valign='top' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>II</font></td>");
          out.println("      <td width='68' height='41' valign='top' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>III</font></td>");
          out.println("      <td width='68' height='41' valign='top' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>I</font></td>");
          out.println("      <td width='68' height='41' valign='top' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>II</font></td>");
          out.println("      <td width='68' height='41' valign='top' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>III</font></td>");
          out.println("      <td width='68' height='38' valign='top' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>I</font></td>");
          out.println("      <td width='68' height='38' valign='top' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>II</font></td>");
          out.println("      <td width='68' height='38' valign='top' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>III</font></td>");
          out.println("      <td width='68' height='38' valign='top' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>I</font></td>");
          out.println("      <td width='68' height='38' valign='top' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>II</font></td>");
          out.println("      <td width='68' height='38' valign='top' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>III</font></td>");
          out.println("      <td width='68' height='38' valign='top' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>I</font></td>");
          out.println("      <td width='68' height='38' valign='top' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>II</font></td>");
          out.println("      <td width='68' height='38' valign='top' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>III</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='68' height='25' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='68' height='25' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='68' height='25' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='68' height='25' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='68' height='25' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='68' height='25' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='68' height='25' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='68' height='25' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='68' height='25' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='68' height='29' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='68' height='29' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='68' height='29' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='68' height='29' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Hrs</font></td>");
          out.println("      <td width='68' height='29' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Hrs</font></td>");
          out.println("      <td width='68' height='29' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Hrs</font></td>");
          out.println("      <td width='68' height='29' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='68' height='29' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='68' height='29' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("    </tr>");

          setVectors();  
          try
          {
               //FW      = new FileWriter("/production/Reports/cardconsc.prn");
//               FW      = new FileWriter("/software/C-Prod-Print/Reports/cardconsc.prn");
               FW      = new FileWriter("D:/C-Prod-Print/cardconsc.prn");
               Lctr = 70;
               Pctr = 1;
               Head();
               Body();
               FW.close();
               createPDFFile();
          }catch(Exception ex){}

          for(int i=0;i<VMis1.size();i++)
          {
                if(validData(i)<=0)
                  continue;
                out.println("    <tr>");
                out.println("      <td width='69' height='5'' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgBody+"'>"+(String)VMis1 .elementAt(i)+"</font></td>");
                out.println("      <td width='69' height='5'' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgBody+"'>"+(String)VMis2 .elementAt(i)+"</font></td>");
                out.println("      <td width='69' height='5'' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgBody+"'>"+(String)VMis3 .elementAt(i)+"</font></td>");
                out.println("      <td width='69' height='5'' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgBody+"'>"+(String)VMis4 .elementAt(i)+"</font></td>");
                out.println("      <td width='68' height='5'' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgBody+"'>"+(String)VMis5 .elementAt(i)+"</font></td>");
                out.println("      <td width='68' height='5'' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgBody+"'>"+(String)VMis6 .elementAt(i)+"</font></td>");
                out.println("      <td width='68' height='5'' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgBody+"'>"+(String)VMis7 .elementAt(i)+"</font></td>");
                out.println("      <td width='68' height='5'' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgBody+"'>"+(String)VMis8 .elementAt(i)+"</font></td>");
                out.println("      <td width='68' height='5'' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgBody+"'>"+(String)VMis9 .elementAt(i)+"</font></td>");
                out.println("      <td width='68' height='5'' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgBody+"'>"+(String)VMis10.elementAt(i)+"</font></td>");
                out.println("      <td width='68' height='5'' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgBody+"'>"+(String)VMis11.elementAt(i)+"</font></td>");
                out.println("      <td width='68' height='5'' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgBody+"'>"+(String)VMis12.elementAt(i)+"</font></td>");
                out.println("      <td width='68' height='5'' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgBody+"'>"+(String)VMis13.elementAt(i)+"</font></td>");
                out.println("      <td width='68' height='5'' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgBody+"'>"+(String)VMis14.elementAt(i)+"</font></td>");
                out.println("      <td width='68' height='5'' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgBody+"'>"+(String)VMis15.elementAt(i)+"</font></td>");
                out.println("      <td width='68' height='5'' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgBody+"'>"+(String)VMis16.elementAt(i)+"</font></td>");
                out.println("      <td width='68' height='5'' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgBody+"'>"+(String)VMis17.elementAt(i)+"</font></td>");
                out.println("      <td width='68' height='5'' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgBody+"'>"+(String)VMis18.elementAt(i)+"</font></td>");
                out.println("      <td width='68' height='5'' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgBody+"'>"+(String)VMis19.elementAt(i)+"</font></td>");
                out.println("      <td width='68' height='5'' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgBody+"'>"+(String)VMis20.elementAt(i)+"</font></td>");
                out.println("      <td width='68' height='5'' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgBody+"'>"+(String)VMis21.elementAt(i)+"</font></td>");
                out.println("      <td width='68' height='5'' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgBody+"'>"+(String)VMis22.elementAt(i)+"</font></td>");
                out.println("    </tr>");
          }

           out.println("    <tr>");    
           out.println("      <td width='69' height='38' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>TOTAL</font></td>");
           out.println("      <td width='69' height='38' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>"+common.getSum(VMis2 ,"S")+"</font></td>");
           out.println("      <td width='69' height='38' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>"+common.getSum(VMis3 ,"S")+"</font></td>");
           out.println("      <td width='69' height='38' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>"+common.getSum(VMis4 ,"S")+"</font></td>");
           out.println("      <td width='68' height='38' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>"+common.getSum(VMis5 ,"S")+"</font></td>");
           out.println("      <td width='68' height='38' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>"+common.getSum(VMis6 ,"S")+"</font></td>");
           out.println("      <td width='68' height='38' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>"+common.getSum(VMis7 ,"A")+"</font></td>");
           out.println("      <td width='68' height='38' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>"+common.getSum(VMis8 ,"A")+"</font></td>");
           out.println("      <td width='68' height='38' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>"+common.getSum(VMis9 ,"A")+"</font></td>");
           out.println("      <td width='68' height='38' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>"+common.getSum(VMis10,"S")+"</font></td>");
           out.println("      <td width='68' height='38' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>"+common.getSum(VMis11,"S")+"</font></td>");
           out.println("      <td width='68' height='38' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>"+common.getSum(VMis12,"S")+"</font></td>");
           out.println("      <td width='68' height='38' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>"+common.getSum(VMis13,"S")+"</font></td>");
           out.println("      <td width='68' height='38' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>"+common.getSum(VMis14,"S")+"</font></td>");
           out.println("      <td width='68' height='38' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>"+common.getSum(VMis15,"S")+"</font></td>");
           out.println("      <td width='68' height='38' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>"+common.getSum(VMis16,"S")+"</font></td>");
           out.println("      <td width='68' height='38' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>"+common.getSum(VMis17,"S")+"</font></td>");
           out.println("      <td width='68' height='38' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>"+common.getSum(VMis18,"S")+"</font></td>");
           out.println("      <td width='68' height='38' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>"+common.getSum(VMis19,"S")+"</font></td>");
           out.println("      <td width='68' height='38' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>"+common.getSum(VMis20,"A")+"</font></td>");
           out.println("      <td width='68' height='38' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>"+common.getSum(VMis21,"A")+"</font></td>");
           out.println("      <td width='68' height='38' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>"+common.getSum(VMis22,"A")+"</font></td>");
           out.println("    </tr>");

          out.println("  </table>");
          out.println("</div>");
          out.println("");
          out.println("</body>");
          out.println("");
          out.println("</html>");
          out.println("");

          out.close();
     }
     public void setVectors()
     {
           VMis1 = new Vector();
           VMis2 = new Vector();
           VMis3 = new Vector();
           VMis4 = new Vector();
           VMis5 = new Vector();
           VMis6 = new Vector();
           VMis7 = new Vector();
           VMis8 = new Vector();
           VMis9 = new Vector();
           VMis10 = new Vector();
           VMis11 = new Vector();
           VMis12 = new Vector();
           VMis13 = new Vector();
           VMis14 = new Vector();
           VMis15 = new Vector();
           VMis16 = new Vector();
           VMis17 = new Vector();
           VMis18 = new Vector();
           VMis19 = new Vector();
           VMis20 = new Vector();
           VMis21 = new Vector();
           VMis22 = new Vector();

             try
             {
                     Connection con = createConnection();
                     Statement stat = con.createStatement();
                     try{stat.execute("Drop Table BC1");}catch(Exception ex){}
                     try{stat.execute("Drop Table BC2");}catch(Exception ex){}
                     try{stat.execute("Drop Table BC3");}catch(Exception ex){}
                     try{stat.execute("Drop Table BMac");}catch(Exception ex){}

                     try{stat.execute(getQS1());}catch(Exception ex){System.out.println(" BC1 : "+ex);}
                     try{stat.execute(getQS2());}catch(Exception ex){System.out.println(" BC2 : "+ex);}
                     try{stat.execute(getQS3());}catch(Exception ex){System.out.println(" BC3 : "+ex);}
                     try{stat.execute(getQS4());}catch(Exception ex){System.out.println(" MCMac : "+ex);}
                     ResultSet res1  = stat.executeQuery(getQString());
                     while(res1.next())
                     {
                          VMis1  .addElement(res1.getString(1));
                          VMis2  .addElement(res1.getString(2));
                          VMis3  .addElement(res1.getString(3));
                          VMis4  .addElement(res1.getString(4)); 
                          VMis5  .addElement(res1.getString(5));
                          VMis6  .addElement(res1.getString(6));
                          VMis7  .addElement(res1.getString(7));
                          VMis8  .addElement(res1.getString(8));
                          VMis9  .addElement(res1.getString(9));
                          VMis10 .addElement(res1.getString(10));
                          VMis11 .addElement(res1.getString(11));
                          VMis12 .addElement(res1.getString(12));
                          VMis13 .addElement(res1.getString(13));
                          VMis14 .addElement(res1.getString(14)); 
                          VMis15 .addElement(res1.getString(15));
                          VMis16 .addElement(res1.getString(16));
                          VMis17 .addElement(res1.getString(17));
                          VMis18 .addElement(res1.getString(18));
                          VMis19 .addElement(res1.getString(19));
                          VMis20 .addElement(res1.getString(20));
                          VMis21 .addElement(res1.getString(21));
                          VMis22 .addElement(res1.getString(22));
                     }
                     res1.close();
                     stat.close();
                     con.close();
              }
             catch(Exception ex)
             {
                     System.out.println("From CardCons.java : 210 "+ex);
             }
           for(int i=0;i<VMis1.size();i++)
           {
               VMis2.setElementAt(common.getRound((String)VMis2.elementAt(i),3),i);
               VMis3.setElementAt(common.getRound((String)VMis3.elementAt(i),3),i);
               VMis4.setElementAt(common.getRound((String)VMis4.elementAt(i),3),i);
               VMis5.setElementAt(common.getRound((String)VMis5.elementAt(i),3),i);
               VMis6.setElementAt(common.getRound((String)VMis6.elementAt(i),3),i);
               VMis7.setElementAt(common.getRound((String)VMis7.elementAt(i),3),i);
               VMis8.setElementAt(common.getRound((String)VMis8.elementAt(i),3),i);
               VMis9.setElementAt(common.getRound((String)VMis9.elementAt(i),3),i);
               VMis10.setElementAt(common.getRound((String)VMis10.elementAt(i),3),i);
               VMis11.setElementAt(common.getRound((String)VMis11.elementAt(i),3),i);
               VMis12.setElementAt(common.getRound((String)VMis12.elementAt(i),3),i);
               VMis13.setElementAt(common.getRound((String)VMis13.elementAt(i),3),i);
               VMis14.setElementAt(common.getRound((String)VMis14.elementAt(i),3),i);
               VMis15.setElementAt(common.getRound((String)VMis15.elementAt(i),3),i);
               VMis16.setElementAt(common.getRound((String)VMis16.elementAt(i),3),i);
               VMis17.setElementAt(common.getRound((String)VMis17.elementAt(i),3),i);
               VMis18.setElementAt(common.getRound((String)VMis18.elementAt(i),3),i);
               VMis19.setElementAt(common.getRound((String)VMis19.elementAt(i),3),i);
               VMis20.setElementAt(common.getRound((String)VMis20.elementAt(i),3),i);
               VMis21.setElementAt(common.getRound((String)VMis21.elementAt(i),3),i);
               VMis22.setElementAt(common.getRound((String)VMis22.elementAt(i),3),i);
           }
     }

     public String getQS1()
     {
      
          String QString = " create table BC1 as " + 
                           " (SELECT CardTran.mach_code, Sum(CardTran.hank) AS Hank1, Sum(CardTran.prod) AS Prod1, Avg(CardTran.effyactual) AS Effy1, Sum(CardTran.prod_tar-CardTran.prod) AS Tar1, Sum(CardTran.prod_exp-CardTran.Prod) AS Exp1, Sum(CardTran.hourrun) AS Hour1, Avg(CardTran.Hourrun*100/CardTran.houralloted) AS Util1 "+
                           " FROM CardTran "+
                           " WHERE (CardTran.shift_code=1 AND CardTran.sp_date>='"+SStDate+"' AND CardTran.sp_date<='"+SEnDate+"' AND CardTran.hourAlloted>0 "+
                           " AND CardTran.Unit_Code = "+SUnit+")"+
                           " GROUP BY CardTran.mach_code )";
          return QString;
     }

     public String getQS2()
     {

          String QString = " create table BC2 as " + 
                           " (SELECT CardTran.mach_code, Sum(CardTran.hank) AS Hank2, Sum(CardTran.prod) AS Prod2, Avg(CardTran.effyactual) AS Effy2, Sum(CardTran.prod_tar-CardTran.prod) AS Tar2, Sum(CardTran.prod_exp-CardTran.Prod) AS Exp2, Sum(CardTran.hourrun) AS Hour2, Avg(CardTran.Hourrun*100/CardTran.houralloted) AS Util2 "+
                           " FROM CardTran "+
                           " WHERE (CardTran.shift_code=2 AND CardTran.sp_date>='"+SStDate+"' AND CardTran.sp_date<='"+SEnDate+"' AND CardTran.hourAlloted>0 "+
                           " AND CardTran.Unit_Code = "+SUnit+")"+
                           " GROUP BY CardTran.mach_code)";
          return QString;
     }

     public String getQS3()
     {

          String QString = " create table BC3 as " + 
                           " (SELECT CardTran.mach_code, Sum(CardTran.hank) AS Hank3, Sum(CardTran.prod) AS Prod3, Avg(CardTran.effyactual) AS Effy3, Sum(CardTran.prod_tar-CardTran.prod) AS Tar3, Sum(CardTran.prod_exp-CardTran.Prod) AS Exp3, Sum(CardTran.hourrun) AS Hour3, Avg(CardTran.Hourrun*100/CardTran.houralloted) AS Util3 "+
                           " FROM CardTran "+
                           " WHERE (CardTran.shift_code=3 AND CardTran.sp_date>='"+SStDate+"' AND CardTran.sp_date<='"+SEnDate+"' AND CardTran.hourAlloted>0 "+
                           " AND CardTran.Unit_Code = "+SUnit+")"+
                           " GROUP BY CardTran.mach_code)";
          return QString;
     }

     public String getQS4()
     {

          String QString = " create table BMac as " + 
                           " (SELECT machine.mach_code, machine.mach_name "+
                           " FROM machine "+
                           " WHERE machine.dept_code=3 AND machine.unit_code="+SUnit+")";
          return QString;
     }

     public String getQString()
     {
                              //          1             2          3          4           5           6         7         8         9           10          11      12        13      14          15         16         17         18         19        20         21          22               
          String QString = "SELECT BMac.mach_name, BC1.Hank1, BC2.Hank2, BC3.Hank3, BC1.Prod1, BC2.Prod2, BC3.Prod3, BC1.Effy1, BC2.Effy2, BC3.Effy3, BC1.Tar1, BC2.Tar2, BC3.Tar3, BC1.Exp1, BC2.Exp2, BC3.Exp3, BC1.Hour1, BC2.Hour2, BC3.Hour3, BC1.Util1, BC2.Util2, BC3.Util3 "+
                           "FROM ((BMac LEFT JOIN BC1 ON BMac.mach_code = BC1.mach_code) LEFT JOIN BC2 ON BMac.mach_code = BC2.mach_code) LEFT JOIN BC3 ON BMac.mach_code = BC3.mach_code "+
                           " ORDER BY 1 ";
//		System.out.println(QString);
          return QString;
    }

	private Connection createConnection()
	{
		Connection conn;
		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
			conn  = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
		}catch(Exception ee)
		{
			return null;
		}
		return conn;
	}


     public int validData(int x)
     {
           if(common.toInt((String)VMis1.elementAt(x))==0)
              return 0;

           double dTot = 0.0;
           dTot = dTot+common.toDouble((String)VMis3.elementAt(x));
           dTot = dTot+common.toDouble((String)VMis4.elementAt(x));
           dTot = dTot+common.toDouble((String)VMis5.elementAt(x));
           dTot = dTot+common.toDouble((String)VMis6.elementAt(x));
           dTot = dTot+common.toDouble((String)VMis7.elementAt(x)); 
           dTot = dTot+common.toDouble((String)VMis8.elementAt(x));
           dTot = dTot+common.toDouble((String)VMis9.elementAt(x));
           dTot = dTot+common.toDouble((String)VMis10.elementAt(x));
           dTot = dTot+common.toDouble((String)VMis11.elementAt(x));
           dTot = dTot+common.toDouble((String)VMis12.elementAt(x));
           dTot = dTot+common.toDouble((String)VMis13.elementAt(x)); 
           dTot = dTot+common.toDouble((String)VMis14.elementAt(x));
           dTot = dTot+common.toDouble((String)VMis15.elementAt(x));
           dTot = dTot+common.toDouble((String)VMis16.elementAt(x));
           dTot = dTot+common.toDouble((String)VMis17.elementAt(x));
           dTot = dTot+common.toDouble((String)VMis18.elementAt(x));
           dTot = dTot+common.toDouble((String)VMis19.elementAt(x)); 
           dTot = dTot+common.toDouble((String)VMis20.elementAt(x));
           dTot = dTot+common.toDouble((String)VMis21.elementAt(x));
           dTot = dTot+common.toDouble((String)VMis22.elementAt(x));

           if(dTot>0.0)
             return 1;
           else
             return 0;     
      }
     public void Head()
     {
          if(Lctr < 60)
             return;
          if(Pctr > 0)
          {
             try
             {
               FW.write(SLine+"\n");
             }catch(Exception ex){}
          }
          Pctr++;

          String Str1    = common.Pad("Company    : AMARJOTHI SPINNING MILLS LIMITED",80);
          String Str2    = common.Pad("Document   : Carding Consolidation Statement From "+common.parseDate(SStDate)+" To "+common.parseDate(SEnDate),80);
          String Str3    = common.Pad("Unit       : "+(SUnit=="1"?(SUnit=="2"?"S":"B"):"A"),70);
          Vector VHead   = new Vector();
          try
          {
               FW.write(""+Str1+"\n");
               FW.write(Str2+"\n");
               FW.write(Str3+"\n\n");

               VHead = common.ConsHead();
               for(int i=0;i<VHead.size();i++)
               {
                    FW.write((String)VHead.elementAt(i));
               }
          }catch(Exception ex){}
          Lctr = 4+VHead.size();
     }
     public void Body()
     {
         String Strl = "";
         for(int i=0;i<VMis1.size();i++)
         {
                Strl = "";
                Strl = Strl+common.Rad((String)VMis1     .elementAt(i),4)+common.Space(2);
                Strl = Strl+common.Rad((String)VMis2   .elementAt(i),6)+common.Space(2);
                Strl = Strl+common.Rad((String)VMis3   .elementAt(i),6)+common.Space(2);
                Strl = Strl+common.Rad((String)VMis4   .elementAt(i),6)+common.Space(2);
                Strl = Strl+common.Rad((String)VMis5   .elementAt(i),9)+common.Space(2);
                Strl = Strl+common.Rad((String)VMis6   .elementAt(i),9)+common.Space(2);
                Strl = Strl+common.Rad((String)VMis7   .elementAt(i),9)+common.Space(2);
                Strl = Strl+common.Rad((String)VMis8    .elementAt(i),6)+common.Space(2);
                Strl = Strl+common.Rad((String)VMis9    .elementAt(i),6)+common.Space(2);
                Strl = Strl+common.Rad((String)VMis10   .elementAt(i),6)+common.Space(2);
                Strl = Strl+common.Rad((String)VMis11    .elementAt(i),9)+common.Space(2);
                Strl = Strl+common.Rad((String)VMis12    .elementAt(i),9)+common.Space(2);
                Strl = Strl+common.Rad((String)VMis13    .elementAt(i),9)+common.Space(2);
                Strl = Strl+common.Rad((String)VMis14    .elementAt(i),9)+common.Space(2);
                Strl = Strl+common.Rad((String)VMis15    .elementAt(i),9)+common.Space(2);
                Strl = Strl+common.Rad((String)VMis16    .elementAt(i),9)+common.Space(2);
                Strl = Strl+common.Rad((String)VMis17   .elementAt(i),6)+common.Space(2);
                Strl = Strl+common.Rad((String)VMis18   .elementAt(i),6)+common.Space(2);
                Strl = Strl+common.Rad((String)VMis19   .elementAt(i),6)+common.Space(2);
                Strl = Strl+common.Rad((String)VMis20   .elementAt(i),6)+common.Space(2);
                Strl = Strl+common.Rad((String)VMis21   .elementAt(i),6)+common.Space(2);
                Strl = Strl+common.Rad((String)VMis22   .elementAt(i),6);

                try
                {
                     FW.write(Strl+"\n");
                     Lctr++;
                     Head();
                }catch(Exception ex){}
         }
           try
           {
               FW.write(SLine);               
           }catch(Exception ex){}

           Strl = "";
           Strl = Strl+common.Rad("Tota",4)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VMis2,"S"),6)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VMis3,"S"),6)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VMis4,"S"),6)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VMis5,"S"),9)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VMis6,"S"),9)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VMis7,"S"),9)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VMis8,"A"),6)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VMis9,"A"),6)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VMis10,"A"),6)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VMis11,"S"),9)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VMis12,"S"),9)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VMis13,"S"),9)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VMis14,"S"),9)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VMis15,"S"),9)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VMis16,"S"),9)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VMis17,"S"),6)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VMis18,"S"),6)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VMis19,"S"),6)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VMis20,"A"),6)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VMis21,"A"),6)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VMis22,"A"),6);

           try
           {
               FW.write(Strl+"\n");
               FW.write(SLine+"\n\n");
               FW.write("<End of Report>");
           }catch(Exception ex){}
     }



	private void setCriteria(HttpServletRequest request)
	{
		SEnDate          = request.getParameter("EndDate");
                SEnDate          = common.pureDate(SEnDate);
		SStDate          = request.getParameter("StartDate");
                SStDate          = common.pureDate(SStDate);
                SUnit            = request.getParameter("Unit");
	}
        
 //PDF
        
    public void createPDFFile(){
     try{
         
                
        SFile       =   "CardingConsolidatedReportPDF.pdf";
        SFile       =   common.getPrintPath()+SFile;    
        document    =   new Document(PageSize.A4.rotate());
        writer      =  PdfWriter.getInstance(document, new FileOutputStream(SFile));
        TableHeader event   = new TableHeader();
        writer      .   setPageEvent(event);
        document    .   open();

        table1      =   new PdfPTable(22);
        table1      .   setWidths(iWidth1);
        table1      .   setSpacingAfter(2);
        table1      .   setWidthPercentage(100);
        table1      .   setHeaderRows(7);
        
            setPDFHead();
            setPDFBody();
            document.add(table1);
            document.close();
      
       }
        catch (Exception e){
           e.printStackTrace();
            System.out.println("Error-->"+e);
        }
        finally{
            try{
                document.close();
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }
    }
    
    public void setPDFHead(){
        try
        {
          Vector VHead   = new Vector();
          try
          {
            String sHead1       = "Company    : AMARJOTHI SPINNING MILLS LIMITED";
            String sHead2       = "Document   : Carding Consolidation Statement From "+common.parseDate(SStDate)+" To "+common.parseDate(SEnDate);
            String sHead3       = "Unit             : "+(SUnit.equals("1")?"A":(SUnit.equals("2")?"B":(SUnit.equals("3")?"C":"S")));
            
            AddCellIntoTable(sHead1, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,22,20f,0,0,0,0, bigBold );
            AddCellIntoTable(sHead2, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,22,20f,0,0,0,0, bigBold );
            AddCellIntoTable(sHead3, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,22,20f,0,0,0,0, bigBold );
//          
//               VHead = common.ConsHead();
//               for(int i=0;i<VHead.size();i++){
//                   AddCellIntoTable((String)VHead.elementAt(i), table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,22,10f,0,0,0,0, tinyBold );
//               }
          }catch(Exception ex){}
          
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,22,10f,0,0,0,0, tinyBold );
            
            AddHeadCellIntoTable("Mach No", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,3,25f,8,3,5,4, tinyBold );
            AddCellIntoTable("Hank Produced", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,3,25f,8,3,5,4, tinyBold );
            AddCellIntoTable("Production", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,3,25f,8,3,5,4, tinyBold );
            AddCellIntoTable("Actual Efficiency", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,3,25f,8,3,5,4, tinyBold );
            AddCellIntoTable("Target Vs Actual Variation", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,3,25f,8,3,5,4, tinyBold );
            AddCellIntoTable("Expected Vs Actual Variation", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,3,25f,8,3,5,4, tinyBold );
            AddCellIntoTable("Worked Hours", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,3,25f,8,3,5,4, tinyBold );
            AddCellIntoTable("Utilization", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,3,25f,8,3,5,4, tinyBold );
            
            for(int i=0;i<=6;i++){
            AddCellIntoTable("I", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,25f,8,3,5,4, tinyBold );
            AddCellIntoTable("II", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,25f,8,3,5,4, tinyBold );
            AddCellIntoTable("II", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,25f,8,3,5,4, tinyBold );
            }
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,25f,8,3,5,4, tinyBold );
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,25f,8,3,5,4, tinyBold );
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,25f,8,3,5,4, tinyBold );
            for(int i=0;i<=2;i++){
            AddCellIntoTable("Kgs", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,25f,8,3,5,4, tinyBold );
            }
            for(int i=0;i<=2;i++){
            AddCellIntoTable("%", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,25f,8,3,5,4, tinyBold );
            }
            for(int i=0;i<=5;i++){
            AddCellIntoTable("Kgs", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,25f,8,3,5,4, tinyBold );
            }
            for(int i=0;i<=2;i++){
            AddCellIntoTable("Hrs", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,25f,8,3,5,4, tinyBold );
            }
            for(int i=0;i<=2;i++){
            AddCellIntoTable("%", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,25f,8,3,5,4, tinyBold );
            }
                   
         }
        catch(Exception Ex){
                Ex.printStackTrace();
                System.out.println("Error Head1-->"+Ex);
        }
    }
    
    public void setPDFBody(){
      try{
          
        for(int i=0;i<VMis1.size();i++){
          AddCellIntoTable(common.parseNull((String)VMis1.elementAt(i)), table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VMis2.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VMis3.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VMis4.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VMis5.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VMis6.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VMis7.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VMis8.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VMis9.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VMis10.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VMis11.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VMis12.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VMis13.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VMis14.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VMis15.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VMis16.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VMis17.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VMis18.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VMis19.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VMis20.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VMis21.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                 
          AddCellIntoTable(common.parseNull((String)VMis22.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          
         }
            AddCellIntoTable(" TOTAL", table1, Element.ALIGN_LEFT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getSum(VMis2,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getSum(VMis3,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getSum(VMis4,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
               
            AddCellIntoTable(common.getSum(VMis5,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VMis6,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VMis7,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VMis8,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VMis9,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VMis10,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VMis11,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VMis12,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VMis13,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VMis14,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VMis15,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VMis16,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VMis17,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VMis18,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VMis19,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VMis20,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VMis21,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VMis22,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            
            
//            AddCellIntoTable(" ", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,22,50f,0,0,0,0, tinyBold );   
//            
//            AddCellIntoTable("P.O", table1, Element.ALIGN_LEFT, Element.ALIGN_TOP,5,20f,0,0,0,0, tinyBold );                                            
//            AddCellIntoTable("S.P.O", table1, Element.ALIGN_LEFT, Element.ALIGN_TOP,5,20f,0,0,0,0, tinyBold );                                            
//            AddCellIntoTable("S.M", table1, Element.ALIGN_LEFT, Element.ALIGN_TOP,5,20f,0,0,0,0, tinyBold );                                            
//            AddCellIntoTable("D.M", table1, Element.ALIGN_LEFT, Element.ALIGN_TOP,2,20f,0,0,0,0, tinyBold );                                            
        }
      catch(Exception Ex){
                Ex.printStackTrace();
                System.out.println("Error Body1-->"+Ex);
        }
    }
        
    private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      } 
    
    private void AddHeadCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iRowSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
    
    private void AddCellIntoTable1(String Str,PdfPTable table,int iHorizontal,int iVertical,int iRowSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
      private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
      private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,int iRowSpan)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          //c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
      private void AddCellTable(Image image,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
          PdfPCell c1 = new PdfPCell();
		  c1.addElement(new Chunk(image, 5, -5));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

		  // image.scalePercent(90f);
      	  // paragraph.add(new Chunk(image, -1f, 1f));
      }
       private void AddCellIntoTable(Image image,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
          PdfPCell c1 = new PdfPCell();
		  //c1.addElement(new Chunk(image, 5, -5));
	  c1.addElement(image);
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

      }
     public void AddCellIntoTable2(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }
    
     class TableHeader extends PdfPageEventHelper {
        String header;
        PdfTemplate total;
        public void setHeader(String header) {
            this.header = header;
      }
      public void onOpenDocument(PdfWriter writer, Document document) {
            try{
            total = writer.getDirectContent().createTemplate(30, 16);
            }
            catch(Exception ex){
            }
      }

        public void onEndPage(PdfWriter writer, Document document) {
            PdfPTable table = new PdfPTable(3);
            try {
//                AddCellIntoTable("", table1, Element.ALIGN_LEFT, Element.ALIGN_TOP,13,18f,1,0,0,0, tinyNormal );
                table.setWidths(new int[]{24, 24, 2});
                table.setTotalWidth(520);
                table.setLockedWidth(true);
                table.getDefaultCell().setFixedHeight(20);
                table.getDefaultCell().setBorder(0);
                table.addCell(header);
                table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell("");
                table.addCell("");
                table.addCell("");
                
                table.addCell(String.format("Page %d of", writer.getPageNumber()));
                PdfPCell cell = new PdfPCell(Image.getInstance(total));
                cell.setBorder(0);
                table.addCell(cell);
                table.writeSelectedRows(0, -1, 10, 50, writer.getDirectContent());
            }
            catch(DocumentException de) {
                throw new ExceptionConverter(de);
            }
        }
        public void onCloseDocument(PdfWriter writer, Document document) {
          try{  
            ColumnText.showTextAligned(total, Element.ALIGN_LEFT,
                    new Phrase(String.valueOf(writer.getPageNumber() - 1)),
                    2, 2, 0);
             }
          catch(Exception ex){
              
          }
        }
    }  
         

}
