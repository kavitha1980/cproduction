/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CProduction;


import java.sql.*;
import java.util.*;

/**
 *
 * @author root
 */
public class FrameClass {

     String SFrame,SFrameCode;

     Vector VCountClass;
     Common common;
     FrameClass(String SFrameCode,String SFrame)
     {
          this.SFrameCode = SFrameCode;
          this.SFrame     = SFrame;

          common = new Common();
          VCountClass = new Vector();
     }
     public void append(String SCountCode,String SCountName,String SDate,String SSpeed,String SSpeedTar,String STPI,String STPITar,String SPneu,String SPneuTar,String SProd,String SProdTar,String SProdExp,String SHour,String SHourRun,String SCount,String SSpdlWkd,String SCountEffy,String SNoOfSpdl)
     {
          int iindex = indexOf(SCountCode);
          if(iindex==-1)
          {
               CountClass cc = new CountClass(SCountCode,SCountName);
               cc.append(SDate,SSpeed,SSpeedTar,STPI,STPITar,SPneu,SPneuTar,SProd,SProdTar,SProdExp,SHour,SHourRun,SCount,SSpdlWkd,SCountEffy,SNoOfSpdl);
               VCountClass.addElement(cc);
          }
          else
          {
               CountClass cc = (CountClass)VCountClass.elementAt(iindex);
               cc.append(SDate,SSpeed,SSpeedTar,STPI,STPITar,SPneu,SPneuTar,SProd,SProdTar,SProdExp,SHour,SHourRun,SCount,SSpdlWkd,SCountEffy,SNoOfSpdl);
               VCountClass.setElementAt(cc,iindex);
          }
     }
     private int indexOf(String SCCode)
     {
          int index = -1;
          for(int i=0;i<VCountClass.size();i++)
          {
               CountClass cc = (CountClass)VCountClass.elementAt(i);
               if(cc.SCountCode.equals(SCCode))
               {
                    index = i;
                    break;
               }
          }
          return index;
     }

}
