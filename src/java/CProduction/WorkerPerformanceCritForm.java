/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CProduction;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Vector;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class WorkerPerformanceCritForm extends HttpServlet {

     HttpSession    session;
     Control        control;
     Common         common;
     Vector         VMonthName,VShift,VShiftCode;
     String         SServer;
     String         bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody;
     
     WorkerPerformanceClass theClass;
     
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        theClass    = new WorkerPerformanceClass();
        setMonthVectors();
        setShiftVector();
        
        String id = request.getParameter("id");
  //          String dept = "";
             
            if(id.trim().equals("1")){ //by default Dept is Spinning..
                SServer ="WorkerPerformanceForm";
//                dept    = "OE";  //spinning..
            }
            
        try {
            /*
             * TODO output your page here. You may use following sample code.
             */
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet WorkerPerformanceCritForm</title>");            
            out.println("</head>");
            out.println("<body>");
            
           RequestDispatcher  dispatch = request.getRequestDispatcher("/Header");
                              dispatch . include(request, response);
            
          out.println("<form accept-charset='UTF-8' name=flogin method='POST' action='"+SServer+"'>");
          out.println("<center>");
	  out.println("<p><FONT face=Verdana size=3 color=black>");
	  out.println("<STRONG>Amar Jothi Spinning Mills Ltd</STRONG></FONT></P>");
	  out.println("</center><center>");
	  out.println("<P><STRONG><FONT face=Verdana size=5 color=navy>");
	  out.println(" WORKER PERFORMANCE REPORT</FONT></STRONG>");
	  out.println("</center><br><br>");
	
          out.println("<center>");
          out.println("<font face=Verdana size='3' color='"+fgBody+"'><b>Name & TicketNo</b>&nbsp;&nbsp;&nbsp;&nbsp;</font></td>");
          out.println(" <select size='1' name='TicketNo' style='HEIGHT: 22px; WIDTH: 110px' > ");
          
          for(int i=0;i<theClass.VEmpName.size();i++)
          //out.println("<option value="+(String)theClass.VEmpCode.elementAt(i)+">"+(String)theClass.VEmpName.elementAt(i)+" & "+(String)theClass.VHRDTicketNo.elementAt(i)+"</option>");
          out.println("<option value="+(String)theClass.VHRDTicketNo.elementAt(i)+">"+(String)theClass.VEmpName.elementAt(i)+" & "+(String)theClass.VHRDTicketNo.elementAt(i)+"</option>");
          out.println(" </select>");
          out.println("</center><br>");
          out.println("<center>");
          out.println("<font face=Verdana size='3' color='"+fgBody+"'><b>Month     </b>&nbsp;&nbsp;&nbsp;</font></td>");
          out.println("<select size='1' name='Month' style='HEIGHT: 22px; WIDTH: 110px'>");

          for(int i=0;i<VMonthName.size();i++)
              out.println("<option value="+(String)VMonthName.elementAt(i)+">"+(String)VMonthName.elementAt(i)+"</option>");
          out.println(" </select>");
          out.println("</center><br>");

          out.println("<center>");
          out.println("<font face=Verdana size='3' color='"+fgBody+"'><b>Shift     </b>&nbsp;&nbsp;&nbsp;</font></td>");
          out.println("<select size='1' name='Month' style='HEIGHT: 22px; WIDTH: 110px'>");
          for(int i=0;i<VShift.size();i++)
              out.println("<option value="+(String)VShiftCode.elementAt(i)+">"+(String)VShift.elementAt(i)+"</option>");
          out.println(" </select>");
          out.println("</center><br>");

            out.println("<center>");
            out.println("<font face=Verdana size='3' color='"+fgBody+"'><b>Year    </b>&nbsp;&nbsp;&nbsp;&nbsp;</font></td>");
            out.println("<input type='text' name='Year' size='15' value=''>");
            out.println("</center><br><br>");
            
            out.println("<center>");
          out.println("<input type='submit' value='Submit' name='B1' style='font-family: Book Antiqua; font-size: 10pt;  text-transform: uppercase; font-weight: bold;' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
          out.println("</center></form></body></html>");
          out.close();
          out.println("</body>");
          out.println("</html>");
            
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

       private void setMonthVectors()     {                                
		VMonthName = new Vector();

                VMonthName.addElement("Jan");
                VMonthName.addElement("Feb");
                VMonthName.addElement("Mar");
                VMonthName.addElement("Apr");
                VMonthName.addElement("May");
                VMonthName.addElement("Jun");
                VMonthName.addElement("Jul");
                VMonthName.addElement("Aug");
                VMonthName.addElement("Sep");
                VMonthName.addElement("Oct");
                VMonthName.addElement("Nov");
                VMonthName.addElement("Dec");
       }
       private  void setShiftVector()   {
           
            VShift        = new Vector();
            VShiftCode    = new Vector();

            VShift.addElement("1st Shift");
            VShift.addElement("2nd Shift");
            VShift.addElement("3rd Shift");

            VShiftCode.addElement("1");
            VShiftCode.addElement("2");
            VShiftCode.addElement("3");

       }         



}
