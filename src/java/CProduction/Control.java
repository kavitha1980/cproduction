package CProduction;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.sql.*;

public class Control 
{
   Common common = new Common();	
   String ID="";
   public Control()
   {

   }

   public String getID(String QueryString)
   {
          try
          {
                Connection theConnection = createConnection();
                Statement theStatement   = theConnection.createStatement();
                ResultSet theResult      = theStatement.executeQuery(QueryString);

                if(theResult.next())
                {
                   ID = theResult.getString(1);
                }
                theConnection.close();
          }
          catch(SQLException SQLE)
          {
            System.out.println("getID2    "+SQLE);
          }
		catch(Exception CNFE)
          {
            System.out.println("getID1    "+CNFE);
          }

			 return ID;
	}	
	public void setID(String AutoNo,String Column)
	{
		try
          {
                Connection theConnection = createConnection();
                Statement theStatement   = theConnection.createStatement();
                String ControlString      = "Update Config set "+Column+"="+AutoNo; 
                theStatement.executeUpdate(ControlString);
                theConnection.close();
          }
          catch(SQLException SQLE)
          {
               System.out.println("Error in Control.java on setID()  :"+SQLE);
              JOptionPane.showMessageDialog(null,"Error in Control.java","Information",JOptionPane.INFORMATION_MESSAGE);
          }
		catch(Exception CNFE) { System.out.println("Error in Control.java on setID() :"+CNFE); }

	}

	private Connection createConnection()
	{
		Connection conn;
		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
			conn  = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
		}catch(Exception ee)
		{
			return null;
		}
		return conn;
	}

}
