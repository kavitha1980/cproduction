package CProduction;

import java.io.*;
import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class StoppagesOverallReport extends HttpServlet
{
     HttpSession    session;
     Common common;

     Vector VCode,VName,VBCode,VBMnt,VCCode,VCMnt,VDCode,VDMnt,VSCode,VSMnt,VSpCode,VSpMnt;

     String SUnit,SStDate,SEnDate,SArray[][],SDays;
     String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody,tbgBody;
     String SBWkd,SCWkd,SDWkd,SSWkd,SSpWkd;
     String SUB,SUC,SUD,SUS,SUSp,SNetUtil,SNet;
     String SBDays,SCDays,SDDays,SSDays,SSpDays;

     FileWriter  FW;
     int Lctr = 70,Pctr = 0;
     String SLine;
     Vector VHead;

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
          common = new Common();

          bgColor = common.bgColor;
          bgHead  = common.bgHead;
          bgUom   = common.bgUom;
          bgBody  = common.bgBody;
          fgHead  = common.fgHead;
          fgUom   = common.fgUom;
          fgBody  = common.fgBody;
		tbgBody = common.tbgBody;
          SLine   = common.Replicate("-",150)+"\n";
     }
     public void doGet(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
            doPost(request,response);
     }
     public void doPost(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
          response.setContentType("text/html");
          PrintWriter out  = response.getWriter();
//		System.out.println(request.getQueryString());
		session = request.getSession(true);
		/*if(session.getValue("AppletTag")==null)
		{
			Control control = new Control();
			String SServer = control.getID("Select ProdTomHost from Server");
			response.sendRedirect(SServer + "/CProduction.SessionExpired");
			return;

		}*/
                
		SStDate  = request.getParameter("Date");
		SEnDate  = request.getParameter("DateTo");
          SDays            = " "+(common.toInt(common.getDateDiff(SEnDate,SStDate))*24);
          SStDate          = common.pureDate(SStDate);
          SEnDate          = common.pureDate(SEnDate);
		SUnit  = request.getParameter("Unit").trim();

		System.out.println("StopPeriod->" + SStDate +":" + SEnDate + ":" + SUnit + ":" + SDays);


          setVectors();
          setSArray();

          try
          {
               FW      = new FileWriter("/software/C-Prod-Print/Reports/stopallc.prn");
               //FW      = new FileWriter("/production/Reports/stopallc.prn");
               Lctr    = 70;
               Pctr    =  0;
               Head();
               Body();
               //AbsPrt();
               FW.close();
          }catch(Exception ex){System.out.println(ex);}

          out.println("<html>");
          out.println("<head>");
          out.println("<title> </title>");
          out.println("</head>");
          out.println("<body bgcolor='" + common.bgBody + "'>");
		//out.println(session.getValue("AppletTag"));
          out.println("<a href='Header'>Home</a>");
		out.println(common.getConsReportHeader("Stoppages - Departments Consolidation - Reasonwise",SUnit,SStDate,SEnDate));
		out.println("<center><br><br>");

          out.println("  <table border='1' width='1300' height='7'>");
          out.println("    <tr>");
          out.println("      <td width='50' height='1' rowspan='2' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'><b>Sl No</b></font></td>");
          out.println("      <td width='350' height='41' valign='top' bgcolor='"+bgHead+"'><p align='right'><font color='"+fgHead+"' size='4'><b>Department ---</b></font></td>");
          out.println("      <td width='125' height='29' colspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'><b>Blow Room</b></font></td>");
          out.println("      <td width='130' height='29' colspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'><b>Carding</b></font></td>");
          out.println("      <td width='128' height='29' colspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'><b>Comber</b></font></td>");
          out.println("      <td width='128' height='29' colspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'><b>Drawing</b></font></td>");
          out.println("      <td width='128' height='29' colspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'><b>Simplex</b></font></td>");
          out.println("      <td width='128' height='27' colspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'><b>Spinning</b></font></td>");
          out.println("      <td width='128' height='27' colspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'><b>Total</b></font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='350' height='1' valign='top' align='right' bgcolor='"+bgUom+"'><p align='left'><font color='"+fgUom+"' size='4'><b>Reasons</b></font></p></td>");
          out.println("      <td width='60' height='1' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>hrs</font></td>");
          out.println("      <td width='65' height='1' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='65' height='1' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>hrs</font></td>");
          out.println("      <td width='65' height='1' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='64' height='1' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>hrs</font></td>");
          out.println("      <td width='64' height='1' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='64' height='1' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>hrs</font></td>");
          out.println("      <td width='64' height='1' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='64' height='1' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>hrs</font></td>");
          out.println("      <td width='64' height='1' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='64' height='1' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>hrs</font></td>");
          out.println("      <td width='64' height='1' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='64' height='1' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>hrs</font></td>");
          out.println("      <td width='64' height='1' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>%</font></td>");
          out.println("    </tr>");

          for(int i=1;i<VCode.size();i++)
          {
               out.println("    <tr>");
               out.println("      <td width='50' height='2' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+SArray[i][0]+"</font></td>");
               out.println("      <td width='350' height='2' valign='top' align='right' bgcolor='"+tbgBody+"'><p align='left'><font color='"+fgBody+"'>"+SArray[i][1]+"</font></td>");
               out.println("      <td width='60' height='2' valign='top' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+SArray[i][2]+"</font></td>");
               out.println("      <td width='65' height='2' valign='top' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+SArray[i][3]+"</font></td>");
               out.println("      <td width='65' height='2' valign='top' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+SArray[i][4]+"</font></td>");
               out.println("      <td width='65' height='2' valign='top' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+SArray[i][5]+"</font></td>");
               out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+SArray[i][6]+"</font></td>");
               out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+SArray[i][7]+"</font></td>");
               out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+SArray[i][8]+"</font></td>");
               out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+SArray[i][9]+"</font></td>");
               out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+SArray[i][10]+"</font></td>");
               out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+SArray[i][11]+"</font></td>");
               out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+SArray[i][12]+"</font></td>");
               out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+SArray[i][13]+"</font></td>");
               out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+SArray[i][14]+"</font></td>");
               out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+SArray[i][15]+"</font></td>");
               out.println("    </tr>");
          }
          out.println("    <tr>");
          out.println("      <td width='50' height='8' rowspan='4' bgcolor='"+bgUom+"'></td>");
          out.println("      <td width='350' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><p align='right'><font color='"+fgUom+"' size='4'><b>Total Stoppages</b></font></td>");
          out.println("      <td width='60' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+common.getSum(VBMnt,"S")+"</font></td>");
          out.println("      <td width='65' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='65' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+common.getSum(VCMnt,"S")+"</font></td>");
          out.println("      <td width='65' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+common.getSum(VDMnt,"S")+"</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+common.getSum(VSMnt,"S")+"</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+common.getSum(VSpMnt,"S")+"</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SNet+"</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='350' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"' size='4'><b>Total Hours Worked</b></font></td>");
          out.println("      <td width='60' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SBWkd+"</font></td>");
          out.println("      <td width='65' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='65' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SCWkd+"</font></td>");
          out.println("      <td width='65' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SDWkd+"</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>~</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SSWkd+"</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SSpWkd+"</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='350' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"' size='4'><b>Total Hours Available</b></font></td>");
          out.println("      <td width='60' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SBDays+"</font></td>");
          out.println("      <td width='65' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='65' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SCDays+"</font></td>");
          out.println("      <td width='65' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SDDays+"</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SSDays+"</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SSpDays+"</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='350' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"' size='4'><b>Utilization</b></font></td>");
          out.println("      <td width='60' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='65' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SUB+"</font></td>");
          out.println("      <td width='65' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='65' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SUC+"</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SUD+"</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SUS+"</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SUSp+"</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>-</font></td>");
          out.println("      <td width='64' height='2' valign='top' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SNetUtil+"</font></td>");
          out.println("    </tr>");
          out.println("  </table>");
          out.println("</div>");
          out.println("</body>");
          out.println("</html>");

          out.close();
     }

     public void setVectors()
     {
          VCode   = new Vector();
          VName   = new Vector();
          VBCode  = new Vector();
          VBMnt   = new Vector();
          VCCode  = new Vector();
          VCMnt   = new Vector();
          VDCode  = new Vector();
          VDMnt   = new Vector();
          VSCode  = new Vector();
          VSMnt   = new Vector();
          VSpCode = new Vector();
          VSpMnt  = new Vector();

          try
          {
                  Connection con = createConnection();
                  Statement stat = con.createStatement();
                  ResultSet res  = stat.executeQuery(getQSReasons());
                  while(res.next())
                  {
                         VName .addElement(res.getString(1));
                         VCode .addElement(res.getString(2));
                  }
                  ResultSet res1 = stat.executeQuery(getBlowQS());
                  while(res1.next())
                  {
                         VBCode .addElement(res1.getString(1));
                         VBMnt  .addElement(res1.getString(2));
                  }
                  ResultSet res2 = stat.executeQuery(getCardQS());
                  while(res2.next())
                  {
                         VCCode .addElement(res2.getString(1));
                         VCMnt  .addElement(res2.getString(2));
                  }
                  ResultSet res3 = stat.executeQuery(getDrawQS());
                  while(res3.next())
                  {
                         VDCode .addElement(res3.getString(1));
                         VDMnt  .addElement(res3.getString(2));
                  }
                  ResultSet res4 = stat.executeQuery(getSimpQS());
                  while(res4.next())
                  {
                         VSCode .addElement(res4.getString(1));
                         VSMnt  .addElement(res4.getString(2));
                  }
                  ResultSet res5 = stat.executeQuery(getSpinQS());
                  while(res5.next())
                  {
                         VSpCode .addElement(res5.getString(1));
                         VSpMnt  .addElement(res5.getString(2));
                  }
                  con.close();
          }
          catch(Exception ex)
          {
               System.out.println("line 227 : "+ex);
          }
     }
     public String getBlowQS()
     {
          String QS = " SELECT stoptran.stop_code, Sum(Stop_Mint/60) AS Expr1 "+
                      " FROM stoptran "+
                      " WHERE (((StopTran.Stop_Date)>='"+SStDate+"' And "+
                      " (StopTran.Stop_Date)<='"+SEnDate+"') AND "+
                      " ((StopTran.Unit_Code)="+SUnit+") AND ((StopTran.Dept_Code)=2)) "+
                      " GROUP BY stoptran.stop_code ";
          return QS;
     }
     public String getCardQS()
     {
          String QS = " SELECT stoptran.stop_code, Sum(Stop_Mint/60) AS Expr1 "+
                      " FROM stoptran "+
                      " WHERE (((StopTran.Stop_Date)>='"+SStDate+"' And "+
                      " (StopTran.Stop_Date)<='"+SEnDate+"') AND "+
                      " ((StopTran.Unit_Code)="+SUnit+") AND ((StopTran.Dept_Code)=3)) "+
                      " GROUP BY stoptran.stop_code ";
          return QS;
     }
     public String getDrawQS()
     {
          String QS = " SELECT stoptran.stop_code, Sum(Stop_Mint/60) AS Expr1 "+
                      " FROM stoptran "+
                      " WHERE (((StopTran.Stop_Date)>='"+SStDate+"' And "+
                      " (StopTran.Stop_Date)<='"+SEnDate+"') AND "+
                      " ((StopTran.Unit_Code)="+SUnit+") AND ((StopTran.Dept_Code)=5)) "+
                      " GROUP BY stoptran.stop_code ";
          return QS;
     }
     public String getSimpQS()
     {
          String QS = " SELECT stoptran.stop_code, Sum(Stop_Mint/60) AS Expr1 "+
                      " FROM stoptran "+
                      " WHERE (((StopTran.Stop_Date)>='"+SStDate+"' And "+
                      " (StopTran.Stop_Date)<='"+SEnDate+"') AND "+
                      " ((StopTran.Unit_Code)="+SUnit+") AND ((StopTran.Dept_Code)=6)) "+
                      " GROUP BY stoptran.stop_code ";
          return QS;
     }
     public String getSpinQS()
     {
          String QS = " SELECT stoptran.stop_code, Sum(Stop_Mint/60) AS Expr1 "+
                      " FROM stoptran "+
                      " WHERE (((StopTran.Stop_Date)>='"+SStDate+"' And "+
                      " (StopTran.Stop_Date)<='"+SEnDate+"') AND "+
                      " ((StopTran.Unit_Code)="+SUnit+") AND ((StopTran.Dept_Code)=7)) "+
                      " GROUP BY stoptran.stop_code ";
          return QS;
     }

     public String getQSReasons()
     {
          String QS = " SELECT stop_name,stop_code from stopreasons Order By Stop_Code";
          return QS;
     }

     public void setSArray()
     {
          SArray = new String[60][16];

          //The StopCode Loop - as Row
          for(int i=0;i<VCode.size();i++)
          {
               SArray[common.toInt((String)VCode.elementAt(i))][0] = " "+(i+1);
               SArray[common.toInt((String)VCode.elementAt(i))][1] = (String)VName.elementAt(i);
               if(VBCode.indexOf((String)VCode.elementAt(i)) < 0)
               {
                    SArray[common.toInt((String)VCode.elementAt(i))][2] = "-";
                    SArray[common.toInt((String)VCode.elementAt(i))][3] = "-";
               }
               else
               {
                    SArray[common.toInt((String)VCode.elementAt(i))][2] = common.getRound((String)VBMnt.elementAt(VBCode.indexOf((String)VCode.elementAt(i))),2);
                    SArray[common.toInt((String)VCode.elementAt(i))][3] = getMntPer(VBMnt,(String)VBMnt.elementAt(VBCode.indexOf((String)VCode.elementAt(i))));
               }
               if(VCCode.indexOf((String)VCode.elementAt(i)) < 0)
               {
                    SArray[common.toInt((String)VCode.elementAt(i))][4] = "-";
                    SArray[common.toInt((String)VCode.elementAt(i))][5] = "-";
               }
               else
               {
                    SArray[common.toInt((String)VCode.elementAt(i))][4] = common.getRound((String)VCMnt.elementAt(VCCode.indexOf((String)VCode.elementAt(i))),2);
                    SArray[common.toInt((String)VCode.elementAt(i))][5] = getMntPer(VCMnt,(String)VCMnt.elementAt(VCCode.indexOf((String)VCode.elementAt(i))));
               }
               if(VDCode.indexOf((String)VCode.elementAt(i)) < 0)
               {
                    SArray[common.toInt((String)VCode.elementAt(i))][8] = "-";
                    SArray[common.toInt((String)VCode.elementAt(i))][9] = "-";
               }
               else
               {
                    SArray[common.toInt((String)VCode.elementAt(i))][8] = common.getRound((String)VDMnt.elementAt(VDCode.indexOf((String)VCode.elementAt(i))),2);
                    SArray[common.toInt((String)VCode.elementAt(i))][9] = getMntPer(VDMnt,(String)VDMnt.elementAt(VDCode.indexOf((String)VCode.elementAt(i))));
               }
               SArray[common.toInt((String)VCode.elementAt(i))][6] = "-";
               SArray[common.toInt((String)VCode.elementAt(i))][7] = "@";
               if(VSCode.indexOf((String)VCode.elementAt(i)) < 0)
               {
                    SArray[common.toInt((String)VCode.elementAt(i))][10] = "-";
                    SArray[common.toInt((String)VCode.elementAt(i))][11] = "-";
               }
               else
               {
                    SArray[common.toInt((String)VCode.elementAt(i))][10] = common.getRound((String)VSMnt.elementAt(VSCode.indexOf((String)VCode.elementAt(i))),2);
                    SArray[common.toInt((String)VCode.elementAt(i))][11] = getMntPer(VSMnt,(String)VSMnt.elementAt(VSCode.indexOf((String)VCode.elementAt(i))));
               }
               if(VSpCode.indexOf((String)VCode.elementAt(i)) < 0)
               {
                    SArray[common.toInt((String)VCode.elementAt(i))][12] = "-";
                    SArray[common.toInt((String)VCode.elementAt(i))][13] = "-";
               }
               else
               {
                    SArray[common.toInt((String)VCode.elementAt(i))][12] = common.getRound((String)VSpMnt.elementAt(VSpCode.indexOf((String)VCode.elementAt(i))),2);
                    SArray[common.toInt((String)VCode.elementAt(i))][13] = getMntPer(VSpMnt,(String)VSpMnt.elementAt(VSpCode.indexOf((String)VCode.elementAt(i))));
               }
               SArray[common.toInt((String)VCode.elementAt(i))][14] = "-";
               SArray[common.toInt((String)VCode.elementAt(i))][15] = "-";
          }

          for(int i=0;i<VCode.size();i++)
          {
               if(SArray[i][1]==null)
               {
                    for(int k=0;k<16;k++)
                         SArray[i][k] = ".";
               }
          }
          SBDays  = common.getRound(common.toDouble(SDays)*2.0*24.0,0);
          SCDays  = common.getRound(common.toDouble(SDays)*15.0*24.0,0);
          SDDays  = common.getRound(common.toDouble(SDays)*9.0*24.0,0);
          SSDays  = common.getRound(common.toDouble(SDays)*11.0*24.0,0);
          SSpDays = common.getRound(common.toDouble(SDays)*12.0*24.0,0);

          SBWkd  = common.getRound(common.toDouble(SBDays)-common.toDouble(common.getSum(VBMnt,"S"))/60.0,2);
          SCWkd  = common.getRound(common.toDouble(SCDays)-common.toDouble(common.getSum(VCMnt,"S"))/60.0,2);
          SDWkd  = common.getRound(common.toDouble(SDDays)-common.toDouble(common.getSum(VDMnt,"S"))/60.0,2);
          SSWkd  = common.getRound(common.toDouble(SSDays)-common.toDouble(common.getSum(VSMnt,"S"))/60.0,2);
          SSpWkd = common.getRound(common.toDouble(SSpDays)-common.toDouble(common.getSum(VSpMnt,"S"))/60.0,2);

          SUB  = common.getRound(common.toDouble(SBWkd)*100.0/common.toDouble(SBDays),2);
          SUC  = common.getRound(common.toDouble(SCWkd)*100.0/common.toDouble(SCDays),2); 
          SUD  = common.getRound(common.toDouble(SDWkd)*100.0/common.toDouble(SDDays),2);
          SUS  = common.getRound(common.toDouble(SSWkd)*100.0/common.toDouble(SSDays),2);
          SUSp = common.getRound(common.toDouble(SSpWkd)*100.0/common.toDouble(SSpDays),2);

          fillTotal();
     }
     public String getMntPer(Vector Vect,String Smnt)
     {
          double dSum=0;
          for(int i=0;i<Vect.size();i++)
          {
               dSum=dSum+common.toDouble((String)Vect.elementAt(i));
          }
          return common.getRound((common.toDouble(Smnt)*100.0/dSum),2);
     }
     public void fillTotal()
     {
          double dNet = 0;
          dNet = dNet+common.toDouble(common.getSum(VBMnt,"S"));
          dNet = dNet+common.toDouble(common.getSum(VCMnt,"S"));
          dNet = dNet+common.toDouble(common.getSum(VDMnt,"S"));
          dNet = dNet+common.toDouble(common.getSum(VSMnt,"S"));
          dNet = dNet+common.toDouble(common.getSum(VSpMnt,"S"));
          SNet = common.getRound(dNet,2);
          for(int i=1;i<VCode.size();i++)
          {
               SArray[i][14] = common.getRound(common.toDouble(SArray[i][2])+common.toDouble(SArray[i][4])+common.toDouble(SArray[i][8])+common.toDouble(SArray[i][10])+common.toDouble(SArray[i][12]),2);
               SArray[i][15] = common.getRound(common.toDouble(SArray[i][14])*100/dNet,2);
          }
          SNetUtil = common.getRound((common.toDouble(SUB)+common.toDouble(SUC)+common.toDouble(SUD)+common.toDouble(SUS)+common.toDouble(SUSp))/5,2);    
     }
     public void Head()
     {
          if(Lctr < 60)
             return;
          if(Pctr > 0)
          {
             try
             {
               FW.write(SLine+"\n");
             }catch(Exception ex){}
          }
          Pctr++;

          String Str1    = common.Pad("Company    : AMARJOTHI SPINNING MILLS LIMITED",80);
          String Str2    = common.Pad("Document   : Stoppage Report Overall : "+common.parseDate(SStDate)+" - "+common.parseDate(SEnDate),80);
          String Str3    = common.Pad("Unit       : "+(SUnit.equals("1")?"A":(SUnit.equals("2")?"B":"S")),70);
          VHead = Stop1Head();
          try
          {
               FW.write(Str1+"\n");
               FW.write(Str2+"\n");
               FW.write(Str3+"\n\n");
               for(int i=0;i<VHead.size();i++)
               {
                    FW.write((String)VHead.elementAt(i));
               }
          }catch(Exception ex){}
          Lctr = 4+VHead.size();
     }
     public void Body()
     {
         String Strl;
         for(int i=0;i<VCode.size();i++)
         {
                Strl = common.Rad(SArray[i][0].trim(),2)+common.Space(2)+
                       common.Pad(SArray[i][1],20);
                for(int k=2;k<16;k++)
                     Strl = Strl+common.Space(2)+common.Rad(SArray[i][k],7);

                try
                {
                     FW.write(Strl+"\n");
                     Lctr++;
                     Head();
                }catch(Exception ex){}
         }
         try
         {
               FW.write(SLine);               
         }catch(Exception ex){}
     }

     public Vector Stop1Head()
     {
          Vector VHead = new Vector();

          String Str1 = common.Rad("Sl",2)+common.Space(2)+
                        common.Pad("Department ---",20)+common.Space(2)+
                        common.Cad("Blow Room",7+2+7)+common.Space(2)+
                        common.Cad("Carding",7+2+7)+common.Space(2)+
                        common.Cad("Drawing",7+2+7)+common.Space(2)+
                        common.Cad("Comber",7+2+7)+common.Space(2)+
                        common.Cad("Simplex",7+2+7)+common.Space(2)+
                        common.Cad("Spinning",7+2+7)+common.Space(2)+
                        common.Cad("Total",7+2+7);

          String Str2 = common.Rad("No",2)+common.Space(2)+
                        common.Pad(" ",20)+common.Space(2)+
                        common.Replicate("-",7+2+7)+common.Space(2)+
                        common.Replicate("-",7+2+7)+common.Space(2)+
                        common.Replicate("-",7+2+7)+common.Space(2)+
                        common.Replicate("-",7+2+7)+common.Space(2)+
                        common.Replicate("-",7+2+7)+common.Space(2)+
                        common.Replicate("-",7+2+7)+common.Space(2)+
                        common.Replicate("-",7+2+7);

          String Str3 = common.Rad(" ",2)+common.Space(2)+
                        common.Pad("Reasons",20)+common.Space(2)+
                        common.Rad("Hrs",7)+common.Space(2)+common.Rad("%",7)+common.Space(2)+
                        common.Rad("Hrs",7)+common.Space(2)+common.Rad("%",7)+common.Space(2)+
                        common.Rad("Hrs",7)+common.Space(2)+common.Rad("%",7)+common.Space(2)+
                        common.Rad("Hrs",7)+common.Space(2)+common.Rad("%",7)+common.Space(2)+
                        common.Rad("Hrs",7)+common.Space(2)+common.Rad("%",7)+common.Space(2)+
                        common.Rad("Hrs",7)+common.Space(2)+common.Rad("%",7)+common.Space(2)+
                        common.Rad("Hrs",7)+common.Space(2)+common.Rad("%",7)+common.Space(2)+
                        common.Rad("Total",7);

          VHead.addElement(SLine);
          VHead.addElement(Str1+"\n");
          VHead.addElement(Str2+"\n");
          VHead.addElement(Str3+"\n");
          VHead.addElement(SLine);

          return VHead;
     }

	private Connection createConnection()
	{
		Connection conn;
		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
			conn  = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
		}catch(Exception ee)
		{
			return null;
		}
		return conn;
	}

}
