/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CProduction;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import java.sql.*;


import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPRow;
import com.itextpdf.text.pdf.PdfPTableEvent;

import java.io.FileOutputStream;
import java.util.StringTokenizer;

/**
 *
 * @author Administrator
 */
public class MonthlyWiseProductionReport extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
     java.util.List lst = null;
    Common common = null;
    
    String   SFileName, SFileOpenPath, SFileWritePath;
    Document document;
    PdfPTable table,table1;
    PdfPCell c1;
    private static Font bigbold = FontFactory.getFont("TIMES_ROMAN", 12, Font.BOLD);
    private static Font mediumbold = FontFactory.getFont("TIMES_ROMAN", 8, Font.BOLD);
    private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN",9, Font.NORMAL);
    private static Font smallbold = FontFactory.getFont("TIMES_ROMAN", 9, Font.BOLD);
    private static Font smallbold1 = FontFactory.getFont("TIMES_ROMAN", 5, Font.BOLD);

    int iWidth[] = {15,30,15,30,20,27,27,25,20,20,23,23,20,25};

    int iTotalColumns = 14;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
          response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        common = new Common();

        try {
            String          unitname    = "";
            String          unitcode    = request.getParameter("Unit");
            String          frDate      = request.getParameter("frDate");
            String          toDate      = request.getParameter("toDate");

                            frDate      = common.pureDate(frDate);
                            toDate      = common.pureDate(toDate);

                            frDate      = frDate.substring(0, 6);
                            toDate      = toDate.substring(0, 6);
            int             imonths     = common.getMonths(common.toInt(frDate), common.toInt(toDate));
            
            
            java.util.List  ls          = common.getMonthList(common.toInt(frDate), common.toInt(toDate));
                            lst         = getStopReasonGroupList(frDate,toDate);
            java.util.List  lutil       = getUtilList(frDate,toDate);
            
             SFileName      = "pdfreports/MonthlyWiseProductionReport.pdf";
            SFileOpenPath  = "http://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/"+SFileName;
            SFileWritePath = request.getRealPath("/")+SFileName; 
          
            out.println("<p>");
            out.println(" <a href='"+SFileOpenPath+"' target='_blank'><b>View this PDF</b></a>");
            out.println("</p>");

         
            createPDFFile() ;
            
            StringTokenizer st          = new StringTokenizer(unitcode, "~");
            while (st.hasMoreTokens()) {
                unitcode = st.nextToken();
                unitname = st.nextToken();
            }
            
            double Totgn = 0,Totma=0,Totel=0,Totpc=0,Totcb=0,Totot=0,Totsmac=0,TotStot=0;
            
            out.println("<body  align   s= 'center'>");
            out.println("<p><a href='ProductionServlet'>Home</a></p>");		
            out.println("<table align   = 'center' border = '1'>");
            out.println("<tr    align   = 'center'><td colspan = '13'>" + unitname + " UTILIZATION DETAILS FOR THE YEAR OF " + getMonthNameofString(frDate) + " To " + getMonthNameofString(toDate) + "</td><td rowspan='4'>TOTAL PERCENTAGE</td></tr>");
            out.println("<tr    align   = 'center'><td rowspan = '3'>S.NO</td><td rowspan = '3'>MONTH</td><td rowspan = '2'>Target</td><td colspan='4'>DETAILS OF PRODUCTION</td><td colspan='6'>OTHER LOSS%</td></tr>");
            out.println("<tr    align   = 'center'><td rowspan='2'>Net Util%</td><td colspan='2'>MACHINERY MAINT LOSS%</td><td rowspan='2'>TOTAL MAINTENANCE LOSS</td><td rowspan='2'>ELECTRICAL WORK</td><td rowspan='2'>POWER CUT</td><td rowspan='2'>COUNT AND BOBBIN CHANGE</td><td rowspan='2'>EMPTIES SHORTAGE</td><td rowspan='2'>BOBBIN SHORTAGE</td><td rowspan='2'>OTHERS</td></tr>");
            out.println("<tr    align   = 'center'><td align='center'>Utility</td></td><td>GENERAL CLEANING</td><td>MAINTENANCE</td></tr>");
            
            
            AddCellIntoTable( unitname + " UTILIZATION DETAILS FOR THE YEAR OF " + getMonthNameofString(frDate) + " To " + getMonthNameofString(toDate), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 13,25f,4,1,8,2, bigbold);
            AddCellIntoTable("TOTAL PERCENTAGE ", table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,4, 1, 8, 2, mediumbold,4);

            AddCellIntoTable("S.NO" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold,3);
            AddCellIntoTable("MONTH" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold,3);
            AddCellIntoTable("Target" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold,2);
            AddCellIntoTable("DETAILS OF PRODUCTION" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 4,25f, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("OTHER LOSS%" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 6,25f, 4, 1, 8, 2, mediumbold);
           
            
            AddCellIntoTable("Net Util%" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold,2);
            AddCellIntoTable("MACHINERY MAINT LOSS%" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2,25f, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("TOTAL MAINTENANCE LOSS" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold,2);
            AddCellIntoTable("ELECTRICAL WORK" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold,2);
            AddCellIntoTable("POWER CUT" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold,2);
            AddCellIntoTable("COUNT AND BOBBIN CHANGE" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold,2);

            AddCellIntoTable("EMPTIES SHORTAGE" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold,2);
            AddCellIntoTable("BOBBIN SHORTAGE" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold,2);
            AddCellIntoTable("OTHERS" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold,2);
            
            AddCellIntoTable("Utility" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("GENERAL CLEANING" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("MAINTENANCE" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
            
            AddCellIntoTable("TARGET" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 4,25f, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("0.30" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("0.33" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
            
            AddCellIntoTable("0.63" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("0.10" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
            AddCellIntoTable("" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, mediumbold);
            
           
            
            int islno = 0;
            double dTotTarUtil=0,dTotNet =0;
            for (int i = 0; i < ls.size(); i++) {
                islno++;
                String smtn = common.parseNull((ls.get(i)).toString());
                String smonths = getMonthNameofString(smtn);
                out.println("<tr><td>" + islno + "</td>");
                out.println("<td>" + smonths + "</td>");
                out.println("<td align = 'center'>99</td>");
                
                 dTotTarUtil =  dTotTarUtil + 99;
                
                AddCellIntoTable(String.valueOf(islno) , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallnormal);
                AddCellIntoTable(smonths , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallnormal);
                AddCellIntoTable("99" , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallnormal);

                String  snoft    = "";
                double  stot     = 0;
                int icn = 0;
                for(int j=0;j<lutil.size();j++){
                    java.util.HashMap mp = (java.util.HashMap)lutil.get(j);
                    String sm = (String)mp.get("MONTHINSTRING");
                    String su = (String)mp.get("UTILITY");
                    String sn = (String)mp.get("NOOFSPDL");
                    
                    if(smtn.equals(sm)) {
                        stot = common.toDouble(common.getRound(common.toDouble(su),2));
                        dTotNet = dTotNet + stot;
                        out.println("<td align = 'right'>"+common.getRound(common.toDouble(su),2)+"</td>");
                        AddCellIntoTable(common.getRound(common.toDouble(su),2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallnormal);
                        snoft = sn;
                        icn++;
                        break;
                    }
                }

                if(icn==0){
                    out.println("<td align = 'right'>&nbsp;</td>");
                    AddCellIntoTable("" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallnormal);

                }

                double gn = getMonthandGroupPercentage(common.toInt(smtn.trim()),1,snoft);
                double ma = getMonthandGroupPercentage(common.toInt(smtn.trim()),2,snoft);
                double el = getMonthandGroupPercentage(common.toInt(smtn.trim()),3,snoft);
                double pc = getMonthandGroupPercentage(common.toInt(smtn.trim()),4,snoft);
                double cb = getMonthandGroupPercentage(common.toInt(smtn.trim()),5,snoft);
                double ot = getMonthandGroupPercentage(common.toInt(smtn.trim()),8,snoft);
                double smac = gn+ma;
                
                stot = stot+gn+ma+el+pc+cb+ot;
                
                Totgn += gn;
                Totma += ma;
                Totel += el;
                Totpc += pc;
                Totcb += cb;
                Totot += ot;
                Totsmac += smac;
                TotStot += stot;

                out.println("<td align = 'right'>"+common.getRound(gn,3)+"</td>");
                out.println("<td align = 'right'>"+common.getRound(ma,3)+"</td>");
                out.println("<td align = 'right'>"+common.getRound(smac,3)+"</td>");
                out.println("<td align = 'right'>"+common.getRound(el,3)+"</td>");
                out.println("<td align = 'right'>"+common.getRound(pc,3)+"</td>");
                out.println("<td align = 'right'>"+common.getRound(cb,3)+"</td>");
                out.println("<td>&nbsp;</td>");
                out.println("<td>&nbsp;</td>");
                out.println("<td align = 'right'>"+common.getRound(ot,3)+"</td>");
                out.println("<td align = 'right'>"+common.getRound(stot,2)+"</td>");
                out.println("</tr>");
                
                AddCellIntoTable(common.getRound(gn,3) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallnormal);
                AddCellIntoTable(common.getRound(ma,3) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallnormal);
                AddCellIntoTable(common.getRound(smac,3), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallnormal);
                AddCellIntoTable(common.getRound(el,3), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallnormal);
                AddCellIntoTable(common.getRound(pc,3) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallnormal);
                AddCellIntoTable(common.getRound(cb,3) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallnormal);
                AddCellIntoTable("" , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallnormal);
                AddCellIntoTable("" , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallnormal);
                AddCellIntoTable(common.getRound(ot,3) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallnormal);
                AddCellIntoTable(common.getRound(stot,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallnormal);
            }
            
            double dAvgTarUtil = dTotTarUtil/imonths;
            double dAvgNet     = dTotNet/imonths;
            double dAvggn = Totgn/imonths;
            double dAvgma = Totma/imonths;
            double dAvgel = Totel/imonths;
            double dAvgpc = Totpc/imonths;
            double dAvgcb = Totcb/imonths;
            double dAvgot = Totot/imonths;
            double dAvgsmac = Totsmac/imonths;
            double dAvgstot = TotStot/imonths;
            
            
            out.println("<td align = 'center' colspan=2>Average</td>");
            out.println("<td align = 'center'>"+common.getRound(dAvgTarUtil,0)+"</td>");
            out.println("<td align = 'right'>"+common.getRound(dAvgNet,2)+"</td>");
            out.println("<td align = 'right'>"+common.getRound(dAvggn,3)+"</td>");
            out.println("<td align = 'right'>"+common.getRound(dAvgma,3)+"</td>");
            out.println("<td align = 'right'>"+common.getRound(dAvgsmac,3)+"</td>");
            out.println("<td align = 'right'>"+common.getRound(dAvgel,3)+"</td>");
            out.println("<td align = 'right'>"+common.getRound(dAvgpc,3)+"</td>");
            out.println("<td align = 'right'>"+common.getRound(dAvgcb,3)+"</td>");
            out.println("<td>&nbsp;</td>");
            out.println("<td>&nbsp;</td>");
            out.println("<td align = 'right'>"+common.getRound(dAvgot,3)+"</td>");
            out.println("<td align = 'right'>"+common.getRound(dAvgstot,2)+"</td>");
            out.println("</tr>");
            
            AddCellIntoTable("Average" , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f, 4, 1, 8, 2, smallbold);
            AddCellIntoTable(common.getRound(dAvgTarUtil,0) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallbold);
            AddCellIntoTable(common.getRound(dAvgNet,2), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallbold);
            AddCellIntoTable(common.getRound(dAvggn,3), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallbold);
            AddCellIntoTable(common.getRound(dAvgma,3) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallbold);
            AddCellIntoTable(common.getRound(dAvgsmac,3) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallbold);
            AddCellIntoTable(common.getRound(dAvgel,3) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallbold);
            AddCellIntoTable(common.getRound(dAvgpc,3) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallbold);
            AddCellIntoTable(common.getRound(dAvgcb,3) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallbold);
            AddCellIntoTable("" , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallbold);
            AddCellIntoTable("" , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallbold);
            AddCellIntoTable(common.getRound(dAvgot,3) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallbold);
            AddCellIntoTable(common.getRound(dAvgstot,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 4, 1, 8, 2, smallbold);
             
                document.add(table);
                document.close();
            
            out.println("</table>");
            out.println("</body>");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
      private void createPDFFile()
    {
        try 
        {
	    document = new Document();
//             String sFileName        = "OEMonthlyProdPDF.pdf";
//                    sFileName        = common.getPrintPath()+sFileName;
//                   PdfWriter.getInstance(document, new FileOutputStream(sFileName));
            PdfWriter.getInstance(document, new FileOutputStream(SFileWritePath));
            document.setPageSize(PageSize.A4.rotate());
            document.open();
                             
            table = new PdfPTable(iTotalColumns);
            table.setWidths(iWidth);
            table.setWidthPercentage(100);
            
          
        } 
        catch (Exception ex) 
        {
            ex.printStackTrace();
            System.out.println(ex);
        }

    }

    public String getMonthNameofString(String SMonthYear) {

        String  SMonthYearString    = "";
        String  Month               = SMonthYear.substring(4, 6);
        String  Year                = SMonthYear.substring(0, 4);
        int     iMonth              = common.toInt(Month);
//        System.out.println(Month + "=" + Year);
        String  MonthName           = "";
        String  YearNumber          = Year;

        if (iMonth == 1) {
            MonthName = "JAN";
        } else if (iMonth == 2) {
            MonthName = "FEB";
        } else if (iMonth == 3) {
            MonthName = "MAR";
        } else if (iMonth == 4) {
            MonthName = "APR";
        } else if (iMonth == 5) {
            MonthName = "MAY";
        } else if (iMonth == 6) {
            MonthName = "JUN";
        } else if (iMonth == 7) {
            MonthName = "JUL";
        } else if (iMonth == 8) {
            MonthName = "AUG";
        } else if (iMonth == 9) {
            MonthName = "SEP";
        } else if (iMonth == 10) {
            MonthName = "OCT";
        } else if (iMonth == 11) {
            MonthName = "NOV";
        } else if (iMonth == 12) {
            MonthName = "DEC";
        }
        SMonthYearString = MonthName + " - " + YearNumber;
        return SMonthYearString;
    }

    public java.util.List getUtilList(String sfrmonth,String stomonth){

        java.util.List  ls  = new java.util.ArrayList();
        StringBuffer    sb  = new StringBuffer();
                        sb  . append(" select round((numofwkd/numofspdl)*100,2) as utility,monthinstring,numofspdl from");
                        sb  . append(" (");
                        sb  . append(" select sum(oetran.noofrot) as numofspdl,sum(oetran.rotwkd) as numofwkd,substr(sp_date,1,6) as monthinstring from oetran");
                        sb  . append(" inner join machine on machine.mach_code = oetran.mach_code and reportincludestatus = 0");
                        sb  . append(" where substr(sp_date,1,6) >= '"+sfrmonth+"' and substr(sp_date,1,6) <= '"+stomonth+"'");
                        sb  . append(" and oetran.sp_Date not in(select HDate from ProdHoliday where leaveday = 1)");
                        sb  . append(" group by substr(sp_date,1,6)");
                        sb  . append(" )");
                        sb  . append(" order by monthinstring");
                        
                        //System.out.println(sb.toString());

        try{
            Connection con = createConnection();
            Statement stat = con.createStatement();
            ResultSet res  = stat.executeQuery(sb.toString());
            while(res.next())
            {
                java.util.HashMap hm  = new java.util.HashMap();
                                  hm  . put("UTILITY",common.parseNull(res.getString(1)));
                                  hm  . put("MONTHINSTRING",common.parseNull(res.getString(2)));
                                  hm  . put("NOOFSPDL",common.parseNull(res.getString(3)));
                ls.add(hm);
            }
            res.close();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return ls;
    }

    public java.util.List getStopReasonGroupList(String sfrmonth,String stomonth){

        java.util.List  ls  = new java.util.ArrayList();
        StringBuffer    sb  = new StringBuffer();

                        sb  . append(" select smonthstring,group_code,sum(spindleideal) as idspd,group_name from (");
                        sb  . append(" select substr(stop_date,1,6) as smonthstring,stop_shift,sum(stop_mint),");
                        sb  . append(" stoptran.mach_code,t.noofrot,round((sum(stop_mint)*((t.noofrot/480))),5) as spindleideal,");
                        sb  . append(" stoptran.stop_code,group_code,group_name from stoptran");
                        sb  . append(" inner join (");
                        sb  . append(" select distinct noofrot,oetran.mach_code,sp_date,shift_code from oetran");
                        sb  . append(" inner join machine on machine.mach_code = oetran.mach_code and reportincludestatus = 0");
                        sb  . append(" where 	substr(sp_date,1,6)	>= '"+sfrmonth+"'");
                        sb  . append(" and 	substr(sp_date,1,6)	<= '"+stomonth+"'");
                        sb  . append(" and 	noofrot>  0 and oetran.sp_Date not in(select HDate from ProdHoliday  where leaveday = 1)) t on t.mach_code = stoptran.mach_code");
                        sb  . append(" and t.shift_code = stoptran.stop_shift and t.sp_date = stoptran.stop_date");
                        sb  . append(" inner join stopreasons on stopreasons.stop_code = stoptran.stop_code");
                        sb  . append(" inner join common.stopreasongrouping on stopreasongrouping.group_code = stopreasons.stopreason_groupcode");
                        sb  . append(" where stoptran.dept_code = 43 and substr(stop_date,1,6) >= '"+sfrmonth+"' and substr(stop_date,1,6) <= '"+stomonth+"' and unit_code = 3");
                        sb  . append(" group by stop_date,stoptran.mach_code,stop_shift,t.noofrot,stoptran.stop_code,group_code,group_name");
                        sb  . append(" order by stop_date,stop_shift,stoptran.stop_code");
                        sb  . append(" )");
                        sb  . append(" group by smonthstring,group_code,group_name");
                        sb  . append(" order by group_code");
                        
                       System.out.println(sb.toString());

        try{
            Connection con = createConnection();
            Statement stat = con.createStatement();
            ResultSet res  = stat.executeQuery(sb.toString());
            while(res.next())
            {
                java.util.HashMap hm  = new java.util.HashMap();

                                  hm  . put("SMONTHSTRING", common.parseNull(res.getString(1)));
                                  hm  . put("GROUPCODE",    common.parseNull(res.getString(2)));
                                  hm  . put("SPINDLEIDEAL", common.parseNull(res.getString(3)));
                                  hm  . put("GROUPNAME",    common.parseNull(res.getString(4)));
                ls.add(hm);
            }
            res.close();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return ls;
    }
    
    public double getMonthandGroupPercentage(int smonthstring,int groupcode,String snoofspdl){
        double sval = 0;
        //lst
        for(int i=0;i<lst.size();i++){

            java.util.HashMap   hm              = (java.util.HashMap)lst.get(i);
            int                 sdmonthstring   = common.toInt(common.parseNull((String)hm  . get("SMONTHSTRING")).trim());
            int                 dgroupcode      = common.toInt(common.parseNull((String)hm  . get("GROUPCODE")).trim());
            double              spdlideal       = common.toDouble(common.parseNull((String)hm  . get("SPINDLEIDEAL")).trim());
            String              groupname       = common.parseNull((String)hm  . get("GROUPNAME")).trim();

            //System.out.println(smonthstring+"-"+sdmonthstring+"-"+groupcode+"-"+dgroupcode+"-"+snoofspdl+"-"+spdlideal+"-"+groupname);
            
            if(smonthstring==sdmonthstring && groupcode==dgroupcode){
                //System.out.println(smonthstring+"-"+groupcode+"-"+snoofspdl+"-"+sdmonthstring+"-"+dgroupcode+"-"+spdlideal+"-"+groupname);
                sval = spdlideal;
            }
        }
        System.out.print(sval+"\t");
        sval = common.toDouble(common.getRound(((sval/common.toDouble(snoofspdl))*100),5));
        System.out.println("sval =>"+sval+"<="+common.toDouble(snoofspdl));
        return sval;
    }
    
    private Connection createConnection()
    {
        Connection conn;
        try
        {
            Class.forName("oracle.jdbc.OracleDriver");
            conn  = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
        }catch(Exception ee)
        {
                return null;
        }
        return conn;
    }
    
     public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        // c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str ));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        //  c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddColumnIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iColWidth, int iLeftBorder, int iTopBorder, int iRightBorder, Font DocFont, int iBottomBorder) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColWidth);
        // c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }  

}
