/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CProduction;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;
import java.util.*;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;

public class OEOverAllProductionRep extends HttpServlet {

    Common common = null;
    String SUnitCode = "", SProdMonth = "", SMonthStDate = "", SMonthEnDate = "", SUnitName = "";
    ArrayList AStoppageList = null, AMachineList = null, AEBList = null, AGenList = null, ACompList = null, AOETranList = null, AWebList = null;

    // For Pdf Creation
    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
    private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);

    private static Font bigBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
    private static Font bigNormal = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

    private static Font mediumBold = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
    private static Font mediumNormal = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

    private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.BOLD);
    private static Font smallNormal = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);

    private static Font tinyBold = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.BOLD);
    private static Font tinyNormal = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.NORMAL);

    private static Font underBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.UNDERLINE);

    Document document;
    PdfPTable prodtable;
    int[] iProdWidth;
    String SFileOpenPath, SFileWritePath, SPDFFile;
    String SDeptCode = "43";

    java.util.List leavedayList = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        common = new Common();
        SUnitCode = (String) request.getParameter("Unit");
        String SMonth = (String) request.getParameter("FromMonth");
        String SYear = (String) request.getParameter("FromYear");
        SProdMonth = SYear.concat(SMonth);

        SMonthStDate = SProdMonth.concat("01");
        SMonthEnDate = common.getMonthEndDate1(SProdMonth);
        RepVect repvect = new RepVect();
        SUnitName = (String) repvect.VUnit.elementAt(repvect.VUnitCode.indexOf(SUnitCode));

        SPDFFile = "COEOverAllProductionRep.pdf";
        SFileOpenPath = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/" + SPDFFile;
        SFileWritePath = request.getRealPath("/") + SPDFFile;

        iProdWidth = new int[]{30, 20, 20, 25, 30, 25, 25, 20, 20, 25, 25, 20, 20, 20, 20, 20, 20, 20, 25, 20, 25, 25, 30, 25, 30, 25, 25, 20, 20, 20, 20, 20, 20, 20};
        try {
            System.out.println("OEOverAllProductionRep");
            initPdf();
            toHead(out);
            setOEDataVector();
            setPackedProduction();
            setStoppageData();
            setUkgData();
            setCdgData();
            setDrgData();
            setConeWindingProduction();
            toBody(out);
            out.close();
        } catch (Exception ex) {
            System.out.println(" OverAllProduction Rep " + ex);
            ex.printStackTrace();
        }
    }

    private Connection createConnection() {
        Connection conn;
        try {
            Class.forName("oracle.jdbc.OracleDriver");
            conn = DriverManager.getConnection(common.getOraDSN(), common.getOraUserName(), common.getOraPassword());
        } catch (Exception ee) {
            return null;
        }
        return conn;
    }

    private void toHead(PrintWriter out) {
        try {

            out.println("<html>");
            out.println("<body>");

            System.out.println(" File open " + SFileOpenPath);
            out.println("<p>");
            out.println(" <a href='" + SFileOpenPath + "' target='_blank'><b>View Report as Pdf</b></a>");
            out.println("</p>");
            out.println("<table border=1>");
            out.println("<tr bgcolor='" + common.bgHead + "'>");
            out.println("<td rowspan='2'>Date</td>");
            out.println("<td colspan='2'>AVG COUNT</td>");
            out.println("<td colspan='3'>Prod/Day</td>");
            out.println("<td>Tar vs Act</td>");
            out.println("<td colspan='2'>UTIL</td>");
            out.println("<td colspan='2'>Avg GPS</td>");
            out.println("<td colspan='2'>UKG</td>");
            out.println("<td colspan='2'>C/F UE</td>");
            out.println("<td colspan='2'>D/F UE</td>");
            out.println("<td>Cheese Stock</td>");
            out.println("<td colspan='3'>A/C Prod</td>");
            out.println("<td colspan='2'>O/E Vs A/C Prod. Diff.</td>");
            out.println("<td colspan='3'>Pack Prod</td>");
            out.println("<td>A/C Hard Waste</td>");
            out.println("<td colspan='2'>R/F Waste%</td>");
            out.println("<td>Maint</td>");
            out.println("<td>Power Off</td>");
            out.println("<td>Total</td>");
            out.println("<td>&nbsp;</td>");
            out.println("<td>SIGN</td>");
            out.println("</tr>");

            out.println("<tr bgcolor='" + common.bgHead + "'>");
            out.println("<td>On</td>");
            out.println("<td>Upto</td>");
            out.println("<td>On</td>");
            out.println("<td>Upto</td>");
            out.println("<td>Date</td>");
            out.println("<td>&nbsp;</td>");
            out.println("<td>On</td>");
            out.println("<td>Upto</td>");
            out.println("<td>On</td>");
            out.println("<td>Upto</td>");
            out.println("<td>On</td>");
            out.println("<td>Upto</td>");
            out.println("<td>On</td>");
            out.println("<td>Upto</td>");
            out.println("<td>On</td>");
            out.println("<td>Upto</td>");
            out.println("<td>&nbsp;</td>");
            out.println("<td>On</td>");
            out.println("<td>Upto</td>");
            out.println("<td>Date</td>");
            out.println("<td>On</td>");
            out.println("<td>Upto</td>");
            out.println("<td>On</td>");
            out.println("<td>Upto</td>");
            out.println("<td>Date</td>");
            out.println("<td>&nbsp;</td>");
            out.println("<td>On</td>");
            out.println("<td>Upto</td>");
            out.println("<td>Loss</td>");
            out.println("<td>Loss</td>");
            out.println("<td>Loss</td>");
            out.println("<td>&nbsp;</td>");
            out.println("<td>SM(A)</td>");
            out.println("</tr>");

            //For  Pdf 
            AddCellTable("OVER ALL PRODUCTION", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 34, 1, 20f, 4, 1, 8, 2, bigBold);
            AddCellTable(SUnitName + " OE ", prodtable, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 34, 1, 20f, 4, 1, 8, 2, bigBold);

            AddCellTable("Date", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Avg Count", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Prod/Day", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 3, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Tar Vs Act", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Util", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Avg Gps", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("UKG", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("C/F UE", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("D/F UE", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Cheese Stock", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("A/C Prod", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 3, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("R/F Vs A/C prod diff", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Pack prod", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 3, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("A/c Hard Waste", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("R/F Waste%", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Maint", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("PowerOff", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Total", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("SIGN", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);

            AddCellTable("", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("On", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Upto", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("On", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Upto", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Date", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Upto", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("On", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Upto", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("On", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Upto", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("On", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Upto", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("On", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Upto", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("On", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Upto", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("On", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Upto", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Date", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("On", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Upto", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("On", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Upto", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Date", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("On", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Upto", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Loss", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Loss", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("Loss", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
            AddCellTable("SM(A)", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
        } catch (Exception ex) {
            System.out.println(" OEOverAllProduction Rep  Head " + ex);
        }
    }

    private void toBody(PrintWriter out) {
        try {
            int iNoOfDays = common.toInt(common.getDateDiff(common.parseDate(SMonthEnDate), common.parseDate(SMonthStDate)));

            String SProdDate = SMonthStDate;
            for (int i = 0; i < iNoOfDays + 1; i++) {

                double dOnDateProdDiff = 0.0, dUptoDateProdDiff = 0.0;

                String SOnDateAvgCount = getOnDateAvgCount(SProdDate);
                String SUptoDateAvgCount = getUptoDateAvgCount(SProdDate);
                String SAvgProdPerDay = getAvgProdPerDay(SProdDate);
                String STarVsActual = getTargetVsActual(SProdDate);
                String SOnDateUtil = getOnDateUtil(SProdDate);
                String SUptoDateUtil = getUptoDateUtil(SProdDate);
                String SOnDateAvgGps = getOnDateAvgGps(SProdDate);
                String SUptoDateAvgGps = getUptoDateAvgGps(SProdDate);
                String SOnDateProd = getOnDateProd(SProdDate);
                String SUptoDateProd = getUptoDateProd(SProdDate);
                String SOnDateAcProd = getOnDateAcProd(SProdDate);
                String SUptoDateAcProd = getUptoDateAcProd(SProdDate);
                String SOnDatePackedProd = getOnDatePackedProd(SProdDate);
                String SUptoDatePackedProd = getUptoDatePackedProd(SProdDate);
                String SOnDateCdgUe = getOnDateCdgUe(SProdDate);
                String SUptoDateCdgUe = getUptoDateCdgUe(SProdDate);
                String SOnDateDrgUe = getOnDateDrgUe(SProdDate);
                String SUptoDateDrgUe = getUptoDateDrgUe(SProdDate);
                String SAvgAcProdPerDay = getAvgAcProdPerDay(SProdDate);
                String SAcHardWaste = getAcHardWaste(SProdDate);
                String SAvgPackedProdPerDay = getAvgPackedProdPerDay(SProdDate);
                String SOnDatePneumafilWaste = getOnDatePneumafilWaste(SProdDate);
                String SUptoDatePneumafilWaste = getUptoDatePneumafilWaste(SProdDate);
                String SMaintPowerLoss1 = getMaintPowerLoss(SProdDate, "2"); // For Maintenance Loss
                String SMaintPowerLoss2 = getMaintPowerLoss(SProdDate, "4"); // For Electric Loss                    
                String STotalLoss = getTotalLoss(SProdDate);

                dOnDateProdDiff = common.toDouble(SOnDateProd) - common.toDouble(SOnDateAcProd);
                dUptoDateProdDiff = common.toDouble(SUptoDateProd) - common.toDouble(SUptoDateAcProd);

                double dEBUnits = getEBUnits(SProdDate);
                double dGensetUnits = getGensetUnits(SProdDate);
                double dCompUnits = getCompressorUnits(SProdDate);
                double dMixingHallUnits = getMixingHallUnits(SProdDate);
                double dWebUnits = getWebUnits(SProdDate);
                double dUptoDateEBUnits = getUptoDateEBUnits(SProdDate);
                double dUptoDateGensetUnits = getUptoDateGensetUnits(SProdDate);
                double dUptoDateCompUnits = getUptoDateCompressorUnits(SProdDate);
                double dUptoDateMixingHallUnits = getUptoDateMixingHallUnits(SProdDate);
                double dUptoDateWebUnits = getUptoDateWebUnits(SProdDate);
                double dOEOnDateProd = common.toDouble(SOnDateProd);
                double dOEUptoDateProd = common.toDouble(SUptoDateProd);

                double dOnDateUkg = (dEBUnits + dGensetUnits - dMixingHallUnits - dWebUnits) / dOEOnDateProd;
                double dUptoDateUkg = (dUptoDateEBUnits + dUptoDateGensetUnits - dUptoDateMixingHallUnits - dUptoDateWebUnits) / dOEUptoDateProd;

                double dProdVsPacked = (common.toDouble(SUptoDatePackedProd) - common.toDouble(SUptoDateProd)) / (common.toDouble(SUptoDateProd)) * 100;
                String SCheeseStock = getCheeseStock(SProdDate);

                out.println("<tr bgcolor=" + common.tbgBody + ">");
                out.println("<td>" + common.parseDate(SProdDate) + "</td>");
                out.println("<td>" + SOnDateAvgCount + "</td>");
                out.println("<td>" + SUptoDateAvgCount + "</td>");
                out.println("<td>" + SOnDateProd + "</td>");
                out.println("<td>" + SUptoDateProd + "</td>");
                out.println("<td>" + SAvgProdPerDay + "</td>");
                out.println("<td>" + STarVsActual + "</td>");
                out.println("<td>" + SOnDateUtil + "</td>");
                out.println("<td>" + SUptoDateUtil + "</td>");
                out.println("<td>" + SOnDateAvgGps + "</td>");
                out.println("<td>" + SUptoDateAvgGps + "</td>");
                out.println("<td>" + common.getRound(dOnDateUkg, 2) + "</td>");
                out.println("<td>" + common.getRound(dUptoDateUkg, 2) + "</td>");
                out.println("<td>" + SOnDateCdgUe + "</td>");
                out.println("<td>" + SUptoDateCdgUe + "</td>");
                out.println("<td>" + SOnDateDrgUe + "</td>");
                out.println("<td>" + SUptoDateDrgUe + "</td>");
                out.println("<td>" + SCheeseStock + "</td>");
                out.println("<td>" + SOnDateAcProd + "</td>");
                out.println("<td>" + SUptoDateAcProd + "</td>");
                out.println("<td>" + SAvgAcProdPerDay + "</td>");
                out.println("<td>" + common.getRound(dOnDateProdDiff, 2) + "</td>");
                out.println("<td>" + common.getRound(dUptoDateProdDiff, 2) + "</td>");
                out.println("<td>" + SOnDatePackedProd + "</td>");
                out.println("<td>" + SUptoDatePackedProd + "</td>");
                out.println("<td>" + SAvgPackedProdPerDay + "</td>");
                out.println("<td>" + SAcHardWaste + "</td>");
                out.println("<td>" + SOnDatePneumafilWaste + "</td>");
                out.println("<td>" + SUptoDatePneumafilWaste + "</td>");
                out.println("<td>" + SMaintPowerLoss1 + "</td>");
                out.println("<td>" + SMaintPowerLoss2 + "</td>");
                out.println("<td>" + STotalLoss + "</td>");
                out.println("<td>" + common.getRound(dProdVsPacked, 2) + "</td>");
                out.println("<td>&nbsp;</td>");
                out.println("</tr>");
                // For Pdf
                AddCellTable(common.parseDate(SProdDate), prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(SOnDateAvgCount, prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(SUptoDateAvgCount, prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(SOnDateProd, prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(SUptoDateProd, prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(SAvgProdPerDay, prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(STarVsActual, prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(SOnDateUtil, prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(SUptoDateUtil, prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(SOnDateAvgGps, prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(SUptoDateAvgGps, prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(common.getRound(dOnDateUkg, 2), prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(common.getRound(dUptoDateUkg, 2), prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(SOnDateCdgUe, prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(SUptoDateCdgUe, prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(SOnDateDrgUe, prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(SUptoDateDrgUe, prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(SCheeseStock, prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(SOnDateAcProd, prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(SUptoDateAcProd, prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(SAvgAcProdPerDay, prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(common.getRound(dOnDateProdDiff, 2), prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(common.getRound(dUptoDateProdDiff, 2), prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(SOnDatePackedProd, prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(SUptoDatePackedProd, prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(SAvgPackedProdPerDay, prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable("", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(SOnDatePneumafilWaste, prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(SUptoDatePneumafilWaste, prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(SMaintPowerLoss1, prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(SMaintPowerLoss2, prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(STotalLoss, prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable(common.getRound(dProdVsPacked, 2), prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                AddCellTable("", prodtable, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1, 1, 20f, 4, 1, 8, 2, smallBold);
                
                SProdDate = String.valueOf(common.getNextDate(common.toInt(SProdDate)));
                
            }
            out.println("</table>");
            out.println("</body>");
            out.println("</html>");
            document.add(prodtable);
            document.close();
        } catch (Exception ex) {
            System.out.println(" overall production rep body " + ex);
        }
    }

    private double getAvgCalDays(double sproddate) {
        double ileaveday = 0.0;
        for (int i = 0; i < leavedayList.size(); i++) {
            java.util.HashMap hm = (java.util.HashMap) leavedayList.get(i);
            double ileavedate = common.toDouble(((String) hm.get("LEAVEDATE")).substring(6, 8));
            System.out.println(ileavedate+"<--->"+sproddate);
            if (sproddate == ileavedate) {
                ileaveday = common.toDouble((String) hm.get("LEAVEDAY"));
                System.out.println("ileaveday--->" + ileaveday + sproddate + "<--->" + ileavedate);
            }
        }
        System.out.println("sdproddate-ileaveday--->" + (sproddate - ileaveday));
        return sproddate - ileaveday;
    }

    private void setOEDataVector() {
        AOETranList = new ArrayList();
        StringBuffer sb = new StringBuffer();
        sb.append(" select OEtran.sp_date,sum(OETran.Prod) as SumOfProd,Sum(OEtran.count*OEtran.Rotwkd) AS SumOfCount, ");
        sb.append(" Sum(OEtran.RotWkd) AS SumOfRotwkd,");
        sb.append(" Sum(prod-prod_Tar) AS TarVsAct, ((sum(Rotwkd)*100)/sum(decode(noofRot,0,1,noofRot))) AS Utilization,");
        sb.append(" Avg(OEtran.gms_Rot) AS AvgGps,Sum(OEtran.hoursrun) AS SumOfhoursrun, Sum(OEtran.HourMeter) AS SumOfHourMeter,");
        sb.append(" Sum(OEtran.hours) AS AllotHour,sum(decode(OEtran.noofRot,0,1,OEtran.noofRot)) as NoOfRot,Sum(OEtran.gms_Rot)  as SumOfGmsRot,  ");
        sb.append(" Sum(OEtran.pneumafil) AS SumOfpneumafil");
        sb.append(" from OETran");
        sb.append(" INNER JOIN unit ON OEtran.Unit_Code = unit.unitcode ");
        sb.append(" INNER JOIN machine ON OEtran.Mach_Code = machine.mach_code ");
        sb.append(" WHERE OEtran.sp_date>=" + SMonthStDate + " and OEtran.sp_date<=" + SMonthEnDate);
        sb.append(" and OETran.sp_Date not in(Select HDate from ProdHoliday where leaveday = 1) ");
        sb.append(" AND OEtran.unit_code=" + SUnitCode);
        sb.append(" group by OEtran.sp_date ");
        //System.out.println(" Overall "+sb.toString());

        StringBuffer sb1 = new StringBuffer();
        //sb1 . append(" select sum(leaveday) as prodleaveday from prodholiday where substr(hdate,0,6) = 202104");
        //sb1 . append(" select sum(leaveday) as prodleaveday from prodholiday where substr(hdate,0,6) = "+"202104"+"and  hdate >="+"20210401"+" and hdate<=20210424");
        sb1.append(" select hdate,leaveday as prodleaveday from prodholiday where hdate >=" + SMonthStDate + " and hdate<=" + SMonthEnDate);
        System.out.println("OEOverAllProductionRep--->" + sb1.toString());
        try {

            Connection theConnection = createConnection();

            PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
            ResultSet theResult = theStatement.executeQuery();

            while (theResult.next()) {
                String SOESpinDate = theResult.getString(1);
                double dOEProd = theResult.getDouble(2);
                double dCount = theResult.getDouble(3);
                double dRotWkd = theResult.getDouble(4);
                double dTarVsAct = theResult.getDouble(5);
                double dUtil = theResult.getDouble(6);
                double dAvgGps = theResult.getDouble(7);
                double dHoursRun = theResult.getDouble(8);
                double dHoursMeter = theResult.getDouble(9);
                double dAllotHour = theResult.getDouble(10);
                double dNoOfSpdl = theResult.getDouble(11);
                double dGmsSpdl = theResult.getDouble(12);
                double dPneumafilWaste = theResult.getDouble(13);

                OEOverAllProductionClass theProductionClass = new OEOverAllProductionClass();

                theProductionClass.setOESpinDate(SOESpinDate);
                theProductionClass.setOEProd(dOEProd);
                theProductionClass.setOECount(dCount);
                theProductionClass.setOERotWkd(dRotWkd);
                theProductionClass.setOETarVsAct(dTarVsAct);
                theProductionClass.setOEUtil(dUtil);
                theProductionClass.setOEAvgGps(dAvgGps);
                theProductionClass.setOEHoursRun(dHoursRun);
                theProductionClass.setOEHoursMeter(dHoursMeter);
                theProductionClass.setOEAllotHour(dAllotHour);
                theProductionClass.setOENoOfRot(dNoOfSpdl);
                theProductionClass.setOEGmsSpdl(dGmsSpdl);
                theProductionClass.setOEPneumafilWaste(dPneumafilWaste);
                AOETranList.add(theProductionClass);
                //int iIndex          =   
            }

            theResult.close();
            theStatement.close();
            leavedayList = new java.util.ArrayList();

            PreparedStatement theStatement1 = theConnection.prepareStatement(sb1.toString());
            ResultSet theResult1 = theStatement1.executeQuery();
            while (theResult1.next()) {
                java.util.HashMap hm = new java.util.HashMap();
                hm.put("LEAVEDATE", theResult1.getString(1));
                hm.put("LEAVEDAY", theResult1.getString(2));
                leavedayList.add(hm);
            }
            theResult1.close();
            theStatement1.close();
        } catch (Exception ex) {
            System.out.println(" set OEDataVector " + ex);
        }

    }

    private void setPackedProduction() {
        try {
            StringBuffer sb = new StringBuffer();
            sb.append(" select BalePacking.PackDate,sum(BalePacking.NetWeight),ProcessingType.OEStatus from balepacking ");
            sb.append(" inner join regularorder on regularorder.orderid=balepacking.orderid");
            sb.append(" Left Join RMixir ON RMixir.OrdNo=RegularOrder.ROrderNO ");
            sb.append(" Left Join yarnM on yarnm.yshcd=RMixir.YarnShadeCode ");
            sb.append(" Inner join yarncount on yarncount.countcode=regularorder.countcode ");
            sb.append(" inner join unit on unit.unitcode = BalePacking.unitcode ");
            sb.append(" inner join scm.ProcessingType on scm.ProcessingType.ProcessCode = regularorder.ProcessTypeCode ");
            sb.append(" where substr(BalePacking.PackDate,0,6)=" + SProdMonth + " and balepacking.Unitcode=" + SUnitCode);
            sb.append(" and Balepacking.Flag<>1 and Nvl(RMixir.CorrectionMixing,0)=0");
            //sb.append(" and ProcessingType.OEStaus=0");
            sb.append(" group by BalePacking.Packdate,ProcessingType.OEStatus order by BalePacking.PackDate,ProcessingType.OEStatus");

            Class.forName("oracle.jdbc.OracleDriver");
//            Connection theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun", "dispatch", "dispatch");
            Connection   theConnection = DriverManager.getConnection("jdbc:oracle:thin:@117.239.247.18:1530:arun","system","hammer");

            PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
            ResultSet theResult = theStatement.executeQuery();

            while (theResult.next()) {

                String SPackedDate = theResult.getString(1);
                double dPackedProd = theResult.getDouble(2);
                int iOEStatus = theResult.getInt(3);

                int iIndex = getIndexOf(SPackedDate);
                if (iIndex != -1) {
                    OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(iIndex);
                    if (iOEStatus == 0) {
                        theProductionClass.setPackedDate(SPackedDate);
                        theProductionClass.setPackedProd(dPackedProd);
                    } else {
                        theProductionClass.setOEPackedDate(SPackedDate);
                        theProductionClass.setOEPackedProd(dPackedProd);
                    }

                }
            }
            theResult.close();
            theStatement.close();
        } catch (Exception ex) {
            System.out.println(" set Packed Prod " + ex);
        }
    }

    private void setStoppageData() {
        AStoppageList = new ArrayList();
        AMachineList = new ArrayList();
        try {

            Connection theConnection = createConnection();

            PreparedStatement theStatement = null;
            ResultSet theResult = null;

            // for stoppage details            
            StringBuffer sb = new StringBuffer();
            sb.append(" Select t.Stop_Date,StopReasons.stop_Code,t.StopMint,t.Shift,t.Mach_Code from StopReasons ");
            sb.append(" left join (select Stop_Shift as Shift,stopTran.stop_Code as Stop_Code,sum(Stop_Mint) as StopMint,");
            sb.append(" StopTran.Stop_Date,StopTran.Mach_code from StopTran ");
            sb.append(" where StopTran.Dept_code=" + SDeptCode + " and substr(Stop_date,0,6)=" + SProdMonth);
            sb.append(" group by StopTran.Stop_Code,stop_Shift,StopTran.Stop_Date,StopTran.Mach_code) t on StopReasons.stop_Code=t.stop_Code ");
            sb.append(" Inner join stoppage on Stoppage.stop_code = stopreasons.STOP_CODE and stoppage.dept_code=" + SDeptCode);
            sb.append(" order by 1 ");

            System.out.println(" stoppage qs " + sb.toString());

            theStatement = theConnection.prepareStatement(sb.toString());
            theResult = theStatement.executeQuery();

            while (theResult.next()) {
                String SStopDate = theResult.getString(1);
                String SStopCode = theResult.getString(2);
                String SStopMint = theResult.getString(3);
                String SStopShift = theResult.getString(4);
                String SMachCode = theResult.getString(5);

                HashMap theMap = new HashMap();
                theMap.put("StopDate", SStopDate);
                theMap.put("StopCode", SStopCode);
                theMap.put("StopMint", SStopMint);
                theMap.put("StopShift", SStopShift);
                theMap.put("StopMachCode", SMachCode);
                AStoppageList.add(theMap);
            }
            theResult.close();
            // For Machine
            sb = new StringBuffer();
            sb.append(" SELECT machine.mach_code, machine.mach_name,machine.NoOfRotters FROM machine  ");
            sb.append(" WHERE machine.dept_code=" + SDeptCode + " and machine.unit_code=" + SUnitCode + " and Machine.Alive_Status=0 and Machine.NoOfRotters>0 ");
            sb.append(" ORDER BY to_number(machine.mach_name) ");

            System.out.println("stop mach qs " + sb.toString());

            theStatement = theConnection.prepareStatement(sb.toString());
            theResult = theStatement.executeQuery();

            while (theResult.next()) {
                String SMachCode = theResult.getString(1);
                String SMachName = theResult.getString(2);
                String SNoOfRotters = theResult.getString(3);

                HashMap theMap = new HashMap();
                theMap.put("MachCode", SMachCode);
                theMap.put("MachName", SMachName);
                theMap.put("NoOfRotters", SNoOfRotters);
                AMachineList.add(theMap);
            }
            theResult.close();
            theStatement.close();

        } catch (Exception ex) {
            System.out.println(" setStoppage Data " + ex);
        }
    }

    private void setUkgData() {
        AEBList = new ArrayList();
        AGenList = new ArrayList();
        ACompList = new ArrayList();
        AWebList = new ArrayList();

        try {
            StringBuffer sb = new StringBuffer();

            Class.forName("oracle.jdbc.OracleDriver");
//            Connection theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun", "maintenance", "electric");
            Connection   theConnection = DriverManager.getConnection("jdbc:oracle:thin:@117.239.247.18:1530:arun","maintenance","electric");

            PreparedStatement theStatement = null;
            ResultSet theResult = null;

            sb.append(" SELECT sum(TNEBDetails.Units) as EBUnits,sum(TNEBDetails.MixingHallUnits) as MixingHallUnits,TNEBDetails.EBDate ");
            sb.append(" FROM TNEBDetails ");
            sb.append(" WHERE TNEBDetails.UnitCode=" + SUnitCode + " And substr(TNEBDetails.EBDate,0,6)=" + SProdMonth);
            sb.append(" group by TNEBDetails.EBDate ");

            theStatement = theConnection.prepareStatement(sb.toString());
            theResult = theStatement.executeQuery();

            while (theResult.next()) {
                HashMap theMap = new HashMap();
                theMap.put("EBUnit", theResult.getString(1));
                theMap.put("MixingHallUnits", theResult.getString(2));
                theMap.put("EBDate", theResult.getString(3));
                AEBList.add(theMap);
            }
            theResult.close();

            sb = new StringBuffer();
            sb.append(" SELECT sum(CompressorDetails.Debit) as CompUnits,CompressorDetails.TrnDate From CompressorDetails ");
            sb.append(" WHERE CompressorDetails.UnitCode=" + SUnitCode);
            sb.append(" And substr(CompressorDetails.TrnDate,0,6)=" + SProdMonth + " and CompressorDetails.ServUnitCode=0");
            sb.append(" And CompressorDetails.machinecode=762");
            sb.append(" group by CompressorDetails.TrnDate");

            theStatement = theConnection.prepareStatement(sb.toString());
            theResult = theStatement.executeQuery();

            while (theResult.next()) {
                HashMap theMap = new HashMap();
                theMap.put("CompUnit", theResult.getString(1));
                theMap.put("CompDate", theResult.getString(2));
                ACompList.add(theMap);
            }
            theResult.close();

            sb = new StringBuffer();
            sb.append(" SELECT sum(GeneratorDetails.GenUnit) as GenUnits,GeneratorDetails.GenDate ");
            sb.append(" FROM GeneratorDetails INNER JOIN ServiceMachine ON GeneratorDetails.MachineCode = ServiceMachine.MachineCode ");
            sb.append(" WHERE GeneratorDetails.UnitCode=" + SUnitCode + " And substr(GeneratorDetails.GenDate,0,6)=" + SProdMonth + " And GeneratorDetails.ServUnitCode=0");
            sb.append(" group by GeneratorDetails.GenDate");

            theStatement = theConnection.prepareStatement(sb.toString());
            theResult = theStatement.executeQuery();

            while (theResult.next()) {
                HashMap theMap = new HashMap();
                theMap.put("GenUnit", theResult.getString(1));
                theMap.put("GenDate", theResult.getString(2));
                AGenList.add(theMap);
            }
            theResult.close();

            sb = new StringBuffer();
            sb.append(" select sum(POCWebUnits+CardedWebUnits) as WebUnits,TrnDate from (");
            sb.append(" SELECT CompressorDetails.TrnDate,sum(CompressorDetails.Debit)  as POCWebUnits,0 as CardedWebUnits From CompressorDetails ");
            sb.append(" WHERE CompressorDetails.UnitCode=" + SUnitCode);
            sb.append(" And substr(CompressorDetails.TrnDate,0,6)=" + SProdMonth + "  and CompressorDetails.ServUnitCode=0");
            sb.append(" And CompressorDetails.machinecode=3875");
            sb.append(" group  by CompressorDetails.TrnDate");
            sb.append(" Union All");
            sb.append(" SELECT CompressorDetails.TrnDate,0 as PocWebUnits,sum(CompressorDetails.Debit) as  CardedWebUnits From CompressorDetails ");
            sb.append(" WHERE CompressorDetails.UnitCode=" + SUnitCode);
            sb.append(" And substr(CompressorDetails.TrnDate,0,6)=" + SProdMonth + " and CompressorDetails.ServUnitCode=0");
            sb.append(" And CompressorDetails.machinecode=2424");
            sb.append(" group by CompressorDetails.TrnDate");
            sb.append(" )");
            sb.append(" group by TrnDate order by TrnDate            ");

            theStatement = theConnection.prepareStatement(sb.toString());
            theResult = theStatement.executeQuery();

            while (theResult.next()) {
                HashMap theMap = new HashMap();
                theMap.put("WebUnits", theResult.getString(1));
                theMap.put("TrnDate", theResult.getString(2));
                AWebList.add(theMap);
            }
            theResult.close();

            theStatement.close();
        } catch (Exception ex) {
            System.out.println("set Ukg Data " + ex);
            ex.printStackTrace();
        }
    }

    private void setDrgData() {
        try {
            StringBuffer sb = new StringBuffer();
            sb.append(" select DrawTran.Sp_Date,avg(UEOA) as DRFUE from DrawTran");
            sb.append(" INNER JOIN unit ON DrawTran.unit_code = unit.unitcode");
            sb.append(" INNER JOIN machine ON DrawTran.mach_code = machine.mach_code ");
            sb.append(" INNER JOIN count ON DrawTran.count_Code = count.count_code  ");
            sb.append(" Left JOIN SHADE ON DrawTran.shade_code = SHADE.shade_code   ");
            sb.append(" Where substr(DrawTran.sp_date,0,6)=" + SProdMonth);
            sb.append(" and DrawTran.Unit_Code = " + SUnitCode + "  group by DrawTran.Sp_Date	order by DrawTran.SP_Date");

            Connection theConnection = createConnection();

            PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
            ResultSet theResult = theStatement.executeQuery();

            while (theResult.next()) {
                String SDrgSpinDate = theResult.getString(1);
                double dDrgUE = theResult.getDouble(2);
                int iIndex = getIndexOf(SDrgSpinDate);
                if (iIndex != -1) {
                    OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(iIndex);
                    theProductionClass.setDrgSpDate(SDrgSpinDate);
                    theProductionClass.setDrgUe(dDrgUE);
                }
            }
            theResult.close();

            theStatement = theConnection.prepareStatement("select StockDate,sum(NoOfCheese) as CheeseStock from OECheeseStock where substr(StockDate,0,6)=" + SProdMonth + " group by StockDate");
            theResult = theStatement.executeQuery();
            while (theResult.next()) {
                String SCheeseStockDate = theResult.getString(1);
                double dCheeseStock = theResult.getDouble(2);
                int iIndex = getIndexOf(SCheeseStockDate);
                if (iIndex != -1) {
                    OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(iIndex);
                    theProductionClass.setSCheeseStockDate(SCheeseStockDate);
                    theProductionClass.setdCheeseStock(dCheeseStock);
                }

            }
            theResult.close();
            theStatement.close();
        } catch (Exception ex) {
            System.out.println(" setDrgData " + ex);
        }
    }

    private void setCdgData() {
        try {
            StringBuffer sb = new StringBuffer();
            sb.append(" select CardTran.Sp_Date,avg(UEOA) as CdgUE from CardTran");
            sb.append(" INNER JOIN unit ON CardTran.unit_code = unit.unitcode");
            sb.append(" INNER JOIN machine ON CardTran.mach_code = machine.mach_code ");
            sb.append(" INNER JOIN count ON CardTran.count_Code = count.count_code  ");
            sb.append(" Left JOIN SHADE ON CardTran.shade_code = SHADE.shade_code   ");
            sb.append(" Where substr(CardTran.sp_date,0,6)=" + SProdMonth);
            sb.append(" and CardTran.Unit_Code = " + SUnitCode + "  group by CardTran.Sp_Date	order by CardTran.SP_Date");

            Connection theConnection = createConnection();

            PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
            ResultSet theResult = theStatement.executeQuery();

            while (theResult.next()) {
                String SCdgSpinDate = theResult.getString(1);
                double dCdgUE = theResult.getDouble(2);
                int iIndex = getIndexOf(SCdgSpinDate);
                if (iIndex != -1) {
                    OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(iIndex);
                    theProductionClass.setCdgSpDate(SCdgSpinDate);
                    theProductionClass.setCdgUe(dCdgUE);
                }
            }
            theResult.close();
            theStatement.close();
        } catch (Exception ex) {
            System.out.println(" setCdgData " + ex);
        }
    }

    private void setConeWindingProduction() {
        StringBuffer sb = new StringBuffer();
        //(sum(Waste)*100/sum(Production)) as hardwaste
        sb.append(" select ProdDate,sum(Production) as Production,(sum(Waste)*100/sum(Production)) as hardwaste from ConeWindingProductionDetails");
        sb.append(" where substr(ConeWindingProductionDetails.ProdDate,0,6)=" + SProdMonth);
        sb.append(" and ConeWindingProductionDetails.SpinOECode=2");
        sb.append(" group by ProdDate order by ProdDate");
        try {
            Connection theConnection = createConnection();

            PreparedStatement theStatement = theConnection.prepareStatement(sb.toString());
            ResultSet theResult = theStatement.executeQuery();

            while (theResult.next()) {
                String SAcProdDate = theResult.getString(1);
                double dAcProduction = theResult.getDouble(2);
                double dAcHardWaste = theResult.getDouble(3);

                int iIndex = getIndexOf(SAcProdDate);
                if (iIndex != -1) {
                    OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(iIndex);
                    theProductionClass.setSAcProdDate(SAcProdDate);
                    theProductionClass.setdAcProduction(dAcProduction);
                    theProductionClass.setdAcHardWaste(dAcHardWaste);
                }
            }
            theResult.close();
            theStatement.close();

        } catch (Exception ex) {
            System.out.println(" setConeWindingProduction " + ex);
        }
    }

    private int getIndexOf(String SProdDate) {
        int iIndex = -1;
        for (int i = 0; i < AOETranList.size(); i++) {
            OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(i);
            if (theProductionClass.OESpinDate.equals(SProdDate)) {
                iIndex = i;
                break;
            }
        }
        return iIndex;
    }

    private String getOnDateAvgCount(String SProdDate) {
        double dValue = 0.0;
        double dSumOfRotWkd = 0.0, dSumOfCount = 0.0;
        for (int i = 0; i < AOETranList.size(); i++) {
            OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(i);
            if (SProdDate.equals(theProductionClass.OESpinDate)) {
                dSumOfRotWkd += theProductionClass.OERotWkd;
                dSumOfCount += theProductionClass.OECount;
            }
        }
        dValue = dSumOfCount / dSumOfRotWkd;
        return common.getRound(dValue, 2);
    }

    private String getUptoDateAvgCount(String SProdDate) {
        double dValue = 0.0;
        double dSumOfRotWkd = 0.0, dSumOfCount = 0.0;
        for (int i = 0; i < AOETranList.size(); i++) {
            OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(i);
            if (common.toInt(theProductionClass.OESpinDate) <= common.toInt(SProdDate)) {
                dSumOfRotWkd += theProductionClass.OERotWkd;
                dSumOfCount += theProductionClass.OECount;
            }
        }
        dValue = dSumOfCount / dSumOfRotWkd;
        return common.getRound(dValue, 2);
    }

    private String getOnDateProd(String SProdDate) {
        double dValue = 0.0;
        double dSumOfProd = 0.0;
        for (int i = 0; i < AOETranList.size(); i++) {
            OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(i);
            if (SProdDate.equals(theProductionClass.OESpinDate)) {
                dSumOfProd += theProductionClass.OEProd;
            }
        }
        dValue = dSumOfProd;
        return common.getRound(dValue, 2);
    }

    private String getUptoDateProd(String SProdDate) {
        double dValue = 0.0;
        for (int i = 0; i < AOETranList.size(); i++) {
            OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(i);
            if (common.toInt((theProductionClass.OESpinDate)) <= common.toInt(SProdDate)) {
                dValue += theProductionClass.OEProd;
            }
        }

        return common.getRound(dValue, 2);
    }

    private String getUptoDateRotWkd(String SProdDate) {
        double dValue = 0.0;
        for (int i = 0; i < AOETranList.size(); i++) {
            OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(i);
            if (common.toInt((theProductionClass.OESpinDate)) <= common.toInt(SProdDate)) {
                dValue += theProductionClass.OERotWkd;
            }
        }

        return common.getRound(dValue, 2);
    }

    private String getAvgProdPerDay(String SProdDate) {
        double dValue = 0.0;
        double sproddate = common.toInt(SProdDate.substring(6, 8));
        double iavgcalday = getAvgCalDays(sproddate);
        System.out.println("iavgcalday--->"+iavgcalday);
        dValue = common.toDouble(getUptoDateProd(SProdDate)) / iavgcalday;
        return common.getRound(dValue, 2);
    }

    private String getTargetVsActual(String SProdDate) {
        double dValue = 0.0;
        for (int i = 0; i < AOETranList.size(); i++) {
            OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(i);
            if (SProdDate.equals(theProductionClass.OESpinDate)) {
                dValue += theProductionClass.OETarVsAct;
            }
        }
        return common.getRound(dValue, 2);
    }

    private String getOnDateUtil(String SProdDate) {
        double dValue = 0.0;
        int iCount = 0;
        for (int i = 0; i < AOETranList.size(); i++) {
            OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(i);
            if (SProdDate.equals(theProductionClass.OESpinDate)) {
                iCount++;
                dValue += theProductionClass.OEUtil;
            }
        }
        dValue = dValue / iCount;
        return common.getRound(dValue, 2);
    }

    private String getUptoDateUtil(String SProdDate) {
        double dValue = 0.0;
        double dSumOfRotWkd = 0.0, dSumNoOfRot = 0.0;
        for (int i = 0; i < AOETranList.size(); i++) {
            OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(i);
            if (common.toInt((theProductionClass.OESpinDate)) <= common.toInt(SProdDate)) {
                dSumOfRotWkd += theProductionClass.OERotWkd;
                dSumNoOfRot += theProductionClass.OENoOfRot;
            }

        }
        dValue = (dSumOfRotWkd * 100) / dSumNoOfRot;
        return common.getRound(dValue, 2);
    }

    private String getOnDateAvgGps(String SProdDate) {
        double dValue = 0.0;
        for (int i = 0; i < AOETranList.size(); i++) {
            OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(i);
            if (SProdDate.equals(theProductionClass.OESpinDate)) {
                dValue += theProductionClass.OEAvgGps;
            }
        }
        return common.getRound(dValue, 2);
    }

    private String getUptoDateAvgGps(String SProdDate) {
        double dValue = 0.0;
        dValue = common.toDouble(getUptoDateProd(SProdDate)) * 1000 / common.toDouble(getUptoDateRotWkd(SProdDate));
        return common.getRound(dValue, 2);
    }

    private String getOnDateAcProd(String SProdDate) {
        double dValue = 0.0;
        for (int i = 0; i < AOETranList.size(); i++) {
            OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(i);
            if (SProdDate.equals(theProductionClass.SAcProdDate)) {
                dValue = theProductionClass.getdAcProduction();
                break;
            }
        }
        return common.getRound(dValue, 2);
    }

    private String getUptoDateAcProd(String SProdDate) {
        double dValue = 0.0;
        int iCount = 0;
        for (int i = 0; i < AOETranList.size(); i++) {
            OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(i);
            if (common.toInt((theProductionClass.SAcProdDate)) <= common.toInt(SProdDate)) {
                dValue += theProductionClass.getdAcProduction();
                iCount++;
            }
        }
        dValue = dValue / iCount;
        return common.getRound(dValue, 2);
    }

    private String getAvgAcProdPerDay(String SProdDate) {
        double dValue = 0.0;
        double sproddate = common.toDouble(SProdDate.substring(6, 8));
        double iavgcalday = getAvgCalDays(sproddate);
        dValue = common.toDouble(getUptoDateAcProd(SProdDate)) / iavgcalday;
        return common.getRound(dValue, 2);
    }

    private String getAcHardWaste(String SProdDate) {
        double dValue = 0;
        for (int i = 0; i < AOETranList.size(); i++) {
            OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(i);
            if (SProdDate.equals(theProductionClass.SAcProdDate)) {
                dValue = theProductionClass.getdAcHardWaste();
                break;
            }
        }
        return common.getRound(dValue, 2);
    }

    private String getOnDatePackedProd(String SProdDate) {
        double dValue = 0.0;
        for (int i = 0; i < AOETranList.size(); i++) {
            OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(i);
            if (theProductionClass.getOEStatus() != 0) {
                continue;
            }
            if (!SProdDate.equals(theProductionClass.OESpinDate)) {
                continue;
            }

            dValue += theProductionClass.dOEPackedProd;

        }
        return common.getRound(dValue, 2);
    }

    private String getUptoDatePackedProd(String SProdDate) {
        double dValue = 0.0;
        for (int i = 0; i < AOETranList.size(); i++) {
            OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(i);
            if (theProductionClass.getOEStatus() != 0) {
                continue;
            }
            if (common.toInt((theProductionClass.OESpinDate)) <= common.toInt(SProdDate)) {
                dValue += theProductionClass.dOEPackedProd;
            }
        }

        return common.getRound(dValue, 2);
    }

    private String getAvgPackedProdPerDay(String SProdDate) {
        double dValue = 0.0;
        
        double sproddate = common.toDouble(SProdDate.substring(6, 8));
        double iavgcalday = getAvgCalDays(sproddate);
        dValue = common.toDouble(getUptoDatePackedProd(SProdDate)) / iavgcalday;
        return common.getRound(dValue, 2);
    }

    private String getOnDatePneumafilWaste(String SProdDate) {
        double dValue = 0.0, dPneumafilWaste = 0.0, dSumOfProd = 0.0;
        for (int i = 0; i < AOETranList.size(); i++) {
            OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(i);
            if (theProductionClass.getOEStatus() != 0) {
                continue;
            }
            if (!SProdDate.equals(theProductionClass.OESpinDate)) {
                continue;
            }

            dPneumafilWaste += theProductionClass.OEPneumafilWaste;
            dSumOfProd += theProductionClass.OEProd;

        }
        dValue = dPneumafilWaste * 100 / (dPneumafilWaste + dSumOfProd);
        return common.getRound(dValue, 2);
    }

    private String getUptoDatePneumafilWaste(String SProdDate) {
        double dValue = 0.0, dPneumafilWaste = 0.0, dSumOfProd = 0.0;
        for (int i = 0; i < AOETranList.size(); i++) {
            OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(i);
            if (theProductionClass.getOEStatus() != 0) {
                continue;
            }
            if (common.toInt((theProductionClass.OESpinDate)) <= common.toInt(SProdDate)) {
                dPneumafilWaste += theProductionClass.OEPneumafilWaste;
                dSumOfProd += theProductionClass.OEProd;
            }

        }
        dValue = dPneumafilWaste * 100 / (dPneumafilWaste + dSumOfProd);
        return common.getRound(dValue, 2);
    }

    private String getMaintPowerLoss(String SProdDate, String SMaintPowerCode) {
        double dValue = 0.0;
        double dNoOfRot = 0.0, dStopMint = 0.0;
        dNoOfRot = getNoOfRot();
        for (int i = 0; i < AStoppageList.size(); i++) {
            HashMap theStopMap = (HashMap) AStoppageList.get(i);
            String SStopCode = (String) theStopMap.get("StopCode");
            String SStopDate = (String) theStopMap.get("StopDate");
            String SStopMachCode = (String) theStopMap.get("StopMachCode");
            if (!SProdDate.equals(SStopDate)) {
                continue;
            }
            if (!SStopCode.equals(SMaintPowerCode)) {
                continue;
            }

            double dMachRot = getNoOfRot(SStopMachCode) / 480;
            dStopMint += common.toDouble(common.getRound(dMachRot * common.toDouble((String) theStopMap.get("StopMint")), 0));
        }

        dValue = dStopMint * 100 / (3 * dNoOfRot);

        return common.getRound(dValue, 3);
    }

    private String getTotalLoss(String SProdDate) {
        double dValue = 0.0;
        double dNoOfRot = 0.0, dStopMint = 0.0;
        dNoOfRot = getNoOfRot();
        for (int i = 0; i < AStoppageList.size(); i++) {
            HashMap theStopMap = (HashMap) AStoppageList.get(i);
            String SStopCode = (String) theStopMap.get("StopCode");
            String SStopDate = (String) theStopMap.get("StopDate");
            String SStopMachCode = (String) theStopMap.get("StopMachCode");
            if (!SProdDate.equals(SStopDate)) {
                continue;
            }

            double dMachRot = getNoOfRot(SStopMachCode) / 480;
            dStopMint += common.toDouble(common.getRound(dMachRot * common.toDouble((String) theStopMap.get("StopMint")), 0));
        }

        dValue = dStopMint * 100 / (3 * dNoOfRot);

        return common.getRound(dValue, 3);
    }

    private double getNoOfRot() {
        double dValue = 0.0;
        for (int i = 0; i < AMachineList.size(); i++) {
            HashMap theMap = (HashMap) AMachineList.get(i);
            dValue += common.toDouble(String.valueOf(theMap.get("NoOfRotters")));
        }
        return dValue;
    }

    private double getNoOfRot(String SMachCode) {
        double dValue = 0.0;
        for (int i = 0; i < AMachineList.size(); i++) {
            HashMap theMap = (HashMap) AMachineList.get(i);
            if (((String) theMap.get("MachCode")).equals(SMachCode)) {
                dValue += common.toDouble(String.valueOf(theMap.get("NoOfRotters")));
            }
        }
        return dValue;
    }

    private double getEBUnits(String SProdDate) {
        double dValue = 0.0;
        for (int i = 0; i < AEBList.size(); i++) {
            HashMap theMap = (HashMap) AEBList.get(i);
            String SEBDate = (String) theMap.get("EBDate");
            if (SProdDate.equals(SEBDate)) {
                dValue = common.toDouble((String) theMap.get("EBUnit"));
                break;
            }
        }
        return dValue;
    }

    private double getMixingHallUnits(String SProdDate) {
        double dValue = 0.0;
        for (int i = 0; i < AEBList.size(); i++) {
            HashMap theMap = (HashMap) AEBList.get(i);
            String SEBDate = (String) theMap.get("EBDate");
            if (SProdDate.equals(SEBDate)) {
                dValue = common.toDouble((String) theMap.get("MixingHallUnits"));
                break;
            }
        }
        return dValue;
    }

    private double getGensetUnits(String SProdDate) {
        double dValue = 0.0;
        for (int i = 0; i < AGenList.size(); i++) {
            HashMap theMap = (HashMap) AGenList.get(i);
            String SGenDate = (String) theMap.get("GenDate");
            if (SProdDate.equals(SGenDate)) {
                dValue = common.toDouble((String) theMap.get("GenUnit"));
                break;
            }
        }
        return dValue;
    }

    private double getCompressorUnits(String SProdDate) {
        double dValue = 0.0;
        for (int i = 0; i < ACompList.size(); i++) {
            HashMap theMap = (HashMap) ACompList.get(i);
            String SCompDate = (String) theMap.get("CompDate");
            if (SProdDate.equals(SCompDate)) {
                dValue = common.toDouble((String) theMap.get("CompUnit"));
                break;
            }
        }
        return dValue;
    }

    private double getWebUnits(String SProdDate) {
        double dValue = 0.0;
        for (int i = 0; i < AWebList.size(); i++) {
            HashMap theMap = (HashMap) AWebList.get(i);
            String STrnDate = (String) theMap.get("TrnDate");
            if (SProdDate.equals(STrnDate)) {
                dValue = common.toDouble((String) theMap.get("WebUnits"));
                break;
            }
        }
        return dValue;
    }

    private double getUptoDateEBUnits(String SProdDate) {
        double dValue = 0.0;
        for (int i = 0; i < AEBList.size(); i++) {
            HashMap theMap = (HashMap) AEBList.get(i);
            String SEBDate = (String) theMap.get("EBDate");
            if (common.toInt(SEBDate) <= common.toInt(SProdDate)) {
                dValue += common.toDouble((String) theMap.get("EBUnit"));
            }
        }
        return dValue;
    }

    private double getUptoDateMixingHallUnits(String SProdDate) {
        double dValue = 0.0;
        for (int i = 0; i < AEBList.size(); i++) {
            HashMap theMap = (HashMap) AEBList.get(i);
            String SEBDate = (String) theMap.get("EBDate");
            if (common.toInt(SEBDate) <= common.toInt(SProdDate)) {
                dValue += common.toDouble((String) theMap.get("MixingHallUnits"));
            }
        }
        return dValue;
    }

    private double getUptoDateGensetUnits(String SProdDate) {
        double dValue = 0.0;
        for (int i = 0; i < AGenList.size(); i++) {
            HashMap theMap = (HashMap) AGenList.get(i);
            String SGenDate = (String) theMap.get("GenDate");
            if (common.toInt(SGenDate) <= common.toInt(SProdDate)) {
                dValue += common.toDouble((String) theMap.get("GenUnit"));
            }
        }
        return dValue;
    }

    private double getUptoDateCompressorUnits(String SProdDate) {
        double dValue = 0.0;
        for (int i = 0; i < ACompList.size(); i++) {
            HashMap theMap = (HashMap) ACompList.get(i);
            String SCompDate = (String) theMap.get("CompDate");
            if (common.toInt(SCompDate) <= common.toInt(SProdDate)) {
                dValue += common.toDouble((String) theMap.get("CompUnit"));
            }
        }
        return dValue;
    }

    private double getUptoDateWebUnits(String SProdDate) {
        double dValue = 0.0;
        for (int i = 0; i < AWebList.size(); i++) {
            HashMap theMap = (HashMap) AWebList.get(i);
            String STrnDate = (String) theMap.get("TrnDate");
            if (common.toInt(STrnDate) <= common.toInt(SProdDate)) {
                dValue += common.toDouble((String) theMap.get("WebUnits"));
            }
        }
        return dValue;
    }

    private String getOnDateCdgUe(String SProdDate) {
        double dValue = 0.0;
        for (int i = 0; i < AOETranList.size(); i++) {
            OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(i);
            if (SProdDate.equals(theProductionClass.OESpinDate)) {
                dValue = theProductionClass.getCdgUe();
                break;
            }
        }
        return common.getRound(dValue, 2);
    }

    private String getUptoDateCdgUe(String SProdDate) {
        double dValue = 0.0;
        int iCount = 0;
        for (int i = 0; i < AOETranList.size(); i++) {
            OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(i);
            if (common.toInt((theProductionClass.OESpinDate)) <= common.toInt(SProdDate)) {
                dValue += theProductionClass.getCdgUe();
                iCount++;
            }
        }
        dValue = dValue / iCount;
        return common.getRound(dValue, 2);
    }

    private String getOnDateDrgUe(String SProdDate) {
        double dValue = 0.0;
        for (int i = 0; i < AOETranList.size(); i++) {
            OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(i);
            if (SProdDate.equals(theProductionClass.OESpinDate)) {
                dValue = theProductionClass.getDrgUe();
                break;
            }
        }
        return common.getRound(dValue, 2);
    }

    private String getUptoDateDrgUe(String SProdDate) {
        double dValue = 0.0;
        int iCount = 0;
        for (int i = 0; i < AOETranList.size(); i++) {
            OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(i);
            if (common.toInt((theProductionClass.OESpinDate)) <= common.toInt(SProdDate)) {
                dValue += theProductionClass.getDrgUe();
                iCount++;
            }
        }
        dValue = dValue / iCount;
        return common.getRound(dValue, 2);
    }

    private String getCheeseStock(String SProdDate) {
        double dValue = 0.0;
        for (int i = 0; i < AOETranList.size(); i++) {
            OEOverAllProductionClass theProductionClass = (OEOverAllProductionClass) AOETranList.get(i);
            if (SProdDate.equals(theProductionClass.SCheeseStockDate)) {
                dValue = theProductionClass.getdCheeseStock();
                break;
            }

        }
        return common.getRound(dValue, 2);
    }

    // For Pdf Creation
    public void initPdf() {
        try {

            document = new Document(PageSize.LEGAL.rotate());
//                 String sFileName        = "OEOverAllProductionPDF1.pdf";
//                   sFileName        = common.getPrintPath()+sFileName;
//                   PdfWriter.getInstance(document, new FileOutputStream(sFileName));
            PdfWriter.getInstance(document, new FileOutputStream(SFileWritePath));
//            document = new Document(PageSize.A3.rotate());
            document.open();
            document.newPage();

            prodtable = new PdfPTable(34);
            prodtable.setWidthPercentage(100);
            prodtable.setWidths(iProdWidth);
            prodtable.setSpacingAfter(5f);
            prodtable.setHeaderRows(4);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * ------------------------ Function for Adding Cell into PDF Table
     * ------------------
     */

    private void AddCellTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, com.itextpdf.text.Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }
// called in this rep

    private void AddCellTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iRowSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, com.itextpdf.text.Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    private void AddCellTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        //c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    private void AddCellTable(double dValue, int iRound, int iRad, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder) {
        Double DValue = new Double(dValue);
        String Str;
        if (DValue.isNaN() || dValue == 0) {
            Str = "";
        } else {
            Str = (common.Rad(common.getRound(dValue, iRound), iRad));
        }

        PdfPCell c1 = new PdfPCell(new Phrase(Str));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);

    }

    private void AddCellTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, com.itextpdf.text.Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    private void AddCellTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, com.itextpdf.text.Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
