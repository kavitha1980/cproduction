/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CProduction;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.DriverManager;
import java.util.Date;
import java.util.Vector;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author root
 */
public class MonthlyReport extends HttpServlet 
{
     HttpSession    session;
     Control        control;
     Common         common;
     Vector         VSCode,VSName,VSPass;
     String         SServer;
     String         bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody;
     RepVect repvect;
     
     String SPassword = "";
      Vector VMonthName, VMonthCode, VFYearCode,VTYearCode;
     
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
    {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        
        setMonth();
        setYear();
        setTYear();
        
        try 
        {
            
            if(session.getAttribute("Password")!=null)
            {
                SPassword = session.getAttribute("Password").toString();
            }
            repvect = new RepVect();
            String id = request.getParameter("id");
            String dept = "";
            String STitle = "";
            System.out.println("id:"+id);
            if(id.trim().equals("1")){
                SServer ="OEOverAllProductionRep";
                dept    = "OE";
                STitle  = "C UNIT OE PRODUCTION DETAILS ";
            }

        out.println("<html><head><title>AMARJOTHI SPINNING MILLS LIMITED</title>");
        out.println("<link rel='stylesheet' type='text/css' href='css/header.css' />");
        out.println("<script src='css/stuHover.js' type='text/javascript'></script>");
        out.println("</head><body bgcolor='white' text='"+ fgBody+  " '>");
          
        RequestDispatcher  dispatch = request.getRequestDispatcher("/Header");
                           dispatch . include(request, response);
            
          out.println("<form accept-charset='UTF-8' name=flogin method='POST' action='"+SServer+"'>");
          out.println("<center>");
	  out.println("<p><FONT face=Verdana size=3 color=black>");
	  out.println("<STRONG>Amar Jothi Spinning Mills Ltd</STRONG></FONT></P>");
	  out.println("</center><center>");
	  out.println("<P><STRONG><FONT face=Verdana size=5 color=navy>");
	  //out.println(" "+dept+" Monthly Report ");
          out.println("<b> "+STitle+" </b></FONT></STRONG>");
	  out.println("</center><br><br><center>");
	  out.println("</CENTER><br><br>");

	   out.println("<center>");
          out.println("<font face=Verdana size='3' color='"+fgBody+"'><b>Unit</b>&nbsp;&nbsp;&nbsp;&nbsp;</font></td>");
          out.println(" <select size='1' name='Unit' style='HEIGHT: 22px; WIDTH: 110px' > ");
          
          for(int i=0;i<repvect.VUnitCode.size();i++)
          out.println("<option value="+(String)repvect.VUnitCode.elementAt(i)+">"+(String)repvect.VUnit.elementAt(i)+"</option>");
          out.println(" </select>");
          out.println("</center><br>");
	                
          out.println("  <center>");
            out.println("<div align='center'>");
            out.println("  <table border='1' width='600' height='14'>");
          
            out.println("    <td  height='10'><font color='"+fgBody+"'><b>From Month</b></td>");
            out.println("    <td> <Select size='1' name='FromMonth'>");
            System.out.println("VMonthCode.size()"+VMonthCode.size());
            for(int i=0;i<VMonthCode.size();i++)
            {
                String SName   = (String)VMonthName.elementAt(i);
                String SCode   = (String)VMonthCode.elementAt(i);
                out.println("     <Option value='"+(String)VMonthCode.elementAt(i)+"'>"+(String)VMonthName.elementAt(i)+"</option>");
            }
            out.println("    </select></td>");
                
            out.println("    <td  height='10'><font color='"+fgBody+"'><b>From Year</b></td>");
            out.println("    <td> <Select size='1' name='FromYear'>");
            for(int i=0;i<VFYearCode.size();i++)
            {
                out.println("<option value='"+(String)VFYearCode.elementAt(i)+"'>"+(String)VFYearCode.elementAt(i)+"</option>");
            }      
            out.println("    </select></td>");
                        
            out.println("  </table>");
            out.println("</div>");
            out.println("  <h1 font color='"+fgBody+"' ><input type='submit' value='Submit' name='B1' color:#800080' ></font><input type='reset'   value='Reset'  name='B2' color:#800080'></h1>");
            out.println("  </center>");
            
          //out.println("<input type='submit' value='Submit' name='B1' style='font-family: Book Antiqua; font-size: 10pt;  text-transform: uppercase; font-weight: bold;' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
          out.println("</form></body></html>");
          out.close();

        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
     private String getCurrentDate()
	{
		String SDate;		
		Date dt = new Date();
		int iDay   = dt.getDate();
		int iMonth = dt.getMonth()+1;
		int iYear  = dt.getYear()+1900;
		if(iDay < 10)
		 SDate = "0"+iDay;
		else
		 SDate = ""+iDay;
		if(iMonth < 10)
		 SDate = SDate + "." + "0"+iMonth;
		else
		 SDate = SDate + "." + iMonth;
		SDate = SDate + "." + iYear;
		return SDate;
	}
     
     
      public void setMonth()
    {
        try
        {
            VMonthName                  	= new Vector();
            VMonthCode                  	= new Vector();
            StringBuffer sb     		= new StringBuffer();

            sb.append(" Select ActualCode,Name from SCM.MonthNames Order by 1");

            Class.forName("oracle.jdbc.OracleDriver");
            Connection   theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","system","hammer");
//            Connection   theConnection = DriverManager.getConnection("jdbc:oracle:thin:@117.239.247.18:1530:arun","system","hammer");

            Statement theStatement    = theConnection.createStatement();
            ResultSet rst 		  = theStatement.executeQuery(sb.toString());

            while (rst.next())
            {
                VMonthCode              . add(rst.getString(1));
                VMonthName              . add(rst.getString(2));
            }
            rst.close();
            rst=null;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.out.println(e);
        }
    }
    public  String getMonthCode(String SMonthName)
    {
        String SMonthCode="";
        try
        {
            SMonthCode   = (String)VMonthCode.elementAt(VMonthName.indexOf(SMonthName));
            return (common.toInt(SMonthCode) >= 10 ? SMonthCode : "0"+SMonthCode);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            System.out.println("MonthCode"+ex);
            return "";
        }
    }

    public void setYear()
    {
        try
        {
            VFYearCode                   	= new Vector();
            StringBuffer sb     		= new StringBuffer();

            sb.append(" Select YearCode from SCM.NoofYears Order by 1");

            Class.forName("oracle.jdbc.OracleDriver");
            Connection   theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","system","hammer");
//            Connection   theConnection = DriverManager.getConnection("jdbc:oracle:thin:@117.239.247.18:1530:arun","system","hammer");

            Statement theStatement     = theConnection.createStatement();
            ResultSet rst 		  = theStatement.executeQuery(sb.toString());

            while (rst.next())
            {
                VFYearCode               . add(rst.getString(1));
            }
            rst.close();
            rst=null;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void setTYear()
    {
    try
    {
          VTYearCode                   	= new Vector();
          StringBuffer sb     		= new StringBuffer();
          sb.append(" Select YearCode from SCM.NoofYears Order by 1");

            Class.forName("oracle.jdbc.OracleDriver");
          
            Connection   theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","system","hammer");
//            Connection   theConnection = DriverManager.getConnection("jdbc:oracle:thin:@117.239.247.18:1530:arun","system","hammer");
          Statement theStatement     = theConnection.createStatement();
          ResultSet rst 		  = theStatement.executeQuery(sb.toString());

          while (rst.next())
          {
              VTYearCode               . add(rst.getString(1));
          }
          rst.close();
          rst=null;
    }
    catch(Exception e)
    {
         e.printStackTrace();
    }
}
}
