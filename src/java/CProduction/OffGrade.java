package CProduction;

import java.io.*;
import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
public class OffGrade extends HttpServlet
{
     HttpSession    session;

     Common common   = new Common();
     Control control = new Control();

     String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody,tbgBody;
     String SStDate,SEnDate,SUnit;
     String SUnitCode,STable,SDeptCode,SDept;
     String SStopName[],SStopCode[];
     Vector VOffGradeClass,VDept,VDeptCode,VStName,VStCode;

     FileWriter  FW;
     int Lctr = 70,Pctr = 0;
     String SLine;
     Vector VHead;

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);

          bgColor = common.bgColor;
          bgHead  = common.bgHead;
          bgUom   = common.bgUom;
          bgBody  = common.bgBody;
          fgHead  = common.fgHead;
          fgUom   = common.fgUom;
          fgBody  = common.fgBody;
		tbgBody = common.tbgBody;
     }
     public void doGet(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
		response.setContentType("text/html");
        PrintWriter out  = response.getWriter();
		SUnitCode        = (request.getParameter("Unit")).trim();
        SDeptCode        = (request.getParameter("Dept")).trim();
	SDept            = (request.getParameter("Dept")).trim();
	//SDept            = (request.getParameter("DeptName")).trim();
        SStDate          = common.pureDate((request.getParameter("Date")).trim());
        SEnDate          = common.pureDate((request.getParameter("DateTo")).trim());
	SUnit            = common.toInt(SUnitCode)==1?"A":(common.toInt(SUnitCode)==2?"B":"S");		
		//System.out.println("\nOffGrade ->" + SUnitCode + ":" + SDeptCode + ":" + SStDate + ":" + SEnDate);

		session = request.getSession(true);
		/*if(session.getValue("AppletTag")==null)
		{
			String SServer = control.getID("Select ProdHost from Server");
			response.sendRedirect(SServer + "/SessionExpired");
			return;

		}*/
          STable    = getTable(SDeptCode);
          if(common.toInt(SDeptCode)<=0 || common.toInt(SDeptCode)>=7)
          {
               return;
          }
          setVectors();
          fillStopNames();
          try
          {
               FW      = new FileWriter("/software/C-Prod-Print/Reports/grade2c.prn");
               //FW      = new FileWriter("/production/Reports/grade2c.prn");
               Lctr    = 70;
               Pctr    =  0;
               Head();
               Body();
               FW.close();
          }catch(Exception ex)
          {
               System.out.println("36 : "+ex);
               ex.printStackTrace();
          }
          out.println("<html>");
          out.println("<body bgcolor='"+bgBody+"' alink='#0000FF'>");
          //out.println(session.getValue("AppletTag"));
          out.println("<a href='Header'>Home</a>");
          out.println("<p align='center'><font size='5' color='maroon'><b>Production Officer Grade Report</b></font></p>");
          out.println("<p align='left' style='FONT-SIZE: small; FONT-WEIGHT: bold'>");
          out.println("<font color='mediumblue'>Unit&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple> " + SUnit + "</font>&nbsp;&nbsp;&nbsp;&nbsp;");
          out.println("<font color='mediumblue'>Start Date&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple>" + common.parseDate(SStDate) + "</font>&nbsp;&nbsp;&nbsp;&nbsp;"); 
          out.println("<font color='mediumblue'>End Date&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple>" + common.parseDate(SEnDate) + "</font>"); 
          out.println("<br><br>");
          out.println("<div align='left'>");
          out.println("  <table border='1' width='1200' height='50'>");
          out.println("    <tr>");
//        out.println("      <td width='60' height='119' rowspan='3' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Department</font></td>");
          out.println("      <td width='88' height='119' rowspan='3' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>P.O/J.P.O.");
          out.println("        Name</font></td>");
          out.println("      <td width='87' height='119' rowspan='3' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Grade</font></td>");
          out.println("      <td width='63' height='119' rowspan='3' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Effy");
          out.println("        %</font></td>");
          out.println("      <td width='63' height='119' rowspan='3' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Util");
          out.println("        % (OA)</font></td>");
          out.println("      <td width='63' height='119' rowspan='3' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Util");
          out.println("        % (M)</font></td>");
          out.println("      <td width='63' height='119' rowspan='3' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>U");
          out.println("        &amp; E (OA)</font></td>");
          out.println("      <td width='63' height='119' rowspan='3' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>U");
          out.println("        &amp; E (M)</font></td>");
          out.println("      <td width='498' height='24' colspan='7' align='center' bgcolor='"+bgHead+"'>");
          out.println("        <p align='center'><font color='"+fgHead+"'>Excess Efficiency Loss Due To</font></td>");
          out.println("      <td width='124' height='62' colspan='2' rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Loss");
          out.println("        Due To</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='250' height='36' colspan='4' align='center' bgcolor='"+bgHead+"'>");
          out.println("        <p align='center'><font color='"+fgHead+"'>Production Officer Reason</font></td>");
          out.println("      <td width='248' height='36' colspan='3' align='center' bgcolor='"+bgHead+"'>");
          out.println("        <p align='center'><font color='"+fgHead+"'>Worker Reason</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='63' height='42' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Lap");
          out.println("        Rod Shortage</font></td>");
          out.println("      <td width='63' height='42' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Absent");
          out.println("        -eeism</font></td>");
          out.println("      <td width='62' height='42' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Want");
          out.println("        of Mixing</font></td>");
          out.println("      <td width='62' height='42' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Wheel");
          out.println("        Changes</font></td>");
          out.println("      <td width='62' height='42' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Shade");
          out.println("        Change With Air Cleaning</font></td>");
          out.println("      <td width='62' height='42' align='center' bgcolor='"+bgHead+"'>");
          out.println("        <p align='center'><font color='"+fgHead+"'>Shade Change Without Air");
          out.println("        Cleaning</font></td>");
          out.println("      <td width='62' height='42' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Shift");
          out.println("        End Cleaning</font></td>");
          out.println("      <td width='62' height='42' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Mechanical</font></td>");
          out.println("      <td width='62' height='42' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Electrical</font></td>");
          out.println("    </tr>");
          for(int i=0;i<VOffGradeClass.size();i++)
          {
               OffGradeClass ogc = (OffGradeClass)VOffGradeClass.elementAt(i);

               out.println("    <tr>");
//             out.println("      <td width='60' height='5' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+SDept+"</font></td>");
               out.println("      <td width='88' height='5' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+ogc.SName+"</font></td>");
               out.println("      <td width='87' height='5' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+getGrade(ogc.dEffy)+"</font></td>");
               out.println("      <td width='63' height='5' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+common.getRound(ogc.dEffy,2)+"</font></td>");
               out.println("      <td width='63' height='5' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+common.getRound(ogc.dUOA,2)+"</font></td>");
               out.println("      <td width='63' height='5' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+common.getRound(ogc.dUM,2)+"</font></td>");
               out.println("      <td width='63' height='5' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+common.getRound(ogc.dUEOA,2)+"</font></td>");
               out.println("      <td width='63' height='5' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+common.getRound(ogc.dUEM,2)+"</font></td>");
               out.println("      <td width='63' height='5' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+ogc.getStopMnt(SStopCode[0])+"</font></td>");
               out.println("      <td width='63' height='5' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+ogc.getStopMnt(SStopCode[1])+"</font></td>");
               out.println("      <td width='62' height='5' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+ogc.getStopMnt(SStopCode[2])+"</font></td>");
               out.println("      <td width='62' height='5' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+ogc.getStopMnt(SStopCode[3])+"</font></td>");
               out.println("      <td width='62' height='5' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+common.getRound(ogc.dSCAC,0)+"</font></td>");
               out.println("      <td width='62' height='5' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+common.getRound(ogc.dSC,0)+"</font></td>");
               out.println("      <td width='62' height='5' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+common.getRound(ogc.dShift,0)+"</font></td>");
               out.println("      <td width='62' height='5' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+common.getRound(ogc.dMaint,0)+"</font></td>");
               out.println("      <td width='62' height='5' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+common.getRound(ogc.dElect,0)+"</font></td>");
               out.println("    </tr>");
          }
          out.println("  </table>");
          out.println("</div>");
          out.println("</body>");
          out.println("</html>");

          out.close();

     }
     private void setVectors()
     {
          VOffGradeClass = new Vector();
          VStName        = new Vector();
          VStCode        = new Vector();

          try
          {
                 Connection con = createConnection();
                 Statement stat = con.createStatement();
                 ResultSet res1 = stat.executeQuery(getQS1());
                 while(res1.next())
                    organizeGradeClass1(res1);
                 try
                 {
                    stat.execute("Drop Table TempPO1");
                 }catch(Exception ex1){}
                 try
                 {
                    stat.execute("Drop Table TempPO2");
                 }catch(Exception ex1){}
                 try
                 {
                    stat.execute(getQSTempPO1());
                 }catch(Exception ex1)
                 {
                    System.out.println("184 : "+ex1);
                 }
                 try
                 {
                    stat.execute(getQSTempPO2());
                 }catch(Exception ex1)
                 {
                    System.out.println("191 : "+ex1);
                 }
                 ResultSet res3 = stat.executeQuery(getQSShiftAC());
                 while(res3.next())
                    organizeGradeClass3(res3);
                 ResultSet res4 = stat.executeQuery(getQSMW());
                 while(res4.next())
                    organizeGradeClass4(res4);
                 ResultSet res5 = stat.executeQuery(getQSEW());
                 while(res5.next())
                    organizeGradeClass5(res5);
                 ResultSet res2 = stat.executeQuery(getQSOff());
                 while(res2.next())
                    organizeGradeClass2(res2);
                 ResultSet res99 = stat.executeQuery("Select Stop_Name,Stop_Code from TempPO2 Order By 1");
                 while(res99.next())
                 {
                    VStName.addElement(res99.getString(1));
                    VStCode.addElement(res99.getString(2));
                 }
                 con.close();
          }
          catch(Exception ex)
          {
                 System.out.println("111 : "+ex);
                 ex.printStackTrace();
          }
     }
     private void organizeGradeClass1(ResultSet res) throws Exception
     {
          String SName  = res.getString(1);
          String SCode  = res.getString(2);
          String SUOA   = common.getRound(res.getString(3),2);
          String SUM    = common.getRound(res.getString(4),2);
          String SUEOA  = common.getRound(res.getString(5),2);
          String SUEM   = common.getRound(res.getString(6),2);
          String SSC    = common.getRound(res.getString(7),2);
          String SSCAC  = common.getRound(res.getString(8),2);
          String SEffy  = common.getRound(res.getString(9),2);

          int iindex = indexOf(SCode);
          if(iindex==-1)
          {
               OffGradeClass ogc = new OffGradeClass(SCode,SName);
               ogc.append1(SEffy,SUOA,SUM,SUEOA,SUEM,SSC,SSCAC);
               VOffGradeClass.addElement(ogc);
          }
          else
          {
               OffGradeClass ogc = (OffGradeClass)VOffGradeClass.elementAt(iindex);
               ogc.append1(SEffy,SUOA,SUM,SUEOA,SUEM,SSC,SSCAC);
               VOffGradeClass.setElementAt(ogc,iindex);
          }
     }
     private void organizeGradeClass2(ResultSet res) throws Exception
     {
          String SName   = res.getString(1);
          String SCode   = res.getString(2);
          String SStName = res.getString(3);
          String SStCode = res.getString(4);
          String SMnt    = res.getString(5);

          int iindex = indexOf(SCode);
          if(iindex==-1)
          {
               return;
          }
          else
          {
               OffGradeClass ogc = (OffGradeClass)VOffGradeClass.elementAt(iindex);
               ogc.append2(SStName,SMnt,SStCode);
               VOffGradeClass.setElementAt(ogc,iindex);
          }
     }

     private void organizeGradeClass3(ResultSet res) throws Exception
     {
          String SName  = res.getString(1);
          String SCode  = res.getString(2);
          String SMnt   = res.getString(3);

          int iindex = indexOf(SCode);
          if(iindex==-1)
          {
               return;
          }
          else
          {
               OffGradeClass ogc = (OffGradeClass)VOffGradeClass.elementAt(iindex);
               ogc.append3(SMnt);
               VOffGradeClass.setElementAt(ogc,iindex);
          }
     }
     private void organizeGradeClass4(ResultSet res) throws Exception
     {
          String SName  = res.getString(1);
          String SCode  = res.getString(2);
          String SMnt   = res.getString(3);

          int iindex = indexOf(SCode);
          if(iindex==-1)
          {
               return;
          }
          else
          {
               OffGradeClass ogc = (OffGradeClass)VOffGradeClass.elementAt(iindex);
               ogc.append4(SMnt);
               VOffGradeClass.setElementAt(ogc,iindex);
          }
     }
     private void organizeGradeClass5(ResultSet res) throws Exception
     {
          String SName  = res.getString(1);
          String SCode  = res.getString(2);
          String SMnt   = res.getString(3);

          int iindex = indexOf(SCode);
          if(iindex==-1)
          {
               return;
          }
          else
          {
               OffGradeClass ogc = (OffGradeClass)VOffGradeClass.elementAt(iindex);
               ogc.append5(SMnt);
               VOffGradeClass.setElementAt(ogc,iindex);
          }
     }

     private String getQS1()
     {
          String QS = "";
          QS = " SELECT supervisor.supervisor, "+STable+".supcode,"+
               " Avg("+STable+".utiloa) AS AvgOfutiloa,  "+
               " Avg("+STable+".utilmu) AS AvgOfutilmu, Avg("+STable+".ueoa) AS AvgOfueoa, "+
               " Avg("+STable+".uem) AS AvgOfuem, Sum("+STable+".rotime) AS SumOfrotime, "+
               " Sum("+STable+".rotimeac) AS SumOfrotimeac, Avg("+STable+".effyactual) AS AvgOfeffyactual "+
               " FROM "+STable+" INNER JOIN supervisor ON "+STable+".supcode = supervisor.supcode "+
               " WHERE "+STable+".unit_code="+SUnitCode+
               " GROUP BY supervisor.supervisor,"+STable+".supcode"+
               " ORDER BY 9 DESC ";
          return QS;
     }
     private String getQSTempPO1()
     {
          String QS = "";
               //The dates attended by that supervisor
          QS = " create table TempPO1 as ("+
               " SELECT supervisor.supervisor, "+STable+".supcode,"+
               " Count("+STable+".mach_code) AS CountOfmach_code, "+
               STable+".sp_date, "+STable+".shift_code "+
               " FROM "+STable+" INNER JOIN supervisor ON "+STable+".supcode = supervisor.supcode "+
               " WHERE "+STable+".sp_date>='"+SStDate+"' And "+STable+".sp_date<='"+SEnDate+"'"+
               " GROUP BY supervisor.supervisor, "+STable+".supcode,"+STable+".sp_date, "+STable+".shift_code)";
          return QS;
     }
     private String getQSTempPO2()
     {
          String QS = "";
               // The stoppages for a particular dept
          QS = " create table TempPO2 as "+
               " (SELECT stopreasons.stop_name, Stoppage.Offrepcode, Stoppage.stop_code "+
               " FROM Stoppage INNER JOIN stopreasons ON Stoppage.stop_code = stopreasons.STOP_CODE "+
               " WHERE Stoppage.Offrepcode>0 AND Stoppage.dept_code="+SDeptCode+")";
          return QS;
     }

     private String getQSMW()
     {
          String QS = "";
               // The stoppages for Maintenance
          QS = " SELECT TempPO1.supervisor,TempPO1.supcode, Sum(StopTran.Stop_Mint) AS SumOfStop_Mint "+
               " FROM TempPO1 INNER JOIN StopTran ON (TempPO1.sp_date=StopTran.Stop_Date) AND (TempPO1.shift_code=StopTran.Stop_Shift) "+
               " WHERE StopTran.Stop_Code=2 Or Stoptran.stop_code=21 "+
               " GROUP BY TempPO1.supervisor,TempPO1.supcode";

          return QS;
     }

     private String getQSEW()
     {
          String QS = "";
               // The stoppages for Electrical
          QS = " SELECT TempPO1.supervisor,TempPO1.supcode, Sum(StopTran.Stop_Mint) AS SumOfStop_Mint "+
               " FROM TempPO1 INNER JOIN StopTran ON (TempPO1.sp_date=StopTran.Stop_Date) AND (TempPO1.shift_code=StopTran.Stop_Shift) "+
               " WHERE StopTran.Stop_Code=3 Or Stoptran.stop_code=22 "+
               " GROUP BY TempPO1.supervisor,TempPO1.supcode";

          return QS;
     }

     private String getQSShiftAC()
     {
          String QS = "";
               // The stoppages for ShiftAirCleaning
          QS = " SELECT TempPO1.supervisor,TempPO1.supcode,Sum(StopTran.Stop_Mint) AS SumOfStop_Mint "+
               " FROM TempPO1 INNER JOIN StopTran ON (TempPO1.shift_code = StopTran.Stop_Shift) "+
               " AND (TempPO1.sp_date = StopTran.Stop_Date) "+
               " WHERE StopTran.Stop_Code=30 "+
               " GROUP BY TempPO1.supervisor,TempPO1.supcode";

          return QS;
     }
     private String getQSOff()
     {
          String QS = "";
               // The stoppages for Supervisor reasons
          QS = " SELECT TempPO1.supervisor,TempPO1.supcode, "+
               " TempPO2.stop_name, StopTran.Stop_Code, "+
               " Sum(StopTran.Stop_Mint) AS SumOfStop_Mint "+
               " FROM (TempPO2 INNER JOIN StopTran ON TempPO2.stop_code = StopTran.Stop_Code) "+
               " INNER JOIN TempPO1 ON (StopTran.Stop_Shift = TempPO1.shift_code) AND (StopTran.Stop_Date = TempPO1.sp_date) "+
               " WHERE StopTran.Unit_Code="+SUnitCode+" AND StopTran.Dept_Code="+SDeptCode+
               " and stoptran.stop_date>='"+SStDate+"' and stop_date<='"+SEnDate+"'"+
               " GROUP BY TempPO2.stop_name, StopTran.Stop_Code,TempPO1.supervisor,TempPO1.supcode"+
               " ORDER BY 2,3";

          return QS;
     }

     private int indexOf(String SCCode)
     {
          int index = -1;
          for(int i=0;i<VOffGradeClass.size();i++)
          {
               OffGradeClass ogc = (OffGradeClass)VOffGradeClass.elementAt(i);
               if(ogc.SCode.equals(SCCode))
               {
                    index = i;
                    break;
               }
          }
          return index;
     }
     private String getTable(String SCode)
     {
          String STable = "";
          if(SCode.equals("2"))
               STable = "BlowTran";
          else if(SCode.equals("3"))
               STable = "CardTran";               
          else if(SCode.equals("4"))
               STable = "CombTran";
          else if(SCode.equals("5"))
               STable = "DrawTran";               
          else if(SCode.equals("6"))
               STable = "SimpTran";
          else if(SCode.equals("9"))
               STable = "SLapTran";               
          return STable;
     }
     private String getGrade(double dEffy)
     {
          String SGrade = "";
          if(dEffy < 70)
               SGrade = "E :< 70";
          else if(dEffy >= 70 && dEffy <= 75)
               SGrade = "D :70-75";
          else if(dEffy > 75 && dEffy <= 80)
               SGrade = "C :76-80";
          else if(dEffy > 80 && dEffy <= 85)
               SGrade = "B :81-85";
          else if(dEffy > 85)
               SGrade = "A :> 85";
          return SGrade;
     }
     private String getGradeCode(double dEffy)
     {
          String SGrade = "";
          if(dEffy < 70)
               SGrade = "5";
          else if(dEffy >= 70 && dEffy <= 75)
               SGrade = "4";
          else if(dEffy > 75 && dEffy <= 80)
               SGrade = "3";
          else if(dEffy > 80 && dEffy <= 85)
               SGrade = "2";
          else if(dEffy > 85)
               SGrade = "1";
          return SGrade;
     }
     public void Head()
     {
          if(Lctr < 60)
             return;
          if(Pctr > 0)
          {
             try
             {
               FW.write(SLine+"\n");
             }catch(Exception ex){}
          }
          Pctr++;

          String Str1    = common.Pad("Company    : AMARJOTHI SPINNING MILLS LIMITED",80);
          String Str2    = common.Pad("Document   : Officer Grade during "+common.parseDate(SStDate)+" & "+common.parseDate(SEnDate),80);
          String Str3    = common.Pad("Unit       : "+SUnit,70);
          String Str4    = common.Pad("Department : "+SDept,70);
          VHead = Stop1Head();
          try
          {
               FW.write(Str1+"\n");
               FW.write(Str2+"\n");
               FW.write(Str3+"\n");
               FW.write(Str4+"\n\n");
               for(int i=0;i<VHead.size();i++)
               {
                    FW.write((String)VHead.elementAt(i));
               }
          }catch(Exception ex){}
          Lctr = 6+VHead.size();
     }
     public void Body()
     {
         String Strl = "";
         for(int i=0;i<VOffGradeClass.size();i++)
         {
               OffGradeClass ogc = (OffGradeClass)VOffGradeClass.elementAt(i);

               Strl  = common.Pad(getGrade(ogc.dEffy),10)+common.Space(2)+
                      common.Pad(ogc.SName,15)+common.Space(2)+
                      common.Rad(common.getRound(ogc.dEffy,2),6)+common.Space(2)+
                      common.Rad(common.getRound(ogc.dUOA,2),6)+common.Space(2)+
                      common.Rad(common.getRound(ogc.dUM,2),6)+common.Space(2)+
                      common.Rad(common.getRound(ogc.dUEOA,2),6)+common.Space(2)+
                      common.Rad(common.getRound(ogc.dUEM,2),6)+common.Space(2)+
                      common.Rad(ogc.getStopMnt(SStopCode[0]),6)+common.Space(2)+
                      common.Rad(ogc.getStopMnt(SStopCode[1]),6)+common.Space(2)+
                      common.Rad(ogc.getStopMnt(SStopCode[2]),6)+common.Space(2)+
                      common.Rad(ogc.getStopMnt(SStopCode[3]),6)+common.Space(2)+
                      common.Rad(common.getRound(ogc.dSCAC,0),6)+common.Space(2)+
                      common.Rad(common.getRound(ogc.dSC,0),6)+common.Space(2)+
                      common.Rad(common.getRound(ogc.dShift,0),6)+common.Space(2)+
                      common.Rad(common.getRound(ogc.dMaint,0),6)+common.Space(2)+
                      common.Rad(common.getRound(ogc.dElect,0),6);

               try
               {
                    Head();
                    FW.write(Strl+"\n");
                    Lctr++;
               }catch(Exception ex){}
         }

         try
         {
              FW.write(SLine);
              FW.write("<End Of Report>");
         }catch(Exception ex){}
     }

     public Vector Stop1Head()
     {
          Vector VHead = new Vector();

          String Str1  = common.Pad("Grade",10)+common.Space(2)+
                      common.Pad("PO/JPO Name",15)+common.Space(2)+
                      common.Rad("Effy %",6)+common.Space(2)+
                      common.Rad("Util %",6)+common.Space(2)+
                      common.Rad("Util %",6)+common.Space(2)+
                      common.Rad("U&E %",6)+common.Space(2)+
                      common.Rad("U&E %",6)+common.Space(2)+
                      common.Cad("Stoppages in Minutes",70);

          String Str2  = common.Pad(" ",10)+common.Space(2)+
                      common.Pad(" ",15)+common.Space(2)+
                      common.Rad(" ",6)+common.Space(2)+
                      common.Rad("(OA)",6)+common.Space(2)+
                      common.Rad("(M)",6)+common.Space(2)+
                      common.Rad("(OA)",6)+common.Space(2)+
                      common.Rad("(M)",6)+common.Space(2)+
                      common.Replicate("-",70);

          String Str3  = common.Space(10+2+15+2+6+2+6+2+6+2+6+2+6+2)+
                      common.Cad("Officer Reason (mnts)",30)+common.Space(2)+
                      common.Cad("Worker Reason (mnts)",22)+common.Space(2)+
                      common.Cad("Others (mnts)",14);

          String Str4  = common.Space(10+2+15+2+6+2+6+2+6+2+6+2+6+2)+
                      common.Replicate("-",30)+common.Space(2)+
                      common.Replicate("-",22)+common.Space(2)+
                      common.Replicate("-",14);

          String Str5  = common.Space(10+2+15+2+6+2+6+2+6+2+6+2+6+2)+
                      common.Rad(SStopName[0].substring(0,6),6)+common.Space(2)+
                      common.Rad(SStopName[1].substring(0,6),6)+common.Space(2)+
                      common.Rad(SStopName[2].substring(0,6),6)+common.Space(2)+
                      common.Rad(SStopName[3].substring(0,6),6)+common.Space(2)+
                      common.Rad("SCwith",6)+common.Space(2)+
                      common.Rad("SCwith",6)+common.Space(2)+
                      common.Rad("Shift",6)+common.Space(2)+
                      common.Rad("Maint",6)+common.Space(2)+
                      common.Rad("Elect",6);

          String Str6  = common.Space(10+2+15+2+6+2+6+2+6+2+6+2+6+2)+
                      common.Rad(SStopName[0].substring(6,SStopName[0].length()),6)+common.Space(2)+
                      common.Rad(SStopName[1].substring(6,SStopName[1].length()),6)+common.Space(2)+
                      common.Rad(SStopName[2].substring(6,SStopName[2].length()),6)+common.Space(2)+
                      common.Rad(SStopName[3].substring(6,SStopName[3].length()),6)+common.Space(2)+
                      common.Rad("AC",6)+common.Space(2)+
                      common.Rad("out AC",6)+common.Space(2)+
                      common.Rad("AC",6)+common.Space(2)+
                      common.Rad(" ",6)+common.Space(2)+
                      common.Rad(" ",6);

          SLine = common.Replicate("-",Str1.length())+"\n";

          VHead.addElement(SLine);
          VHead.addElement(Str1+"\n");
          VHead.addElement(Str2+"\n");
          VHead.addElement(Str3+"\n");
          VHead.addElement(Str4+"\n");
          VHead.addElement(Str5+"\n");
          VHead.addElement(Str6+"\n");
          VHead.addElement(SLine);

          return VHead;
     }
     private void fillStopNames()
     {
          SStopName = new String[VStName.size()];
          SStopCode = new String[VStName.size()];
          for(int i=0;i<VStName.size();i++)
          {
               SStopName[i] = (String)VStName.elementAt(i);
               SStopCode[i] = (String)VStCode.elementAt(i);
          }
     }

	private Connection createConnection()
	{
		Connection conn;
		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
			conn  = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
		}catch(Exception ee)
		{
			return null;
		}
		return conn;
	}
}

