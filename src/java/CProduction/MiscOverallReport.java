/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CProduction;

import java.io.*;
import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
 * @author root
 */
public class MiscOverallReport extends HttpServlet {

     HttpSession    session;
     Common common;

     String S1[],S2[],S3[],ST[];

     String SUnit,SStDate,SEnDate;
     String bgColor,bgHead,bgUom,bgBody,tbgBody,fgHead,fgUom,fgBody;
     int    iW[] = {9,9,9,9,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6};
     int    iSlNo=0,iUnit;

     FileWriter  FW;
     String SLine;
     Vector VStrl = new Vector();

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
          common = new Common();

          S1     = new String[20];
          S2     = new String[20];
          S3     = new String[20];
          ST     = new String[20];

          bgColor = common.bgColor;
          bgHead  = common.bgHead;
          bgUom   = common.bgUom;
		bgBody  = common.bgBody;
          tbgBody  = common.tbgBody;
          fgHead  = common.fgHead;
          fgUom   = common.fgUom;
          fgBody  = common.fgBody;
          SLine   = common.Replicate("-",186);
     }
     
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /*
             * TODO output your page here. You may use following sample code.
             */
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MiscOverallReport</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MiscOverallReport at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            doPost(request,response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
	     response.setContentType("text/html");
          PrintWriter out  = response.getWriter();
		session = request.getSession(true);
		setCriteria(request);
          int iCtr         = 0;
          
          out.println("<html>");
          out.println("<head>");
          out.println("<title>Spinning Stoppages Report - Period </title>");
          out.println("</head>");
          out.println("<body bgcolor='" + common.bgBody + "'>");
          out.println(common.getConsReportHeader("MIS - Overall Performance",SUnit,SStDate,SEnDate));
          out.println("  <table border='1' width='1629' height='174'>");
          out.println("    <tr>");
          out.println("      <td width='28' height='112' rowspan='3' bgcolor='"+bgHead+"'><b><font size='4' color='"+fgHead+"'>Sl</font></b><p><b><font size='4' color='"+fgHead+"'>No</font></b></td>");
          out.println("      <td width='3' height='112' rowspan='3' bgcolor='"+bgHead+"'><p align='center'><b><font color='"+fgHead+"' size='4'>Department</font></b></td>");
          out.println("      <td width='196' height='27' colspan='4' valign='middle' align='right' bgcolor='"+bgHead+"' ><b><font size='4' color='"+fgHead+"'>Production</font></b></td>");
          out.println("      <td width='207' height='24' colspan='5' valign='middle' align='right' bgcolor='"+bgHead+"' ><b><font size='4' color='"+fgHead+"'>Utilization</font></b></td>");
          out.println("      <td width='257' height='24' colspan='5' valign='middle' align='right' bgcolor='"+bgHead+"' ><b><font size='4' color='"+fgHead+"'>Efficiency</font></b></td>");
          out.println("      <td width='104' height='23' colspan='2' valign='middle' align='center' bgcolor='"+bgHead+"'><p align='center'><b><font size='4' color='"+fgHead+"'>Stoppages</font></b></td>");
          out.println("      <td width='208' height='23' colspan='4' valign='middle' align='center' bgcolor='"+bgHead+"'><p align='center'><b><font size='4' color='"+fgHead+"'>Run-outs</font></b></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='74' height='47' align='center' bgcolor='"+bgHead+"' ><font size='4' color='"+fgHead+"'>Target</font></td>");
          out.println("      <td width='74' height='47' align='center' bgcolor='"+bgHead+"' ><font size='4' color='"+fgHead+"'>Expected</font></td>");
          out.println("      <td width='74' height='47' align='center' bgcolor='"+bgHead+"' ><font size='4' color='"+fgHead+"'>Actual</font></td>");
          out.println("      <td width='74' height='47' align='center' bgcolor='"+bgHead+"' ><font size='4' color='"+fgHead+"'>Loss</font></td>");
          out.println("      <td width='74' height='47' align='center' bgcolor='"+bgHead+"' ><font size='4' color='"+fgHead+"'>Allotted</font></td>");
          out.println("      <td width='74' height='47' align='center' bgcolor='"+bgHead+"' ><font size='4' color='"+fgHead+"'>Available</font></td>");
          out.println("      <td width='74' height='47' align='center' bgcolor='"+bgHead+"' ><font size='4' color='"+fgHead+"'>Worked</font></td>");
          out.println("      <td width='74' height='47' align='center' bgcolor='"+bgHead+"' ><font size='4' color='"+fgHead+"'>(OA)</font></td>");
          out.println("      <td width='74' height='47' align='center' bgcolor='"+bgHead+"' ><font size='4' color='"+fgHead+"'>(M)</font></td>");
          out.println("      <td width='74' height='47' align='center' bgcolor='"+bgHead+"' ><font size='4' color='"+fgHead+"'>Actual</font></td>");
          out.println("      <td width='75' height='47' align='center' bgcolor='"+bgHead+"' ><font size='4' color='"+fgHead+"'>Effective</font></td>");
          out.println("      <td width='75' height='48' align='center' bgcolor='"+bgHead+"' ><font size='4' color='"+fgHead+"'>UxE (OA)</font></td>");
          out.println("      <td width='75' height='48' align='center' bgcolor='"+bgHead+"' ><font size='4' color='"+fgHead+"'>UxE (M)</font></td>");
          out.println("      <td width='75' height='48' align='center' bgcolor='"+bgHead+"' ><font size='4' color='"+fgHead+"'>Worker Effy</font></td>");
          out.println("      <td width='75' height='48' align='center' bgcolor='"+bgHead+"' ><font size='4' color='"+fgHead+"'>Accepted</font></td>");
          out.println("      <td width='75' height='48' align='center' bgcolor='"+bgHead+"' ><font size='4' color='"+fgHead+"'>Non Accepted</font></td>");
          out.println("      <td width='75' height='48' align='center' bgcolor='"+bgHead+"' ><font size='4' color='"+fgHead+"'>No of Times</font></td>");
          out.println("      <td width='75' height='48' align='center' bgcolor='"+bgHead+"' ><font size='4' color='"+fgHead+"'>No of Persons</font></td>");
          out.println("      <td width='75' height='48' align='center' bgcolor='"+bgHead+"' ><font size='4' color='"+fgHead+"'>Actual Mints</font></td>");
          out.println("      <td width='75' height='48' align='center' bgcolor='"+bgHead+"' ><font size='4' color='"+fgHead+"'>Ex/Sh Mints</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='74' height='23' align='right' bgcolor='"+bgUom+"' ><b><font color='"+fgUom+"'>Kgs</font></b></td>");
          out.println("      <td width='74' height='23' align='right' bgcolor='"+bgUom+"' ><b><font color='"+fgUom+"'>Kgs</font></b></td>");
          out.println("      <td width='74' height='23' align='right' bgcolor='"+bgUom+"' ><b><font color='"+fgUom+"'>Kgs</font></b></td>");
          out.println("      <td width='74' height='23' align='right' bgcolor='"+bgUom+"' ><b><font color='"+fgUom+"'>Kgs</font></b></td>");
          out.println("      <td width='74' height='23' align='right' bgcolor='"+bgUom+"' ><b><font color='"+fgUom+"'>Hrs/Spdl</font></b></td>");
          out.println("      <td width='74' height='23' align='right' bgcolor='"+bgUom+"' ><b><font color='"+fgUom+"'>Hrs</font></b></td>");
          out.println("      <td width='74' height='23' align='right' bgcolor='"+bgUom+"' ><b><font color='"+fgUom+"'>Hrs/Spdl</font></b></td>");
          out.println("      <td width='74' height='23' align='right' bgcolor='"+bgUom+"' ><b><font color='"+fgUom+"'>%</font></b></td>");
          out.println("      <td width='74' height='23' align='right' bgcolor='"+bgUom+"' ><b><font color='"+fgUom+"'>%</font></b></td>");
          out.println("      <td width='74' height='23' align='right' bgcolor='"+bgUom+"' ><b><font color='"+fgUom+"'>%</font></b></td>");
          out.println("      <td width='75' height='23' align='right' bgcolor='"+bgUom+"' ><b><font color='"+fgUom+"'>%</font></b></td>");
          out.println("      <td width='75' height='22' align='right' bgcolor='"+bgUom+"' ><b><font color='"+fgUom+"'>%</font></b></td>");
          out.println("      <td width='75' height='22' align='right' bgcolor='"+bgUom+"' ><b><font color='"+fgUom+"'>%</font></b></td>");
          out.println("      <td width='75' height='22' align='right' bgcolor='"+bgUom+"' ><b><font color='"+fgUom+"'>%</font></b></td>");
          out.println("      <td width='75' height='22' align='right' bgcolor='"+bgUom+"' ><b><font color='"+fgUom+"'>Mnt</font></b></td>");
          out.println("      <td width='75' height='22' align='right' bgcolor='"+bgUom+"' ><b><font color='"+fgUom+"'>Mnt</font></b></td>");
          out.println("      <td width='75' height='22' align='right' bgcolor='"+bgUom+"' ><b><font color='"+fgUom+"'>No</font></b></td>");
          out.println("      <td width='75' height='22' align='right' bgcolor='"+bgUom+"' ><b><font color='"+fgUom+"'>No</font></b></td>");
          out.println("      <td width='75' height='22' align='right' bgcolor='"+bgUom+"' ><b><font color='"+fgUom+"'>Mnt</font></b></td>");
          out.println("      <td width='75' height='22' align='right' bgcolor='"+bgUom+"' ><b><font color='"+fgUom+"'>Mnt</font></b></td>");
          out.println("    </tr>");

          getBlowData();
          getTotal();
          strHead();
          toStrl(S1,S2,S3,ST,"Blow Room");
          iCtr++;
          htmlDept(out,"Blow Room",iCtr);

          getCardData();
          getTotal();
          toStrl(S1,S2,S3,ST,"Carding");
          iCtr++;
          htmlDept(out,"Carding",iCtr);

          if(iUnit==2)
          {
               getCombData();
               getTotal();
               toStrl(S1,S2,S3,ST,"Comber");
               iCtr++;
               htmlDept(out,"Comber",iCtr);
          }

          getDrawData();
          getTotal();
          toStrl(S1,S2,S3,ST,"Drawing");
          iCtr++;
          htmlDept(out,"Drawing",iCtr);

          getSimpData();
          getTotal();
          toStrl(S1,S2,S3,ST,"Simplex");
          iCtr++;
          htmlDept(out,"Simplex",iCtr);

          getSpinData();
          getTotal();
          toStrl(S1,S2,S3,ST,"Spinning");
          iCtr++;
          htmlDept(out,"Spinning",iCtr);

          out.println("  </table>");
          out.println("</div>");
          out.println("</body>");
          out.println("</html>");

          out.close();

          FW = new FileWriter("//software/A-Prod-Print/misA.prn");
          for(int i=0;i<VStrl.size();i++)
               FW.write((String)VStrl.elementAt(i)+"\n");
          FW.write("\n\n\nS.M.                   D.M.                   J.M.D.\n");
          FW.write("\n<End of report>");
          FW.close();
      }

     public void getBlowData()
     {
          try
          {
                  Connection con = createConnection();
                  Statement stat = con.createStatement();
                  ResultSet res  = stat.executeQuery(getQS1());
                  while(res.next())
                  {
                         S1[0]  = common.getRound(res.getString(1),2);
                         S1[1]  = common.getRound(res.getString(2),2);
                         S1[2]  = common.getRound(res.getString(3),2);
                         S1[3]  = common.getRound(res.getString(4),2);
                         S1[4]  = common.getRound(res.getString(5),2);
                         S1[5]  = common.getRound(res.getString(6),2);
                         S1[6]  = common.getRound(res.getString(7),2);
                         S1[7]  = common.getRound(res.getString(8),2);
                         S1[8]  = common.getRound(res.getString(9),2);
                         S1[11] = common.getRound(res.getString(10),2);
                         S1[12] = common.getRound(res.getString(11),2);
                         S1[13] = common.getRound(res.getString(12),2);
                         S1[14] = common.getRound(res.getString(13),2);
                         S1[15] = common.getRound(res.getString(14),2);
                         S1[16] = common.getRound(res.getString(15),2);
                         S1[17] = common.getRound(res.getString(16),2);
                         S1[18] = common.getRound(res.getString(17),2);
                  }
                  ResultSet res2  = stat.executeQuery(getQS2());
                  while(res2.next())
                  {
                         S2[0]  = common.getRound(res2.getString(1),2);
                         S2[1]  = common.getRound(res2.getString(2),2);
                         S2[2]  = common.getRound(res2.getString(3),2);
                         S2[3]  = common.getRound(res2.getString(4),2);
                         S2[4]  = common.getRound(res2.getString(5),2);
                         S2[5]  = common.getRound(res2.getString(6),2);
                         S2[6]  = common.getRound(res2.getString(7),2);
                         S2[7]  = common.getRound(res2.getString(8),2);
                         S2[8]  = common.getRound(res2.getString(9),2);
                         S2[11] = common.getRound(res2.getString(10),2);
                         S2[12] = common.getRound(res2.getString(11),2);
                         S2[13] = common.getRound(res2.getString(12),2);
                         S2[14] = common.getRound(res2.getString(13),2);
                         S2[15] = common.getRound(res2.getString(14),2);
                         S2[16] = common.getRound(res2.getString(15),2);
                         S2[17] = common.getRound(res2.getString(16),2);
                         S2[18] = common.getRound(res2.getString(17),2);
                  }                  
                  ResultSet res3 = stat.executeQuery(getQS3());
                  while(res3.next())
                  {
                         S3[0]  = common.getRound(res3.getString(1),2);
                         S3[1]  = common.getRound(res3.getString(2),2);
                         S3[2]  = common.getRound(res3.getString(3),2);
                         S3[3]  = common.getRound(res3.getString(4),2);
                         S3[4]  = common.getRound(res3.getString(5),2);
                         S3[5]  = common.getRound(res3.getString(6),2);
                         S3[6]  = common.getRound(res3.getString(7),2);
                         S3[7]  = common.getRound(res3.getString(8),2);
                         S3[8]  = common.getRound(res3.getString(9),2);
                         S3[11] = common.getRound(res3.getString(10),2);
                         S3[12] = common.getRound(res3.getString(11),2);
                         S3[13] = common.getRound(res3.getString(12),2);
                         S3[14] = common.getRound(res3.getString(13),2);
                         S3[15] = common.getRound(res3.getString(14),2);
                         S3[16] = common.getRound(res3.getString(15),2);
                         S3[17] = common.getRound(res3.getString(16),2);
                         S3[18] = common.getRound(res3.getString(17),2);
                  }
                  con.close();
          }
          catch(Exception ex)
          {
               System.out.println("line 846 : "+ex);
          }
     }

     public String getQS1()
     {
          String QS = " SELECT Sum(blowtran.prod_tar) AS sumtar, "+
                      " Sum(blowtran.prod_exp) AS SumOfprod_exp, "+
                      " Sum(blowtran.prod) AS SumOfprod, "+
                      " Sum(prod_tar-Prod) AS Expr1, "+
                      " Sum(blowtran.houralloted) AS SumOfhouralloted, "+
                      " Sum(blowtran.hourmeter) AS SumOfhourmeter, "+
                      " Sum(blowtran.hourrun) AS SumOfhourrun, "+
                      " Avg(blowtran.utiloa) AS AvgOfutiloa, "+
                      " Avg(blowtran.utilmu) AS AvgOfutilmu, "+
                      " Avg(blowtran.ueoa) AS AvgOfueoa, "+
                      " Avg(blowtran.uem) AS AvgOfuem, "+
                      " Avg(blowtran.workereffy) AS AvgOfworkereffy, "+
                      " Sum(blowtran.stopacc) AS SumOfstopacc, "+
                      " Sum(blowtran.stopnonacc) AS SumOfstopnonacc,"+
                      " Sum(noofro+noofroac), Sum(noofpersons+noofpersonsac), "+
                      " Sum(rotime+rotimeac) "+
                      " FROM blowtran "+
                      " WHERE (((blowtran.unit_code)="+SUnit+") And "+
                      " ((blowtran.sp_date)>='"+SStDate+"' And (blowtran.sp_date)<='"+SEnDate+"') And ((blowtran.shift_code)=1)) ";
          return QS;
     }

     public String getQS2()
     {
          String QS = " SELECT Sum(blowtran.prod_tar) AS sumtar, "+
                      " Sum(blowtran.prod_exp) AS SumOfprod_exp, "+
                      " Sum(blowtran.prod) AS SumOfprod, "+
                      " Sum(prod_tar-Prod) AS Expr1, "+
                      " Sum(blowtran.houralloted) AS SumOfhouralloted, "+
                      " Sum(blowtran.hourmeter) AS SumOfhourmeter, "+
                      " Sum(blowtran.hourrun) AS SumOfhourrun, "+
                      " Avg(blowtran.utiloa) AS AvgOfutiloa, "+
                      " Avg(blowtran.utilmu) AS AvgOfutilmu, "+
                      " Avg(blowtran.ueoa) AS AvgOfueoa, "+
                      " Avg(blowtran.uem) AS AvgOfuem, "+
                      " Avg(blowtran.workereffy) AS AvgOfworkereffy, "+
                      " Sum(blowtran.stopacc) AS SumOfstopacc, "+
                      " Sum(blowtran.stopnonacc) AS SumOfstopnonacc,"+
                      " Sum(noofro+noofroac), Sum(noofpersons+noofpersonsac), "+
                      " Sum(rotime+rotimeac) "+
                      " FROM blowtran "+
                      " WHERE (((blowtran.unit_code)="+SUnit+") And "+
                      " ((blowtran.sp_date)>='"+SStDate+"' And (blowtran.sp_date)<='"+SEnDate+"') And ((blowtran.shift_code)=2)) ";
          return QS;
     }

     public String getQS3()
     {
          String QS = " SELECT Sum(blowtran.prod_tar) AS sumtar, "+
                      " Sum(blowtran.prod_exp) AS SumOfprod_exp, "+
                      " Sum(blowtran.prod) AS SumOfprod, "+
                      " Sum(prod_tar-Prod) AS Expr1, "+
                      " Sum(blowtran.houralloted) AS SumOfhouralloted, "+
                      " Sum(blowtran.hourmeter) AS SumOfhourmeter, "+
                      " Sum(blowtran.hourrun) AS SumOfhourrun, "+
                      " Avg(blowtran.utiloa) AS AvgOfutiloa, "+
                      " Avg(blowtran.utilmu) AS AvgOfutilmu, "+
                      " Avg(blowtran.ueoa) AS AvgOfueoa, "+
                      " Avg(blowtran.uem) AS AvgOfuem, "+
                      " Avg(blowtran.workereffy) AS AvgOfworkereffy, "+
                      " Sum(blowtran.stopacc) AS SumOfstopacc, "+
                      " Sum(blowtran.stopnonacc) AS SumOfstopnonacc,"+
                      " Sum(noofro+noofroac), Sum(noofpersons+noofpersonsac), "+
                      " Sum(rotime+rotimeac) "+
                      " FROM blowtran "+
                      " WHERE (((blowtran.unit_code)="+SUnit+") And "+
                      " ((blowtran.sp_date)>='"+SStDate+"' And (blowtran.sp_date)<='"+SEnDate+"') And ((blowtran.shift_code)=3)) ";
          return QS;
     }

     public void getCardData()
     {
          try
          {
                  Connection con = createConnection();
                  Statement stat = con.createStatement();
                  ResultSet res  = stat.executeQuery(getQSC1());
                  while(res.next())
                  {
                         S1[0]  = common.getRound(res.getString(1),2);
                         S1[1]  = common.getRound(res.getString(2),2);
                         S1[2]  = common.getRound(res.getString(3),2);
                         S1[3]  = common.getRound(res.getString(4),2);
                         S1[4]  = common.getRound(res.getString(5),2);
                         S1[5]  = common.getRound(res.getString(6),2);
                         S1[6]  = common.getRound(res.getString(7),2);
                         S1[7]  = common.getRound(res.getString(8),2);
                         S1[8]  = common.getRound(res.getString(9),2);
                         S1[11] = common.getRound(res.getString(10),2);
                         S1[12] = common.getRound(res.getString(11),2);
                         S1[13] = common.getRound(res.getString(12),2);
                         S1[14] = common.getRound(res.getString(13),2);
                         S1[15] = common.getRound(res.getString(14),2);
                         S1[16] = common.getRound(res.getString(15),2);
                         S1[17] = common.getRound(res.getString(16),2);
                         S1[18] = common.getRound(res.getString(17),2);
                  }
                  ResultSet res2  = stat.executeQuery(getQSC2());
                  while(res2.next())
                  {
                         S2[0]  = common.getRound(res2.getString(1),2);
                         S2[1]  = common.getRound(res2.getString(2),2);
                         S2[2]  = common.getRound(res2.getString(3),2);
                         S2[3]  = common.getRound(res2.getString(4),2);
                         S2[4]  = common.getRound(res2.getString(5),2);
                         S2[5]  = common.getRound(res2.getString(6),2);
                         S2[6]  = common.getRound(res2.getString(7),2);
                         S2[7]  = common.getRound(res2.getString(8),2);
                         S2[8]  = common.getRound(res2.getString(9),2);
                         S2[11] = common.getRound(res2.getString(10),2);
                         S2[12] = common.getRound(res2.getString(11),2);
                         S2[13] = common.getRound(res2.getString(12),2);
                         S2[14] = common.getRound(res2.getString(13),2);
                         S2[15] = common.getRound(res2.getString(14),2);
                         S2[16] = common.getRound(res2.getString(15),2);
                         S2[17] = common.getRound(res2.getString(16),2);
                         S2[18] = common.getRound(res2.getString(17),2);
                  }                  
                  ResultSet res3 = stat.executeQuery(getQSC3());
                  while(res3.next())
                  {
                         S3[0]  = common.getRound(res3.getString(1),2);
                         S3[1]  = common.getRound(res3.getString(2),2);
                         S3[2]  = common.getRound(res3.getString(3),2);
                         S3[3]  = common.getRound(res3.getString(4),2);
                         S3[4]  = common.getRound(res3.getString(5),2);
                         S3[5]  = common.getRound(res3.getString(6),2);
                         S3[6]  = common.getRound(res3.getString(7),2);
                         S3[7]  = common.getRound(res3.getString(8),2);
                         S3[8]  = common.getRound(res3.getString(9),2);
                         S3[11] = common.getRound(res3.getString(10),2);
                         S3[12] = common.getRound(res3.getString(11),2);
                         S3[13] = common.getRound(res3.getString(12),2);
                         S3[14] = common.getRound(res3.getString(13),2);
                         S3[15] = common.getRound(res3.getString(14),2);
                         S3[16] = common.getRound(res3.getString(15),2);
                         S3[17] = common.getRound(res3.getString(16),2);
                         S3[18] = common.getRound(res3.getString(17),2);
                  }
                  con.close();
          }
          catch(Exception ex)
          {
               System.out.println("line 1002 : "+ex);
          }
     }

     public String getQSC1()
     {
          String QS = " SELECT Sum(cardtran.prod_tar) AS sumtar, "+
                      " Sum(cardtran.prod_exp) AS SumOfprod_exp, "+
                      " Sum(cardtran.prod) AS SumOfprod, "+
                      " Sum(prod_tar-Prod) AS Expr1, "+
                      " Sum(cardtran.houralloted) AS SumOfhouralloted, "+
                      " Sum(cardtran.hourmeter) AS SumOfhourmeter, "+
                      " Sum(cardtran.hourrun) AS SumOfhourrun, "+
                      " Avg(cardtran.utiloa) AS AvgOfutiloa, "+
                      " Avg(cardtran.utilmu) AS AvgOfutilmu, "+
                      " Avg(cardtran.ueoa) AS AvgOfueoa, "+
                      " Avg(cardtran.uem) AS AvgOfuem, "+
                      " Avg(cardtran.workereffy) AS AvgOfworkereffy, "+
                      " Sum(cardtran.stopacc) AS SumOfstopacc, "+
                      " Sum(cardtran.stopnonacc) AS SumOfstopnonacc, "+
                      " Sum(noofro+noofroac), Sum(noofpersons+noofpersonsac), "+
                      " Sum(rotime+rotimeac) "+
                      " FROM cardtran "+
                      " WHERE (((cardtran.unit_code)="+SUnit+") And "+
                      " ((cardtran.sp_date)>='"+SStDate+"' And (cardtran.sp_date)<='"+SEnDate+"') And ((cardtran.shift_code)=1) And Prod_Exp>0 And Prod_tar>0) ";
          return QS;
     }

     public String getQSC2()
     {
          String QS = " SELECT Sum(cardtran.prod_tar) AS sumtar, "+
                      " Sum(cardtran.prod_exp) AS SumOfprod_exp, "+
                      " Sum(cardtran.prod) AS SumOfprod, "+
                      " Sum(prod_tar-Prod) AS Expr1, "+
                      " Sum(cardtran.houralloted) AS SumOfhouralloted, "+
                      " Sum(cardtran.hourmeter) AS SumOfhourmeter, "+
                      " Sum(cardtran.hourrun) AS SumOfhourrun, "+
                      " Avg(cardtran.utiloa) AS AvgOfutiloa, "+
                      " Avg(cardtran.utilmu) AS AvgOfutilmu, "+
                      " Avg(cardtran.ueoa) AS AvgOfueoa, "+
                      " Avg(cardtran.uem) AS AvgOfuem, "+
                      " Avg(cardtran.workereffy) AS AvgOfworkereffy, "+
                      " Sum(cardtran.stopacc) AS SumOfstopacc, "+
                      " Sum(cardtran.stopnonacc) AS SumOfstopnonacc, "+
                      " Sum(noofro+noofroac), Sum(noofpersons+noofpersonsac), "+
                      " Sum(rotime+rotimeac) "+
                      " FROM cardtran "+
                      " WHERE (((cardtran.unit_code)="+SUnit+") And "+
                      " ((cardtran.sp_date)>='"+SStDate+"' And (cardtran.sp_date)<='"+SEnDate+"') And ((cardtran.shift_code)=2) And Prod_Exp>0 And Prod_tar>0) ";
          return QS;
     }

     public String getQSC3()
     {
          String QS = " SELECT Sum(cardtran.prod_tar) AS sumtar, "+
                      " Sum(cardtran.prod_exp) AS SumOfprod_exp, "+
                      " Sum(cardtran.prod) AS SumOfprod, "+
                      " Sum(prod_tar-Prod) AS Expr1, "+
                      " Sum(cardtran.houralloted) AS SumOfhouralloted, "+
                      " Sum(cardtran.hourmeter) AS SumOfhourmeter, "+
                      " Sum(cardtran.hourrun) AS SumOfhourrun, "+
                      " Avg(cardtran.utiloa) AS AvgOfutiloa, "+
                      " Avg(cardtran.utilmu) AS AvgOfutilmu, "+
                      " Avg(cardtran.ueoa) AS AvgOfueoa, "+
                      " Avg(cardtran.uem) AS AvgOfuem, "+
                      " Avg(cardtran.workereffy) AS AvgOfworkereffy, "+
                      " Sum(cardtran.stopacc) AS SumOfstopacc, "+
                      " Sum(cardtran.stopnonacc) AS SumOfstopnonacc, "+
                      " Sum(noofro+noofroac), Sum(noofpersons+noofpersonsac), "+
                      " Sum(rotime+rotimeac) "+
                      " FROM cardtran "+
                      " WHERE (((cardtran.unit_code)="+SUnit+") And "+
                      " ((cardtran.sp_date)>='"+SStDate+"' And (cardtran.sp_date)<='"+SEnDate+"') And ((cardtran.shift_code)=3) And Prod_Exp>0 And Prod_tar>0) ";
          return QS;
     }
     public void getCombData()
     {
          try
          {
                  Connection con = createConnection();
                  Statement stat = con.createStatement();
                  ResultSet res  = stat.executeQuery(getQSCo1());
                  while(res.next())
                  {
                         S1[0]  = common.getRound(res.getString(1),2);
                         S1[1]  = common.getRound(res.getString(2),2);
                         S1[2]  = common.getRound(res.getString(3),2);
                         S1[3]  = common.getRound(res.getString(4),2);
                         S1[4]  = common.getRound(res.getString(5),2);
                         S1[5]  = common.getRound(res.getString(6),2);
                         S1[6]  = common.getRound(res.getString(7),2);
                         S1[7]  = common.getRound(res.getString(8),2);
                         S1[8]  = common.getRound(res.getString(9),2);
                         S1[11] = common.getRound(res.getString(10),2);
                         S1[12] = common.getRound(res.getString(11),2);
                         S1[13] = common.getRound(res.getString(12),2);
                         S1[14] = common.getRound(res.getString(13),2);
                         S1[15] = common.getRound(res.getString(14),2);
                         S1[16] = common.getRound(res.getString(15),2);
                         S1[17] = common.getRound(res.getString(16),2);
                         S1[18] = common.getRound(res.getString(17),2);
                  }
                  ResultSet res2  = stat.executeQuery(getQSCo2());
                  while(res2.next())
                  {
                         S2[0]  = common.getRound(res2.getString(1),2);
                         S2[1]  = common.getRound(res2.getString(2),2);
                         S2[2]  = common.getRound(res2.getString(3),2);
                         S2[3]  = common.getRound(res2.getString(4),2);
                         S2[4]  = common.getRound(res2.getString(5),2);
                         S2[5]  = common.getRound(res2.getString(6),2);
                         S2[6]  = common.getRound(res2.getString(7),2);
                         S2[7]  = common.getRound(res2.getString(8),2);
                         S2[8]  = common.getRound(res2.getString(9),2);
                         S2[11] = common.getRound(res2.getString(10),2);
                         S2[12] = common.getRound(res2.getString(11),2);
                         S2[13] = common.getRound(res2.getString(12),2);
                         S2[14] = common.getRound(res2.getString(13),2);
                         S2[15] = common.getRound(res2.getString(14),2);
                         S2[16] = common.getRound(res2.getString(15),2);
                         S2[17] = common.getRound(res2.getString(16),2);
                         S2[18] = common.getRound(res2.getString(17),2);
                  }                  
                  ResultSet res3 = stat.executeQuery(getQSCo3());
                  while(res3.next())
                  {
                         S3[0]  = common.getRound(res3.getString(1),2);
                         S3[1]  = common.getRound(res3.getString(2),2);
                         S3[2]  = common.getRound(res3.getString(3),2);
                         S3[3]  = common.getRound(res3.getString(4),2);
                         S3[4]  = common.getRound(res3.getString(5),2);
                         S3[5]  = common.getRound(res3.getString(6),2);
                         S3[6]  = common.getRound(res3.getString(7),2);
                         S3[7]  = common.getRound(res3.getString(8),2);
                         S3[8]  = common.getRound(res3.getString(9),2);
                         S3[11] = common.getRound(res3.getString(10),2);
                         S3[12] = common.getRound(res3.getString(11),2);
                         S3[13] = common.getRound(res3.getString(12),2);
                         S3[14] = common.getRound(res3.getString(13),2);
                         S3[15] = common.getRound(res3.getString(14),2);
                         S3[16] = common.getRound(res3.getString(15),2);
                         S3[17] = common.getRound(res3.getString(16),2);
                         S3[18] = common.getRound(res3.getString(17),2);
                  }
                  con.close();
          }
          catch(Exception ex)
          {
               System.out.println("line 1144 : "+ex);
          }
     }

     public String getQSCo1()
     {
          String QS = " SELECT Sum(combtran.prod_tar) AS sumtar, "+
                      " Sum(combtran.prod_exp) AS SumOfprod_exp, "+
                      " Sum(combtran.prod) AS SumOfprod, "+
                      " Sum(prod_tar-Prod) AS Expr1, "+
                      " Sum(combtran.houralloted) AS SumOfhouralloted, "+
                      " Sum(combtran.hourmeter) AS SumOfhourmeter, "+
                      " Sum(combtran.hourrun) AS SumOfhourrun, "+
                      " Avg(combtran.utiloa) AS AvgOfutiloa, "+
                      " Avg(combtran.utilmu) AS AvgOfutilmu, "+
                      " Avg(combtran.ueoa) AS AvgOfueoa, "+
                      " Avg(combtran.uem) AS AvgOfuem, "+
                      " Avg(combtran.workereffy) AS AvgOfworkereffy, "+
                      " Sum(combtran.stopacc) AS SumOfstopacc, "+
                      " Sum(combtran.stopnonacc) AS SumOfstopnonacc, "+
                      " Sum(noofro+noofroac), Sum(noofpersons+noofpersonsac), "+
                      " Sum(rotime+rotimeac) "+
                      " FROM combtran "+
                      " WHERE (((combtran.unit_code)="+SUnit+") And "+
                      " ((combtran.sp_date)>='"+SStDate+"' And (combtran.sp_date)<='"+SEnDate+"') And ((combtran.shift_code)=1) And Prod_Exp>0 And Prod_tar>0) ";
          return QS;
     }

     public String getQSCo2()
     {
          String QS = " SELECT Sum(combtran.prod_tar) AS sumtar, "+
                      " Sum(combtran.prod_exp) AS SumOfprod_exp, "+
                      " Sum(combtran.prod) AS SumOfprod, "+
                      " Sum(prod_tar-Prod) AS Expr1, "+
                      " Sum(combtran.houralloted) AS SumOfhouralloted, "+
                      " Sum(combtran.hourmeter) AS SumOfhourmeter, "+
                      " Sum(combtran.hourrun) AS SumOfhourrun, "+
                      " Avg(combtran.utiloa) AS AvgOfutiloa, "+
                      " Avg(combtran.utilmu) AS AvgOfutilmu, "+
                      " Avg(combtran.ueoa) AS AvgOfueoa, "+
                      " Avg(combtran.uem) AS AvgOfuem, "+
                      " Avg(combtran.workereffy) AS AvgOfworkereffy, "+
                      " Sum(combtran.stopacc) AS SumOfstopacc, "+
                      " Sum(combtran.stopnonacc) AS SumOfstopnonacc, "+
                      " Sum(noofro+noofroac), Sum(noofpersons+noofpersonsac), "+
                      " Sum(rotime+rotimeac) "+
                      " FROM combtran "+
                      " WHERE (((combtran.unit_code)="+SUnit+") And "+
                      " ((combtran.sp_date)>='"+SStDate+"' And (combtran.sp_date)<='"+SEnDate+"') And ((combtran.shift_code)=2) And Prod_Exp>0 And Prod_tar>0) ";
          return QS;
     }

     public String getQSCo3()
     {
          String QS = " SELECT Sum(combtran.prod_tar) AS sumtar, "+
                      " Sum(combtran.prod_exp) AS SumOfprod_exp, "+
                      " Sum(combtran.prod) AS SumOfprod, "+
                      " Sum(prod_tar-Prod) AS Expr1, "+
                      " Sum(combtran.houralloted) AS SumOfhouralloted, "+
                      " Sum(combtran.hourmeter) AS SumOfhourmeter, "+
                      " Sum(combtran.hourrun) AS SumOfhourrun, "+
                      " Avg(combtran.utiloa) AS AvgOfutiloa, "+
                      " Avg(combtran.utilmu) AS AvgOfutilmu, "+
                      " Avg(combtran.ueoa) AS AvgOfueoa, "+
                      " Avg(combtran.uem) AS AvgOfuem, "+
                      " Avg(combtran.workereffy) AS AvgOfworkereffy, "+
                      " Sum(combtran.stopacc) AS SumOfstopacc, "+
                      " Sum(combtran.stopnonacc) AS SumOfstopnonacc, "+
                      " Sum(noofro+noofroac), Sum(noofpersons+noofpersonsac), "+
                      " Sum(rotime+rotimeac) "+
                      " FROM combtran "+
                      " WHERE (((combtran.unit_code)="+SUnit+") And "+
                      " ((combtran.sp_date)>='"+SStDate+"' And (combtran.sp_date)<='"+SEnDate+"') And ((combtran.shift_code)=3) And Prod_Exp>0 And Prod_tar>0) ";
          return QS;
     }

     public void getDrawData()
     {
          try
          {
                  Connection con = createConnection();
                  Statement stat = con.createStatement();
                  ResultSet res  = stat.executeQuery(getQSD1());
                  while(res.next())
                  {
                         S1[0]  = common.getRound(res.getString(1),2);
                         S1[1]  = common.getRound(res.getString(2),2);
                         S1[2]  = common.getRound(res.getString(3),2);
                         S1[3]  = common.getRound(res.getString(4),2);
                         S1[4]  = common.getRound(res.getString(5),2);
                         S1[5]  = common.getRound(res.getString(6),2);
                         S1[6]  = common.getRound(res.getString(7),2);
                         S1[7]  = common.getRound(res.getString(8),2);
                         S1[8]  = common.getRound(res.getString(9),2);
                         S1[11] = common.getRound(res.getString(10),2);
                         S1[12] = common.getRound(res.getString(11),2);
                         S1[13] = common.getRound(res.getString(12),2);
                         S1[14] = common.getRound(res.getString(13),2);
                         S1[15] = common.getRound(res.getString(14),2);
                         S1[16] = common.getRound(res.getString(15),2);
                         S1[17] = common.getRound(res.getString(16),2);
                         S1[18] = common.getRound(res.getString(17),2);
                  }
                  ResultSet res2  = stat.executeQuery(getQSD2());
                  while(res2.next())
                  {
                         S2[0]  = common.getRound(res2.getString(1),2);
                         S2[1]  = common.getRound(res2.getString(2),2);
                         S2[2]  = common.getRound(res2.getString(3),2);
                         S2[3]  = common.getRound(res2.getString(4),2);
                         S2[4]  = common.getRound(res2.getString(5),2);
                         S2[5]  = common.getRound(res2.getString(6),2);
                         S2[6]  = common.getRound(res2.getString(7),2);
                         S2[7]  = common.getRound(res2.getString(8),2);
                         S2[8]  = common.getRound(res2.getString(9),2);
                         S2[11] = common.getRound(res2.getString(10),2);
                         S2[12] = common.getRound(res2.getString(11),2);
                         S2[13] = common.getRound(res2.getString(12),2);
                         S2[14] = common.getRound(res2.getString(13),2);
                         S2[15] = common.getRound(res2.getString(14),2);
                         S2[16] = common.getRound(res2.getString(15),2);
                         S2[17] = common.getRound(res2.getString(16),2);
                         S2[18] = common.getRound(res2.getString(17),2);
                  }                  
                  ResultSet res3 = stat.executeQuery(getQSD3());
                  while(res3.next())
                  {
                         S3[0]  = common.getRound(res3.getString(1),2);
                         S3[1]  = common.getRound(res3.getString(2),2);
                         S3[2]  = common.getRound(res3.getString(3),2);
                         S3[3]  = common.getRound(res3.getString(4),2);
                         S3[4]  = common.getRound(res3.getString(5),2);
                         S3[5]  = common.getRound(res3.getString(6),2);
                         S3[6]  = common.getRound(res3.getString(7),2);
                         S3[7]  = common.getRound(res3.getString(8),2);
                         S3[8]  = common.getRound(res3.getString(9),2);
                         S3[11] = common.getRound(res3.getString(10),2);
                         S3[12] = common.getRound(res3.getString(11),2);
                         S3[13] = common.getRound(res3.getString(12),2);
                         S3[14] = common.getRound(res3.getString(13),2);
                         S3[15] = common.getRound(res3.getString(14),2);
                         S3[16] = common.getRound(res3.getString(15),2);
                         S3[17] = common.getRound(res3.getString(16),2);
                         S3[18] = common.getRound(res3.getString(17),2);
                  }
                  con.close();
          }
          catch(Exception ex)
          {
               System.out.println("line 1149 : "+ex);
          }
     }

     public String getQSD1()
     {
          String QS = " SELECT Sum(drawtran.prod_tar) AS sumtar, "+
                      " Sum(drawtran.prod_exp) AS SumOfprod_exp, "+
                      " Sum(drawtran.prod) AS SumOfprod, "+
                      " Sum(prod_tar-Prod) AS Expr1, "+
                      " Sum(drawtran.houralloted) AS SumOfhouralloted, "+
                      " Sum(drawtran.hourmeter) AS SumOfhourmeter, "+
                      " Sum(drawtran.hourrun) AS SumOfhourrun, "+
                      " Avg(drawtran.utiloa) AS AvgOfutiloa, "+
                      " Avg(drawtran.utilmu) AS AvgOfutilmu, "+
                      " Avg(drawtran.ueoa) AS AvgOfueoa, "+
                      " Avg(drawtran.uem) AS AvgOfuem, "+
                      " Avg(drawtran.workereffy) AS AvgOfworkereffy, "+
                      " Sum(drawtran.stopacc) AS SumOfstopacc, "+
                      " Sum(drawtran.stopnonacc) AS SumOfstopnonacc, "+
                      " Sum(noofro+noofroac), Sum(noofpersons+noofpersonsac), "+
                      " Sum(rotime+rotimeac) "+
                      " FROM drawtran "+
                      " WHERE (((drawtran.unit_code)="+SUnit+") And "+
                      " ((drawtran.sp_date)>='"+SStDate+"' And (drawtran.sp_date)<='"+SEnDate+"') And ((drawtran.shift_code)=1) And Prod_Exp>0 And Prod_tar>0) ";
          return QS;
     }

     public String getQSD2()
     {
          String QS = " SELECT Sum(drawtran.prod_tar) AS sumtar, "+
                      " Sum(drawtran.prod_exp) AS SumOfprod_exp, "+
                      " Sum(drawtran.prod) AS SumOfprod, "+
                      " Sum(prod_tar-Prod) AS Expr1, "+
                      " Sum(drawtran.houralloted) AS SumOfhouralloted, "+
                      " Sum(drawtran.hourmeter) AS SumOfhourmeter, "+
                      " Sum(drawtran.hourrun) AS SumOfhourrun, "+
                      " Avg(drawtran.utiloa) AS AvgOfutiloa, "+
                      " Avg(drawtran.utilmu) AS AvgOfutilmu, "+
                      " Avg(drawtran.ueoa) AS AvgOfueoa, "+
                      " Avg(drawtran.uem) AS AvgOfuem, "+
                      " Avg(drawtran.workereffy) AS AvgOfworkereffy, "+
                      " Sum(drawtran.stopacc) AS SumOfstopacc, "+
                      " Sum(drawtran.stopnonacc) AS SumOfstopnonacc, "+
                      " Sum(noofro+noofroac), Sum(noofpersons+noofpersonsac), "+
                      " Sum(rotime+rotimeac) "+
                      " FROM drawtran "+
                      " WHERE (((drawtran.unit_code)="+SUnit+") And "+
                      " ((drawtran.sp_date)>='"+SStDate+"' And (drawtran.sp_date)<='"+SEnDate+"') And ((drawtran.shift_code)=2) And Prod_Exp>0 And Prod_tar>0) ";
          return QS;
     }

     public String getQSD3()
     {
          String QS = " SELECT Sum(drawtran.prod_tar) AS sumtar, "+
                      " Sum(drawtran.prod_exp) AS SumOfprod_exp, "+
                      " Sum(drawtran.prod) AS SumOfprod, "+
                      " Sum(prod_tar-Prod) AS Expr1, "+
                      " Sum(drawtran.houralloted) AS SumOfhouralloted, "+
                      " Sum(drawtran.hourmeter) AS SumOfhourmeter, "+
                      " Sum(drawtran.hourrun) AS SumOfhourrun, "+
                      " Avg(drawtran.utiloa) AS AvgOfutiloa, "+
                      " Avg(drawtran.utilmu) AS AvgOfutilmu, "+
                      " Avg(drawtran.ueoa) AS AvgOfueoa, "+
                      " Avg(drawtran.uem) AS AvgOfuem, "+
                      " Avg(drawtran.workereffy) AS AvgOfworkereffy, "+
                      " Sum(drawtran.stopacc) AS SumOfstopacc, "+
                      " Sum(drawtran.stopnonacc) AS SumOfstopnonacc, "+
                      " Sum(noofro+noofroac), Sum(noofpersons+noofpersonsac), "+
                      " Sum(rotime+rotimeac) "+
                      " FROM drawtran "+
                      " WHERE (((drawtran.unit_code)="+SUnit+") And "+
                      " ((drawtran.sp_date)>='"+SStDate+"' And (drawtran.sp_date)<='"+SEnDate+"') And ((drawtran.shift_code)=3) And Prod_Exp>0 And Prod_tar>0) ";
          return QS;
     }

     public void getSimpData()
     {
          try
          {
                  Connection con = createConnection();
                  Statement stat = con.createStatement();
                  ResultSet res  = stat.executeQuery(getQSSim1());
                  while(res.next())
                  {
                         S1[0]  = common.getRound(res.getString(1),2);
                         S1[1]  = common.getRound(res.getString(2),2);
                         S1[2]  = common.getRound(res.getString(3),2);
                         S1[3]  = common.getRound(res.getString(4),2);
                         S1[4]  = common.getRound(res.getString(5),2);
                         S1[5]  = common.getRound(res.getString(6),2);
                         S1[6]  = common.getRound(res.getString(7),2);
                         S1[7]  = common.getRound(res.getString(8),2);
                         S1[8]  = common.getRound(res.getString(9),2);
                         S1[11] = common.getRound(res.getString(10),2);
                         S1[12] = common.getRound(res.getString(11),2);
                         S1[13] = common.getRound(res.getString(12),2);
                         S1[14] = common.getRound(res.getString(13),2);
                         S1[15] = common.getRound(res.getString(14),2);
                         S1[16] = common.getRound(res.getString(15),2);
                         S1[17] = common.getRound(res.getString(16),2);
                         S1[18] = common.getRound(res.getString(17),2);
                  }
                  ResultSet res2  = stat.executeQuery(getQSSim2());
                  while(res2.next())
                  {
                         S2[0]  = common.getRound(res2.getString(1),2);
                         S2[1]  = common.getRound(res2.getString(2),2);
                         S2[2]  = common.getRound(res2.getString(3),2);
                         S2[3]  = common.getRound(res2.getString(4),2);
                         S2[4]  = common.getRound(res2.getString(5),2);
                         S2[5]  = common.getRound(res2.getString(6),2);
                         S2[6]  = common.getRound(res2.getString(7),2);
                         S2[7]  = common.getRound(res2.getString(8),2);
                         S2[8]  = common.getRound(res2.getString(9),2);
                         S2[11] = common.getRound(res2.getString(10),2);
                         S2[12] = common.getRound(res2.getString(11),2);
                         S2[13] = common.getRound(res2.getString(12),2);
                         S2[14] = common.getRound(res2.getString(13),2);
                         S2[15] = common.getRound(res2.getString(14),2);
                         S2[16] = common.getRound(res2.getString(15),2);
                         S2[17] = common.getRound(res2.getString(16),2);
                         S2[18] = common.getRound(res2.getString(17),2);
                  }                  
                  ResultSet res3 = stat.executeQuery(getQSSim3());
                  while(res3.next())
                  {
                         S3[0]  = common.getRound(res3.getString(1),2);
                         S3[1]  = common.getRound(res3.getString(2),2);
                         S3[2]  = common.getRound(res3.getString(3),2);
                         S3[3]  = common.getRound(res3.getString(4),2);
                         S3[4]  = common.getRound(res3.getString(5),2);
                         S3[5]  = common.getRound(res3.getString(6),2);
                         S3[6]  = common.getRound(res3.getString(7),2);
                         S3[7]  = common.getRound(res3.getString(8),2);
                         S3[8]  = common.getRound(res3.getString(9),2);
                         S3[11] = common.getRound(res3.getString(10),2);
                         S3[12] = common.getRound(res3.getString(11),2);
                         S3[13] = common.getRound(res3.getString(12),2);
                         S3[14] = common.getRound(res3.getString(13),2);
                         S3[15] = common.getRound(res3.getString(14),2);
                         S3[16] = common.getRound(res3.getString(15),2);
                         S3[17] = common.getRound(res3.getString(16),2);
                         S3[18] = common.getRound(res3.getString(17),2);
                  }
                  con.close();
          }
          catch(Exception ex)
          {
               System.out.println("line 1296 : "+ex);
          }
     }

     public String getQSSim1()
     {
          String QS = " SELECT Sum(simptran.prod_tar) AS sumtar, "+
                      " Sum(simptran.prod_exp) AS SumOfprod_exp, "+
                      " Sum(simptran.prod) AS SumOfprod, "+
                      " Sum(prod_tar-Prod) AS Expr1, "+
                      " Sum(simptran.noofspdl) AS SumOfhouralloted, "+
                      " Sum(simptran.hoursrun) AS SumOfhourmeter, "+
                      " Sum(simptran.spdlwkd) AS SumOfhourrun, "+
                      " Avg(simptran.utiloa) AS AvgOfutiloa, "+
                      " Avg(simptran.utilmu) AS AvgOfutilmu, "+
                      " Avg(simptran.ueoa) AS AvgOfueoa, "+
                      " Avg(simptran.uem) AS AvgOfuem, "+
                      " Avg(simptran.workereffy) AS AvgOfworkereffy, "+
                      " Sum(simptran.stopacc) AS SumOfstopacc, "+
                      " Sum(simptran.stopnonacc) AS SumOfstopnonacc, "+
                      " Sum(noofro+noofroac), Sum(noofpersons+noofpersonsac), "+
                      " Sum(rotime+rotimeac) "+
                      " FROM simptran "+
                      " WHERE (((simptran.unit_code)="+SUnit+") And "+
                      " ((simptran.sp_date)>='"+SStDate+"' And (simptran.sp_date)<='"+SEnDate+"') And ((simptran.shift_code)=1) And Prod_Exp>0 And Prod_tar>0) ";
          return QS;
     }

     public String getQSSim2()
     {
          String QS = " SELECT Sum(simptran.prod_tar) AS sumtar, "+
                      " Sum(simptran.prod_exp) AS SumOfprod_exp, "+
                      " Sum(simptran.prod) AS SumOfprod, "+
                      " Sum(prod_tar-Prod) AS Expr1, "+
                      " Sum(simptran.noofspdl) AS SumOfhouralloted, "+
                      " Sum(simptran.hoursrun) AS SumOfhourmeter, "+
                      " Sum(simptran.spdlwkd) AS SumOfhourrun, "+
                      " Avg(simptran.utiloa) AS AvgOfutiloa, "+
                      " Avg(simptran.utilmu) AS AvgOfutilmu, "+
                      " Avg(simptran.ueoa) AS AvgOfueoa, "+
                      " Avg(simptran.uem) AS AvgOfuem, "+
                      " Avg(simptran.workereffy) AS AvgOfworkereffy, "+
                      " Sum(simptran.stopacc) AS SumOfstopacc, "+
                      " Sum(simptran.stopnonacc) AS SumOfstopnonacc, "+
                      " Sum(noofro+noofroac), Sum(noofpersons+noofpersonsac), "+
                      " Sum(rotime+rotimeac) "+
                      " FROM simptran "+
                      " WHERE (((simptran.unit_code)="+SUnit+") And "+
                      " ((simptran.sp_date)>='"+SStDate+"' And (simptran.sp_date)<='"+SEnDate+"') And ((simptran.shift_code)=2) And Prod_Exp>0 And Prod_tar>0) ";
          return QS;
     }

     public String getQSSim3()
     {
          String QS = " SELECT Sum(simptran.prod_tar) AS sumtar, "+
                      " Sum(simptran.prod_exp) AS SumOfprod_exp, "+
                      " Sum(simptran.prod) AS SumOfprod, "+
                      " Sum(prod_tar-Prod) AS Expr1, "+
                      " Sum(simptran.noofspdl) AS SumOfhouralloted, "+
                      " Sum(simptran.hoursrun) AS SumOfhourmeter, "+
                      " Sum(simptran.spdlwkd) AS SumOfhourrun, "+
                      " Avg(simptran.utiloa) AS AvgOfutiloa, "+
                      " Avg(simptran.utilmu) AS AvgOfutilmu, "+
                      " Avg(simptran.ueoa) AS AvgOfueoa, "+
                      " Avg(simptran.uem) AS AvgOfuem, "+
                      " Avg(simptran.workereffy) AS AvgOfworkereffy, "+
                      " Sum(simptran.stopacc) AS SumOfstopacc, "+
                      " Sum(simptran.stopnonacc) AS SumOfstopnonacc, "+
                      " Sum(noofro+noofroac), Sum(noofpersons+noofpersonsac), "+
                      " Sum(rotime+rotimeac) "+
                      " FROM simptran "+
                      " WHERE (((simptran.unit_code)="+SUnit+") And "+
                      " ((simptran.sp_date)>='"+SStDate+"' And (simptran.sp_date)<='"+SEnDate+"') And ((simptran.shift_code)=3) And Prod_Exp>0 And Prod_tar>0) ";
          return QS;
     }

     public void getSpinData()
     {
          try
          {
                  Connection con = createConnection();
                  Statement stat = con.createStatement();
                  ResultSet res  = stat.executeQuery(getQSSpin1());
                  while(res.next())
                  {
                         S1[0]  = common.getRound(res.getString(1),2);
                         S1[1]  = common.getRound(res.getString(2),2);
                         S1[2]  = common.getRound(res.getString(3),2);
                         S1[3]  = common.getRound(res.getString(4),2);
                         S1[4]  = common.getRound(res.getString(5),2);
                         S1[5]  = common.getRound(res.getString(6),2);
                         S1[6]  = common.getRound(res.getString(7),2);
                         S1[7]  = common.getRound(res.getString(8),2);
                         S1[8]  = common.getRound(res.getString(9),2);
                         S1[11] = common.getRound(res.getString(10),2);
                         S1[12] = common.getRound(res.getString(11),2);
                         S1[13] = common.getRound(res.getString(12),2);
                         S1[14] = common.getRound(res.getString(13),2);
                         S1[15] = common.getRound(res.getString(14),2);
                         S1[16] = "0";
                         S1[17] = "0";
                         S1[18] = "0";
                  }
                  ResultSet res2  = stat.executeQuery(getQSSpin2());
                  while(res2.next())
                  {
                         S2[0]  = common.getRound(res2.getString(1),2);
                         S2[1]  = common.getRound(res2.getString(2),2);
                         S2[2]  = common.getRound(res2.getString(3),2);
                         S2[3]  = common.getRound(res2.getString(4),2);
                         S2[4]  = common.getRound(res2.getString(5),2);
                         S2[5]  = common.getRound(res2.getString(6),2);
                         S2[6]  = common.getRound(res2.getString(7),2);
                         S2[7]  = common.getRound(res2.getString(8),2);
                         S2[8]  = common.getRound(res2.getString(9),2);
                         S2[11] = common.getRound(res2.getString(10),2);
                         S2[12] = common.getRound(res2.getString(11),2);
                         S2[13] = common.getRound(res2.getString(12),2);
                         S2[14] = common.getRound(res2.getString(13),2);
                         S2[15] = common.getRound(res2.getString(14),2);
                         S2[16] = "0";
                         S2[17] = "0";
                         S2[18] = "0";
                  }                  
                  ResultSet res3 = stat.executeQuery(getQSSpin3());
                  while(res3.next())
                  {
                         S3[0]  = common.getRound(res3.getString(1),2);
                         S3[1]  = common.getRound(res3.getString(2),2);
                         S3[2]  = common.getRound(res3.getString(3),2);
                         S3[3]  = common.getRound(res3.getString(4),2);
                         S3[4]  = common.getRound(res3.getString(5),2);
                         S3[5]  = common.getRound(res3.getString(6),2);
                         S3[6]  = common.getRound(res3.getString(7),2);
                         S3[7]  = common.getRound(res3.getString(8),2);
                         S3[8]  = common.getRound(res3.getString(9),2);
                         S3[11] = common.getRound(res3.getString(10),2);
                         S3[12] = common.getRound(res3.getString(11),2);
                         S3[13] = common.getRound(res3.getString(12),2);
                         S3[14] = common.getRound(res3.getString(13),2);
                         S3[15] = common.getRound(res3.getString(14),2);
                         S3[16] = "0";
                         S3[17] = "0";
                         S3[18] = "0";
                  }
                  con.close();
          }
          catch(Exception ex)
          {
               System.out.println("line 1443 : "+ex);
          }
     }

     public String getQSSpin1()
     {
          String QS = " SELECT Sum(spintran.prod_tar) AS sumtar, "+
                      " Sum(spintran.prod_exp) AS SumOfprod_exp, "+
                      " Sum(spintran.prod) AS SumOfprod, "+
                      " Sum(prod_tar-Prod) AS Expr1, "+
                      " Sum(spintran.noofspdl) AS SumOfhouralloted, "+
                      " Sum(spintran.hoursrun) AS SumOfhourmeter, "+
                      " Sum(spintran.spdlwkd) AS SumOfhourrun, "+
                      " Avg(spintran.utiloa) AS AvgOfutiloa, "+
                      " Avg(spintran.utilmu) AS AvgOfutilmu, "+
                      " Avg(spintran.ueoa) AS AvgOfueoa, "+
                      " Avg(spintran.uem) AS AvgOfuem, "+
                      " Avg(spintran.workereffy) AS AvgOfworkereffy, "+
                      " Sum(spintran.stopacc) AS SumOfstopacc, "+
                      " Sum(spintran.stopnonacc) AS SumOfstopnonacc "+
                      " FROM spintran "+
                      " WHERE (((spintran.unit_code)="+SUnit+") And "+
                      " ((spintran.sp_date)>='"+SStDate+"' And (spintran.sp_date)<='"+SEnDate+"') And ((spintran.shift_code)=1) And Prod_Exp>0 And Prod_tar>0) ";
          return QS;
     }

     public String getQSSpin2()
     {
          String QS = " SELECT Sum(spintran.prod_tar) AS sumtar, "+
                      " Sum(spintran.prod_exp) AS SumOfprod_exp, "+
                      " Sum(spintran.prod) AS SumOfprod, "+
                      " Sum(prod_tar-Prod) AS Expr1, "+
                      " Sum(spintran.noofspdl) AS SumOfhouralloted, "+
                      " Sum(spintran.hoursrun) AS SumOfhourmeter, "+
                      " Sum(spintran.spdlwkd) AS SumOfhourrun, "+
                      " Avg(spintran.utiloa) AS AvgOfutiloa, "+
                      " Avg(spintran.utilmu) AS AvgOfutilmu, "+
                      " Avg(spintran.ueoa) AS AvgOfueoa, "+
                      " Avg(spintran.uem) AS AvgOfuem, "+
                      " Avg(spintran.workereffy) AS AvgOfworkereffy, "+
                      " Sum(spintran.stopacc) AS SumOfstopacc, "+
                      " Sum(spintran.stopnonacc) AS SumOfstopnonacc "+
                      " FROM spintran "+
                      " WHERE (((spintran.unit_code)="+SUnit+") And "+
                      " ((spintran.sp_date)>='"+SStDate+"' And (spintran.sp_date)<='"+SEnDate+"') And ((spintran.shift_code)=2) And Prod_Exp>0 And Prod_tar>0) ";
          return QS;
     }

     public String getQSSpin3()
     {
          String QS = " SELECT Sum(spintran.prod_tar) AS sumtar, "+
                      " Sum(spintran.prod_exp) AS SumOfprod_exp, "+
                      " Sum(spintran.prod) AS SumOfprod, "+
                      " Sum(prod_tar-Prod) AS Expr1, "+
                      " Sum(spintran.noofspdl) AS SumOfhouralloted, "+
                      " Sum(spintran.hoursrun) AS SumOfhourmeter, "+
                      " Sum(spintran.spdlwkd) AS SumOfhourrun, "+
                      " Avg(spintran.utiloa) AS AvgOfutiloa, "+
                      " Avg(spintran.utilmu) AS AvgOfutilmu, "+
                      " Avg(spintran.ueoa) AS AvgOfueoa, "+
                      " Avg(spintran.uem) AS AvgOfuem, "+
                      " Avg(spintran.workereffy) AS AvgOfworkereffy, "+
                      " Sum(spintran.stopacc) AS SumOfstopacc, "+
                      " Sum(spintran.stopnonacc) AS SumOfstopnonacc "+
                      " FROM spintran "+
                      " WHERE (((spintran.unit_code)="+SUnit+") And "+
                      " ((spintran.sp_date)>='"+SStDate+"' And (spintran.sp_date)<='"+SEnDate+"') And ((spintran.shift_code)=3) And Prod_Exp>0 And Prod_tar>0) ";
          return QS;
     }

     public void getTotal()
     {
          // Prod Exp or Prod Tar may be Zero
          S1[9]  = common.toDouble(S1[0])==0?"0":common.getRound(common.toDouble(S1[2])*100/common.toDouble(S1[0]),2);
          S1[10] = common.toDouble(S1[1])==0?"0":common.getRound(common.toDouble(S1[2])*100/common.toDouble(S1[1]),2);

          S2[9]  = common.toDouble(S2[0])==0?"0":common.getRound(common.toDouble(S1[2])*100/common.toDouble(S1[0]),2);
          S2[10] = common.toDouble(S2[1])==0?"0":common.getRound(common.toDouble(S1[2])*100/common.toDouble(S1[1]),2);

          S3[9]  = common.toDouble(S3[0])==0?"0":common.getRound(common.toDouble(S1[2])*100/common.toDouble(S1[0]),2);
          S3[10] = common.toDouble(S3[1])==0?"0":common.getRound(common.toDouble(S1[2])*100/common.toDouble(S1[1]),2);

          //the run-outs not included.
          for(int i=0;i<19;i++)
          {
               ST[i] = common.getRound(common.toDouble(S1[i])+common.toDouble(S2[i])+common.toDouble(S3[i]),2);
          }
          ST[7]  = common.getRound((common.toDouble(ST[7])/3.0),2);
          ST[8]  = common.getRound((common.toDouble(ST[8])/3.0),2);
          ST[9]  = common.getRound((common.toDouble(ST[9])/3.0),2);
          ST[10] = common.getRound((common.toDouble(ST[10])/3.0),2);
          ST[11] = common.getRound((common.toDouble(ST[11])/3.0),2);
          ST[12] = common.getRound((common.toDouble(ST[12])/3.0),2);
          ST[13] = common.getRound((common.toDouble(ST[13])/3.0),2);
     }
     private void strHead()
     {
          String StrH  = "Company   : Amarjothi Spinning Mills Ltd, Nambiyur";
          VStrl.addElement(StrH);
          StrH  = "Document  : MIS Overall Report between "+common.parseDate(SStDate)+" and "+common.parseDate(SEnDate);
          VStrl.addElement(StrH);
          StrH  = "Unit      :  "+(iUnit==1?"A":(iUnit==2?"B":"S"))+"\n";
          VStrl.addElement(StrH);

          VStrl.addElement(SLine);

          String Str1 = common.Space(2)+common.Space(2)+common.Space(10)+common.Space(2)+
                        common.Cad("Production",42)+common.Space(2)+common.Cad("Utilization",38)+common.Space(2)+
                        common.Cad("Efficiency",38)+common.Space(2)+common.Cad("Stoppages",14)+common.Space(2)+
                        common.Cad("Shade Change",30);
          VStrl.addElement(Str1);

                 Str1 = common.Rad("No",2)+common.Space(2)+common.Rad("Department",10)+common.Space(2)+
                        common.Replicate("Ä",42)+common.Space(2)+common.Replicate("Ä",38)+common.Space(2)+
                        common.Replicate("Ä",38)+common.Space(2)+common.Replicate("Ä",14)+common.Space(2)+
                        common.Replicate("Ä",30);
          VStrl.addElement(Str1);

                 Str1 = common.Space(2)+common.Space(2)+common.Space(10)+common.Space(2)+
                        common.Rad("Target",9)+common.Space(2)+common.Rad("Expected",9)+common.Space(2)+
                        common.Rad("Actual",9)+common.Space(2)+common.Rad("Loss",9)+common.Space(2)+
                        common.Rad("Allotted",6)+common.Space(2)+common.Rad("Avail",6)+common.Space(2)+
                        common.Rad("Hours",6)+common.Space(2)+common.Rad("(OA)",6)+common.Space(2)+
                        common.Rad("(M)",6)+common.Space(2)+
                        common.Rad("Actual",6)+common.Space(2)+common.Rad("Effec",6)+common.Space(2)+
                        common.Rad("UxE OA",6)+common.Space(2)+common.Rad("UxE M",6)+common.Space(2)+
                        common.Rad("Worker",6)+common.Space(2)+
                        common.Rad("Accep",6)+common.Space(2)+common.Rad("Non Acc",6)+common.Space(2)+
                        common.Rad("No of",6)+common.Space(2)+common.Rad("No of",6)+common.Space(2)+
                        common.Rad("Actual",6)+common.Space(2)+common.Rad("Ex/Sh",6);
          VStrl.addElement(Str1);

                 Str1 = common.Space(2)+common.Space(2)+common.Space(10)+common.Space(2)+
                        common.Space(9)+common.Space(2)+common.Space(9)+common.Space(2)+
                        common.Space(9)+common.Space(2)+common.Space(9)+common.Space(2)+
                        common.Space(6)+common.Space(2)+common.Rad("able",6)+common.Space(2)+
                        common.Space(6)+common.Space(2)+common.Space(6)+common.Space(2)+
                        common.Space(6)+common.Space(2)+
                        common.Space(6)+common.Space(2)+common.Rad("tive",6)+common.Space(2)+
                        common.Space(6)+common.Space(2)+common.Space(6)+common.Space(2)+
                        common.Rad("Effy",6)+common.Space(2)+
                        common.Rad("ted",6)+common.Space(2)+common.Rad("epted",6)+common.Space(2)+
                        common.Rad("Times",6)+common.Space(2)+common.Rad("Persons",6)+common.Space(2)+
                        common.Rad("Mints",6)+common.Space(2)+common.Rad("Mints",6);
          VStrl.addElement(Str1);

                 Str1 = common.Space(2)+common.Space(2)+common.Space(10)+common.Space(2)+
                        common.Rad("kgs",9)+common.Space(2)+common.Rad("kgs",9)+common.Space(2)+
                        common.Rad("kgs",9)+common.Space(2)+common.Rad("kgs",9)+common.Space(2)+
                        common.Rad("no",6)+common.Space(2)+common.Rad("no",6)+common.Space(2)+
                        common.Rad("no",6)+common.Space(2)+common.Rad("%",6)+common.Space(2)+
                        common.Rad("%",6)+common.Space(2)+
                        common.Rad("%",6)+common.Space(2)+common.Rad("%",6)+common.Space(2)+
                        common.Rad("%",6)+common.Space(2)+common.Rad("%",6)+common.Space(2)+
                        common.Rad("%",6)+common.Space(2)+
                        common.Rad("mnt",6)+common.Space(2)+common.Rad("mnt",6)+common.Space(2)+
                        common.Rad("no",6)+common.Space(2)+common.Rad("no",6)+common.Space(2)+
                        common.Rad("mnt",6)+common.Space(2)+common.Rad("mnt",6);
          VStrl.addElement(Str1);
          VStrl.addElement(SLine);
     }
     private void toStrl(String[] s1,String[] s2,String[] s3,String[] st,String dept)
     {
          iSlNo++;
          String Str1 = common.Rad(""+iSlNo,2)+common.Space(2)+dept+"\n";
          VStrl.addElement(Str1);

          try
          {
               Str1 = common.Space(2)+common.Space(2)+common.Pad("Shift 1",10);
               for(int j=0;j<19;j++)
                    Str1=Str1+common.Space(2)+common.Rad(s1[j],iW[j]);
               VStrl.addElement(Str1);
          }catch(Exception ex){System.out.println("1639 : "+ex+"  strl : "+Str1);}

          try
          {
               Str1 = common.Space(2)+common.Space(2)+common.Pad("Shift 2",10);
               for(int j=0;j<19;j++)
                    Str1=Str1+common.Space(2)+common.Rad(s2[j],iW[j]);
               VStrl.addElement(Str1);
          }catch(Exception ex){System.out.println("1639 : "+ex+"  strl : "+Str1);}

          try
          {
               Str1 = common.Space(2)+common.Space(2)+common.Pad("Shift 3",10);
               for(int j=0;j<19;j++)
                    Str1=Str1+common.Space(2)+common.Rad(s3[j],iW[j]);
               VStrl.addElement(Str1);
               VStrl.addElement(SLine);
          }catch(Exception ex){System.out.println("1639 : "+ex+"  strl : "+Str1);}

          try
          {
               Str1 = common.Space(2)+common.Space(2)+common.Pad("Total/Avg",10);
               for(int j=0;j<19;j++)
                    Str1=Str1+common.Space(2)+common.Rad(st[j],iW[j]);
               VStrl.addElement(Str1);
               VStrl.addElement(SLine);
          }catch(Exception ex){System.out.println("1639 : "+ex+"  strl : "+Str1);}
     }
     private void htmlDept(PrintWriter out,String SDept,int iCtr)
     {
          out.println("    <tr>");
          out.println("      <td width='28' height='138' bgcolor='"+tbgBody+"' valign='top' rowspan='4'><b><font size='4'>"+iCtr+"</font></b></td>");
          out.println("      <td width='3' height='38' bgcolor='"+tbgBody+"' valign='top'><p align='right'><font color='"+fgBody+"' size='4'><b><u>"+SDept+"</u> </b> Day</font></p></td>");
          out.println("      <td width='74' height='38' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S1[0]+"</font></td>");
          out.println("      <td width='74' height='38' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S1[1]+"</font></td>");
          out.println("      <td width='74' height='38' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S1[2]+"</font></td>");
          out.println("      <td width='74' height='38' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S1[3]+"</font></td>");
          out.println("      <td width='74' height='38' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S1[4]+"</font></td>");
          out.println("      <td width='74' height='38' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S1[5]+"</font></td>");
          out.println("      <td width='74' height='38' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S1[6]+"</font></td>");
          out.println("      <td width='74' height='38' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S1[7]+"</font></td>");
          out.println("      <td width='74' height='38' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S1[8]+"</font></td>");
          out.println("      <td width='74' height='38' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S1[9]+"</font></td>");
          out.println("      <td width='75' height='46' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S1[10]+"</font></td>");
          out.println("      <td width='75' height='46' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S1[11]+"</font></td>");
          out.println("      <td width='75' height='46' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S1[12]+"</font></td>");
          out.println("      <td width='75' height='46' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S1[13]+"</font></td>");
          out.println("      <td width='75' height='46' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S1[14]+"</font></td>");
          out.println("      <td width='75' height='46' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S1[15]+"</font></td>");
          out.println("      <td width='75' height='46' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S1[16]+"</font></td>");
          out.println("      <td width='75' height='46' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S1[17]+"</font></td>");
          out.println("      <td width='75' height='46' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S1[18]+"</font></td>");
          out.println("      <td width='75' height='46' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>0</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='3' height='32' bgcolor='"+tbgBody+"' align='right'><font color='"+fgBody+"' size='4'>HalfNight</font></td>");
          out.println("      <td width='74' height='32' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S2[0]+"</font></td>");
          out.println("      <td width='74' height='32' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S2[1]+"</font></td>");
          out.println("      <td width='74' height='32' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S2[2]+"</font></td>");
          out.println("      <td width='74' height='32' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S2[3]+"</font></td>");
          out.println("      <td width='74' height='32' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S2[4]+"</font></td>");
          out.println("      <td width='74' height='32' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S2[5]+"</font></td>");
          out.println("      <td width='74' height='32' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S2[6]+"</font></td>");
          out.println("      <td width='74' height='32' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S2[7]+"</font></td>");
          out.println("      <td width='74' height='32' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S2[8]+"</font></td>");
          out.println("      <td width='74' height='32' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S2[9]+"</font></td>");
          out.println("      <td width='75' height='26' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S2[10]+"</font></td>");
          out.println("      <td width='75' height='26' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S2[11]+"</font></td>");
          out.println("      <td width='75' height='26' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S2[12]+"</font></td>");
          out.println("      <td width='75' height='26' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S2[13]+"</font></td>");
          out.println("      <td width='75' height='26' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S2[14]+"</font></td>");
          out.println("      <td width='75' height='26' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S2[15]+"</font></td>");
          out.println("      <td width='75' height='26' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S2[16]+"</font></td>");
          out.println("      <td width='75' height='26' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S2[17]+"</font></td>");
          out.println("      <td width='75' height='26' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S2[18]+"</font></td>");
          out.println("      <td width='75' height='26' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>0</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='3' height='36' bgcolor='"+tbgBody+"' align='right'><font color='"+fgBody+"' size='4'>FullNight</font></td>");
          out.println("      <td width='74' height='36' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S3[0]+"</font></td>");
          out.println("      <td width='74' height='36' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S3[1]+"</font></td>");
          out.println("      <td width='74' height='36' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S3[2]+"</font></td>");
          out.println("      <td width='74' height='36' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S3[3]+"</font></td>");
          out.println("      <td width='74' height='36' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S3[4]+"</font></td>");
          out.println("      <td width='74' height='36' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S3[5]+"</font></td>");
          out.println("      <td width='74' height='36' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S3[6]+"</font></td>");
          out.println("      <td width='74' height='36' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S3[7]+"</font></td>");
          out.println("      <td width='74' height='36' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S3[8]+"</font></td>");
          out.println("      <td width='74' height='36' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S3[9]+"</font></td>");
          out.println("      <td width='75' height='30' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S3[10]+"</font></td>");
          out.println("      <td width='75' height='30' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S3[11]+"</font></td>");
          out.println("      <td width='75' height='30' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S3[12]+"</font></td>");
          out.println("      <td width='75' height='30' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S3[13]+"</font></td>");
          out.println("      <td width='75' height='30' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S3[14]+"</font></td>");
          out.println("      <td width='75' height='30' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S3[15]+"</font></td>");
          out.println("      <td width='75' height='30' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S3[16]+"</font></td>");
          out.println("      <td width='75' height='30' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S3[17]+"</font></td>");
          out.println("      <td width='75' height='30' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S3[18]+"</font></td>");
          out.println("      <td width='75' height='30' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>0</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='3' height='32' bgcolor='"+bgUom+"' align='right'><font color='"+fgUom+"' size='4'>Total</font></td>");
          out.println("      <td width='74' height='32' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+ST[0]+"</font></td>");
          out.println("      <td width='74' height='32' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+ST[1]+"</font></td>");
          out.println("      <td width='74' height='32' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+ST[2]+"</font></td>");
          out.println("      <td width='74' height='32' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+ST[3]+"</font></td>");
          out.println("      <td width='74' height='32' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+ST[4]+"</font></td>");
          out.println("      <td width='74' height='32' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+ST[5]+"</font></td>");
          out.println("      <td width='74' height='32' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+ST[6]+"</font></td>");
          out.println("      <td width='74' height='32' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+ST[7]+"</font></td>");
          out.println("      <td width='74' height='32' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+ST[8]+"</font></td>");
          out.println("      <td width='74' height='32' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+ST[9]+"</font></td>");
          out.println("      <td width='75' height='26' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+ST[10]+"</font></td>");
          out.println("      <td width='75' height='26' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+ST[11]+"</font></td>");
          out.println("      <td width='75' height='26' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+ST[12]+"</font></td>");
          out.println("      <td width='75' height='26' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+ST[13]+"</font></td>");
          out.println("      <td width='75' height='26' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+ST[14]+"</font></td>");
          out.println("      <td width='75' height='26' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+ST[15]+"</font></td>");
          out.println("      <td width='75' height='26' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+ST[16]+"</font></td>");
          out.println("      <td width='75' height='26' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+ST[17]+"</font></td>");
          out.println("      <td width='75' height='26' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+ST[18]+"</font></td>");
          out.println("      <td width='75' height='26' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>0</font></td>");
          out.println("    </tr>");
     }

	private void setCriteria(HttpServletRequest request)
	{
		SUnit            = request.getParameter("Unit");
		SStDate          = request.getParameter("Date");
		SEnDate          = request.getParameter("DateTo");		
                SStDate          = common.pureDate(SStDate);
		SEnDate          = common.pureDate(SEnDate);
		iUnit            = common.toInt(SUnit);
		System.out.println("MisOverall->" + SUnit + ":" + SStDate + ":" + SEnDate);
	}

	private Connection createConnection()
	{
		Connection conn;
		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
			conn  = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
		}catch(Exception ee)
		{
			return null;
		}
		return conn;
	}
        
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
