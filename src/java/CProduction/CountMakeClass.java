package CProduction;

import java.util.*;
public class CountMakeClass
{
     String SCount,SCountCode;
     int iEnDate;
     Vector VMakeClass;
     CountMakeClass(String SCountCode,String SCount,int iEnDate)
     {
          this.SCountCode = SCountCode;
          this.SCount     = SCount;
          this.iEnDate    = iEnDate;
          VMakeClass      = new Vector();
     }
     public void setData(String SMakeCode,String SMake,int iDate,double dGms,double dSpdl)
     {
          MakeClass mc = null;
          int index = indexOf(SCountCode);
          if(index == -1)
          {
               mc = new MakeClass(SMakeCode,SMake);
               VMakeClass.addElement(mc);
               index = VMakeClass.size()-1;
          }
          mc = (MakeClass)VMakeClass.elementAt(index);
          mc.setData(iDate,dGms,dSpdl);
          VMakeClass.setElementAt(mc,index);          
     }
     private int indexOf(String SMakeCode)
     {
          int index = -1;
          for(int i=0;i<VMakeClass.size();i++)
          {
               MakeClass mc = (MakeClass)VMakeClass.elementAt(i);
               if(mc.SMakeCode.equals(SMakeCode))
               {
                    index=i;
                    break;
               }
          }
          return index;
     }
     public double getMakeGms(String SMakeCode)
     {
          int index = indexOf(SMakeCode);
          if(index == -1)
               return 0;
          MakeClass mc = (MakeClass)VMakeClass.elementAt(index);
          return mc.getGms();
     }
     public double getGms()
     {
          double dGms=0;
          for(int i=0;i<VMakeClass.size();i++)
          {
               MakeClass mc = (MakeClass)VMakeClass.elementAt(i);
               dGms=dGms+mc.getGms();
          }
          return dGms;
     }
     public double getDayMakeGms(String SMakeCode)
     {
          int index = indexOf(SMakeCode);
          if(index == -1)
               return 0;
          MakeClass mc = (MakeClass)VMakeClass.elementAt(index);
          return mc.getDayGms(iEnDate);
     }
     public double getDayGms()
     {
          double dGms=0;
          for(int i=0;i<VMakeClass.size();i++)
          {
               MakeClass mc = (MakeClass)VMakeClass.elementAt(i);
               dGms=dGms+mc.getDayGms(iEnDate);
          }
          return dGms;
     }

}
