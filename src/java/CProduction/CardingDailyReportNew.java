package CProduction;

import java.io.*;
import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
public class CardingDailyReportNew extends HttpServlet
{
     HttpSession    session;
     Common common;

     Vector VDate,VUnit,VShift,VMachNo,VCount,VTicket,VName;
     Vector VOrderNo,VShade,VSpeed,VSlHank,VHank;
     Vector VHourMeter,VHourRun,VDraft;
     Vector VHankExp,VHankTar,VProd,VProdExp,VProdTar;
     Vector VStopAcc,VStopNonAcc,VWorkerEffy,VProdLoss,VActualEffy;
     Vector VUEM,VUEOA,VEffyAvai,VHourAllot;
     Vector VNoRunOut,VNoPersons,VRoTime,VFeed,VWM,VBreaks;
     Vector VCardClass;

     String SDate,SShift,SUnit,SFromDate,SToDate,SMachCode,SType;
     String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody,tbgBody;

     FileWriter  FW;
     int Lctr = 70,Pctr = 0;
     String SLine;
     Vector VHead;
	boolean bFinalized;
	Control control = new Control();

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
          common = new Common();

          bgColor = common.bgColor;
          bgHead  = common.bgHead;
          bgUom   = common.bgUom;
          bgBody  = common.bgBody;
          fgHead  = common.fgHead;
          fgUom   = common.fgUom;
          fgBody  = common.fgBody;
		tbgBody = common.tbgBody;
          SLine   = common.Replicate("-",229)+"\n";
     }

     public void doGet(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
            doPost(request,response);
     }
     public void doPost(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
		response.setContentType("text/html");
          PrintWriter out  = response.getWriter();
		session = request.getSession(true);
		/*if(session.getValue("AppletTag")==null)
		{
			String SServer = control.getID("Select ProdHost from Server");
			response.sendRedirect(SServer + "/SessionExpired");
			return;

		}*/
		setCriteria(request);
		setFinalized();
          out.println("<html>");
          out.println("<head>");
          out.println("<title>Carding Production Report </title>");
          out.println("</head>");
          out.println("<body bgcolor='" + common.bgBody + "'>");
		//out.println(session.getValue("AppletTag"));
          out.println("<a href='Header'>Home</a>");
	  out.println(common.getDailyReportHeader("Carding Production Report (F CDG 1)",SUnit,SShift,SFromDate,getReportType()));
          out.println("  <table border='1' cellspacing='0' >");
          out.println("    <tr>");
          if(SType.equals("9999"))
          {
              out.println("      <td width='59' height='82' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Mach Nº</font></td>");
              out.println("      <td width='59' height='82' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Date</font></td>");
          }
          else
          {
              out.println("      <td width='59' height='82' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Date</font></td>");
              out.println("      <td width='59' height='82' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Mach Nº</font></td>");
          
          }
          out.println("      <td width='59' height='30' valign='middle' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Shift</font></td>");
          out.println("      <td width='59' height='30' valign='middle' align='center' bgcolor='"+bgHead+"'>");
          out.println("       <font size='4' color='"+fgHead+"'>");
          out.println("       Production </font></td>");
          out.println("      <td width='59' height='30' valign='middle' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Utilization (U&E OA)</font></td>");
          out.println("      <td width='59' height='30' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Worker Efficiency *%)</font></td>");
          out.println("      <td width='59' height='30' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>BreaksPer100Km</font></td>");
          
          out.println("    </tr>");
/*        out.println("    <tr>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'></td>");          
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='60' height='1' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("    </tr>");*/
/*
          out.println("    <tr>");
          out.println("      <td width='1062' height='38' valign='top' bgcolor='"+bgUom+"' colspan='18'><font color='"+fgHead+"' size='5'><b>Target</b></font></td>");
          out.println("      <td width='59' height='38' valign='top' bgcolor='"+bgUom+"' align='right'>&nbsp;</td>");
          out.println("      <td width='118' height='38' valign='top' bgcolor='"+bgUom+"' align='right' colspan='2'>&nbsp;</td>");
          out.println("      <td width='59' height='38' valign='top' bgcolor='"+bgUom+"' align='right'>&nbsp;</td>");
          out.println("      <td width='59' height='38' valign='top' bgcolor='"+bgUom+"' align='right'>&nbsp;</td>");
          out.println("      <td width='59' height='38' valign='top' bgcolor='"+bgUom+"' align='right'>&nbsp;</td>");
          out.println("      <td width='60' height='38' valign='top' bgcolor='"+bgUom+"' align='right'>&nbsp;</td>");
          out.println("      <td width='60' height='38' valign='top' bgcolor='"+bgUom+"' align='right'>&nbsp;</td>");
          out.println("      <td width='60' height='38' valign='top' bgcolor='"+bgUom+"' align='right'>&nbsp;</td>");
          out.println("    </tr>");
*/
          setVectorData();
          calcVectors();

          try
          {
               //FW      = new FileWriter("//home/arun/reports/cardc.prn");
//               FW      = new FileWriter("/software/C-Prod-Print/Reports/cardc.prn");
              FW      = new FileWriter("D:/C-Prod-Print/cardc.prn");
              System.out.println("FW-->"+FW);
               Lctr    = 70;
               Pctr    =  0;
               Head();
               Body();
               FW.close();
          }catch(Exception ex){}
          String STempMachNo="-1";
          String STempDate="-1";

          for(int i=0;i<VMachNo.size();i++)
          {
               out.println("    <tr>");
               if(SType.equals("9999"))
               {
                   if(!STempMachNo.equals((String)VMachNo.elementAt(i)))
                   {
                       out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"'><font   color='"+fgBody+"'>"+(String)VMachNo.elementAt(i)+"</font></td>");    
                       STempMachNo=(String)VMachNo.elementAt(i);
                   }    
                   else
                   {
                       out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"'><font   color='"+fgBody+"'></font></td>");    
                   }
                   out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"'><font   color='"+fgBody+"'>"+(String)VDate.elementAt(i)+"</font></td>");    
               }
               else
               {
                   if(!STempDate.equals((String)VDate.elementAt(i)))
                   {
                       out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"'><font   color='"+fgBody+"'>"+(String)VDate.elementAt(i)+"</font></td>");    
                       STempDate=(String)VDate.elementAt(i);
                   }    
                   else
                   {
                       out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"'><font   color='"+fgBody+"'></font></td>");    
                   }
                   out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"'><font   color='"+fgBody+"'>"+(String)VMachNo.elementAt(i)+"</font></td>");    
               }
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>"+(String)VShift .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>"+(String)VProd .elementAt(i)+"</font></td>");               
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>"+common.getRound((String)VUEOA .elementAt(i),2)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>"+common.getRound((String)VWorkerEffy .elementAt(i),2)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>"+common.getRound((String)VBreaks .elementAt(i),2)+"</font></td>");
               
               out.println("    </tr>");
          }

          //Sum Display
          out.println("    <tr>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"'><font size='4'   color='"+fgUom+"'>.</font></td>");          
          out.println("      <td width='100' height='5' valign='top' bgcolor='"+bgUom+"'><font size='4'  color='"+fgUom+"'>&nbsp;</font></td>");
          out.println("      <td width='100' height='5' valign='top' bgcolor='"+bgUom+"'><font size='4'  color='"+fgUom+"'>Total/Avg</font></td>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font  size='4' color='"+fgUom+"'>"+common.getSum(VProd ,"S")+"</font></td>");          
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font  size='4' color='"+fgUom+"'>"+common.getSum(VUEOA ,"A")+"</font></td>");          
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font  size='4' color='"+fgUom+"'>"+common.getSum(VWorkerEffy ,"A")+"</font></td>");
           out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font  size='4' color='"+fgUom+"'>"+common.getSum(VBreaks ,"A")+"</font></td>");
          out.println("    </tr>");

          out.println("  </table>");
          out.println("</body>");
          out.println("</html>");

          out.close();
     }
     private void setVectorData()
     {
		 String SQ1="",SQ2="",SQ3="";
		 VCardClass = new Vector();

           VDate  = new Vector();
           VUnit  = new Vector();
           VShift = new Vector();
           VMachNo= new Vector();
           VCount = new Vector();
           VTicket= new Vector();
           VName  = new Vector();
     
           VSpeed       = new Vector();
           VDraft       = new Vector();
           VSlHank      = new Vector();
           VHankExp     = new Vector();
           VHankTar     = new Vector();
           VHank        = new Vector();
           VProdExp     = new Vector();
           VProdTar     = new Vector();
           VProd        = new Vector();
           VProdLoss    = new Vector();
           VActualEffy  = new Vector();
           VHourRun     = new Vector();
           VHourMeter   = new Vector();
           VUEOA        = new Vector();
           VUEM         = new Vector();
           VEffyAvai    = new Vector();
           VWorkerEffy  = new Vector();
           VBreaks      = new Vector();
           VStopAcc     = new Vector();
           VStopNonAcc  = new Vector();
           VHourAllot   = new Vector();
           VNoRunOut    = new Vector();
           VNoPersons   = new Vector();
           VRoTime      = new Vector();
           VFeed        = new Vector();
           VWM          = new Vector();
                SQ1 = getQString();
                SQ3 = getAbsQS();

             try
             {
                     Connection con = createConnection();
                     Statement stat = con.createStatement();
                     ResultSet res  = stat.executeQuery(SQ1);
                     while(res.next())
                     {
                          VDate  .addElement(common.parseDate(res.getString(1))); 
                          VMachNo.addElement(res.getString(2));
                          VProdExp     .addElement(res.getString(3));
                          VProdTar     .addElement(res.getString(4));
                          VProd        .addElement(res.getString(5));
                          VHourRun     .addElement(res.getString(6));
                          VHourMeter   .addElement(res.getString(7));
                          VUEOA        .addElement(res.getString(8));
                          VUEM         .addElement(res.getString(9));
                          VWorkerEffy  .addElement(res.getString(10));
                          VHourAllot   .addElement(res.getString(11));
                          VBreaks      .addElement(res.getString(12));
                          VShift       .addElement(res.getString(13));
                     }
                     res.close();

                     ResultSet res6 = stat.executeQuery(SQ3);
                     while(res6.next())
                         organizeCardClass(res6);

                     res6.close();
                     con.close();
                     for(int i=0;i<VMachNo.size();i++)
                     {
                         int index=common.toInt((String)VWM.elementAt(i));
                         String SWM    = (index==0?"M":"W");
                         String SMachNo=((String)VMachNo.elementAt(i)).trim();
                         VMachNo.setElementAt(SMachNo+"-"+SWM,i);
                     }
             }
             catch(Exception ex)
             {
                     System.out.println("From Card.java : 252 "+ex);
             }
     }
     private void organizeCardClass(ResultSet res6) throws Exception
     {
          int    iCode = res6.getInt(1);
          String SName = res6.getString(2);
          int    iWM   = res6.getInt(3);
          double dProd = res6.getDouble(4);

          int index = indexOf(iCode);
          CardClass cc = null;
          if(index==-1)
          {
               cc = new CardClass(iCode,SName);
               VCardClass.addElement(cc);
               index = VCardClass.size()-1;
          }
          cc = (CardClass)VCardClass.elementAt(index);
          cc.append(iWM,dProd);
     }
     private int indexOf(int iCode)
     {
          int index=-1;
          for(int i=0;i<VCardClass.size();i++)
          {
               CardClass cc = (CardClass)VCardClass.elementAt(i);
               if(cc.iWebCode==iCode)
               {
                    index = i;
                    break;
               }
          }
          return index;
     }
     private void calcVectors()
     {
               for(int i=0;i<VMachNo.size();i++)
               {
                     double dProdTar = common.toDouble((String)VProdTar.elementAt(i));
                     double dProd    = common.toDouble((String)VProd.elementAt(i));
                     double dProdExp = common.toDouble((String)VProdExp.elementAt(i));
                     double dHal     = common.toDouble((String)VHourAllot.elementAt(i));
                     VProdLoss    .addElement((String)common.getRound(dProd-dProdTar,3));
                     VActualEffy  .addElement((String)common.getRound((dProd/dProdExp*100),3));
               }
     }

     private String getQString()
     {
		String QString = " SELECT  cardtran.sp_date, machine.mach_name, " + 
					"  sum(cardtran.prod_exp),sum(cardtran.prod_tar),sum(cardtran.prod)," + 
					" sum(cardtran.hourrun),sum(cardtran.hourmeter),Avg(cardtran.ueoa),Avg(cardtran.uem)," + 
					" Avg(cardtran.workereffy),sum(cardtran.houralloted),Avg(BreaksPer100Km),CardTran.Shift_Code" + 
					" From ((((cardtran Left JOIN unit ON cardtran.unit_code = unit.unitcode) " + 
					" Left JOIN machine ON cardtran.mach_code = machine.mach_code)" + 
					" Left JOIN count ON cardtran.count_Code = count.count_code) " + 
					" Left JOIN SHADE ON cardtran.shade_code = SHADE.shade_code) " + 
					" Where cardtran.sp_date>='"+SFromDate+"' and cardtran.sp_date<='"+SToDate+"'   and " + 
					" CardTran.Unit_Code = "+SUnit;
                        if(!SMachCode.equals("9999"))
                                QString= QString+" and Machine.Mach_Code="+SMachCode;
                            
                                    QString= QString+" group by CardTran.sp_date,Machine.Mach_Name,CardTran.Shift_Code ";
                        if(SType.equals("9999"))                                        
                                    QString= QString+" ORDER BY to_number(machine.mach_name),1,CardTran.Shift_Code";
                        else
                                    QString= QString+" ORDER BY 1,to_number(machine.mach_name),CardTran.Shift_Code" ;
		   System.out.println("SQ-->"+QString);
             return QString;
     }
     
     private String getAbsQS()
     {
          String QS="";
          QS = " SELECT cardtran.web_code,WebType.Web_Name, cardtran.wm, Sum(cardtran.prod) AS SumOfprod "+
               " FROM cardtran INNER JOIN WebType ON cardtran.WEB_code = WebType.Web_Code "+
               " WHERE cardtran.unit_code="+SUnit+" AND "+ " cardtran.sp_date>='"+SFromDate+"' and cardtran.sp_date<='"+SToDate+"'"+ // AND "+
               " GROUP BY WebType.Web_Name, cardtran.wm,cardtran.web_code";
          System.out.println("Abs-->"+QS);
          return QS;          
     }
	  private String getTicket(String SEmpCode)
	  {	
		   String SRet="UnKnown";
		   if(SEmpCode.equals("0"))
			   return SRet;
		   try
		   {			   
			   String SQry = " select empname from hrdnew.SchemeApprentice where empcode="+SEmpCode+" union " +
							 " select empname from hrdnew.ActApprentice where empcode="+SEmpCode+" union  " + 
							 " select empname from hrdnew.ContractApprentice where empcode="+SEmpCode;
			   Class.forName("oracle.jdbc.OracleDriver");
			   Connection conn = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
			   Statement stat  = conn.createStatement();
			   ResultSet res   = stat.executeQuery(SQry);
			   if(res.next())
				   SRet            = res.getString(1);

                           res.close();
                           stat.close();  
//			   System.out.println(SQry + ":" + SRet);
		   }catch(Exception ee)
		   {
			   System.out.println("getTicket->" + ee);
			   return SRet;
		   }
		   return SRet;
      }





     private void Head()
     {
          if(Lctr < 60)
             return;
          if(Pctr > 0)
          {
             try
             {
               FW.write(SLine+"\n");
             }catch(Exception ex){}
          }
          Pctr++;

          String Str1    = common.Pad("Company    : AMARJOTHI SPINNING MILLS LIMITED",80);
          String Str2    = common.Pad("Document   : Carding Periodical Report From  "+common.parseDate(SFromDate)+" To  "+common.parseDate(SToDate),80);
          String Str3    = common.Pad("Unit       : "+(SUnit.equals("1")?"A":(SUnit.equals("2")?"B":"S")),70);
          String Str4    = common.Pad("Shift      : "+(SShift.equals("1")?"Day":(SShift.equals("2")?"Half Night":"Full Night")),70);
          VHead = Spin1Head();
          try
          {
               FW.write(""+Str1+"\n");
               FW.write(Str2+"\n");
               FW.write(Str3+"\n");
               FW.write(Str4+"\n");
               for(int i=0;i<VHead.size();i++)
               {
                    FW.write((String)VHead.elementAt(i));
               }
          }catch(Exception ex){}
          Lctr = 4+VHead.size();
     }
     private void Body()
     {
         String Strl = "";
         for(int i=0;i<VMachNo.size();i++)
         {
                Strl  = common.Pad((String)VMachNo.elementAt(i),4)+common.Space(2)+
                        common.Pad((String)VTicket.elementAt(i),6)+common.Space(2)+
                        common.Pad((String)VName.elementAt(i),10)+common.Space(2)+
                        common.Pad((String)VCount.elementAt(i),10)+common.Space(2)+
                        common.Rad((String)VSpeed.elementAt(i),3)+common.Space(2)+
                        common.Rad((String)VDraft.elementAt(i),7)+common.Space(2)+
                        common.Rad((String)VSlHank.elementAt(i),7)+common.Space(2)+
                        common.Rad((String)VNoRunOut.elementAt(i),3)+common.Space(2)+
                        common.Rad((String)VNoPersons.elementAt(i),3)+common.Space(2)+
                        common.Rad((String)VRoTime.elementAt(i),4)+common.Space(2)+
                        common.Rad(" ",4)+common.Space(2)+
                        common.Rad((String)VHankExp.elementAt(i),9)+common.Space(2)+
                        common.Rad((String)VHankTar.elementAt(i),9)+common.Space(2)+
                        common.Rad((String)VHank.elementAt(i),9)+common.Space(2)+
                        common.Rad((String)VProdExp.elementAt(i),9)+common.Space(2)+
                        common.Rad((String)VProdTar.elementAt(i),9)+common.Space(2)+
                        common.Rad((String)VProd.elementAt(i),9)+common.Space(2)+
                        common.Rad((String)VProdLoss.elementAt(i),9)+common.Space(2)+
                        common.Rad((String)VActualEffy.elementAt(i),7)+common.Space(2)+
                        common.Rad((String)VHourRun.elementAt(i),5)+common.Space(2)+
                        common.Rad((String)VHourMeter.elementAt(i),5)+common.Space(2)+
                        common.Rad((String)VUEOA.elementAt(i),6)+common.Space(2)+
                        common.Rad((String)VUEM.elementAt(i),6)+common.Space(2)+
                        common.Rad((String)VEffyAvai.elementAt(i),6)+common.Space(2)+
                        common.Rad((String)VWorkerEffy.elementAt(i),6)+common.Space(2)+
                        common.Rad((String)VStopAcc.elementAt(i),6)+common.Space(2)+
                        common.Rad((String)VStopNonAcc.elementAt(i),6)+"\n";

                try
                {
                     FW.write(Strl);
                     Lctr++;
                     Head();
                }catch(Exception ex){}
         }

           try
           {
               FW.write(SLine);                   
           }catch(Exception ex){}

           Strl  = common.Pad(" ",4)+common.Space(2)+
                   common.Pad(" ",6)+common.Space(2)+
                   common.Pad("Total",10)+common.Space(2)+
                   common.Pad(" ",10)+common.Space(2)+
                   common.Rad(common.getSum(VSpeed,"A"),3)+common.Space(2)+
                   common.Rad(common.getSum(VDraft,"A"),7)+common.Space(2)+
                   common.Rad(common.getSum(VSlHank,"A"),7)+common.Space(2)+
                   common.Rad(" ",3)+common.Space(2)+
                   common.Rad(" ",3)+common.Space(2)+
                   common.Rad(" ",4)+common.Space(2)+
                   common.Rad(" ",4)+common.Space(2)+
                   common.Rad(common.getSum(VHankExp,"S"),9)+common.Space(2)+
                   common.Rad(common.getSum(VHankTar,"S"),9)+common.Space(2)+
                   common.Rad(common.getSum(VHank,"S"),9)+common.Space(2)+
                   common.Rad(common.getSum(VProdExp,"S"),9)+common.Space(2)+
                   common.Rad(common.getSum(VProdTar,"S"),9)+common.Space(2)+
                   common.Rad(common.getSum(VProd,"S"),9)+common.Space(2)+
                   common.Rad(common.getSum(VProdLoss,"S"),9)+common.Space(2)+
                   common.Rad(common.getSum(VActualEffy,"A"),7)+common.Space(2)+
                   common.Rad(common.getSum(VHourRun,"S"),5)+common.Space(2)+
                   common.Rad(common.getSum(VHourMeter,"S"),5)+common.Space(2)+
                   common.Rad(common.getSum(VUEOA,"A"),6)+common.Space(2)+
                   common.Rad(common.getSum(VUEM,"A"),6)+common.Space(2)+
                   common.Rad(common.getSum(VEffyAvai,"A"),6)+common.Space(2)+
                   common.Rad(common.getSum(VWorkerEffy,"A"),6)+common.Space(2)+
                   common.Rad(common.getSum(VStopAcc,"S"),6)+common.Space(2)+
                   common.Rad(common.getSum(VStopNonAcc,"S"),6)+"\n";

           try
           {
               FW.write(Strl);
               FW.write(SLine);
           }catch(Exception ex){}

           try
           {
/*             FW.write(getFootString(0,0,"Lap Melange"));
               FW.write(getFootString(0,1,"Lap White"));
               FW.write(getFootString(0,-1,"Lap Total"));
               FW.write("\n");

               FW.write(getFootString(1,0,"Chute Melange"));
               FW.write(getFootString(1,1,"Chute White"));
               FW.write(getFootString(1,-1,"Chute Total"));
               FW.write("\n");

               FW.write(getFootString(0,"Melange Total"));
               FW.write(getFootString(1,"White Total"));
               FW.write("\n"); */
           }catch(Exception ex){}

           try
           {
               FW.write(SLine+"\n");
               Lctr+=15;
               prtWebAbs();
               FW.write("\n\nP.O          S.P.O.           S.M.             D.M.\n");
               FW.write("<End Of Report>");
           }catch(Exception ex){}

     }
     private String getFootString(int iFeed,int iWM,String str)
     {
           String Strl  = common.Pad(" ",4)+common.Space(2)+
                  common.Pad(" ",6)+common.Space(2)+
                  common.Pad(str,10)+common.Space(2)+
                  common.Pad(" ",10)+common.Space(2)+
                  common.Rad(getSum(VSpeed,"A",iFeed,iWM),3)+common.Space(2)+
                  common.Rad(getSum(VDraft,"A",iFeed,iWM),7)+common.Space(2)+
                  common.Rad(getSum(VSlHank,"S",iFeed,iWM),7)+common.Space(2)+
                  common.Rad(" ",3)+common.Space(2)+
                  common.Rad(" ",3)+common.Space(2)+
                  common.Rad(" ",4)+common.Space(2)+
                  common.Rad(" ",4)+common.Space(2)+
                  common.Rad(getSum(VHankExp,"S",iFeed,iWM),9)+common.Space(2)+
                  common.Rad(getSum(VHankTar,"S",iFeed,iWM),9)+common.Space(2)+
                  common.Rad(getSum(VHank,"S",iFeed,iWM),9)+common.Space(2)+
                  common.Rad(getSum(VProdExp,"S",iFeed,iWM),9)+common.Space(2)+
                  common.Rad(getSum(VProdTar,"S",iFeed,iWM),9)+common.Space(2)+
                  common.Rad(getSum(VProd,"S",iFeed,iWM),9)+common.Space(2)+
                  common.Rad(getSum(VProdLoss,"S",iFeed,iWM),9)+common.Space(2)+
                  common.Rad(getSum(VActualEffy,"A",iFeed,iWM),7)+common.Space(2)+
                  common.Rad(getSum(VHourRun,"S",iFeed,iWM),5)+common.Space(2)+
                  common.Rad(getSum(VHourMeter,"S",iFeed,iWM),5)+common.Space(2)+
                  common.Rad(getSum(VUEOA,"A",iFeed,iWM),6)+common.Space(2)+
                  common.Rad(getSum(VUEM,"A",iFeed,iWM),6)+common.Space(2)+
                  common.Rad(getSum(VEffyAvai,"A",iFeed,iWM),6)+common.Space(2)+
                  common.Rad(getSum(VWorkerEffy,"A",iFeed,iWM),6)+common.Space(2)+
                  common.Rad(getSum(VStopAcc,"S",iFeed,iWM),6)+common.Space(2)+
                  common.Rad(getSum(VStopNonAcc,"S",iFeed,iWM),6)+"\n";

          return Strl;

     }
     private String getFootString(int iWM,String str)
     {
           String Strl  = common.Pad(" ",4)+common.Space(2)+
                  common.Pad(" ",6)+common.Space(2)+
                  common.Pad(str,10)+common.Space(2)+
                  common.Pad(" ",10)+common.Space(2)+
                  common.Rad(getSum(VSpeed,"A",iWM),3)+common.Space(2)+
                  common.Rad(getSum(VDraft,"A",iWM),7)+common.Space(2)+
                  common.Rad(getSum(VSlHank,"S",iWM),7)+common.Space(2)+
                  common.Rad(" ",3)+common.Space(2)+
                  common.Rad(" ",3)+common.Space(2)+
                  common.Rad(" ",4)+common.Space(2)+
                  common.Rad(" ",4)+common.Space(2)+
                  common.Rad(getSum(VHankExp,"S",iWM),9)+common.Space(2)+
                  common.Rad(getSum(VHankTar,"S",iWM),9)+common.Space(2)+
                  common.Rad(getSum(VHank,"S",iWM),9)+common.Space(2)+
                  common.Rad(getSum(VProdExp,"S",iWM),9)+common.Space(2)+
                  common.Rad(getSum(VProdTar,"S",iWM),9)+common.Space(2)+
                  common.Rad(getSum(VProd,"S",iWM),9)+common.Space(2)+
                  common.Rad(getSum(VProdLoss,"S",iWM),9)+common.Space(2)+
                  common.Rad(getSum(VActualEffy,"A",iWM),7)+common.Space(2)+
                  common.Rad(getSum(VHourRun,"S",iWM),5)+common.Space(2)+
                  common.Rad(getSum(VHourMeter,"S",iWM),5)+common.Space(2)+
                  common.Rad(getSum(VUEOA,"A",iWM),6)+common.Space(2)+
                  common.Rad(getSum(VUEM,"A",iWM),6)+common.Space(2)+
                  common.Rad(getSum(VEffyAvai,"A",iWM),6)+common.Space(2)+
                  common.Rad(getSum(VWorkerEffy,"A",iWM),6)+common.Space(2)+
                  common.Rad(getSum(VStopAcc,"S",iWM),6)+common.Space(2)+
                  common.Rad(getSum(VStopNonAcc,"S",iWM),6)+"\n";

          return Strl;
     }

     private Vector Spin1Head()
     {
          Vector VHead = new Vector();

          String Str2 = common.Space(4)+common.Space(2)+
                        common.Space(6)+common.Space(2)+
                        common.Space(10)+common.Space(2)+
                        common.Space(10)+common.Space(2)+
                        common.Space(3)+common.Space(2)+
                        common.Space(7)+common.Space(2)+
                        common.Space(7)+common.Space(2)+
                        common.Cad("Shade Change",20)+common.Space(2)+
                        common.Cad("Production Details",84)+common.Space(2)+
                        common.Cad("Utilization",28)+common.Space(2)+
                        common.Cad("Worker Effy",30)+"\n";

          String Str3 = common.Pad("Mach",4)+common.Space(2)+
                        common.Pad("Ticket",6)+common.Space(2)+
                        common.Pad("Sider",10)+common.Space(2)+
                        common.Pad("Count",10)+common.Space(2)+
                        common.Pad("Dof",3)+common.Space(2)+
                        common.Pad("Tension",7)+common.Space(2)+
                        common.Pad("Sliver",7)+common.Space(2)+
                        common.Replicate("-",20)+common.Space(2)+
                        common.Replicate("-",84)+common.Space(2)+
                        common.Replicate("-",28)+common.Space(2)+
                        common.Replicate("-",30)+"\n";

          String Str4 = common.Pad("No",4)+common.Space(2)+
                        common.Pad("No",6)+common.Space(2)+
                        common.Pad("Name",10)+common.Space(2)+
                        common.Pad("Shade",10)+common.Space(2)+
                        common.Rad("Spe",3)+common.Space(2)+
                        common.Rad("Draft",7)+common.Space(2)+
                        common.Rad("Hank",7)+common.Space(2)+
                        common.Rad("No",3)+common.Space(2)+
                        common.Rad("Per",3)+common.Space(2)+
                        common.Rad("Actual",4)+common.Space(2)+
                        common.Rad("Exc/",4)+common.Space(2)+
                        common.Rad("Exp Hank",9)+common.Space(2)+
                        common.Rad("Tar Hank",9)+common.Space(2)+
                        common.Rad("Act Hank",9)+common.Space(2)+
                        common.Rad("Exp Prod",9)+common.Space(2)+
                        common.Rad("Tar Prod",9)+common.Space(2)+
                        common.Rad("Act Prod",9)+common.Space(2)+
                        common.Rad("Prod Loss",9)+common.Space(2)+
                        common.Rad("ActEffy",7)+common.Space(2)+
                        common.Rad("Hour",5)+common.Space(2)+
                        common.Rad("Hour",5)+common.Space(2)+
                        common.Rad("UxE",6)+common.Space(2)+
                        common.Rad("UxE",6)+common.Space(2)+
                        common.Rad("Effy",6)+common.Space(2)+
                        common.Rad("Worker",6)+common.Space(2)+
                        common.Rad("Acc",6)+common.Space(2)+
                        common.Rad("NonAcc",6)+"\n";

          String Str5 = common.Pad(" ",4)+common.Space(2)+
                        common.Pad(" ",6)+common.Space(2)+
                        common.Pad(" ",10)+common.Space(2)+
                        common.Pad(" ",10)+common.Space(2)+
                        common.Rad(" ",3)+common.Space(2)+
                        common.Rad(" ",7)+common.Space(2)+
                        common.Rad(" ",7)+common.Space(2)+
                        common.Rad(" ",3)+common.Space(2)+
                        common.Rad("son",3)+common.Space(2)+
                        common.Rad(" ",4)+common.Space(2)+
                        common.Rad("Shor",4)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad("A - T",9)+common.Space(2)+
                        common.Rad("A / E",7)+common.Space(2)+
                        common.Rad("toWork",5)+common.Space(2)+
                        common.Rad("Meter",5)+common.Space(2)+
                        common.Rad("(OA)",6)+common.Space(2)+
                        common.Rad("(MU)",6)+common.Space(2)+
                        common.Rad("Avail",6)+common.Space(2)+
                        common.Rad("Effy",6)+common.Space(2)+
                        common.Rad("Stop",6)+common.Space(2)+
                        common.Rad("Stop",6)+"\n";

          String Str6 = common.Pad(" ",4)+common.Space(2)+
                        common.Pad(" ",6)+common.Space(2)+
                        common.Pad(" ",10)+common.Space(2)+
                        common.Pad(" ",10)+common.Space(2)+
                        common.Rad("mpm",3)+common.Space(2)+
                        common.Rad(" ",7)+common.Space(2)+
                        common.Rad(" ",7)+common.Space(2)+
                        common.Rad(" ",3)+common.Space(2)+
                        common.Rad(" ",3)+common.Space(2)+
                        common.Rad("min",4)+common.Space(2)+
                        common.Rad("min",4)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad("in Kgs",9)+common.Space(2)+
                        common.Rad("in Kgs",9)+common.Space(2)+
                        common.Rad("in Kgs",9)+common.Space(2)+
                        common.Rad("in Kgs",9)+common.Space(2)+
                        common.Rad("100%",7)+common.Space(2)+
                        common.Rad("Hr",5)+common.Space(2)+
                        common.Rad("Hr",5)+common.Space(2)+
                        common.Rad("80%",6)+common.Space(2)+
                        common.Rad("85%",6)+common.Space(2)+
                        common.Rad("%",6)+common.Space(2)+
                        common.Rad("95%",6)+common.Space(2)+
                        common.Rad("min",6)+common.Space(2)+
                        common.Rad("min",6)+"\n";

          String StrL = common.Space(4)+common.Space(2)+
                        common.Space(6)+common.Space(2)+
                        common.Space(10)+common.Space(2)+
                        common.Space(10)+common.Space(2)+
                        common.Replicate("-",3)+common.Space(2)+
                        common.Replicate("-",7)+common.Space(2)+
                        common.Replicate("-",7)+common.Space(2)+
                        common.Replicate("-",20)+common.Space(2)+
                        common.Replicate("-",84)+common.Space(2)+
                        common.Replicate("-",28)+common.Space(2)+
                        common.Replicate("-",30)+"\n";

          VHead.addElement(SLine);
          VHead.addElement(Str2);
          VHead.addElement(Str3);
          VHead.addElement(Str4);
          VHead.addElement(Str5);
          VHead.addElement(StrL);
          VHead.addElement(Str6);
          VHead.addElement(SLine);

          return VHead;
     }
     private String getSum(Vector Vx,String SOp,int iWM)
     {
            double dAmount=0;
            int    iCtr=0;
            for(int i=0;i<Vx.size();i++)
            {
                if(common.toInt((String)VWM.elementAt(i))==iWM)
                {
                      dAmount = dAmount+common.toDouble((String)Vx.elementAt(i));
                      iCtr++;
                }
            }
            if(SOp.equals("A"))
            {
              dAmount = iCtr==0?0:dAmount/iCtr;
            }
            return common.getRound(dAmount,2);
      }
      private String getSum(Vector Vx,String SOp,int iFeed,int iWM)
      {
            double dAmount=0;
            int    iCtr=0;
            for(int i=0;i<Vx.size();i++)
            {
                if(common.toInt((String)VFeed.elementAt(i))==iFeed)
                {
                      if(iWM == -1)
                      {
                         dAmount = dAmount+common.toDouble((String)Vx.elementAt(i));
                         iCtr++;
                      }
                      else
                      {
                         if(common.toInt((String)VWM.elementAt(i))==iWM)
                         {
                              dAmount = dAmount+common.toDouble((String)Vx.elementAt(i));
                              iCtr++;
                         }
                      }
                }
            }
            if(SOp.equals("A"))
            {
              dAmount = iCtr==0?0:dAmount/iCtr;
            }
            return common.getRound(dAmount,2);
      }
     private void prtWebAbs() throws Exception
     {
          if(Lctr > 56)
               FW.write("\n");

          double dW=0,dM=0;
          FW.write("Web Typewise Abstract\n");
          FW.write(common.Replicate("-",15+2+10+2+10+2+10)+"\n");
          FW.write(common.Pad("Web Type",15)+common.Space(2)+common.Rad("Melange",10)+common.Space(2)+common.Rad("White",10)+common.Space(2)+common.Rad("Total",10)+"\n");
          FW.write(common.Replicate("-",15+2+10+2+10+2+10)+"\n");
          for(int i=0;i<VCardClass.size();i++)
          {
               CardClass cc = (CardClass)VCardClass.elementAt(i);
               FW.write(common.Pad(cc.SWebName,15)+common.Space(2)+common.Rad(common.getRound(cc.dWhite,2),10)+common.Space(2)+common.Rad(common.getRound(cc.dMelange,2),10)+common.Space(2)+common.Rad(common.getRound(cc.dWhite+cc.dMelange,2),10)+"\n");
               dW += cc.dWhite;
               dM += cc.dMelange;
          }
          FW.write(common.Replicate("-",15+2+10+2+10+2+10)+"\n");
          FW.write(common.Pad("Total",15)+common.Space(2)+common.Rad(common.getRound(dW,2),10)+common.Space(2)+common.Rad(common.getRound(dM,2),10)+common.Space(2)+common.Rad(common.getRound(dW+dM,2),10)+"\n");
          FW.write(common.Replicate("-",15+2+10+2+10+2+10)+"\n");
     }

	private void setCriteria(HttpServletRequest request)
	{
		SFromDate            = common.pureDate(request.getParameter("Date1"));
                SToDate              = common.pureDate(request.getParameter("Date2"));
		SUnit            = request.getParameter("Unit");
                SMachCode        = request.getParameter("Machine");
                SType            = request.getParameter("Type");
                SShift           = "1";
                System.out.println(SFromDate+","+SToDate);
                
		session.putValue("Unit",SUnit);
		session.putValue("Shift",SShift);
		session.putValue("FromDate",SFromDate);
                session.putValue("TomDate",SToDate);

		//System.out.println("Card -> " + SUnit + ":" + SShift + ":" + SDate);
	}

     private String getReportType()
	{
		if(bFinalized)
			return "Finalized";
		else
			return "UnFinalized";
	}

	private void setFinalized()
	{
		String SQry = " select count(*) from CardTran " + 
				    " Where sp_date>='"+SFromDate+"' and sp_Date<='"+SToDate+"' and  " +
				    " Unit_Code = "+SUnit;
		try
		{
                        FileWriter fw = new FileWriter("d:\\final.sql");
                        fw.write(SQry);
                        fw.close();
			Connection con = createConnection();
			Statement   st = con.createStatement();
			ResultSet   rs = st.executeQuery(SQry);
			rs.next();
			if(rs.getInt(1) > 0)
				bFinalized = true;
			else
				bFinalized = false;

                        rs.close();
                        st.close();
		}catch(Exception ee){
                    System.out.println("setFinalized->" + ee);
                    
                }	
	}

	private Connection createConnection()
	{
		Connection conn;
		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
			conn  = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
		}catch(Exception ee)
		{
			return null;
		}
		return conn;
	}

}
