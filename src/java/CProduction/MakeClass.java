package CProduction;

import java.util.*;
public class MakeClass
{
     String SMake,SMakeCode;
     Vector VDate,VGms,VSpdl;

     Common common = new Common();

     MakeClass(String SMakeCode,String SMake)
     {
          this.SMakeCode = SMakeCode;
          this.SMake     = SMake;
          VDate = new Vector();
          VGms  = new Vector();
          VSpdl = new Vector();
     }
     public void setData(int iDate,double dGms,double dSpdl)
     {
          VDate.addElement(String.valueOf(iDate));
          VGms.addElement(common.getRound(dGms,3));
          VSpdl.addElement(common.getRound(dSpdl,3));
     }
     public double getGms()
     {
          double dGms  = 0;
          double dSpdl = 0;
          for(int i=0;i<VGms.size();i++)
          {
               dGms  = dGms+common.toDouble((String)VGms.elementAt(i));
               dSpdl = dSpdl+common.toDouble((String)VSpdl.elementAt(i));
          }
          if(dSpdl==0)
               return 0;
          return dGms/dSpdl;
     }
     public double getDayGms(int iEnDate)
     {
          double dGms  = 0;
          double dSpdl = 0;
          for(int i=0;i<VGms.size();i++)
          {
               int iDate = common.toInt((String)VDate.elementAt(i));
               if(iDate != iEnDate)
                    continue;
               dGms  = dGms+common.toDouble((String)VGms.elementAt(i));
               dSpdl = dSpdl+common.toDouble((String)VSpdl.elementAt(i));
          }
          if(dSpdl==0)
               return 0;
          return dGms/dSpdl;
     }
}
