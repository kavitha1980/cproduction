package CProduction;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import java.util.Date;

public class Login extends HttpServlet
{
     HttpSession    session;
     Control        control;
     Common         common;
     Vector         VSCode,VSName,VSPass;
     String         SServer;
     String         bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody;
     RepVect repvect;
     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
          control = new Control();
          common  = new Common();
	     repvect = new RepVect();

          bgColor = common.bgColor;
          bgHead  = common.bgHead;
          bgUom   = common.bgUom;
          bgBody  = common.bgBody;
          fgHead  = common.fgHead;
          fgUom   = common.fgUom;
          fgBody  = common.fgBody;
     }
     public void doGet(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
          response.setContentType("text/html");
          PrintWriter out  = response.getWriter();
          SServer   = control.getID("Select ProdTomHost from Server");
//        getUsers();
          out.println("<html><head><title>AMARJOTHI SPINNING MILLS LIMITED</title></head><body bgcolor='white' text='"+ fgBody+  " '>");
          out.println("<form name=flogin method='post' action='Header'>");//<div align='center'>");
	  out.println("<center>");
	  out.println("<p><FONT face=Verdana size=6 color=black>");
	  out.println("<STRONG>Amar Jothi Spinning Mills Ltd</STRONG></FONT></P>");
	  out.println("</center><center>");
	  out.println("<P><STRONG><FONT face=Verdana size=5 color=navy>");
	  out.println("Production Monitoring System</FONT></STRONG>");
	  out.println("</center><br><br><center>");
	  out.println("<P><FONT face=Verdana size=5 color=green>");
	  out.println("<STRONG><U>LOGIN</U></STRONG></FONT>");
	  out.println("</CENTER><br><br>");
		
          out.println("<table align='center'>");
          out.println("<tr>");
          out.println("<td><font face=Verdana size='3' color='"+fgBody+"'><b>Unit</b>&nbsp;&nbsp;&nbsp;&nbsp;</font></td>");
          out.println("<td><select size='1' name='Unit' style='HEIGHT: 22px; WIDTH: 110px' > ");
        for(int i=0;i<repvect.VUnitCode.size();i++)
          out.println("<option value="+(String)repvect.VUnitCode.elementAt(i)+">"+(String)repvect.VUnit.elementAt(i)+"</option>");
          out.println(" </select></td>");
          out.println("</tr>");

	  out.println("<tr>");
	  out.println("<td><font face=Verdana size='3' color='"+fgBody+"'><b>Shift</b>&nbsp;&nbsp;&nbsp;</font></td>");
          out.println("<td><select size='1' name='Shift' style='HEIGHT: 22px; WIDTH: 110px'>");
          for(int i=0;i<repvect.VShiftCode.size();i++)
          out.println("<option value="+(String)repvect.VShiftCode.elementAt(i)+">"+(String)repvect.VShift.elementAt(i)+"</option>");
          out.println(" </select></td>");
	  out.println("</tr>");

	  out.println("<tr>");
	  out.println("<td><font face=Verdana size='3' color='"+fgBody+"'><b>Date</b>&nbsp;&nbsp;&nbsp;&nbsp;</font></td>");
	  out.println("<td><input type='text' name='Date' size='15' value='" + getCurrentDate() + "'></td>");
	  out.println("</tr>");
          
	  out.println("<tr>");
	  out.println("<td><font face=Verdana size='3' color='"+fgBody+"'><b>Password</b>&nbsp;&nbsp;&nbsp;&nbsp;</font></td>");
	  out.println("<td><input type='password' name='Password' size='15' value=''></td>");
	  out.println("</tr>");
          
	  out.println("<tr>");
	  out.println("<td>&nbsp;</td>");
          out.println("<td><input type='submit' value='Submit' name='B1' style='font-family: Book Antiqua; font-size: 10pt;  text-transform: uppercase; font-weight: bold;' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>");
          out.println("</tr>");
          out.println("</table>");
          
          out.println("</form></body></html>");
          out.close();
     }
     public void doPost(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
		response.setContentType("text/html");
          session          = request.getSession(true);
          session.putValue("Server",SServer);
          String SShift  = (request.getParameter("Shift")).trim();
          String SUnit   = (request.getParameter("Unit")).trim();
          String SDate   = (request.getParameter("Date")).trim();
          session          = request.getSession(true);
          session.putValue("Server",SServer);
		session.putValue("Unit",SUnit);
		session.putValue("Shift",SShift);
		session.putValue("Date",SDate);
          response.sendRedirect(SServer + "/CProduction.ProductionServlet");
     }
     public void getUsers()
     {
          VSCode  = new Vector();
          VSName  = new Vector();
          VSPass  = new Vector();

          String QS = "Select StaffName,StaffCode,Password From Login Where StaffCode <> 1000 Order by StaffName";

          try
          {
                Connection theConnection = createConnection();
                Statement theStatement   = theConnection.createStatement();
                ResultSet res = theStatement.executeQuery(QS);
                while(res.next())
                {
                    VSName.addElement(res.getString(1));
                    VSCode.addElement(res.getString(2));
                    VSPass.addElement(res.getString(3));
                }
                theConnection.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }

	private String getCurrentDate()
	{
		String SDate;		
		Date dt = new Date();
		int iDay   = dt.getDate();
		int iMonth = dt.getMonth()+1;
		int iYear  = dt.getYear()+1900;
		if(iDay < 10)
		 SDate = "0"+iDay;
		else
		 SDate = ""+iDay;
		if(iMonth < 10)
		 SDate = SDate + "." + "0"+iMonth;
		else
		 SDate = SDate + "." + iMonth;
		SDate = SDate + "." + iYear;
		return SDate;
	}

	private Connection createConnection()
	{
		Connection conn;
		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
                        conn  = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
		}catch(Exception ee)
		{
			return null;
		}
		return conn;
	}


}
/*
		out.println("<center><table border='0' width='41%'>");
          out.println("<tr>");

		out.println("<td width='42%' bgcolor='"+bgHead+"'><font face='Book Antiqua' size='4' color='"+fgHead+"'><b>User ID</b></font></td>");
          out.println("<td width='64%'>");

		out.println("<select size='1' name='UserId' style='font-family: Book Antiqua; font-size: 12pt; font-weight: bold'>");
          for(int i=0;i<VSCode.size();i++)
          {
               String SName   = (String)VSName.elementAt(i);
               String SCode   = (String)VSCode.elementAt(i);
               out.println("<option Value='"+SCode+"'>"+SName+"</Option>");
          }
          out.println("</select>");

          out.println("</td></tr>");
          out.println("<tr>");
          out.println("<td width='42%' bgcolor='"+bgHead+"'><font face='Book Antiqua' size='4' color='"+fgHead+"'><b>Password</b></font></td>");
          out.println("<td width='64%'><input type='password' name='Password' size='22' style='font-family: Book Antiqua; font-size: 10pt; color: #003E48; font-weight: bold; border: 2 solid #003E48'></td>");
          out.println("</tr>");

          out.println("</table></center></div><p align='center'>");
*/
