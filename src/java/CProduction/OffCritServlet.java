package CProduction;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;

public class OffCritServlet extends HttpServlet
{
	String SUnit,SDate,SDept,SDateTo="";	
	Vector VEmpName,VEmpCode,VEmpDesig;
	Common common = new Common();

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
     }

	public void doGet(HttpServletRequest request,HttpServletResponse response)
	{
		try
		{ 
     		setEmployees();	

			ObjectOutputStream out = new ObjectOutputStream(response.getOutputStream());
			out.writeObject(VEmpName);
			out.writeObject(VEmpCode);
			out.writeObject(VEmpDesig);
 		}
		catch(Exception ee)
		{
			System.out.println("Service -> " + ee);
		}
    }

	public void doPost(HttpServletRequest request,HttpServletResponse response)
	{
		doGet(request,response);
	}

	private void setEmployees()
	{
		VEmpCode  = new Vector();
		VEmpName  = new Vector();
		VEmpDesig = new Vector();
		String SQry = "Select Supervisor,SupCode,Designation From Supervisor Order By 1";
		try
		{
			Connection con = createConnection();
			Statement st   = con.createStatement();
			ResultSet rs   = st.executeQuery(SQry);
			while(rs.next())
			{
				VEmpName.addElement(rs.getString(1));
				VEmpCode.addElement(rs.getString(2));
				VEmpDesig.addElement(rs.getString(3));
			}
		}catch(Exception ee){System.out.println("setEmployees()->" + ee);}

//		for(int i=0;i<VEmpCode.size();i++)
//			System.out.println((String)VEmpCode.elementAt(i) + " : " + (String)VEmpDesig.elementAt(i) + ":" + (String)VEmpName.elementAt(i)  );
	}

	private Connection createConnection()
	{
		Connection conn;
		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
			conn  = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
		}catch(Exception ee)
		{
			return null;
		}
		return conn;
	}
	
}