package CProduction;

import java.io.*;
import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class StoppagesPeriodReport extends HttpServlet
{
     HttpSession    session;
     RepVect repVect = new RepVect();
     Common common   = new Common();

     String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody,tbgBody;
     String SStDate,SEnDate,SUnit,SDept,SDeptName;
     String S1Array[][],S2Array[][],S3Array[][],STArray[][],STotArray[];
     String S1T[],S2T[],S3T[],STT[],SLoss[],SDiff[],SUpto[];
     int    iDateDiff=0;
     double dNetZ=0,dLossZ=0,dUptoZ=0;

     Vector VCode,VName,VMCode,VMName,VStd;
     Vector V1Code,V1Mnt,V2Code,V2Mnt,V3Code,V3Mnt;

     FileWriter  FW;
     int Lctr = 70,Pctr = 0;
     String SLine;
     Vector VHead;

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);

          bgColor = common.bgColor;
          bgHead  = common.bgHead;
          bgUom   = common.bgUom;
          bgBody  = common.bgBody;
          fgHead  = common.fgHead;
          fgUom   = common.fgUom;
          fgBody  = common.fgBody;
		tbgBody = common.tbgBody;
     }
     public void doGet(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
            doPost(request,response);
     }
     public void doPost(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
          response.setContentType("text/html");
          PrintWriter out  = response.getWriter();
//		System.out.println(request.getQueryString());
		session = request.getSession(true);
		/*if(session.getValue("AppletTag")==null)
		{
			Control control = new Control();
			String SServer = control.getID("Select ProdTomHost from Server");
			response.sendRedirect(SServer + "/CProduction.SessionExpired");
			return;

		}*/		
		SStDate  = request.getParameter("Date");
		SEnDate  = request.getParameter("DateTo");
          iDateDiff        = common.toInt(common.getDateDiff(SEnDate,SStDate))+1;
          SStDate          = common.pureDate(SStDate);
          SEnDate          = common.pureDate(SEnDate);
		SUnit  = request.getParameter("Unit").trim();
		SDept  = request.getParameter("Dept").trim();

		System.out.println("StopPeriod->" + SStDate +":" + SEnDate + ":" + SUnit + ":" + SDept);

		setVectors();
          setSArray();

          try
          {
               FW      = new FileWriter("/software/C-Prod-Print/Reports/stopconsc.prn");
               //FW      = new FileWriter("/production/Reports/stopconsc.prn");
               Lctr    = 70;
               Pctr    =  0;
               Head();
               Body();
               FW.close();
          }catch(Exception ex){}

		String SDeptName = (String)repVect.VDept.elementAt(repVect.VDeptCode.indexOf(SDept));
		String STitle = "Stoppages Report" + " - "+ SDeptName;

          out.println("<html>");
          out.println("<head>");
          out.println("<title>Stoppages Period Report </title>");
          out.println("</head>");
          out.println("<body bgcolor='" + common.bgBody + "'>");
          out.println("<a href='Header'>Home</a>");
		//out.println(session.getValue("AppletTag"));
		out.println(common.getStopPeriodReportHeader(STitle,SUnit,SStDate,SEnDate,SDeptName));
		out.println("<center><br><br>");

		out.println("<table border='1' width='361' height='303'>");
          out.println("<tr>");
          out.println("<td width='121' height='61' rowspan='3' valign='top' bgcolor='"+bgHead+"'>");
          out.println("<p align='right'><font size='4' color='"+fgHead+"'>Reason</font></p>");
          out.println("<p align='right'><font size='4' color='"+fgHead+"'>Shift</font></p>");
          out.println("<p align='center'><font size='4' color='"+fgHead+"'>Mach Nº</font></td>");
          for(int k=0;k<VName.size();k++)
          {
               out.println("<td width='224' height='24' colspan='4' bgcolor='"+bgHead+"'><p align='center'><font size='4' color='"+fgHead+"'>"+(String)VName.elementAt(k)+"</font></td>");
          }
          out.println("</tr>");
          out.println("<tr>");
          for(int k=0;k<VName.size();k++)
          {
               out.println("<td width='70' height='19' valign='top' bgcolor='"+bgHead+"'><p align='center'><font color='"+fgHead+"'>I</font></td>");
               out.println("<td width='70' height='19' valign='top' bgcolor='"+bgHead+"'><p align='center'><font color='"+fgHead+"'>II</font></td>");
               out.println("<td width='70' height='19' valign='top' bgcolor='"+bgHead+"'><p align='center'><font color='"+fgHead+"'>III</font></td>");
               out.println("<td width='70' height='19' valign='top' bgcolor='"+bgHead+"'><p align='center'><font color='"+fgHead+"'>Total</font></td>");
          }
          out.println("</tr>");
          out.println("<tr>");
          for(int k=0;k<VName.size();k++)
          {
               out.println("<td width='70' height='18' valign='top' bgcolor='"+bgUom+"'><p align='center'><font color='"+fgUom+"'>mnt</font></td>");
               out.println("<td width='70' height='18' valign='top' bgcolor='"+bgUom+"'><p align='center'><font color='"+fgUom+"'>mnt</font></td>");
               out.println("<td width='70' height='18' valign='top' bgcolor='"+bgUom+"'><p align='center'><font color='"+fgUom+"'>mnt</font></td>");
               out.println("<td width='70' height='18' valign='top' bgcolor='"+bgUom+"'><p align='center'><font color='"+fgUom+"'>mnt</font></td>");
          }
          out.println("</tr>");

          for(int i=0;i<VMName.size();i++)
          {
               out.println("<tr>");
               out.println("<td width='121' height='31' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VMName.elementAt(i)+"</font></td>");
               for(int k=0;k<VName.size();k++)
               {
                    out.println("<td width='70' height='31' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S1Array[i][k]+"</font></td>");
                    out.println("<td width='70' height='31' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S2Array[i][k]+"</font></td>");
                    out.println("<td width='70' height='31' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+S3Array[i][k]+"</font></td>");
                    out.println("<td width='70' height='31' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+STArray[i][k]+"</font></td>");
               }
               out.println("</tr>");
          }
          out.println("<tr>");
          out.println("<td width='121' height='34' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>Total</font></td>");
          for(int k=0;k<VName.size();k++)
          {
               out.println("<td width='70' height='34' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+S1T[k]+"</font></td>");
               out.println("<td width='70' height='34' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+S2T[k]+"</font></td>");
               out.println("<td width='70' height='34' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+S3T[k]+"</font></td>");
               out.println("<td width='70' height='34' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+STT[k]+"</font></td>");
          }
          out.println("</tr>");
          out.println("<tr>");
          out.println("<td width='121' height='31' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>Loss %</font></td>");
          for(int k=0;k<VName.size();k++)
          {
               out.println("<td width='70' height='31' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>.</font></td>");
               out.println("<td width='70' height='31' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>.</font></td>");
               out.println("<td width='70' height='31' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>.</font></td>");
               out.println("<td width='70' height='31' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SLoss[k]+"</font></td>");
          }
          out.println("</tr>");
          out.println("<tr>");
          out.println("<td width='121' height='25' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>Upto Date Loss %</font></td>");
          for(int k=0;k<VName.size();k++)
          {
               out.println("<td width='70' height='25' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>.</font></td>");
               out.println("<td width='70' height='25' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>.</font></td>");
               out.println("<td width='70' height='25' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>.</font></td>");
               out.println("<td width='70' height='25' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>"+SUpto[k]+"</font></td>");
          }
          out.println("</tr>");
          out.println("<tr>");
          out.println("<td width='121' height='29' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>Std %</font></td>");
          for(int k=0;k<VName.size();k++)
          {
               out.println("<td width='70' height='29' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>.</font></td>");
               out.println("<td width='70' height='29' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>.</font></td>");
               out.println("<td width='70' height='29' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>.</font></td>");
               out.println("<td width='70' height='29' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+(String)VStd.elementAt(k)+"</font></td>");
          }
          out.println("</tr>");
          out.println("<tr>");
          out.println("<td width='121' height='31' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>Ex/Short</font></td>");
          for(int k=0;k<VName.size();k++)
          {
               out.println("<td width='70' height='31' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>.</font></td>");
               out.println("<td width='70' height='31' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>.</font></td>");
               out.println("<td width='70' height='31' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>.</font></td>");
               if(common.toDouble(SDiff[k])<0)
                    out.println("<td width='70' height='31' bgcolor='"+fgUom+"'><font color='"+bgUom+"'>"+SDiff[k]+"</font></td>");
               else
                    out.println("<td width='70' height='31' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>"+SDiff[k]+"</font></td>");
          }
          out.println("</tr>");
          out.println("</table>");
          out.println("</div>");
          out.println("</body>");
          out.println("</html>");

          out.close();
     }

     public void setVectors()
     {
          VCode   = new Vector();
          VName   = new Vector();
          VMCode  = new Vector();
          VMName  = new Vector();
          VStd    = new Vector();
          V1Code  = new Vector();
          V1Mnt   = new Vector();
          V2Code  = new Vector();
          V2Mnt   = new Vector();
          V3Code  = new Vector();
          V3Mnt   = new Vector();

          try
          {
                  Connection con = createConnection();
                  Statement stat = con.createStatement();

                  ResultSet resx = stat.executeQuery("Select DeptName From Department Where ProdCode="+SDept);
                  while(resx.next())
                        SDeptName = resx.getString(1);

                  ResultSet res  = stat.executeQuery(getQSReasons(SDept));
                  while(res.next())
                  {
                         VCode .addElement(res.getString(1));
                         VName .addElement(res.getString(2));
                         VStd  .addElement(res.getString(3));
                         String SPri  = res.getString(4);
                  }
                  ResultSet res1 = stat.executeQuery(getQSMacs(SDept,SUnit));
                  while(res1.next())
                  {
                         VMCode .addElement(res1.getString(1));
                         VMName .addElement(res1.getString(2));
                  }
                  try{stat.execute("Drop Table gskStop1");}catch(Exception ex){}
                  try{stat.execute("Drop Table gskStop2");}catch(Exception ex){}
                  try{stat.execute(getQS1());}catch(Exception ex){System.out.println("from : 211 "+ex);}
                  try{stat.execute(getQSMonth());}catch(Exception ex){System.out.println("from : 212 "+ex);}
                  ResultSet res10 = stat.executeQuery(getQS3());
                  SUpto   = new String[VCode.size()];  //for Total
                  int k=0;
                  double dNet = VMCode.size()*3*8*60*common.toInt(SStDate.substring(6,8));
                  while(res10.next())
                  {
                          String SDummy = res10.getString(1);
                          double dUptoX = res10.getDouble(2);
                          dUptoZ       += dUptoX;
                          SUpto[k]      = common.getRound(dUptoX*100/dNet,3);
                          k++;
                  }
                  con.close();
          }
          catch(Exception ex)
          {
               System.out.println("StopPeriod 226 : "+ex);
               ex.printStackTrace();
          }
     }
     public String getQSReasons(String SDept)
     {
          String QS = " SELECT stoppage.stop_code, stopreasons.stop_name,"+
                      " stoppage.MillStd,stoppage.RepPriority "+
                      " FROM stoppage INNER JOIN "+
                      " stopreasons ON stoppage.stop_code = stopreasons.STOP_CODE "+
                      " WHERE stoppage.dept_code="+SDept+
                      " ORDER BY Stoppage.RepPriority,Stoppage.stop_code ";
          return QS;
     }
     public String getQSMacs(String SDept,String SUnit)
     {
          String QS = "SELECT machine.mach_code, machine.mach_name FROM machine "+
                      " WHERE machine.dept_code="+SDept+" and machine.unit_code="+SUnit+
                      " ORDER BY to_number(machine.mach_name) ";
          return QS;
     }
     public String getQS1()
     {
          String QS = " create table gskstop1 "+
                      " as (SELECT stoppage.stop_code "+
                      " FROM stoppage "+
                      " WHERE stoppage.dept_code="+SDept+") Order By stoppage.RepPriority";
          return QS;
     }
     public String getQS2(String SMac)
     {
          String QS = " create table gskStop2 "+
                      " as (SELECT Sum(StopTran.Stop_Mint) AS SumOfStop_Mint, "+
                      " gskstop1.stop_code AS Expr1 "+
                      " FROM gskstop1 INNER JOIN StopTran ON "+
                      " gskstop1.stop_code = StopTran.Stop_Code "+
                      " WHERE (((StopTran.Stop_Date)>='"+SStDate+"' And "+
                      " (StopTran.Stop_Date)<='"+SEnDate+"') AND "+
                      " ((StopTran.Stop_Shift)=1) AND "+
                      " ((StopTran.Unit_Code)="+SUnit+") AND "+
                      " ((StopTran.Dept_Code)="+SDept+") AND "+
                      " ((StopTran.Mach_Code)="+SMac+")) "+
                      " GROUP BY gskstop1.stop_code)";

          return QS;
     }
     public String getQS22(String SMac)
     {
          String QS = " create table gskStop2 "+
                      " as (SELECT Sum(StopTran.Stop_Mint) AS SumOfStop_Mint, "+
                      " gskstop1.stop_code AS Expr1 "+
                      " FROM gskstop1 INNER JOIN StopTran ON "+
                      " gskstop1.stop_code = StopTran.Stop_Code "+
                      " WHERE (((StopTran.Stop_Date)>='"+SStDate+"' And "+
                      " (StopTran.Stop_Date)<='"+SEnDate+"') AND "+
                      " ((StopTran.Stop_Shift)=2) AND "+
                      " ((StopTran.Unit_Code)="+SUnit+") AND "+
                      " ((StopTran.Dept_Code)="+SDept+") AND "+
                      " ((StopTran.Mach_Code)="+SMac+")) "+
                      " GROUP BY gskstop1.stop_code)";

          return QS;
     }
     public String getQS23(String SMac)
     {
          String QS = " create table gskStop2 "+
                      " as (SELECT Sum(StopTran.Stop_Mint) AS SumOfStop_Mint, "+
                      " gskstop1.stop_code AS Expr1 "+
                      " FROM gskstop1 INNER JOIN StopTran ON "+
                      " gskstop1.stop_code = StopTran.Stop_Code "+
                      " WHERE (((StopTran.Stop_Date)>='"+SStDate+"' And "+
                      " (StopTran.Stop_Date)<='"+SEnDate+"') AND "+
                      " ((StopTran.Stop_Shift)=3) AND "+
                      " ((StopTran.Unit_Code)="+SUnit+") AND "+
                      " ((StopTran.Dept_Code)="+SDept+") AND "+
                      " ((StopTran.Mach_Code)="+SMac+")) "+
                      " GROUP BY gskstop1.stop_code)";

          return QS;
     }
     public String getQSMonth()
     {
          String QS = " create table GskStop2 "+
                      " as (SELECT Sum(StopTran.Stop_Mint) AS SumOfStop_Mint, "+
                      " gskstop1.stop_code AS Expr1 "+
                      " FROM gskstop1 INNER JOIN StopTran ON "+
                      " gskstop1.stop_code=StopTran.Stop_Code "+
                      " WHERE  substr(StopTran.Stop_Date,5,2)="+SStDate.substring(4,6)+" And "+
                      " StopTran.Unit_Code="+SUnit+" And "+
                      " StopTran.Dept_Code="+SDept+" "+
                      " GROUP BY gskstop1.stop_code)";
          return QS;
     }
     public String getQS3()
     {
          String QS = " SELECT gskstop1.stop_code, gskstop2.SumOfStop_Mint "+
                      " FROM gskstop1 LEFT JOIN gskstop2 ON "+
                      " gskstop1.stop_code = gskstop2.Expr1 ";
          return QS;
     }
     public void setSArray()
     {
          String SMac;
          double dTotal,d1T,d2T,d3T;
          S1Array   = new String[VMCode.size()][VCode.size()];  //for I Shift
          S2Array   = new String[VMCode.size()][VCode.size()];  //for II Shift
          S3Array   = new String[VMCode.size()][VCode.size()];  //for III Shift
          STArray   = new String[VMCode.size()][VCode.size()];  //for Total
          STotArray = new String[VMCode.size()];  //for Net Total

          S1T     = new String[VCode.size()+1];  //for Total
          S2T     = new String[VCode.size()+1];  //for Total
          S3T     = new String[VCode.size()+1];  //for Total
          STT     = new String[VCode.size()+1];  //for Total
          SLoss   = new String[VCode.size()+1];  //for Total
          SDiff   = new String[VCode.size()+1];  //for Total

          //Row - Machines
          for(int i=0;i<VMCode.size();i++)
          {
               SMac = (String)VMCode.elementAt(i);
               getMntVectors(SMac);

               double dNet = 0;
               //Col - Stop Reasons
               for(int j=0;j<VCode.size();j++)
               {
                    dTotal = 0;
                    STArray[i][j] = "";
                    if((String)V1Mnt.elementAt(j)==null)
                         S1Array[i][j] = "-";
                    else
                    {
                         S1Array[i][j] = common.getRound((String)V1Mnt.elementAt(j),0);
                         dTotal        = dTotal+common.toDouble(S1Array[i][j]);
                    }
                    if((String)V2Mnt.elementAt(j)==null)
                         S2Array[i][j] = "-";
                    else
                    {
                         S2Array[i][j] = common.getRound((String)V2Mnt.elementAt(j),0);
                         dTotal        = dTotal+common.toDouble(S2Array[i][j]);
                    }
                    if((String)V3Mnt.elementAt(j)==null)
                         S3Array[i][j] = "-";
                    else
                    {
                         S3Array[i][j] = common.getRound((String)V3Mnt.elementAt(j),0);
                         dTotal        = dTotal+common.toDouble(S3Array[i][j]);
                    }
                    if(dTotal<=0)
                         STArray[i][j] = ".";
                    else
                         STArray[i][j] = common.getRound(String.valueOf(dTotal),0);
                    dNet += dTotal;
               }
               STotArray[i] = common.getRound(dNet,0);
               dNetZ        += dNet;
          }
          for(int j=0;j<VCode.size();j++)
          {
               d1T=0;d2T=0;d3T=0;
               for(int i=0;i<VMCode.size();i++)
               {
                    d1T = d1T+common.toDouble(S1Array[i][j]);
                    d2T = d2T+common.toDouble(S2Array[i][j]);
                    d3T = d3T+common.toDouble(S3Array[i][j]);
               }
               S1T[j]   = (d1T<=0?".":common.getRound(String.valueOf(d1T),0));
               S2T[j]   = (d2T<=0?".":common.getRound(String.valueOf(d2T),0));
               S3T[j]   = (d3T<=0?".":common.getRound(String.valueOf(d3T),0));
               STT[j]   = ((d1T+d2T+d3T)<=0?".":common.getRound(String.valueOf(d1T+d2T+d3T),0));
               SLoss[j] = common.getRound(String.valueOf((d1T+d2T+d3T)*100/(3*8*VMCode.size()*60*iDateDiff)),3);
               SLoss[j] = ((common.toDouble(SLoss[j])==0)?".":SLoss[j]);

               double dStd  = common.toDouble((String)VStd.elementAt(j));
               double dUpto = common.toDouble(SUpto[j]+" ");
               SDiff[j]     = common.getRound((dUpto-dStd),3);
               SDiff[j] = ((common.toDouble(SDiff[j])==0)?".":SDiff[j]);
          }
     }
     public void getMntVectors(String SMac)
     {
          V1Code.removeAllElements();
          V1Mnt.removeAllElements();
          V2Code.removeAllElements();
          V2Mnt.removeAllElements();
          V3Code.removeAllElements();
          V3Mnt.removeAllElements();

          try
          {
                  Connection con = createConnection();
                  Statement stat = con.createStatement();
                  try{stat.execute("Drop Table gskStop1");}catch(Exception ex){}
                  try{stat.execute("Drop Table gskStop2");}catch(Exception ex){}
                  try{stat.execute(getQS1());}catch(Exception ex){System.out.println("from : 415 "+ex);}
                  try{stat.execute(getQS2(SMac));}catch(Exception ex){System.out.println("from : 416 "+ex);}
                  ResultSet res2 = stat.executeQuery(getQS3());
                  while(res2.next())
                  {
                          V1Code .addElement(res2.getString(1));
                          V1Mnt  .addElement(res2.getString(2));
                  }
                  try{stat.execute("Drop Table gskStop2");}catch(Exception ex){}
                  try{stat.execute(getQS22(SMac));}catch(Exception ex){System.out.println("from : 424 "+ex);}
                  ResultSet res3 = stat.executeQuery(getQS3());
                  while(res3.next())
                  {
                          V2Code .addElement(res3.getString(1));
                          V2Mnt  .addElement(res3.getString(2));
                  }
                  try{stat.execute("Drop Table gskStop2");}catch(Exception ex){}
                  try{stat.execute(getQS23(SMac));}catch(Exception ex){System.out.println("from : 432 "+ex);}
                  ResultSet res4 = stat.executeQuery(getQS3());
                  while(res4.next())
                  {
                          V3Code .addElement(res4.getString(1));
                          V3Mnt  .addElement(res4.getString(2));
                  }
                  con.close();
          }
          catch(Exception ex)
          {
               System.out.println("StopPeriod 443 : "+ex);
          }
     }
     public void Head()
     {
          if(Lctr < 60)
             return;
          if(Pctr > 0)
          {
             try
             {
               FW.write(SLine+"\n");
             }catch(Exception ex){}
          }
          Pctr++;

          String Str1    = common.Pad("Company    : AMARJOTHI SPINNING MILLS LIMITED",80);
          String Str2    = common.Pad("Document   : Stoppages Report During "+common.parseDate(SStDate)+" & "+common.parseDate(SEnDate),80);
          String Str3    = common.Pad("Unit       : "+(SUnit.equals("1")?"A":(SUnit.equals("2")?"B":"S")),70);
          String Str4    = common.Pad("Department : "+SDeptName,70);

          VHead = Stop1Head();
          try
          {
               FW.write(Str1+"\n");
               FW.write(Str2+"\n");
               FW.write(Str3+"\n");
               FW.write(Str4+"\n\n");
               for(int i=0;i<VHead.size();i++)
               {
                    FW.write((String)VHead.elementAt(i));
               }
          }catch(Exception ex){}
          Lctr = 4+VHead.size();
     }
     public void Body()
     {
         String Strl = "";
         for(int i=0;i<VMName.size();i++)
         {
               Strl = common.Pad((String)VMName.elementAt(i),6)+common.Space(2);
               for(int k=0;k<VName.size();k++)
               {
                     Strl = Strl+common.Rad(S1Array[i][k],3)+" "+
                            common.Rad(S2Array[i][k],3)+" "+
                            common.Rad(S3Array[i][k],3)+" "+
                            common.Rad(STArray[i][k],4)+common.Space(2);
               }
               Strl += common.Rad(STotArray[i],6);
                try
                {
                     Head();
                     FW.write(Strl+"\n");
                     Lctr++;
                }catch(Exception ex){}
         }
          String ST = common.Pad("Total",6)+common.Space(2);
          String SL = common.Pad("Loss %",6)+common.Space(2);
          String SU = common.Pad("Upto %",6)+common.Space(2);
          String SD = common.Pad("Diff %",6)+common.Space(2);
          String SS = common.Pad("Std  %",6)+common.Space(2);
          for(int k=0;k<VName.size();k++)
          {
                ST = ST+common.Rad(" ",3)+" "+common.Rad(" ",3)+" "+common.Rad(STT[k],8)+common.Space(2);
                SL = SL+common.Rad(" ",3)+" "+common.Rad(" ",3)+" "+common.Rad(SLoss[k],8)+common.Space(2);
                SD = SD+common.Rad(" ",3)+" "+common.Rad(" ",3)+" "+common.Rad(SDiff[k],8)+common.Space(2);
                SU = SU+common.Rad(" ",3)+" "+common.Rad(" ",3)+" "+common.Rad(SUpto[k],8)+common.Space(2);
                SS = SS+common.Rad(" ",3)+" "+common.Rad(" ",3)+" "+common.Rad(common.getRound((String)VStd.elementAt(k),3),8)+common.Space(2);
          }
          dLossZ   = dNetZ*100/(3*8*VMCode.size()*60*iDateDiff);
          dUptoZ   = dUptoZ*100/(3*8*VMCode.size()*60*common.toInt(SStDate.substring(6,8)));
           try
           {
                FW.write(SLine+"\n");
                FW.write(ST+common.Rad(common.getRound(dNetZ,0),6)+"\n\n");
                FW.write(SL+common.Rad(common.getRound(dLossZ,3),6)+"\n\n");
                FW.write(SU+common.Rad(common.getRound(dUptoZ,3),6)+"\n\n");
                FW.write(SS+"\n\n");
                FW.write(SD+"\n\n");
                FW.write(SLine+"\n");
                FW.write("<End Of Report>");
           }catch(Exception ex){}
     }

     public Vector Stop1Head()
     {
          Vector VHead = new Vector();
          Vector VName1 = new Vector();
          Vector VName2 = new Vector();

          String Str1  = common.Pad("Reason",6)+common.Space(2);
          String Str2  = common.Space(6)+common.Space(2);
          String Str3  = common.Pad(" ",6)+common.Space(2);
          String Str4  = common.Pad("Shift",6)+common.Space(2);
          String Str5  = common.Pad("Mac No",6)+common.Space(2);
          SLine = common.Replicate("-",6+2);
          for(int i=0;i<VName.size();i++)
          {
                  String str = (String)VName.elementAt(i);
                  if(str.length() <= 15)
                  {
                        VName1.addElement(str);
                        VName2.addElement(" ");
                  }
                  else
                  {
                        VName1.addElement(str.substring(0,15));
                        VName2.addElement(str.substring(15,str.length()));
                  }
          }
          for(int i=0;i<VName.size();i++)
          {
               Str1  = Str1+common.Cad((String)VName1.elementAt(i),16)+common.Space(2);
               Str2  = Str2+common.Cad((String)VName2.elementAt(i),16)+common.Space(2);
               Str3  = Str2+common.Replicate("-",16)+common.Space(2);
               Str4  = Str3+common.Pad("I  "+" "+"II "+" "+"III"+" "+" Tot",16)+common.Space(2);
               Str5  = Str4+common.Pad("mnt"+" "+"mnt"+" "+"mnt"+" "+" mnt",16)+common.Space(2);
               SLine = SLine+common.Replicate("-",16+2);
          }
          Str1  = Str1+common.Rad("Total",6)+"\n";
          Str2  = Str2+common.Space(6)+"\n";
          Str3  = Str3+common.Space(6)+"\n";
          Str4  = Str4+common.Rad(" ",6)+"\n";
          Str5  = Str5+common.Rad("mnt",6)+"\n";
          SLine = SLine+common.Replicate("-",6)+"\n";

          VHead.addElement(SLine);
          VHead.addElement(Str1);
          VHead.addElement(Str2);
          VHead.addElement(Str3);
          VHead.addElement(Str4);
          VHead.addElement(Str5);
          VHead.addElement(SLine);

          return VHead;
     }

	private Connection createConnection()
	{
		Connection conn;
		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
			conn  = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
		}catch(Exception ee)
		{
			return null;
		}
		return conn;
	}

}
