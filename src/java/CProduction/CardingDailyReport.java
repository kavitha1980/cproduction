package CProduction;

import java.io.*;
import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import java.io.FileOutputStream;


public class CardingDailyReport extends HttpServlet
{
     HttpSession    session;
     Common common;

     Vector VDate,VUnit,VShift,VMachNo,VCount,VTicket,VName;
     Vector VOrderNo,VShade,VSpeed,VSlHank,VHank;
     Vector VHourMeter,VHourRun,VDraft;
     Vector VHankExp,VHankTar,VProd,VProdExp,VProdTar;
     Vector VStopAcc,VStopNonAcc,VWorkerEffy,VProdLoss,VActualEffy;
     Vector VUEM,VUEOA,VEffyAvai,VHourAllot;
     Vector VNoRunOut,VNoPersons,VRoTime,VFeed,VWM,VBreaks;
     Vector VCardClass;

     String SDate,SShift,SUnit,SSupName;
     String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody,tbgBody;

     FileWriter  FW;
     int Lctr = 70,Pctr = 0;
     String SLine;
     Vector VHead;
	boolean bFinalized;
	Control control = new Control();
        
        String SFile                     = "";
     String[]                         ColumnName,ColumnType,ColumnName1,ColumnType1,ColumnName2,ColumnType2,sBody;
     int[]                            iColumnWidth,iColumnWidth1,iColumnWidth2;

     private static Font catFont     = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
     private static Font redFont     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
     private static Font subFont     = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);

     private static Font bigBold     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
     private static Font bigNormal   = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);

     private static Font mediumBold  = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
     private static Font mediumNormal= new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font smallBold   = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
     private static Font smallNormal = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font tinyBold    = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.BOLD);
     private static Font tinyNormal  = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.NORMAL);

     private static Font underBold   = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.UNDERLINE);
    
     int    iWidth1[]   = {20,25,38,38,25,20,20,25,20,20,20,20,20,25,25,25,25,28,30,25,25,25,25,25,25,25,25,25};
     int    iWidth0[]   = {20,25,35,35};
     Document   document;
     PdfPTable  table1,table0;
     int        iUserCode;
     PdfWriter  writer      = null;
   
     
     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
          common = new Common();

          bgColor = common.bgColor;
          bgHead  = common.bgHead;
          bgUom   = common.bgUom;
          bgBody  = common.bgBody;
          fgHead  = common.fgHead;
          fgUom   = common.fgUom;
          fgBody  = common.fgBody;
		tbgBody = common.tbgBody;
          SLine   = common.Replicate("-",229)+"\n";
     }

     public void doGet(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
            doPost(request,response);
     }
     public void doPost(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
		response.setContentType("text/html");
          PrintWriter out  = response.getWriter();
		session = request.getSession(true);
		/*if(session.getValue("AppletTag")==null)
		{
			String SServer = control.getID("Select ProdHost from Server");
			response.sendRedirect(SServer + "/SessionExpired");
			return;

		}*/
		setCriteria(request);
		setFinalized();
          out.println("<html>");
          out.println("<head>");
          out.println("<title>Carding Production Report </title>");
          out.println("</head>");
          out.println("<body bgcolor='" + common.bgBody + "'>");
		//out.println(session.getValue("AppletTag"));
          out.println("<a href='Header'>Home</a>");
	  out.println(common.getDailyReportHeader("Carding Production Report (F CDG 1)",SUnit,SShift,SDate,getReportType()));
          out.println("  <center>");
          out.println("  <table border='1' cellspacing='0' width='1600' height='1''>");
          out.println("    <tr>");
          out.println("      <td width='59' height='82' rowspan='4' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Mach Nº</font></td>");
          out.println("      <td width='59' height='82' rowspan='4' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Count</font></td>");
          out.println("      <td width='59' height='82' rowspan='4' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Tenter Nº</font></td>");
          out.println("      <td width='100' height='82' rowspan='4' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Tenter Name</font></td>");
          out.println("      <td width='59' height='105' rowspan='3' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Doffer Speed</font></td>");
          out.println("      <td width='59' height='105' rowspan='3' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Tension Draft</font></td>");
          out.println("      <td width='59' height='105' rowspan='3' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Sliver Hank</font></td>");
          out.println("      <td width='201' height='30' rowspan='3' valign='middle' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Breaks Per 100Km");
          out.println("        Time</font></td>");
          out.println("      <td width='201' height='30' colspan='4' valign='middle' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>SC");
          out.println("        Time</font></td>");
          out.println("      <td width='376' height='30' colspan='8' valign='middle' align='center' bgcolor='"+bgHead+"'>");
          out.println("       <font size='4' color='"+fgHead+"'>");
          out.println("       Production Details</font></td>");
          out.println("      <td width='194' height='30' colspan='4' valign='middle' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Utilization</font></td>");
          out.println("      <td width='177' height='30' colspan='4' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Worker Efficiency</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='59' height='69' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Nº of SC</font></td>");
          out.println("      <td width='59' height='69' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Nº of person</font></td>");
          out.println("      <td width='59' height='69' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Actual</font></td>");
          out.println("      <td width='59' height='69' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Ex/Sh</font></td>");
          out.println("      <td width='136' height='21' valign='top' colspan='3' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Hank</font></td>");
          out.println("      <td width='188' height='21' valign='top' colspan='4' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Production</font></td>");
          out.println("      <td width='59' height='69' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Actual Effy</font></td>");
          out.println("      <td width='59' height='69' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Hours to be worked</font></td>");
          out.println("      <td width='59' height='69' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Hour Meter Reading</font></td>");
          out.println("      <td width='59' height='69' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>UxE (OA)</font></td>");
          out.println("      <td width='59' height='69' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>UxE");
          out.println("        (MU)</font></td>");
          out.println("      <td width='59' height='69' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Effy Avai lable</font></td>");
          out.println("      <td width='60' height='69' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Worker Effy</font></td>");
          out.println("      <td width='60' height='69' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Accep Stoppage</font></td>");
          out.println("      <td width='60' height='69' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Non Acc Stoppage</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='59' height='42' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Expected</font></td>");
          out.println("      <td width='59' height='42' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Target</font></td>");
          out.println("      <td width='59' height='42' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Actual</font></td>");
          out.println("      <td width='59' height='42' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Expected</font></td>");
          out.println("      <td width='59' height='42' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Target</font></td>");
          out.println("      <td width='59' height='42' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Actual</font></td>");
          out.println("      <td width='59' height='42' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Prod Loss</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4' color='"+fgUom+"'>MPM</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4' color='"+fgUom+"'>Nº</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4' color='"+fgUom+"'>Nº</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4' color='"+fgUom+"'>Min</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4' color='"+fgUom+"'>Min</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4' color='"+fgUom+"'>Hr</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4' color='"+fgUom+"'>Hr</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='60' height='1' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='60' height='1' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4' color='"+fgUom+"'>Mnt</font></td>");
          out.println("      <td width='60' height='1' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4' color='"+fgUom+"'>Mnt</font></td>");
          out.println("    </tr>");
/*
          out.println("    <tr>");
          out.println("      <td width='1062' height='38' valign='top' bgcolor='"+bgUom+"' colspan='18'><font color='"+fgHead+"' size='5'><b>Target</b></font></td>");
          out.println("      <td width='59' height='38' valign='top' bgcolor='"+bgUom+"' align='right'>&nbsp;</td>");
          out.println("      <td width='118' height='38' valign='top' bgcolor='"+bgUom+"' align='right' colspan='2'>&nbsp;</td>");
          out.println("      <td width='59' height='38' valign='top' bgcolor='"+bgUom+"' align='right'>&nbsp;</td>");
          out.println("      <td width='59' height='38' valign='top' bgcolor='"+bgUom+"' align='right'>&nbsp;</td>");
          out.println("      <td width='59' height='38' valign='top' bgcolor='"+bgUom+"' align='right'>&nbsp;</td>");
          out.println("      <td width='60' height='38' valign='top' bgcolor='"+bgUom+"' align='right'>&nbsp;</td>");
          out.println("      <td width='60' height='38' valign='top' bgcolor='"+bgUom+"' align='right'>&nbsp;</td>");
          out.println("      <td width='60' height='38' valign='top' bgcolor='"+bgUom+"' align='right'>&nbsp;</td>");
          out.println("    </tr>");
*/
          setVectorData();
          calcVectors();

          try
          {
//              FW      = new FileWriter("//home/arun/reports/cardc.prn");
//               FW      = new FileWriter("/software/C-Prod-Print/Reports/cardc.prn");
               FW      = new FileWriter("D:/C-Prod-Print/cardc.prn");
               Lctr    = 70;
               Pctr    =  0;
               Head();
               Body();
               FW.close();
               createPDFFile();
          }catch(Exception ex){}

          for(int i=0;i<VMachNo.size();i++)
          {
               out.println("    <tr>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"'><font   color='"+fgBody+"'>"+(String)VMachNo.elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"'><font   color='"+fgBody+"'>"+(String)VCount .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"'><font   color='"+fgBody+"'>"+(String)VTicket .elementAt(i)+"</font></td>");
               out.println("      <td width='100' height='5' valign='top' bgcolor='"+tbgBody+"'><font   color='"+fgBody+"'>"+(String)VName .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>"+(String)VSpeed .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>"+(String)VDraft .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>"+(String)VSlHank .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VBreaks.elementAt(i)+"</font></td>");               
               out.println("      <td width='59' height='5' valign='top' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VNoRunOut.elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VNoPersons.elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VRoTime.elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>.</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>"+(String)VHankExp .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>"+(String)VHankTar .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>"+(String)VHank .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>"+(String)VProdExp .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>"+(String)VProdTar .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>"+(String)VProd .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>"+(String)VProdLoss .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>"+(String)VActualEffy .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>"+(String)VHourRun .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>"+(String)VHourMeter .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>"+(String)VUEOA .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>"+(String)VUEM .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>"+(String)VEffyAvai .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>"+(String)VWorkerEffy .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>"+(String)VStopAcc .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>"+(String)VStopNonAcc .elementAt(i)+"</font></td>");
               out.println("    </tr>");
          }

          //Sum Display
          out.println("    <tr>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"'><font size='4'   color='"+fgUom+"'>.</font></td>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"'><font size='4'  color='"+fgUom+"'>.</font></td>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>.</font></td>");
          out.println("      <td width='100' height='5' valign='top' bgcolor='"+bgUom+"'><font size='4'  color='"+fgUom+"'>Total/Avg</font></td>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4'  color='"+fgUom+"'>"+common.getSum(VSpeed ,"A")+"</font></td>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4'  color='"+fgUom+"'>"+common.getSum(VDraft ,"A")+"</font></td>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4'   color='"+fgUom+"'>"+common.getSum(VSlHank,"A")+"</font></td>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4'   color='"+fgUom+"'>"+common.getSum(VBreaks,"S")+"</font></td>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4' color='"+fgUom+"'>"+"-"+"</font></td>");
          out.println("      <td width='60' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font  size='4' color='"+fgUom+"'>"+"-"+"</font></td>");
          out.println("      <td width='60' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font  size='4' color='"+fgUom+"'>"+"-"+"</font></td>");
          out.println("      <td width='60' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4'  color='"+fgUom+"'>"+"-"+"</font></td>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font  size='4' color='"+fgUom+"'>"+common.getSum(VHankExp ,"S")+"</font></td>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font  size='4' color='"+fgUom+"'>"+common.getSum(VHankTar ,"S")+"</font></td>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4'  color='"+fgUom+"'>"+common.getSum(VHank ,"S")+"</font></td>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font  size='4' color='"+fgUom+"'>"+common.getSum(VProdExp ,"S")+"</font></td>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font  size='4' color='"+fgUom+"'>"+common.getSum(VProdTar ,"S")+"</font></td>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4'  color='"+fgUom+"'>"+common.getSum(VProd ,"S")+"</font></td>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4'  color='"+fgUom+"'>"+common.getSum(VProdLoss ,"S")+"</font></td>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4'  color='"+fgUom+"'>"+common.getSum(VActualEffy ,"A")+"</font></td>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font  size='4' color='"+fgUom+"'>"+common.getSum(VHourRun ,"S")+"</font></td>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font  size='4' color='"+fgUom+"'>"+common.getSum(VHourMeter ,"S")+"</font></td>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font  size='4' color='"+fgUom+"'>"+common.getSum(VUEOA ,"A")+"</font></td>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font size='4'  color='"+fgUom+"'>"+common.getSum(VUEM ,"A")+"</font></td>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font  size='4' color='"+fgUom+"'>"+common.getSum(VEffyAvai ,"A")+"</font></td>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font  size='4' color='"+fgUom+"'>"+common.getSum(VWorkerEffy ,"A")+"</font></td>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font  size='4' color='"+fgUom+"'>"+common.getSum(VStopAcc ,"S")+"</font></td>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font  size='4' color='"+fgUom+"'>"+common.getSum(VStopNonAcc ,"S")+"</font></td>");
          out.println("    </tr>");

          out.println("  </table></center>");
          out.println("</body>");
          out.println("</html>");

          out.close();
     }
     private void setVectorData()
     {
		 String SQ1="",SQ2="",SQ3="";
		 VCardClass = new Vector();

           VDate  = new Vector();
           VUnit  = new Vector();
           VShift = new Vector();
           VMachNo= new Vector();
           VCount = new Vector();
           VTicket= new Vector();
           VName  = new Vector();
     
           VSpeed       = new Vector();
           VDraft       = new Vector();
           VSlHank      = new Vector();
           VHankExp     = new Vector();
           VHankTar     = new Vector();
           VHank        = new Vector();
           VProdExp     = new Vector();
           VProdTar     = new Vector();
           VProd        = new Vector();
           VProdLoss    = new Vector();
           VActualEffy  = new Vector();
           VHourRun     = new Vector();
           VHourMeter   = new Vector();
           VUEOA        = new Vector();
           VUEM         = new Vector();
           VEffyAvai    = new Vector();
           VWorkerEffy  = new Vector();
           VStopAcc     = new Vector();
           VStopNonAcc  = new Vector();
           VHourAllot   = new Vector();
           VNoRunOut    = new Vector();
           VNoPersons   = new Vector();
           VRoTime      = new Vector();
           VFeed        = new Vector();
           VWM          = new Vector();
           VBreaks      = new Vector();
           
		 if(bFinalized)
		 {
			SQ1 = getQString();
			SQ2 = getSupCd();
			SQ3 = getAbsQS();
		 }	
		 else
		 {	
		     SQ1 = getQStringTemp();
			SQ2 = getSupCdTemp();
			SQ3 = getAbsQSTemp();
		 }

             try
             {
                     Connection con = createConnection();
                     Statement stat = con.createStatement();
                     ResultSet res  = stat.executeQuery(SQ1);
                     while(res.next())
                     {
                          VDate  .addElement(res.getString(1)); 
                          VUnit  .addElement(res.getString(2));
                          VShift .addElement(res.getString(3));
                          VMachNo.addElement(res.getString(4));
                          VCount .addElement(res.getString(5));
                          VTicket.addElement(res.getString(6));
                          VName  .addElement(getTicket(res.getString(7),con));
                          VSpeed       .addElement(res.getString(8));
                          VDraft       .addElement(res.getString(9));
                          VSlHank      .addElement(res.getString(10));
                          VHankExp     .addElement(res.getString(11));
                          VHankTar     .addElement(res.getString(12));
                          VHank        .addElement(res.getString(13));
                          VProdExp     .addElement(res.getString(14));
                          VProdTar     .addElement(res.getString(15));
                          VProd        .addElement(res.getString(16));
                          VHourRun     .addElement(res.getString(17));
                          VHourMeter   .addElement(res.getString(18));
                          VUEOA        .addElement(res.getString(19));
                          VUEM         .addElement(res.getString(20));
                          VWorkerEffy  .addElement(res.getString(21));
                          VStopAcc     .addElement(res.getString(22));
                          VStopNonAcc  .addElement(res.getString(23));
                          VHourAllot   .addElement(res.getString(24));
                          VNoRunOut    .addElement(res.getString(25));
                          VNoPersons   .addElement(res.getString(26));
                          VRoTime      .addElement(res.getString(27));
                          VFeed        .addElement(res.getString(28));
                          VWM          .addElement(res.getString(29));
                          VBreaks      .addElement(res.getString(30));
                     }
                     res.close();

                     ResultSet res3 = stat.executeQuery(SQ2);
                     while(res3.next())
                         SSupName = res3.getString(1);

                     res3.close();

                     ResultSet res6 = stat.executeQuery(SQ3);
                     while(res6.next())
                         organizeCardClass(res6);
                     res6.close();
                     stat.close(); 
                     con.close();
                     for(int i=0;i<VMachNo.size();i++)
                     {
                         int index=common.toInt((String)VWM.elementAt(i));
                         String SWM    = (index==0?"M":"W");
                         String SMachNo=((String)VMachNo.elementAt(i)).trim();
                         VMachNo.setElementAt(SMachNo+"-"+SWM,i);
                     }
             }
             catch(Exception ex)
             {
                     System.out.println("From Card.java : 252 "+ex);
             }
     }
     private void organizeCardClass(ResultSet res6) throws Exception
     {
          int    iCode = res6.getInt(1);
          String SName = res6.getString(2);
          int    iWM   = res6.getInt(3);
          double dProd = res6.getDouble(4);

          int index = indexOf(iCode);
          CardClass cc = null;
          if(index==-1)
          {
               cc = new CardClass(iCode,SName);
               VCardClass.addElement(cc);
               index = VCardClass.size()-1;
          }
          cc = (CardClass)VCardClass.elementAt(index);
          cc.append(iWM,dProd);
     }
     private int indexOf(int iCode)
     {
          int index=-1;
          for(int i=0;i<VCardClass.size();i++)
          {
               CardClass cc = (CardClass)VCardClass.elementAt(i);
               if(cc.iWebCode==iCode)
               {
                    index = i;
                    break;
               }
          }
          return index;
     }
     private void calcVectors()
     {
               for(int i=0;i<VMachNo.size();i++)
               {
                     double dProdTar = common.toDouble((String)VProdTar.elementAt(i));
                     double dProd    = common.toDouble((String)VProd.elementAt(i));
                     double dProdExp = common.toDouble((String)VProdExp.elementAt(i));
                     double dHal     = common.toDouble((String)VHourAllot.elementAt(i));
                     double dStopAcc = common.toDouble((String)VStopAcc.elementAt(i))/60;
                     VProdLoss    .addElement((String)common.getRound(dProd-dProdTar,3));
                     VActualEffy  .addElement((String)common.getRound((dProd/dProdExp*100),3));
                     VEffyAvai    .addElement((String)common.getRound(((dHal-(dStopAcc/60))*100/dHal),3));
               }
     }

     private String getQString()
     {
		String QString = " SELECT  cardtran.sp_date,unit.unitname,cardtran.shift_code, machine.mach_name, " + 
					" count.count_name, cardtran.ticket, cardtran.ticket, cardtran.doffer_rpm, " + 
					" cardtran.tensiondraft,cardtran.sliverhank, cardtran.hank_exp,cardtran.hank_tar," + 
					" cardtran.hank,  cardtran.prod_exp, cardtran.prod_tar, cardtran.prod," + 
					" cardtran.hourrun,cardtran.hourmeter,cardtran.ueoa,cardtran.uem," + 
					" cardtran.workereffy,cardtran.stopacc,cardtran.stopnonacc,cardtran.houralloted," + 
					" cardtran.noofro+cardtran.noofroac,cardtran.noofpersons+cardtran.noofpersonsac," + 
					" cardtran.rotime+cardtran.rotimeac,machine.com,cardtran.WM,CardTran.BreaksPer100Km  " + 
					" FROM ((((cardtran Left JOIN unit ON cardtran.unit_code = unit.unitcode) " + 
					" Left JOIN machine ON cardtran.mach_code = machine.mach_code)" + 
					" Left JOIN count ON cardtran.count_Code = count.count_code) " + 
					" Left JOIN SHADE ON cardtran.shade_code = SHADE.shade_code) " + 
					" Where cardtran.sp_date='"+SDate+"' and cardtran.Shift_Code = "+SShift+" and " + 
					" CardTran.Unit_Code = "+SUnit+ " ORDER BY to_number(machine.mach_name)";
		   //System.out.println(QString);
             return QString;
     }
     public String getSupCd()
     {
		String QS = " SELECT supervisor.supervisor FROM cardtran " + 
 				  " INNER JOIN supervisor ON cardtran.supcode = supervisor.supcode " +
				  " GROUP BY supervisor.supervisor, cardtran.Unit_Code, " +
				  " cardtran.sp_date, cardtran.shift_code " +
                      " HAVING (((cardtran.Unit_Code)="+SUnit+") AND "+
                      " ((cardtran.sp_date)='"+SDate+"') AND "+
                      " ((cardtran.shift_code)="+SShift+")) ";
		//System.out.println(QS);

          return QS;
     }
     private String getAbsQS()
     {
          String QS="";
          QS = " SELECT cardtran.web_code,WebType.Web_Name, cardtran.wm, Sum(cardtran.prod) AS SumOfprod "+
               " FROM cardtran INNER JOIN WebType ON cardtran.WEB_code = WebType.Web_Code "+
               " WHERE cardtran.unit_code="+SUnit+" AND "+ " cardtran.sp_date='"+SDate+"' AND "+
               " cardtran.shift_code="+SShift+ " GROUP BY WebType.Web_Name, cardtran.wm,cardtran.web_code";
          return QS;          
     }
     private String getQStringTemp()
     {
		String QString = " SELECT  CardTran_Temp.sp_date,unit.unitname,CardTran_Temp.shift_code, machine.mach_name, " + 
					" count.count_name, CardTran_Temp.ticket, CardTran_Temp.ticket, CardTran_Temp.doffer_rpm, " + 
					" CardTran_Temp.tensiondraft,CardTran_Temp.sliverhank, CardTran_Temp.hank_exp,CardTran_Temp.hank_tar," + 
					" CardTran_Temp.hank,  CardTran_Temp.prod_exp, CardTran_Temp.prod_tar, CardTran_Temp.prod," + 
					" CardTran_Temp.hourrun,CardTran_Temp.hourmeter,CardTran_Temp.ueoa,CardTran_Temp.uem," + 
					" CardTran_Temp.workereffy,CardTran_Temp.stopacc,CardTran_Temp.stopnonacc,CardTran_Temp.houralloted," + 
					" CardTran_Temp.noofro+CardTran_Temp.noofroac,CardTran_Temp.noofpersons+CardTran_Temp.noofpersonsac," + 
					" CardTran_Temp.rotime+CardTran_Temp.rotimeac,machine.com,CardTran_Temp.WM,CardTran_Temp.BreaksPer100Km  " + 
					" FROM ((((CardTran_Temp Left JOIN unit ON CardTran_Temp.unit_code = unit.unitcode) " + 
					" Left JOIN machine ON CardTran_Temp.mach_code = machine.mach_code)" + 
					" Left JOIN count ON CardTran_Temp.count_Code = count.count_code) " + 
					" Left JOIN SHADE ON CardTran_Temp.shade_code = SHADE.shade_code) " + 
					" Where CardTran_Temp.sp_date='"+SDate+"' and CardTran_Temp.Shift_Code = "+SShift+" and " + 
					" CardTran_Temp.Unit_Code = "+SUnit+ " ORDER BY to_number(machine.mach_name)";
		   //System.out.println(QString);
             return QString;
     }

     public String getSupCdTemp()
     {
		String QS = " SELECT supervisor.supervisor FROM CardTran_Temp " + 
 				  " INNER JOIN supervisor ON CardTran_Temp.supcode = supervisor.supcode " +
				  " GROUP BY supervisor.supervisor, CardTran_Temp.Unit_Code, " +
				  " CardTran_Temp.sp_date, CardTran_Temp.shift_code " +
                      " HAVING (((CardTran_Temp.Unit_Code)="+SUnit+") AND "+
                      " ((CardTran_Temp.sp_date)='"+SDate+"') AND "+
                      " ((CardTran_Temp.shift_code)="+SShift+")) ";
		//System.out.println(QS);

          return QS;
     }
     private String getAbsQSTemp()
     {
          String QS="";
          QS = " SELECT CardTran_Temp.web_code,WebType.Web_Name, CardTran_Temp.wm, Sum(CardTran_Temp.prod) AS SumOfprod "+
               " FROM CardTran_Temp INNER JOIN WebType ON CardTran_Temp.WEB_code = WebType.Web_Code "+
               " WHERE CardTran_Temp.unit_code="+SUnit+" AND "+ " CardTran_Temp.sp_date='"+SDate+"' AND "+
               " CardTran_Temp.shift_code="+SShift+ " GROUP BY WebType.Web_Name, CardTran_Temp.wm,CardTran_Temp.web_code";
          return QS;          
     }

	  private String getTicket(String SEmpCode,Connection conn)
	  {	
		   String SRet="UnKnown";
		   if(SEmpCode.equals("0"))
			   return SRet;
		   try
		   {			   
			   String SQry = " select empname from hrdnew.SchemeApprentice where empcode="+SEmpCode+" union " +
							 " select empname from hrdnew.ActApprentice where empcode="+SEmpCode+" union  " + 
							 " select empname from hrdnew.ContractApprentice where empcode="+SEmpCode;
			  // Class.forName("oracle.jdbc.OracleDriver");
			  // Connection conn = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
			   Statement stat  = conn.createStatement();
			   ResultSet res   = stat.executeQuery(SQry);
			   if(res.next())
				   SRet            = res.getString(1);
                           res.close();
                           stat.close();
//			   System.out.println(SQry + ":" + SRet);
		   }catch(Exception ee)
		   {
			   System.out.println("getTicket->" + ee);
			   return SRet;
		   }
		   return SRet;
      }





     private void Head()
     {
          if(Lctr < 60)
             return;
          if(Pctr > 0)
          {
             try
             {
               FW.write(SLine+"\n");
             }catch(Exception ex){}
          }
          Pctr++;

          String Str1    = common.Pad("Company    : AMARJOTHI SPINNING MILLS LIMITED",80);
          String Str2    = common.Pad("Document   : Carding Daily Report On "+common.parseDate(SDate),80);
          String Str3    = common.Pad("Unit       : "+(SUnit.equals("1")?"A":(SUnit.equals("2")?"B":(SUnit.equals("3")?"C":"S"))),70);
          String Str4    = common.Pad("Shift      : "+(SShift.equals("1")?"Day":(SShift.equals("2")?"Half Night":"Full Night")),70);
          String Str5    = common.Pad("P.O/J.P.O  : "+SSupName,70);
          VHead = Spin1Head();
          try
          {
               FW.write(""+Str1+"\n");
               FW.write(Str2+"\n");
               FW.write(Str3+"\n");
               FW.write(Str4+"\n");
               FW.write(Str5+"\n");
               for(int i=0;i<VHead.size();i++)
               {
                    FW.write((String)VHead.elementAt(i));
               }
          }catch(Exception ex){}
          Lctr = 4+VHead.size();
     }
     private void Body()
     {
         String Strl = "";
         for(int i=0;i<VMachNo.size();i++)
         {
                Strl  = common.Pad((String)VMachNo.elementAt(i),4)+common.Space(2)+
                        common.Pad((String)VTicket.elementAt(i),6)+common.Space(2)+
                        common.Pad((String)VName.elementAt(i),10)+common.Space(2)+
                        common.Pad((String)VCount.elementAt(i),10)+common.Space(2)+
                        common.Rad((String)VSpeed.elementAt(i),3)+common.Space(2)+
                        common.Rad((String)VDraft.elementAt(i),7)+common.Space(2)+
                        common.Rad((String)VSlHank.elementAt(i),7)+common.Space(2)+
                        common.Rad(common.parseNull((String)VBreaks.elementAt(i)),15)+common.Space(2)+
                        common.Rad((String)VNoRunOut.elementAt(i),3)+common.Space(2)+
                        common.Rad((String)VNoPersons.elementAt(i),3)+common.Space(2)+
                        common.Rad((String)VRoTime.elementAt(i),4)+common.Space(2)+
                        common.Rad(" ",4)+common.Space(2)+
                        common.Rad((String)VHankExp.elementAt(i),9)+common.Space(2)+
                        common.Rad((String)VHankTar.elementAt(i),9)+common.Space(2)+
                        common.Rad((String)VHank.elementAt(i),9)+common.Space(2)+
                        common.Rad((String)VProdExp.elementAt(i),9)+common.Space(2)+
                        common.Rad((String)VProdTar.elementAt(i),9)+common.Space(2)+
                        common.Rad((String)VProd.elementAt(i),9)+common.Space(2)+
                        common.Rad((String)VProdLoss.elementAt(i),9)+common.Space(2)+
                        common.Rad((String)VActualEffy.elementAt(i),7)+common.Space(2)+
                        common.Rad((String)VHourRun.elementAt(i),5)+common.Space(2)+
                        common.Rad((String)VHourMeter.elementAt(i),5)+common.Space(2)+
                        common.Rad((String)VUEOA.elementAt(i),6)+common.Space(2)+
                        common.Rad((String)VUEM.elementAt(i),6)+common.Space(2)+
                        common.Rad((String)VEffyAvai.elementAt(i),6)+common.Space(2)+
                        common.Rad((String)VWorkerEffy.elementAt(i),6)+common.Space(2)+
                        common.Rad((String)VStopAcc.elementAt(i),6)+common.Space(2)+
                        common.Rad((String)VStopNonAcc.elementAt(i),6)+"\n";

                try
                {
                     FW.write(Strl);
                     Lctr++;
                     Head();
                }catch(Exception ex){}
         }

           try
           {
               FW.write(SLine);                   
           }catch(Exception ex){}

           Strl  = common.Pad(" ",4)+common.Space(2)+
                   common.Pad(" ",6)+common.Space(2)+
                   common.Pad("Total",10)+common.Space(2)+
                   common.Pad(" ",10)+common.Space(2)+
                   common.Rad(common.getSum(VSpeed,"A"),3)+common.Space(2)+
                   common.Rad(common.getSum(VDraft,"A"),7)+common.Space(2)+
                   common.Rad(common.getSum(VSlHank,"A"),7)+common.Space(2)+
                   common.Rad(common.getSum(VBreaks,"S"),15)+common.Space(2)+
                   common.Rad(" ",3)+common.Space(2)+
                   common.Rad(" ",3)+common.Space(2)+
                   common.Rad(" ",4)+common.Space(2)+
                   common.Rad(" ",4)+common.Space(2)+
                   common.Rad(common.getSum(VHankExp,"S"),9)+common.Space(2)+
                   common.Rad(common.getSum(VHankTar,"S"),9)+common.Space(2)+
                   common.Rad(common.getSum(VHank,"S"),9)+common.Space(2)+
                   common.Rad(common.getSum(VProdExp,"S"),9)+common.Space(2)+
                   common.Rad(common.getSum(VProdTar,"S"),9)+common.Space(2)+
                   common.Rad(common.getSum(VProd,"S"),9)+common.Space(2)+
                   common.Rad(common.getSum(VProdLoss,"S"),9)+common.Space(2)+
                   common.Rad(common.getSum(VActualEffy,"A"),7)+common.Space(2)+
                   common.Rad(common.getSum(VHourRun,"S"),5)+common.Space(2)+
                   common.Rad(common.getSum(VHourMeter,"S"),5)+common.Space(2)+
                   common.Rad(common.getSum(VUEOA,"A"),6)+common.Space(2)+
                   common.Rad(common.getSum(VUEM,"A"),6)+common.Space(2)+
                   common.Rad(common.getSum(VEffyAvai,"A"),6)+common.Space(2)+
                   common.Rad(common.getSum(VWorkerEffy,"A"),6)+common.Space(2)+
                   common.Rad(common.getSum(VStopAcc,"S"),6)+common.Space(2)+
                   common.Rad(common.getSum(VStopNonAcc,"S"),6)+"\n";

           try
           {
               FW.write(Strl);
               FW.write(SLine);
           }catch(Exception ex){}

           try
           {
/*             FW.write(getFootString(0,0,"Lap Melange"));
               FW.write(getFootString(0,1,"Lap White"));
               FW.write(getFootString(0,-1,"Lap Total"));
               FW.write("\n");

               FW.write(getFootString(1,0,"Chute Melange"));
               FW.write(getFootString(1,1,"Chute White"));
               FW.write(getFootString(1,-1,"Chute Total"));
               FW.write("\n");

               FW.write(getFootString(0,"Melange Total"));
               FW.write(getFootString(1,"White Total"));
               FW.write("\n"); */
           }catch(Exception ex){}

           try
           {
               FW.write(SLine+"\n");
               Lctr+=15;
               prtWebAbs();
               FW.write("\n\nP.O          S.P.O.           S.M.             D.M.\n");
               FW.write("<End Of Report>");
           }catch(Exception ex){}

     }
     private String getFootString(int iFeed,int iWM,String str)
     {
           String Strl  = common.Pad(" ",4)+common.Space(2)+
                  common.Pad(" ",6)+common.Space(2)+
                  common.Pad(str,10)+common.Space(2)+
                  common.Pad(" ",10)+common.Space(2)+
                  common.Rad(getSum(VSpeed,"A",iFeed,iWM),3)+common.Space(2)+
                  common.Rad(getSum(VDraft,"A",iFeed,iWM),7)+common.Space(2)+
                  common.Rad(getSum(VSlHank,"S",iFeed,iWM),7)+common.Space(2)+
                  common.Rad(" ",15)+common.Space(2)+
                  common.Rad(" ",3)+common.Space(2)+
                  common.Rad(" ",3)+common.Space(2)+
                  common.Rad(" ",4)+common.Space(2)+
                  common.Rad(" ",4)+common.Space(2)+
                  common.Rad(getSum(VHankExp,"S",iFeed,iWM),9)+common.Space(2)+
                  common.Rad(getSum(VHankTar,"S",iFeed,iWM),9)+common.Space(2)+
                  common.Rad(getSum(VHank,"S",iFeed,iWM),9)+common.Space(2)+
                  common.Rad(getSum(VProdExp,"S",iFeed,iWM),9)+common.Space(2)+
                  common.Rad(getSum(VProdTar,"S",iFeed,iWM),9)+common.Space(2)+
                  common.Rad(getSum(VProd,"S",iFeed,iWM),9)+common.Space(2)+
                  common.Rad(getSum(VProdLoss,"S",iFeed,iWM),9)+common.Space(2)+
                  common.Rad(getSum(VActualEffy,"A",iFeed,iWM),7)+common.Space(2)+
                  common.Rad(getSum(VHourRun,"S",iFeed,iWM),5)+common.Space(2)+
                  common.Rad(getSum(VHourMeter,"S",iFeed,iWM),5)+common.Space(2)+
                  common.Rad(getSum(VUEOA,"A",iFeed,iWM),6)+common.Space(2)+
                  common.Rad(getSum(VUEM,"A",iFeed,iWM),6)+common.Space(2)+
                  common.Rad(getSum(VEffyAvai,"A",iFeed,iWM),6)+common.Space(2)+
                  common.Rad(getSum(VWorkerEffy,"A",iFeed,iWM),6)+common.Space(2)+
                  common.Rad(getSum(VStopAcc,"S",iFeed,iWM),6)+common.Space(2)+
                  common.Rad(getSum(VStopNonAcc,"S",iFeed,iWM),6)+"\n";

          return Strl;

     }
     private String getFootString(int iWM,String str)
     {
           String Strl  = common.Pad(" ",4)+common.Space(2)+
                  common.Pad(" ",6)+common.Space(2)+
                  common.Pad(str,10)+common.Space(2)+
                  common.Pad(" ",10)+common.Space(2)+
                  common.Rad(getSum(VSpeed,"A",iWM),3)+common.Space(2)+
                  common.Rad(getSum(VDraft,"A",iWM),7)+common.Space(2)+
                  common.Rad(getSum(VSlHank,"S",iWM),7)+common.Space(2)+
                   common.Rad(" ",15)+common.Space(2)+
                  common.Rad(" ",3)+common.Space(2)+
                  common.Rad(" ",3)+common.Space(2)+
                  common.Rad(" ",4)+common.Space(2)+
                  common.Rad(" ",4)+common.Space(2)+
                  common.Rad(getSum(VHankExp,"S",iWM),9)+common.Space(2)+
                  common.Rad(getSum(VHankTar,"S",iWM),9)+common.Space(2)+
                  common.Rad(getSum(VHank,"S",iWM),9)+common.Space(2)+
                  common.Rad(getSum(VProdExp,"S",iWM),9)+common.Space(2)+
                  common.Rad(getSum(VProdTar,"S",iWM),9)+common.Space(2)+
                  common.Rad(getSum(VProd,"S",iWM),9)+common.Space(2)+
                  common.Rad(getSum(VProdLoss,"S",iWM),9)+common.Space(2)+
                  common.Rad(getSum(VActualEffy,"A",iWM),7)+common.Space(2)+
                  common.Rad(getSum(VHourRun,"S",iWM),5)+common.Space(2)+
                  common.Rad(getSum(VHourMeter,"S",iWM),5)+common.Space(2)+
                  common.Rad(getSum(VUEOA,"A",iWM),6)+common.Space(2)+
                  common.Rad(getSum(VUEM,"A",iWM),6)+common.Space(2)+
                  common.Rad(getSum(VEffyAvai,"A",iWM),6)+common.Space(2)+
                  common.Rad(getSum(VWorkerEffy,"A",iWM),6)+common.Space(2)+
                  common.Rad(getSum(VStopAcc,"S",iWM),6)+common.Space(2)+
                  common.Rad(getSum(VStopNonAcc,"S",iWM),6)+"\n";

          return Strl;
     }

     private Vector Spin1Head()
     {
          Vector VHead = new Vector();

          String Str2 = common.Space(4)+common.Space(2)+
                        common.Space(6)+common.Space(2)+
                        common.Space(10)+common.Space(2)+
                        common.Space(10)+common.Space(2)+
                        common.Space(3)+common.Space(2)+
                        common.Space(7)+common.Space(2)+
                        common.Space(7)+common.Space(2)+
                        common.Space(15)+common.Space(2)+
                        common.Cad("Shade Change",20)+common.Space(2)+
                        common.Cad("Production Details",84)+common.Space(2)+
                        common.Cad("Utilization",28)+common.Space(2)+
                        common.Cad("Worker Effy",30)+"\n";

          String Str3 = common.Pad("Mach",4)+common.Space(2)+
                        common.Pad("Ticket",6)+common.Space(2)+
                        common.Pad("Sider",10)+common.Space(2)+
                        common.Pad("Count",10)+common.Space(2)+
                        common.Pad("Dof",3)+common.Space(2)+
                        common.Pad("Tension",7)+common.Space(2)+
                        common.Pad("Sliver",7)+common.Space(2)+
                        common.Pad("Breaks Per",15)+common.Space(2)+
                        common.Replicate("-",20)+common.Space(2)+
                        common.Replicate("-",84)+common.Space(2)+
                        common.Replicate("-",28)+common.Space(2)+
                        common.Replicate("-",30)+"\n";

          String Str4 = common.Pad("No",4)+common.Space(2)+
                        common.Pad("No",6)+common.Space(2)+
                        common.Pad("Name",10)+common.Space(2)+
                        common.Pad("Shade",10)+common.Space(2)+
                        common.Rad("Spe",3)+common.Space(2)+
                        common.Rad("Draft",7)+common.Space(2)+
                        common.Rad("Hank",7)+common.Space(2)+
                        common.Rad("100Km",15)+common.Space(2)+
                        common.Rad("No",3)+common.Space(2)+
                        common.Rad("Per",3)+common.Space(2)+
                        common.Rad("Actual",4)+common.Space(2)+
                        common.Rad("Exc/",4)+common.Space(2)+
                        common.Rad("Exp Hank",9)+common.Space(2)+
                        common.Rad("Tar Hank",9)+common.Space(2)+
                        common.Rad("Act Hank",9)+common.Space(2)+
                        common.Rad("Exp Prod",9)+common.Space(2)+
                        common.Rad("Tar Prod",9)+common.Space(2)+
                        common.Rad("Act Prod",9)+common.Space(2)+
                        common.Rad("Prod Loss",9)+common.Space(2)+
                        common.Rad("ActEffy",7)+common.Space(2)+
                        common.Rad("Hour",5)+common.Space(2)+
                        common.Rad("Hour",5)+common.Space(2)+
                        common.Rad("UxE",6)+common.Space(2)+
                        common.Rad("UxE",6)+common.Space(2)+
                        common.Rad("Effy",6)+common.Space(2)+
                        common.Rad("Worker",6)+common.Space(2)+
                        common.Rad("Acc",6)+common.Space(2)+
                        common.Rad("NonAcc",6)+"\n";

          String Str5 = common.Pad(" ",4)+common.Space(2)+
                        common.Pad(" ",6)+common.Space(2)+
                        common.Pad(" ",10)+common.Space(2)+
                        common.Pad(" ",10)+common.Space(2)+
                        common.Rad(" ",3)+common.Space(2)+
                        common.Rad(" ",7)+common.Space(2)+
                        common.Rad(" ",7)+common.Space(2)+
                        common.Rad("Time",15)+common.Space(2)+
                        common.Rad(" ",3)+common.Space(2)+
                        common.Rad("son",3)+common.Space(2)+
                        common.Rad(" ",4)+common.Space(2)+
                        common.Rad("Shor",4)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad("A - T",9)+common.Space(2)+
                        common.Rad("A / E",7)+common.Space(2)+
                        common.Rad("toWork",5)+common.Space(2)+
                        common.Rad("Meter",5)+common.Space(2)+
                        common.Rad("(OA)",6)+common.Space(2)+
                        common.Rad("(MU)",6)+common.Space(2)+
                        common.Rad("Avail",6)+common.Space(2)+
                        common.Rad("Effy",6)+common.Space(2)+
                        common.Rad("Stop",6)+common.Space(2)+
                        common.Rad("Stop",6)+"\n";

          String Str6 = common.Pad(" ",4)+common.Space(2)+
                        common.Pad(" ",6)+common.Space(2)+
                        common.Pad(" ",10)+common.Space(2)+
                        common.Pad(" ",10)+common.Space(2)+
                        common.Rad("mpm",3)+common.Space(2)+
                        common.Rad(" ",7)+common.Space(2)+
                        common.Rad(" ",7)+common.Space(2)+
                        common.Rad(" ",15)+common.Space(2)+
                        common.Rad(" ",3)+common.Space(2)+
                        common.Rad(" ",3)+common.Space(2)+
                        common.Rad("min",4)+common.Space(2)+
                        common.Rad("min",4)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad("in Kgs",9)+common.Space(2)+
                        common.Rad("in Kgs",9)+common.Space(2)+
                        common.Rad("in Kgs",9)+common.Space(2)+
                        common.Rad("in Kgs",9)+common.Space(2)+
                        common.Rad("100%",7)+common.Space(2)+
                        common.Rad("Hr",5)+common.Space(2)+
                        common.Rad("Hr",5)+common.Space(2)+
                        common.Rad("80%",6)+common.Space(2)+
                        common.Rad("85%",6)+common.Space(2)+
                        common.Rad("%",6)+common.Space(2)+
                        common.Rad("95%",6)+common.Space(2)+
                        common.Rad("min",6)+common.Space(2)+
                        common.Rad("min",6)+"\n";

          String StrL = common.Space(4)+common.Space(2)+
                        common.Space(6)+common.Space(2)+
                        common.Space(10)+common.Space(2)+
                        common.Space(10)+common.Space(2)+
                        common.Replicate("-",3)+common.Space(2)+
                        common.Replicate("-",7)+common.Space(2)+
                        common.Replicate("-",7)+common.Space(2)+
                        common.Replicate("-",15)+common.Space(2)+
                        common.Replicate("-",20)+common.Space(2)+
                        common.Replicate("-",84)+common.Space(2)+
                        common.Replicate("-",28)+common.Space(2)+
                        common.Replicate("-",30)+"\n";

          VHead.addElement(SLine);
          VHead.addElement(Str2);
          VHead.addElement(Str3);
          VHead.addElement(Str4);
          VHead.addElement(Str5);
          VHead.addElement(StrL);
          VHead.addElement(Str6);
          VHead.addElement(SLine);

          return VHead;
     }
     private String getSum(Vector Vx,String SOp,int iWM)
     {
            double dAmount=0;
            int    iCtr=0;
            for(int i=0;i<Vx.size();i++)
            {
                if(common.toInt((String)VWM.elementAt(i))==iWM)
                {
                      dAmount = dAmount+common.toDouble((String)Vx.elementAt(i));
                      iCtr++;
                }
            }
            if(SOp.equals("A"))
            {
              dAmount = iCtr==0?0:dAmount/iCtr;
            }
            return common.getRound(dAmount,2);
      }
      private String getSum(Vector Vx,String SOp,int iFeed,int iWM)
      {
            double dAmount=0;
            int    iCtr=0;
            for(int i=0;i<Vx.size();i++)
            {
                if(common.toInt((String)VFeed.elementAt(i))==iFeed)
                {
                      if(iWM == -1)
                      {
                         dAmount = dAmount+common.toDouble((String)Vx.elementAt(i));
                         iCtr++;
                      }
                      else
                      {
                         if(common.toInt((String)VWM.elementAt(i))==iWM)
                         {
                              dAmount = dAmount+common.toDouble((String)Vx.elementAt(i));
                              iCtr++;
                         }
                      }
                }
            }
            if(SOp.equals("A"))
            {
              dAmount = iCtr==0?0:dAmount/iCtr;
            }
            return common.getRound(dAmount,2);
      }
     private void prtWebAbs() throws Exception
     {
          if(Lctr > 56)
               FW.write("\n");

          double dW=0,dM=0;
          FW.write("Web Typewise Abstract\n");
          FW.write(common.Replicate("-",15+2+10+2+10+2+10)+"\n");
          FW.write(common.Pad("Web Type",15)+common.Space(2)+common.Rad("Melange",10)+common.Space(2)+common.Rad("White",10)+common.Space(2)+common.Rad("Total",10)+"\n");
          FW.write(common.Replicate("-",15+2+10+2+10+2+10)+"\n");
          for(int i=0;i<VCardClass.size();i++)
          {
               CardClass cc = (CardClass)VCardClass.elementAt(i);
               FW.write(common.Pad(cc.SWebName,15)+common.Space(2)+common.Rad(common.getRound(cc.dWhite,2),10)+common.Space(2)+common.Rad(common.getRound(cc.dMelange,2),10)+common.Space(2)+common.Rad(common.getRound(cc.dWhite+cc.dMelange,2),10)+"\n");
               dW += cc.dWhite;
               dM += cc.dMelange;
          }
          FW.write(common.Replicate("-",15+2+10+2+10+2+10)+"\n");
          FW.write(common.Pad("Total",15)+common.Space(2)+common.Rad(common.getRound(dW,2),10)+common.Space(2)+common.Rad(common.getRound(dM,2),10)+common.Space(2)+common.Rad(common.getRound(dW+dM,2),10)+"\n");
          FW.write(common.Replicate("-",15+2+10+2+10+2+10)+"\n");
     }

	private void setCriteria(HttpServletRequest request)
	{
		SDate            = request.getParameter("Date");
          SDate            = (String)common.pureDate(SDate);
          SShift           = request.getParameter("Shift");
		SUnit            = request.getParameter("Unit");

		session.putValue("Unit",SUnit);
		session.putValue("Shift",SShift);
		session.putValue("Date",SDate);

		//System.out.println("Card -> " + SUnit + ":" + SShift + ":" + SDate);
	}

     private String getReportType()
	{
		if(bFinalized)
			return "Finalized";
		else
			return "UnFinalized";
	}

	private void setFinalized()
	{
		String SQry = " select count(*) from CardTran " + 
				    " Where sp_date='"+SDate+"' and Shift_Code = "+SShift+" and  " +
				    " Unit_Code = "+SUnit;
		try
		{
                        FileWriter fw = new FileWriter("d:\\final.sql");
                        fw.write(SQry);
                        fw.close();
			Connection con = createConnection();
			Statement   st = con.createStatement();
			ResultSet   rs = st.executeQuery(SQry);
			rs.next();
			if(rs.getInt(1) > 0)
				bFinalized = true;
			else
				bFinalized = false;

                        rs.close();
                        st.close();  
		}catch(Exception ee){System.out.println("setFinalized->" + ee);}	
	}

	private Connection createConnection()
	{
		Connection conn;
		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
			conn  = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
		}catch(Exception ee)
		{
			return null;
		}
		return conn;
	}
        
//PDF
        
    public void createPDFFile(){
     try{
         
                
        SFile       =   "CardingDailyReportPDF.pdf";
        SFile       =   common.getPrintPath()+SFile;    
        document    =   new Document(PageSize.A4.rotate());
        writer      =  PdfWriter.getInstance(document, new FileOutputStream(SFile));
        TableHeader event   = new TableHeader();
        writer      .   setPageEvent(event);
        document    .   open();

        table1      =   new PdfPTable(28);
        table1      .   setWidths(iWidth1);
        table1      .   setSpacingAfter(30);
        table1      .   setWidthPercentage(100);
        table1      .   setHeaderRows(9);
        
        table0      =   new PdfPTable(4);
        table0      .   setWidths(iWidth0);
        table0      .   setSpacingAfter(5);
        table0      .   setWidthPercentage(50);
        table0      .   setHorizontalAlignment(Element.ALIGN_LEFT);
        

            setPDFHead();
            setPDFBody();
            setWebAbs();
            document.add(table1);
            document.add(table0);
            document.close();
      
       }
        catch (Exception e){
           e.printStackTrace();
            System.out.println("Error-->"+e);
        }
        finally{
            try{
                document.close();
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }
    }
    
    public void setPDFHead(){
        try
        {
            
            String sHead1       = "Company    : AMARJOTHI SPINNING MILLS LIMITED";
            String sHead2       = "Document   : Carding Daily Report On "+common.parseDate(SDate);
            String sHead3       = "Unit             : "+(SUnit.equals("1")?"A":(SUnit.equals("2")?"B":(SUnit.equals("3")?"C":"S")));
            String sHead4       = "Shift            : "+(SShift.equals("1")?"Day":(SShift.equals("2")?"Half Night":"Full Night"));
            String sHead5       = "P.O/J.P.O   : "+SSupName;
        
            AddCellIntoTable(sHead1, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,28,20f,0,0,0,0, bigBold );
            AddCellIntoTable(sHead2, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,28,20f,0,0,0,0, bigBold );
            AddCellIntoTable(sHead3, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,28,20f,0,0,0,0, bigBold );
            AddCellIntoTable(sHead4, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,28,20f,0,0,0,0, bigBold );
            AddCellIntoTable(sHead5, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,28,20f,0,0,0,0, bigBold );
           
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,28,10f,0,0,0,0, tinyBold );
            
            AddHeadCellIntoTable("Mach No", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Ticket No", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Sider Name", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Count / Shade", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Doffer Speed", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Tension Draft", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Silver Hank", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Breaks Per 100Km Times", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,20f,8,3,5,4, tinyBold );
           
            AddCellIntoTable("Shade Change", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,4,20f,8,3,5,4, tinyBold );
            AddCellIntoTable("Production Details", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,8,20f,8,3,5,4, tinyBold );
            AddCellIntoTable("Utilization", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,4,20f,8,3,5,4, tinyBold );
            AddCellIntoTable("Worker Effy", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,4,20f,8,3,5,4, tinyBold );
            
            
            //SECOND
            
            AddHeadCellIntoTable("Noof SC", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Noof Person", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Actual", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Ex / Sh", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,20f,8,3,5,4, tinyBold );
            
            AddCellIntoTable("Hank", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,20f,8,3,5,4, tinyBold );
            AddCellIntoTable("Production ", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,4,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Actual Eff A/E", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,20f,8,3,5,4, tinyBold );
            
            AddHeadCellIntoTable("Hours tobe worked", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Hour Meter Reading", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("UxE (OA)", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("UxE (MU)", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,20f,8,3,5,4, tinyBold );
            
            AddHeadCellIntoTable("Effy Available", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Worker Effy", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Accep Stoppage", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Non Acc Stopage", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,20f,8,3,5,4, tinyBold );
            // THIRD
            AddCellIntoTable("Expected", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,20f,8,3,5,4, tinyBold );
            AddCellIntoTable("Target", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,20f,8,3,5,4, tinyBold );
            AddCellIntoTable("Actual", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,20f,8,3,5,4, tinyBold );
            
            AddCellIntoTable("Expected", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,25f,8,3,5,4, tinyBold );
            AddCellIntoTable("Target", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,25f,8,3,5,4, tinyBold );
            AddCellIntoTable("Actual", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,25f,8,3,5,4, tinyBold );
            AddCellIntoTable("Prod Loss      A-T ", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,25f,8,3,5,4, tinyBold );
//            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,25f,8,3,5,4, tinyBold );

            //4TH
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("rpm", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
//            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );

            for(int i=0;i<=4;i++){
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            }
            
            AddCellIntoTable("min", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("min", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            
            for(int i=0;i<=2;i++){
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            }
            AddCellIntoTable("Kgs ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("Kgs ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("Kgs ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("Kgs ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
           
            AddCellIntoTable("100% ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("Hrs ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("Hrs ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("80% ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("85% ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("% ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("95% ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            
            AddCellIntoTable("min", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("min ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            
//            for(int i=0;i<=2;i++){
//            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
//            }
                   
         }
        catch(Exception Ex){
                Ex.printStackTrace();
                System.out.println("Error Head1-->"+Ex);
        }
    }
    
    public void setPDFBody(){
      try{
          for(int i=0;i<VMachNo.size();i++){
          
          AddCellIntoTable(common.parseNull((String)VMachNo.elementAt(i)), table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VTicket.elementAt(i)), table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VName.elementAt(i)), table1, Element.ALIGN_LEFT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VCount.elementAt(i)), table1, Element.ALIGN_LEFT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
        
          AddCellIntoTable(common.parseNull((String)VSpeed.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VDraft.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VSlHank.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VBreaks.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
         
          AddCellIntoTable(common.parseNull((String)VNoRunOut.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VNoPersons.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VRoTime.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(" ", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          
          AddCellIntoTable(common.parseNull((String)VHankExp.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VHankTar.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VHank.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VProdExp.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VProdTar.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          
          AddCellIntoTable(common.parseNull((String)VProd.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VProdLoss.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VActualEffy.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VHourRun.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VHourMeter.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VUEOA.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VUEM.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                 
          AddCellIntoTable(common.parseNull((String)VEffyAvai.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          
          AddCellIntoTable(common.parseNull((String)VWorkerEffy.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VStopAcc.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VStopNonAcc.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          
         }
            AddCellIntoTable(" TOTAL", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,4,20f,5,3,8,4, smallBold );                    
            AddCellIntoTable(common.getSum(VSpeed,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getSum(VDraft,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getSum(VSlHank,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getSum(VBreaks,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    

            AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            
            AddCellIntoTable(common.getSum(VHankExp,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VHankTar,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VHank,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VProdExp,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VProdTar,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VProd,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VProdLoss,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            
            AddCellIntoTable(common.getSum(VActualEffy,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VHourRun,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VHourMeter,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VUEOA,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VUEM,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VEffyAvai,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VWorkerEffy,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VStopAcc,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VStopNonAcc,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
          
           try
           {
//               getPDFFootString(0,0,"Lap Melange");
//               getPDFFootString(0,1,"Lap White");
//               getPDFFootString(0,-1,"Lap Total");
//               getFootString(1,0,"Chute Melange");
//               getFootString(1,1,"Chute White");
//               getFootString(1,-1,"Chute Total");
//               getPDFFootString(0,"Melange Total");
//               getPDFFootString(1,"White Total");
               
           }catch(Exception ex){}

             AddCellIntoTable(" ", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,27,20f,0,0,0,0, tinyBold );   
        }
      catch(Exception Ex){
                Ex.printStackTrace();
                System.out.println("Error Body1-->"+Ex);
        }
    }
    
     private void getPDFFootString(int iFeed,int iWM,String str)
     {
         
                AddCellIntoTable(str, table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,4,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VSpeed,"A",iFeed,iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VDraft,"A",iFeed,iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VSlHank,"A",iFeed,iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(" ", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(" ", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(" ", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(" ", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                
                AddCellIntoTable(getSum(VHankExp,"S",iFeed,iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VHankTar,"S",iFeed,iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VHank,"S",iFeed,iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VProdExp,"S",iFeed,iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VProdTar,"S",iFeed,iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                
                AddCellIntoTable(getSum(VProd,"S",iFeed,iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VProdLoss,"S",iFeed,iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VActualEffy,"A",iFeed,iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VHourRun,"S",iFeed,iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VHourMeter,"S",iFeed,iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VUEOA,"A",iFeed,iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VUEM,"A",iFeed,iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VEffyAvai,"A",iFeed,iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VWorkerEffy,"A",iFeed,iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VStopAcc,"S",iFeed,iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VStopNonAcc,"S",iFeed,iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                
     }
     
     private void getPDFFootString(int iWM,String str)
     {
                AddCellIntoTable(str, table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,4,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VSpeed,"A",iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VDraft,"A",iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VSlHank,"A",iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(" ", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(" ", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(" ", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(" ", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                
                AddCellIntoTable(getSum(VHankExp,"S",iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VHankTar,"S",iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VHank,"S",iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VProdExp,"S",iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VProdTar,"S",iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                
                AddCellIntoTable(getSum(VProd,"S",iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VProdLoss,"S",iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VActualEffy,"A",iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VHourRun,"S",iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VHourMeter,"S",iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VUEOA,"A",iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VUEM,"A",iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VEffyAvai,"A",iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VWorkerEffy,"A",iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VStopAcc,"S",iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
                AddCellIntoTable(getSum(VStopNonAcc,"S",iWM), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
//                AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
//                AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
           
     }

     private void setWebAbs() throws Exception
     {
         
          double dW=0,dM=0;
           AddCellIntoTable("Web Typewise Abstract", table0, Element.ALIGN_CENTER, Element.ALIGN_TOP,4,20f,5,3,8,4, bigBold );                    
           AddCellIntoTable("Web Type", table0, Element.ALIGN_LEFT, Element.ALIGN_TOP,1,20f,5,3,8,4, smallBold );                    
           AddCellIntoTable("White", table0, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,20f,5,3,8,4, smallBold );                    
           AddCellIntoTable("Melange", table0, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,20f,5,3,8,4, smallBold );                    
           AddCellIntoTable("Total", table0, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,20f,5,3,8,4, smallBold );                    
           
          for(int i=0;i<VCardClass.size();i++)
          {
               CardClass cc = (CardClass)VCardClass.elementAt(i);
               AddCellIntoTable(cc.SWebName, table0, Element.ALIGN_LEFT, Element.ALIGN_TOP,1,20f,5,3,8,4, smallNormal );                    
               AddCellIntoTable(common.getRound(cc.dWhite,2), table0, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, smallNormal );                    
               AddCellIntoTable(common.getRound(cc.dMelange,2), table0, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, smallNormal );                    
               AddCellIntoTable(common.getRound(cc.dWhite+cc.dMelange,2), table0, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, smallNormal );                    
               
               dW += cc.dWhite;
               dM += cc.dMelange;
          }
          AddCellIntoTable("Total", table0, Element.ALIGN_LEFT, Element.ALIGN_TOP,1,20f,5,3,8,4, smallBold );                    
          AddCellIntoTable(common.getRound(dW,2), table0, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, smallBold );   
          AddCellIntoTable(common.getRound(dM,2), table0, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, smallBold );                    
          AddCellIntoTable(common.getRound(dW+dM,2), table0, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, smallBold );                    
          
            AddCellIntoTable(" ", table0, Element.ALIGN_RIGHT, Element.ALIGN_TOP,5,30f,0,0,0,0, smallBold );   
            
            AddCellIntoTable("P.O", table0, Element.ALIGN_LEFT, Element.ALIGN_TOP,1,20f,0,0,0,0, smallBold );                                            
            AddCellIntoTable("S.P.O", table0, Element.ALIGN_LEFT, Element.ALIGN_TOP,1,20f,0,0,0,0, smallBold );                                            
            AddCellIntoTable("S.M", table0, Element.ALIGN_LEFT, Element.ALIGN_TOP,1,20f,0,0,0,0, smallBold );                                            
            AddCellIntoTable("D.M", table0, Element.ALIGN_LEFT, Element.ALIGN_TOP,1,20f,0,0,0,0, smallBold );                                            
     }
     
        
    private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      } 
    
    private void AddHeadCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iRowSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
    
    private void AddCellIntoTable1(String Str,PdfPTable table,int iHorizontal,int iVertical,int iRowSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
      private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
      private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,int iRowSpan)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          //c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
      private void AddCellTable(Image image,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
          PdfPCell c1 = new PdfPCell();
		  c1.addElement(new Chunk(image, 5, -5));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

		  // image.scalePercent(90f);
      	  // paragraph.add(new Chunk(image, -1f, 1f));
      }
       private void AddCellIntoTable(Image image,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
          PdfPCell c1 = new PdfPCell();
		  //c1.addElement(new Chunk(image, 5, -5));
	  c1.addElement(image);
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

      }
     public void AddCellIntoTable2(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }
    
     class TableHeader extends PdfPageEventHelper {
        String header;
        PdfTemplate total;
        public void setHeader(String header) {
            this.header = header;
      }
      public void onOpenDocument(PdfWriter writer, Document document) {
            try{
            total = writer.getDirectContent().createTemplate(30, 16);
            }
            catch(Exception ex){
            }
      }

        public void onEndPage(PdfWriter writer, Document document) {
            PdfPTable table = new PdfPTable(3);
            try {
//                AddCellIntoTable("", table1, Element.ALIGN_LEFT, Element.ALIGN_TOP,13,18f,1,0,0,0, tinyNormal );
                table.setWidths(new int[]{24, 24, 2});
                table.setTotalWidth(520);
                table.setLockedWidth(true);
                table.getDefaultCell().setFixedHeight(20);
                table.getDefaultCell().setBorder(0);
                table.addCell(header);
                table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell("");
                table.addCell("");
                table.addCell("");
                
                table.addCell(String.format("Page %d of", writer.getPageNumber()));
                PdfPCell cell = new PdfPCell(Image.getInstance(total));
                cell.setBorder(0);
                table.addCell(cell);
                table.writeSelectedRows(0, -1, 10, 50, writer.getDirectContent());
            }
            catch(DocumentException de) {
                throw new ExceptionConverter(de);
            }
        }
        public void onCloseDocument(PdfWriter writer, Document document) {
          try{  
            ColumnText.showTextAligned(total, Element.ALIGN_LEFT,
                    new Phrase(String.valueOf(writer.getPageNumber() - 1)),
                    2, 2, 0);
             }
          catch(Exception ex){
              
          }
        }
    }
            

}