package CProduction;

import java.io.*;
import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;


import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;

import java.io.FileOutputStream;


public class OEConsReport extends HttpServlet
{
     HttpSession    session;
     Common common;

     Vector V1,V2,V3,V4,V5,V6,V7,V8,V9,V10,V11,V12,V13,V14,V15,V16;
     Vector V17,V18,V19,V20,V21,V22,V23,V24,Va1,Va2,Va3,Va4,Va5;
     Vector Va6,Va7,Va8,Va9,Va10,Va11,Va12,Va13,Va14,Va15,Va16,Va17,Va18,Va19,Va20,Va21,Va22,Va23,Va24;
     Vector VMakeClass;

     Vector VMake,VMakeCode,VMakeDay,VMakeTot;

     String SUnit,SStDate,SEnDate,SDummy;
     String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody,tbgBody;

     double d2=0.0,d3=0.0,d4=0.0,d5=0.0,d6=0.0,d7=0.0,d8=0.0,d9=0.0;
     double d11=0.0,d12=0.0,d13=0.0,d14=0.0,d15=0.0,d16=0.0,d17=0.0,d18=0.0;

     FileWriter  FW;
     int Lctr = 70,Pctr = 0;
     String SLine,SLine2;
     Vector VHead,VHeadAbs;
     int iMacCode=-1,iPMacCode=-1;
     int k=1;
     
       String   SFileName,SFileOpenPath,SFileWritePath;
    Document document;
    PdfPTable table,table1;
    PdfPCell c1;
    private static Font bigbold = FontFactory.getFont("TIMES_ROMAN", 10, Font.BOLD);
    private static Font mediumbold = FontFactory.getFont("TIMES_ROMAN", 9, Font.BOLD);
    private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN", 7, Font.NORMAL);
    private static Font smallbold = FontFactory.getFont("TIMES_ROMAN", 9, Font.BOLD);
    private static Font smallbold1 = FontFactory.getFont("TIMES_ROMAN", 5, Font.BOLD);
      
    int iWidth[] = {30,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25};
    int iTotalColumns = 21;
    
      int iWidth1[] = {15,15,15,15};
    int iTotalColumns1 = 4;
     
     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
          common = new Common();

          bgColor = common.bgColor;
          bgHead  = common.bgHead;
          bgUom   = common.bgUom;
          bgBody  = common.bgBody;
          fgHead  = common.fgHead;
          fgUom   = common.fgUom;
          fgBody  = common.fgBody;
		tbgBody = common.tbgBody;
          SLine   = common.Replicate("-",194)+"\n";
          SLine2  = common.Replicate("-",164)+"\n";
     }
     public void doGet(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
            doPost(request,response);
     }
     
          private void createPDFFile()
    {
        try 
        {
			document = new Document();
//                    String sFileName        = "OEConsolidatedPDF.pdf";
//                   sFileName        = common.getPrintPath()+sFileName;
//                   PdfWriter.getInstance(document, new FileOutputStream(sFileName));
            PdfWriter.getInstance(document, new FileOutputStream(SFileWritePath));
            document.setPageSize(PageSize.LEGAL.rotate());
            document.open();
                             
            table = new PdfPTable(iTotalColumns);
            table.setWidths(iWidth);
            table.setWidthPercentage(100);
            
            table1 = new PdfPTable(iTotalColumns1);
            table1.setWidths(iWidth1);
            table1.setWidthPercentage(100);
            //table.setHeaderRows(3);
        } 
        catch (Exception ex) 
        {
            ex.printStackTrace();
            System.out.println(ex);
        }

    }
    
     
     public void doPost(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
          response.setContentType("text/html");
          PrintWriter out  = response.getWriter();
		session = request.getSession(true);
		/*if(session.getValue("AppletTag")==null)
		{
			Control control = new Control();
			String SServer = control.getID("Select ProdHost from Server");
			response.sendRedirect(SServer + "/SessionExpired");
			return;

		}*/		
		setCriteria(request);
                
                
    
 //     SFileName = "d:\\CProduction\\OEConsolidatednew1.pdf";
      
      SFileName      = "pdfreports/C-UnitOEConsolidatedReport.pdf";
            SFileOpenPath  = "http://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/"+SFileName;
            SFileWritePath = request.getRealPath("/")+SFileName; 

            out.println("<p>");
            out.println(" <a href='"+SFileOpenPath+"' target='_blank'><b>View this PDF</b></a>");
            out.println("</p>"); 
                
                createPDFFile();
                
          out.println("<html>");
          out.println("<head>");
          out.println("<title>OE Production Report </title>");
          out.println("</head>");
          out.println("<body bgcolor='" + common.bgBody + "'>");
		//out.println(session.getValue("AppletTag"));
          out.println("<a href='Header'>Home</a>");
          out.println(common.getConsReportHeader("OE Production Report - Consolidated (Final RF2)",SUnit,SStDate,SEnDate));
		out.println("  <table border='2' cellspacing='1' width='1600' height='59' bgcolor='"+bgHead+"'></tr>");
          out.println("      <td width='67' height='145' rowspan='3' valign='middle' align='center'><font color='"+fgHead+"' size='4'>Mach Nº</font></td>");
          out.println("      <td width='67' height='145' rowspan='3' valign='middle' align='center'><font color='"+fgHead+"' size='4'>Count</font></td>");
          out.println("      <td width='67' height='145' rowspan='3' valign='middle' align='center'><font color='"+fgHead+"' size='4'>Nº of Rott</font></td>");
          out.println("      <td width='134' height='33' colspan='2' valign='middle' align='center'><font color='"+fgHead+"' size='4'>Spindle Speed</font></td>");
          out.println("      <td width='201' height='33' colspan='3' valign='middle' align='center'><font color='"+fgHead+"' size='4'>Production Details</font></td>");
          out.println("      <td width='125' height='33' colspan='2' valign='middle' align='center'><font color='"+fgHead+"' size='4'>Variations</font></td>");
          out.println("      <td width='179' height='33' colspan='3' valign='middle' align='center'><font color='"+fgHead+"' size='4'>Efficiency</font></td>");
          out.println("      <td width='349' height='33' colspan='5' valign='middle' align='center'><font color='"+fgHead+"' size='4'>Utilization</font></td>");
          out.println("      <td width='147' height='30' colspan='2' valign='middle' align='center'><font color='"+fgHead+"' size='4'>Miscellaneous</font></td>");
          out.println("      <td width='264' height='30' colspan='4' valign='middle' align='center'><font color='"+fgHead+"' size='4'>Pneumafil Details</font></td></tr>");
          out.println("    <tr>");
          out.println("      <td width='67' height='68' valign='top' align='center'><font color='"+fgHead+"' size='4'>Target</font></td>");
          out.println("      <td width='67' height='68' valign='top' align='center'><font color='"+fgHead+"' size='4'>Actual</font></td>");
          out.println("      <td width='67' height='68' valign='top' align='center'><font color='"+fgHead+"' size='4'>Target</font></td>");
          out.println("      <td width='67' height='68' valign='top' align='center'><font color='"+fgHead+"' size='4'>Expected</font></td>");
          out.println("      <td width='67' height='68' valign='top' align='center'><font color='"+fgHead+"' size='4'>Actual</font></td>");
          out.println("      <td width='67' height='68' valign='top' align='center'><font color='"+fgHead+"' size='4'>Target");
          out.println("        Vs Actual</font></td>");
          out.println("      <td width='58' height='68' valign='top' align='center'><font color='"+fgHead+"' size='4'>Expect");
          out.println("        Vs Actual </font></td>");
          out.println("      <td width='58' height='68' valign='top' align='center'><font color='"+fgHead+"' size='4'>Target</font></td>");
          out.println("      <td width='61' height='68' valign='top' align='center'><font color='"+fgHead+"' size='4'>Expect");
          out.println("        Vs Actual</font></td>");
          out.println("      <td width='60' height='68' valign='top' align='center'><font color='"+fgHead+"' size='4'>Target");
          out.println("        Vs Actual</font></td>");
          out.println("      <td width='61' height='68' valign='top' align='center'><font color='"+fgHead+"' size='4'>Hours");
          out.println("        to be worked</font></td>");
          out.println("      <td width='75' height='68' valign='top' align='center'><font color='"+fgHead+"' size='4'>Actual");
          out.println("        Hours Worked</font></td>");
          out.println("      <td width='74' height='68' valign='top' align='center'><font color='"+fgHead+"' size='4'>Stop");
          out.println("        pages</font></td>");
          out.println("      <td width='73' height='72' valign='top' align='center'><font color='"+fgHead+"' size='4'>&nbsp;(Net)</font></td>");
          out.println("      <td width='66' height='72' valign='top' align='center'><font color='"+fgHead+"' size='4'>Doff</font></td>");
          out.println("      <td width='81' height='72' valign='top' align='center'><font color='"+fgHead+"' size='4'>Worked");
          out.println("        Spindle</font></td>");
          out.println("      <td width='66' height='72' valign='top' align='center'><font color='"+fgHead+"' size='4'>Grams/");
          out.println("        Spindle</font></td>");
          out.println("      <td width='66' height='72' valign='top' align='center'><font color='"+fgHead+"' size='4'>Prod.</font></td>");
          out.println("      <td width='66' height='72' valign='top' align='center'><font color='"+fgHead+"' size='4'>Target</font></td>");
          out.println("      <td width='66' height='72' valign='top' align='center'><font color='"+fgHead+"' size='4'>Varia");
          out.println("        tion</font></td>");
          out.println("      <td width='66' height='72' valign='top' align='center'><font color='"+fgHead+"' size='4'>Pneu-");
          out.println("        mafil</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='67' height='29' valign='top' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>RPM</font></td>");
          out.println("      <td width='67' height='29' valign='top' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>RPM</font></td>");
          out.println("      <td width='67' height='29' valign='top' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='67' height='29' valign='top' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='67' height='29' valign='top' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='67' height='29' valign='top' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='58' height='29' valign='top' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='58' height='29' valign='top' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='61' height='29' valign='top' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='60' height='29' valign='top' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='61' height='29' valign='top' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Hrs</font></td>");
          out.println("      <td width='75' height='29' valign='top' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Hrs</font></td>");
          out.println("      <td width='74' height='29' valign='top' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Min</font></td>");
          out.println("      <td width='73' height='25' valign='top' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='66' height='25' valign='top' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='81' height='25' valign='top' align='right' bgcolor='"+bgUom+"'>&nbsp;</td>");
          out.println("      <td width='66' height='25' valign='top' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>gms</font></td>");
          out.println("      <td width='66' height='25' valign='top' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='66' height='25' valign='top' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='66' height='25' valign='top' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='66' height='25' valign='top' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("    </tr>");

          setVectors();

          try
          {
               FW      = new FileWriter("/software/C-Prod-Print/Reports/spinConsc.prn");
               //FW      = new FileWriter("D:/production/reports/spinConsc.prn");

               Lctr    = 70;
               k       =  1;
               Pctr    =  0;

               Head();
               Body();
               HeadAbs();
               BodyAbs();
               FootAbs();
               FW.close();
          }catch(Exception ex){}

          for(int i=0;i<V1.size();i++)
          {
               out.println("    <tr>");
               out.println("      <td width='67' height='5' valign='middle' align='center' bgcolor='"+tbgBody+"'><font size='3' color='"+fgBody+"'>"+(String)V1.elementAt(i)+"</font></td>");
               out.println("      <td width='67' height='5' valign='middle' align='center' bgcolor='"+tbgBody+"'><font size='3' color='"+fgBody+"'>"+(String)V2.elementAt(i)+"</font></td>");
               out.println("      <td width='67' height='5' valign='middle' align='center' bgcolor='"+tbgBody+"'><font size='3' color='"+fgBody+"'>"+(String)V3.elementAt(i)+"</font></td>");
               out.println("      <td width='67' height='5' valign='top' align='right' bgcolor='"+tbgBody+"' ><font size='3' color='"+fgBody+"'>"+(String)V4.elementAt(i)+"</font></td>");
               out.println("      <td width='67' height='5' valign='top' align='right' bgcolor='"+tbgBody+"' ><font size='3' color='"+fgBody+"'>"+(String)V5.elementAt(i)+"</font></td>");
               out.println("      <td width='67' height='5' valign='top' align='right' bgcolor='"+tbgBody+"' ><font size='3' color='"+fgBody+"'>"+(String)V6.elementAt(i)+"</font></td>");
               out.println("      <td width='67' height='5' valign='top' align='right' bgcolor='"+tbgBody+"' ><font size='3' color='"+fgBody+"'>"+(String)V7.elementAt(i)+"</font></td>");
               out.println("      <td width='67' height='5' valign='top' align='right' bgcolor='"+tbgBody+"' ><font size='3' color='"+fgBody+"'>"+(String)V8.elementAt(i)+"</font></td>");
               out.println("      <td width='67' height='5' valign='top' align='right' bgcolor='"+tbgBody+"' ><font size='3' color='"+fgBody+"'>"+(String)V9.elementAt(i)+"</font></td>");
               out.println("      <td width='58' height='5' valign='top' align='right' bgcolor='"+tbgBody+"' ><font size='3' color='"+fgBody+"'>"+(String)V10.elementAt(i)+"</font></td>");
               out.println("      <td width='58' height='5' valign='top' align='right' bgcolor='"+tbgBody+"' ><font size='3' color='"+fgBody+"'>"+(String)V11.elementAt(i)+"</font></td>");
               out.println("      <td width='61' height='5' valign='top' align='right' bgcolor='"+tbgBody+"' ><font size='3' color='"+fgBody+"'>"+(String)V12.elementAt(i)+"</font></td>");
               out.println("      <td width='60' height='5' valign='top' align='right' bgcolor='"+tbgBody+"' ><font size='3' color='"+fgBody+"'>"+(String)V13.elementAt(i)+"</font></td>");
               out.println("      <td width='61' height='5' valign='top' align='right' bgcolor='"+tbgBody+"' ><font size='3' color='"+fgBody+"'>"+(String)V14.elementAt(i)+"</font></td>");
               out.println("      <td width='75' height='5' valign='top' align='right' bgcolor='"+tbgBody+"' ><font size='3' color='"+fgBody+"'>"+(String)V15.elementAt(i)+"</font></td>");
               out.println("      <td width='74' height='5' valign='top' align='right' bgcolor='"+tbgBody+"' ><font size='3' color='"+fgBody+"'>"+(String)V16.elementAt(i)+"</font></td>");
               out.println("      <td width='73' height='5' valign='top' align='right' bgcolor='"+tbgBody+"' ><font size='3' color='"+fgBody+"'>"+(String)V17.elementAt(i)+"</font></td>");
               out.println("      <td width='66' height='5' valign='top' align='right' bgcolor='"+tbgBody+"' ><font size='3' color='"+fgBody+"'>"+(String)V18.elementAt(i)+"</font></td>");
               out.println("      <td width='81' height='5' valign='top' align='right' bgcolor='"+tbgBody+"' ><font size='3' color='"+fgBody+"'>"+(String)V19.elementAt(i)+"</font></td>");
               out.println("      <td width='66' height='5' valign='top' align='right' bgcolor='"+tbgBody+"' ><font size='3' color='"+fgBody+"'>"+(String)V20.elementAt(i)+"</font></td>");
               out.println("      <td width='66' height='5' valign='top' align='right' bgcolor='"+tbgBody+"' ><font size='3' color='"+fgBody+"'>"+(String)V21.elementAt(i)+"</font></td>");
               out.println("      <td width='66' height='5' valign='top' align='right' bgcolor='"+tbgBody+"' ><font size='3' color='"+fgBody+"'>"+(String)V22.elementAt(i)+"</font></td>");
               out.println("      <td width='66' height='5' valign='top' align='right' bgcolor='"+tbgBody+"' ><font size='3' color='"+fgBody+"'>"+(String)V23.elementAt(i)+"</font></td>");
               out.println("      <td width='66' height='5' valign='top' align='right' bgcolor='"+tbgBody+"' ><font size='3' color='"+fgBody+"'>"+(String)V24.elementAt(i)+"</font></td>");
               out.println("    </tr>");
          }

          out.println("  </table>");
          out.println("</div>");
          out.println("<p>&nbsp;</p>");
		out.println("<p align='center' style='COLOR: maroon; FONT-SIZE: large; FONT-WEIGHT: bold'>Count wise Abstract</p>"); 
//        out.println("<p><b><font size='5' color='#0000FF'>Count wise Abstract</font></b></p>");
          out.println("<div align='left'>");
          out.println("  <table border='2' cellspacing='1' width='970' height='1' bgcolor='"+bgBody+"' ");
         
              AddCellIntoTable("AMARJOTHI SPINNING MILLS LIMITED", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,20f,0,0,0,0, bigbold);
	AddCellIntoTable("OE COUNTWISE ABSTRACT FROM " +common.parseDate(String.valueOf(SStDate))+"--"+common.parseDate(String.valueOf(SEnDate)), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,20f,0,0,0,0, bigbold);
AddCellIntoTable("UNIT       : "+(SUnit.equals("1")?"A":(SUnit.equals("2")?"B":(SUnit.equals("3")?"C":"S"))), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,20f,0,0,0,0, bigbold);      
         AddCellIntoTable("Count_Name", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
           AddCellIntoTable("Spindle Details", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 6,25f,4,1,8,2, smallbold);
          AddCellIntoTable("Actual Production Details", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 9,25f,4,1,8,2, smallbold);
          AddCellIntoTable("Pneumafil", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 5,25f,4,1,8,2, smallbold);
          //AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 3,25f,4,1,8,2, smallbold);
           AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
          AddCellIntoTable("Allt I", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
          AddCellIntoTable("Allt II", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
          AddCellIntoTable("Allt III", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
          AddCellIntoTable("Allot", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
          AddCellIntoTable("Wkd 1", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
          AddCellIntoTable("Wkd 2", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
          AddCellIntoTable("Wkd 3", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
          AddCellIntoTable("Wkd Tot", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
          AddCellIntoTable("Util", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
          AddCellIntoTable("I", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
          AddCellIntoTable("II", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
          AddCellIntoTable("III", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
          AddCellIntoTable("Total", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
          AddCellIntoTable("Target", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
          AddCellIntoTable("Diff", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
          AddCellIntoTable("gms/rot", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
          AddCellIntoTable("30s", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
          AddCellIntoTable("40s", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
          AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2,25f,4,1,8,2, smallnormal);
          //AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
          AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
          AddCellIntoTable("Nos", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
        AddCellIntoTable("Nos", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
        AddCellIntoTable("Nos", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
        AddCellIntoTable("Nos", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
        AddCellIntoTable("No", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
        AddCellIntoTable("No", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
        AddCellIntoTable("No", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
        AddCellIntoTable("No", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
        AddCellIntoTable("%", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
        AddCellIntoTable("Kgs", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
        AddCellIntoTable("Kgs", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
        AddCellIntoTable("Kgs", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
        AddCellIntoTable("Kgs", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
        AddCellIntoTable("Kgs", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
        AddCellIntoTable("Kgs", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
        AddCellIntoTable("gms", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
        AddCellIntoTable("gms", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
        AddCellIntoTable("gms", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
        AddCellIntoTable("Kgs", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
        AddCellIntoTable("%", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
         for(int i=0;i<Va1.size();i++)
          {
        AddCellIntoTable((String)Va1.elementAt(i), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
     AddCellIntoTable((String)Va22.elementAt(i), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
     AddCellIntoTable((String)Va23.elementAt(i), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
     AddCellIntoTable((String)Va24.elementAt(i), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
     AddCellIntoTable((String)Va2.elementAt(i), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);     
     AddCellIntoTable((String)Va3.elementAt(i), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
    AddCellIntoTable((String)Va4.elementAt(i), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);
    AddCellIntoTable((String)Va5.elementAt(i), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);      
    AddCellIntoTable((String)Va6.elementAt(i), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);      
    AddCellIntoTable((String)Va7.elementAt(i), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);      
    AddCellIntoTable((String)Va8.elementAt(i), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);      
    AddCellIntoTable((String)Va9.elementAt(i), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);      
    AddCellIntoTable((String)Va10.elementAt(i), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal);     
    AddCellIntoTable((String)Va11.elementAt(i), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
    AddCellIntoTable((String)Va12.elementAt(i), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
    AddCellIntoTable((String)Va13.elementAt(i), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
    AddCellIntoTable((String)Va14.elementAt(i), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
    AddCellIntoTable((String)Va15.elementAt(i), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
    AddCellIntoTable((String)Va16.elementAt(i), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
    AddCellIntoTable((String)Va17.elementAt(i), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
    AddCellIntoTable((String)Va18.elementAt(i), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
     }
    String SUtill = common.getRound(common.toDouble(common.getSum(Va6,"S"))*100/common.toDouble(common.getSum(Va2,"S")),2);
           double dGPSS1 = common.toDouble(common.getSum(Va11,"S"))*1000/common.toDouble(common.getSum(Va7,"S"));

         
AddCellIntoTable("Total/Avg", table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
AddCellIntoTable(common.getSum(Va22,"S"), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
AddCellIntoTable(common.getSum(Va23,"S"), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
AddCellIntoTable(common.getSum(Va24,"S"), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
AddCellIntoTable(common.getSum(Va2,"S"), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
AddCellIntoTable(common.getSum(Va3,"S"), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
AddCellIntoTable(common.getSum(Va4,"S"), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
AddCellIntoTable(common.getSum(Va5,"S"), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
AddCellIntoTable(common.getSum(Va6,"S"), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
AddCellIntoTable(SUtill, table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
AddCellIntoTable(common.getSum(Va8,"S"), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
AddCellIntoTable(common.getSum(Va9,"S"), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
AddCellIntoTable(common.getSum(Va10,"S"), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
AddCellIntoTable(common.getSum(Va11,"S"), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
AddCellIntoTable(common.getSum(Va12,"S"), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
AddCellIntoTable(common.getSum(Va13,"S"), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
AddCellIntoTable(common.getRound(dGPSS1,3), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
AddCellIntoTable(common.getSum(Va15,"S"), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
AddCellIntoTable(common.getSum(Va16,"S"), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
AddCellIntoTable(common.getSum(Va17,"S"), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
 double dProdAb = common.toDouble(common.getSum(Va11,"S"));
         double dPneuAb = common.toDouble(common.getSum(Va17,"S"));
AddCellIntoTable(common.Rad(common.getRound(dPneuAb*100/(dProdAb+dPneuAb),2),7), table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallnormal); 
    
 
  String SGPS1 = common.getRound(common.toDouble(common.getSum(Va11,"S"))*1000/common.toDouble(common.getSum(Va6,"S")),3);

         double dProdAbs = common.toDouble(common.getSum(Va11,"S"));
         double dPneuAbs = common.toDouble(common.getSum(Va17,"S"));

         String SUtil1 = common.getRound(common.toDouble(common.getSum(Va6,"S"))*100/common.toDouble(common.getSum(Va2,"S")),2);

         double dSpdlWkd = common.toDouble(common.getSum(Va6,"S"));
         double dCount   = common.toDouble(common.getSum(Va21,"S"));

         double d30sConv = common.toDouble(common.getSum(Va19,"S"));
         double d40sConv = common.toDouble(common.getSum(Va20,"S"));

         double dAvg30s  = d30sConv*1000/dSpdlWkd;
         double dAvg40s  = d40sConv*1000/dSpdlWkd;
    
         String SAvgCount = common.getRound(dCount/dSpdlWkd,2);


AddCellIntoTable("30s Converted", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable(common.Rad(common.getSum(Va19,"S"),10), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("40s Converted", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable(common.Rad(common.getSum(Va20,"S"),10), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("Average Count", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable( common.Rad(SAvgCount,10), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("Average Pneumafil %", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable( common.Rad(common.getRound(dPneuAbs*100/(dProdAbs+dPneuAbs),2),7), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("Average 30s gms/Rot", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable(common.Rad(String.valueOf(dAvg30s),7), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("Average 40s gms/Rot", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable(common.Rad(String.valueOf(dAvg40s),7), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,25f,4,1,8,2, smallbold);
AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("Average gms/Rot Today", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable(common.Rad(SGPS1,7), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,25f,4,1,8,2, smallbold);
AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("Average Spindle Utilization", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable(common.Rad(SUtil1,7), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);   
AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 1,25f,4,1,8,2, smallbold);
    
          
          
          
          
          
          out.println("    <tr>");
          out.println("      <td width='78' height='1' rowspan='4' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'>Count_Name</font></td>");
          out.println("      <td width='468' height='1' colspan='6' valign='middle' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'>Spindle Details</font></td>");
          out.println("      <td width='700' height='1' colspan='9' valign='middle' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'>Actual Production Details</font></td>");
          out.println("      <td width='154' height='1' colspan='2' valign='middle' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'>Pneumafil</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='78' height='66' colspan='3' valign='top' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'>Allotted</font></td>"); // changes
          out.println("      <td width='78' height='60' rowspan='2' valign='top' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'>Total Alloted Spindles</font></td>");
          out.println("      <td width='234' height='18' colspan='3' valign='top' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'>Worked Spindles - Shift</font></td>");
          out.println("      <td width='78' height='60' rowspan='2' valign='top' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'>Total Wkd Spindles</font></td>");
          out.println("      <td width='78' height='60' rowspan='2' valign='top' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'>Utili zation</font></td>");
          out.println("      <td width='234' height='22' colspan='3' valign='top' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'>Shift</font></td>");
          out.println("      <td width='78' height='66' rowspan='2' valign='top' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'>Total</font></td>");
          out.println("      <td width='78' height='66' rowspan='2' valign='top' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'>Target</font></td>");
          out.println("      <td width='78' height='66' rowspan='2' valign='top' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'>Difference</font></td>");
          out.println("      <td width='78' height='66' rowspan='2' valign='top' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'>gms/ spindle</font></td>");
          out.println("      <td width='77' height='66' rowspan='2' valign='top' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'>30s</font></td>");
          out.println("      <td width='77' height='66' rowspan='2' valign='top' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'>40s</font></td>");
          out.println("      <td width='77' height='66' rowspan='2' valign='top' align='center' bgcolor='"+bgHead+"'></td>");
          out.println("      <td width='77' height='66' rowspan='2' valign='top' align='center' bgcolor='"+bgHead+"'></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='78' height='36' valign='top' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'>I</font></td>");
          out.println("      <td width='78' height='36' valign='top' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'>II</font></td>");
          out.println("      <td width='78' height='36' valign='top' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'>III</font></td>");
          out.println("      <td width='78' height='36' valign='top' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'>I</font></td>");
          out.println("      <td width='78' height='36' valign='top' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'>II</font></td>");
          out.println("      <td width='78' height='36' valign='top' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'>III</font></td>");
          out.println("      <td width='78' height='32' valign='top' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'>I</font></td>");
          out.println("      <td width='78' height='32' valign='top' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'>II</font></td>");
          out.println("      <td width='78' height='32' valign='top' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'>III</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='239' height='33' align='right' colspan='5' bgcolor='"+bgHead+"'>&nbsp;</td>");
          out.println("      <td width='78' height='33' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>%</font></td>");
          out.println("      <td width='78' height='33' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>Kgs</font></td>");
          out.println("      <td width='78' height='33' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>Kgs</font></td>");
          out.println("      <td width='78' height='33' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>Kgs</font></td>");
          out.println("      <td width='78' height='33' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>Kgs</font></td>");
          out.println("      <td width='78' height='33' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>Kgs</font></td>");
          out.println("      <td width='78' height='33' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>Kgs</font></td>");
          out.println("      <td width='77' height='34' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>gms</font></td>");
          out.println("      <td width='77' height='34' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>kgs</font></td>");
          out.println("      <td width='78' height='34' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>kgs</font></td>");
          out.println("      <td width='77' height='34' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>Kgs</font></td>");
          out.println("      <td width='77' height='34' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgBody+"'>%</font></td>");
          out.println("    </tr>");

          for(int i=0;i<Va1.size();i++)
          {
               out.println("    <tr>");
               out.println("      <td width='78' height='1' bgcolor='"+tbgBody+"'><font size='3' color='"+fgUom+"'>"+(String)Va1.elementAt(i)+"</font></td>");
               out.println("      <td width='58' height='33' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgUom+"'>"+(String)Va22.elementAt(i)+"</font></td>");
               out.println("      <td width='58' height='33' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgUom+"'>"+(String)Va23.elementAt(i)+"</font></td>");
               out.println("      <td width='58' height='33' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgUom+"'>"+(String)Va24.elementAt(i)+"</font></td>");
               out.println("      <td width='58' height='33' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgUom+"'>"+(String)Va2.elementAt(i)+"</font></td>");
               out.println("      <td width='34' height='33' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgUom+"'>"+(String)Va3.elementAt(i)+"</font></td>");
               out.println("      <td width='34' height='33' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgUom+"'>"+(String)Va4.elementAt(i)+"</font></td>");
               out.println("      <td width='35' height='33' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgUom+"'>"+(String)Va5.elementAt(i)+"</font></td>");
               out.println("      <td width='60' height='33' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgUom+"'>"+(String)Va6.elementAt(i)+"</font></td>");
               out.println("      <td width='78' height='33' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgUom+"'>"+(String)Va7.elementAt(i)+"</font></td>");
               out.println("      <td width='78' height='33' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgUom+"'>"+(String)Va8.elementAt(i)+"</font></td>");
               out.println("      <td width='78' height='33' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgUom+"'>"+(String)Va9.elementAt(i)+"</font></td>");
               out.println("      <td width='78' height='33' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgUom+"'>"+(String)Va10.elementAt(i)+"</font></td>");
               out.println("      <td width='78' height='33' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgUom+"'>"+(String)Va11.elementAt(i)+"</font></td>");
               out.println("      <td width='78' height='33' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgUom+"'>"+(String)Va12.elementAt(i)+"</font></td>");
               out.println("      <td width='78' height='33' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgUom+"'>"+(String)Va13.elementAt(i)+"</font></td>");
               out.println("      <td width='77' height='34' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgUom+"'>"+(String)Va14.elementAt(i)+"</font></td>");
               out.println("      <td width='77' height='34' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgUom+"'>"+(String)Va15.elementAt(i)+"</font></td>");
               out.println("      <td width='78' height='34' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgUom+"'>"+(String)Va16.elementAt(i)+"</font></td>");
               out.println("      <td width='77' height='34' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgUom+"'>"+(String)Va17.elementAt(i)+"</font></td>");
               out.println("      <td width='77' height='34' align='right' bgcolor='"+tbgBody+"'><font size='3' color='"+fgUom+"'>"+(String)Va18.elementAt(i)+"</font></td>");
               out.println("    </tr>");
          }

          out.println("    <tr>");
          out.println("      <td width='78' height='1' bgcolor='"+bgUom+"'><font size='3' color='"+fgBody+"'>T O T A L</font></td>");
          out.println("      <td width='58' height='33' align='right' bgcolor='"+bgUom+"'><font size='3' color='"+fgBody+"'>"+common.getSum(Va22,"S")+"</font></td>");
          out.println("      <td width='58' height='33' align='right' bgcolor='"+bgUom+"'><font size='3' color='"+fgBody+"'>"+common.getSum(Va23,"S")+"</font></td>");
          out.println("      <td width='58' height='33' align='right' bgcolor='"+bgUom+"'><font size='3' color='"+fgBody+"'>"+common.getSum(Va24,"S")+"</font></td>");
          out.println("      <td width='58' height='33' align='right' bgcolor='"+bgUom+"'><font size='3' color='"+fgBody+"'>"+common.getSum(Va2,"S")+"</font></td>");
          out.println("      <td width='34' height='33' align='right' bgcolor='"+bgUom+"'><font size='3' color='"+fgBody+"'>"+common.getSum(Va3,"S")+"</font></td>");
          out.println("      <td width='34' height='33' align='right' bgcolor='"+bgUom+"'><font size='3' color='"+fgBody+"'>"+common.getSum(Va4,"S")+"</font></td>");
          out.println("      <td width='35' height='33' align='right' bgcolor='"+bgUom+"'><font size='3' color='"+fgBody+"'>"+common.getSum(Va5,"S")+"</font></td>");
          out.println("      <td width='60' height='33' align='right' bgcolor='"+bgUom+"'><font size='3' color='"+fgBody+"'>"+common.getSum(Va6,"S")+"</font></td>");

          String SUtil = common.getRound(common.toDouble(common.getSum(Va6,"S"))*100/common.toDouble(common.getSum(Va2,"S")),2);
          out.println("      <td width='78' height='33' align='right' bgcolor='"+bgUom+"'><font size='3' color='"+fgBody+"'>"+SUtil+"</font></td>");
          out.println("      <td width='78' height='33' align='right' bgcolor='"+bgUom+"'><font size='3' color='"+fgBody+"'>"+common.getSum(Va8,"S")+"</font></td>");
          out.println("      <td width='78' height='33' align='right' bgcolor='"+bgUom+"'><font size='3' color='"+fgBody+"'>"+common.getSum(Va9,"S")+"</font></td>");
          out.println("      <td width='78' height='33' align='right' bgcolor='"+bgUom+"'><font size='3' color='"+fgBody+"'>"+common.getSum(Va10,"S")+"</font></td>");
          out.println("      <td width='78' height='33' align='right' bgcolor='"+bgUom+"'><font size='3' color='"+fgBody+"'>"+common.getSum(Va11,"S")+"</font></td>");
          out.println("      <td width='78' height='33' align='right' bgcolor='"+bgUom+"'><font size='3' color='"+fgBody+"'>"+common.getSum(Va12,"S")+"</font></td>");
          out.println("      <td width='78' height='33' align='right' bgcolor='"+bgUom+"'><font size='3' color='"+fgBody+"'>"+common.getSum(Va13,"S")+"</font></td>");

          double dGPS1 = common.toDouble(common.getSum(Va11,"S"))*1000/common.toDouble(common.getSum(Va7,"S"));

          out.println("      <td width='77' height='34' align='right' bgcolor='"+bgUom+"'><font size='3' color='"+fgBody+"'>"+common.getRound(dGPS1,3)+"</font></td>");
          out.println("      <td width='77' height='34' align='right' bgcolor='"+bgUom+"'><font size='3' color='"+fgBody+"'>"+common.getSum(Va15,"S")+"</font></td>");
          out.println("      <td width='78' height='34' align='right' bgcolor='"+bgUom+"'><font size='3' color='"+fgBody+"'>"+common.getSum(Va16,"S")+"</font></td>");
          out.println("      <td width='77' height='34' align='right' bgcolor='"+bgUom+"'><font size='3' color='"+fgBody+"'>"+common.getSum(Va17,"S")+"</font></td>");
          out.println("      <td width='77' height='34' align='right' bgcolor='"+bgUom+"'><font size='3' color='"+fgBody+"'>"+common.getSum(Va18,"S")+"</font></td>");
          out.println("    </tr>");

          out.println("  </table>");
          out.println("<p>&nbsp;</p>");

          try
          {
               setCountMakeTable(out);
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
          out.println("</div>");
          out.println("</body>");
          out.println("</html>");

          out.close();
          
             try
            {
            document.add(table);
            document.newPage();
            document.add(table1);
            document.close();            
            }
            catch(Exception e)		 
            {
            e.printStackTrace();
            System.out.println(e);
            }
		  
          
          
     }
     private void setCountMakeTable(PrintWriter out) throws Exception
     {
          double dDay=0;
          double dTot=0;
//        out.println("<p align='center'><font size='3'><b>Make and Modelwise GPS Achieved Report</b></font></p>");
		out.println("<p align='center' style='COLOR: maroon; FONT-SIZE: large; FONT-WEIGHT: bold'>Make and Modelwise GPS Achieved Report</p>" );
          out.println("  <table border='1' cellspacing='1' width='970' height='1' >");

          out.println("  <tr>"); 
          out.println("      <td rowspan='2' valign='middle' align='center' bgcolor='"+bgHead+"'><font color='"+fgUom+"' size='4'>Count</font></td>");
          for(int i=0;i<VMake.size();i++)
               out.println("      <td colspan='2' valign='middle' align='center' bgcolor='"+bgHead+"'><font color='"+fgUom+"' size='4'>"+(String)VMake.elementAt(i)+"</font></td>");
          out.println("      <td colspan='2' valign='middle' align='center' bgcolor='"+bgHead+"'><font color='"+fgUom+"' size='4'>Total</font></td>");
          out.println("  </tr>");

          out.println("  <tr>");
          for(int i=0;i<VMake.size();i++)
          {
               out.println("      <td valign='middle' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"' size='4'>For "+common.parseDate(SEnDate)+"</font></td>");
               out.println("      <td valign='middle' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"' size='4'>"+common.parseDate(SStDate)+" - "+common.parseDate(SEnDate)+"</font></td>");
          }
          out.println("      <td  valign='middle' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"' size='4'>For "+common.parseDate(SEnDate)+"</font></td>");
          out.println("      <td  valign='middle' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"' size='4'>"+common.parseDate(SStDate)+" - "+common.parseDate(SEnDate)+"</font></td>");
          out.println("  </tr>");           


          for(int i=0;i<VMakeClass.size();i++)
          {
               CountMakeClass cmc = (CountMakeClass)VMakeClass.elementAt(i);
               String SCount      = cmc.SCount;
               out.println("  <tr>");
               out.println("      <td align='left' bgcolor='"+tbgBody+"'><font color='"+fgUom+"' size='3'>"+SCount+"<font></td>");                                   
               for(int j=0;j<VMakeCode.size();j++)
               {

                    String SMakeCode = (String)VMakeCode.elementAt(j);
                    double dDayMakeGms = cmc.getDayMakeGms(SMakeCode);
                    double dMakeGms    = cmc.getMakeGms(SMakeCode);
                    out.println("      <td align='right' bgcolor='"+tbgBody+"'><font color='"+fgUom+"' size='3'>"+common.getRound(dDayMakeGms,3)+"<font></td>");
                    out.println("      <td align='right' bgcolor='"+tbgBody+"'><font color='"+fgUom+"' size='3'>"+common.getRound(dMakeGms,3)+"<font></td>");
                    VMakeDay.setElementAt(common.getRound(common.toDouble((String)VMakeDay.elementAt(j))+dDayMakeGms,3),j);
                    VMakeTot.setElementAt(common.getRound(common.toDouble((String)VMakeTot.elementAt(j))+dMakeGms,3),j);

               }
               out.println("      <td align='right' bgcolor='"+tbgBody+"'><font color='"+fgUom+"' size='3'>"+common.getRound(cmc.getDayGms(),3)+"<font></td>");
               out.println("      <td align='right' bgcolor='"+tbgBody+"'><font color='"+fgUom+"' size='3'>"+common.getRound(cmc.getGms(),3)+"<font></td>");
               out.println("  </tr>");
          }

          out.println("  <tr>");
          out.println("      <td align='left' bgcolor='"+bgUom+"'><font color='"+fgUom+"' size='4'>TOTAL<font></td>");
          for(int j=0;j<VMakeCode.size();j++)
          {
               out.println("      <td align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"' size='4'>"+common.getRound((String)VMakeDay.elementAt(j),3)+"<font></td>");
               out.println("      <td align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"' size='4'>"+common.getRound((String)VMakeTot.elementAt(j),3)+"<font></td>");
               dDay = dDay+common.toDouble((String)VMakeDay.elementAt(j));
               dTot = dTot+common.toDouble((String)VMakeTot.elementAt(j));
          }
          out.println("      <td align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"' size='4'>"+common.getRound(dDay,3)+"<font></td>");
          out.println("      <td align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"' size='4'>"+common.getRound(dTot,3)+"<font></td>");
          out.println("  </tr>");

          out.println("  </table>");
     }

     public void setVectors()
     {
          VMakeClass = new Vector();
          VMake      = new Vector();
          VMakeCode  = new Vector();
          VMakeDay   = new Vector();
          VMakeTot   = new Vector();

          V1    = new Vector();
          V2    = new Vector();
          V3    = new Vector();
          V4    = new Vector();
          V5    = new Vector();
          V6    = new Vector();
          V7    = new Vector();
          V8    = new Vector();
          V9    = new Vector();
          V10   = new Vector();
          V11   = new Vector();
          V12   = new Vector();
          V13   = new Vector();
          V14   = new Vector();
          V15   = new Vector();
          V16   = new Vector();
          V17   = new Vector();
          V18   = new Vector();
          V19   = new Vector();
          V20   = new Vector();
          V21   = new Vector();
          V22   = new Vector();
          V23   = new Vector();
          V24   = new Vector();

          Va1    = new Vector();
          Va2    = new Vector();
          Va3    = new Vector();
          Va4    = new Vector();
          Va5    = new Vector();
          Va6    = new Vector();
          Va7    = new Vector();
          Va8    = new Vector();
          Va9    = new Vector();
          Va10   = new Vector();
          Va11   = new Vector();
          Va12   = new Vector();
          Va13   = new Vector();
          Va14   = new Vector();
          Va15   = new Vector();
          Va16   = new Vector();
          Va17   = new Vector();
          Va18   = new Vector();

          Va19   = new Vector();
          Va20   = new Vector();
          Va21   = new Vector();
          Va22   = new Vector();
          Va23   = new Vector();
          Va24   = new Vector();

             try
             {
                     Connection con = createConnection();
                     Statement stat = con.createStatement();
                     ResultSet res1 = stat.executeQuery(getQString());
                     while(res1.next())
                     {
                          V1  .addElement(res1.getString(1));
                          V2  .addElement(res1.getString(2));
                          V3  .addElement(res1.getString(3));
                          V4  .addElement(res1.getString(4)); 
                          V5  .addElement(res1.getString(5));
                          V6  .addElement(res1.getString(6));
                          V7  .addElement(res1.getString(7));
                          V8  .addElement(res1.getString(8));
                          V9  .addElement(res1.getString(9));
                          V10 .addElement(res1.getString(10));
                          V11 .addElement(res1.getString(11));
                          V12 .addElement(res1.getString(12));
                          V13 .addElement(res1.getString(13));
                          V14 .addElement(res1.getString(14)); 
                          V15 .addElement(res1.getString(15));
                          V16 .addElement(res1.getString(16));
                          V17 .addElement(res1.getString(17));
                          V18 .addElement(res1.getString(18));
                          V19 .addElement(res1.getString(19));
                          V20 .addElement(res1.getString(20));
                          V21 .addElement(res1.getString(21));
                          V22 .addElement(res1.getString(22));
                          V23 .addElement(res1.getString(23));
                          V24 .addElement(res1.getString(24));
                     }
                     res1.close();

                     try{stat.execute("Drop Table tempcount");}catch(Exception ex){}
                     try{stat.execute("Drop Table tempspin1");}catch(Exception ex){}
                     try{stat.execute("Drop Table tempspin2");}catch(Exception ex){}
                     try{stat.execute("Drop Table tempspin3");}catch(Exception ex){}

                     stat.execute(getQStemp());
                     stat.execute(getQS2());
                     stat.execute(getQS3());
                     stat.execute(getQS4());

				 ResultSet res2 = stat.executeQuery(getQSFinal());
                     {
                          while(res2.next())
                          {
                              Va3.addElement(res2.getString(1));
                              Va22.addElement(res2.getString(2));
                              Va8.addElement(res2.getString(3));
                              Va4.addElement(res2.getString(4));
                              Va23.addElement(res2.getString(5));
                              Va9.addElement(res2.getString(6));
                              Va5.addElement(res2.getString(7));
                              Va24.addElement(res2.getString(8));   
                              Va10.addElement(res2.getString(9));
                          }
                     }
                     res2.close();
                     ResultSet res5 = stat.executeQuery(getQS1());
                     {
                          while(res5.next())
                          {
                              Va1.addElement(res5.getString(1));
                              Va2.addElement(res5.getString(2));
                              Va6.addElement(res5.getString(3));
                              Va7.addElement(res5.getString(4));
                              Va11.addElement(res5.getString(5));
                              Va12.addElement(res5.getString(6));
                              Va13.addElement(res5.getString(7));
                              Va14.addElement(res5.getString(8));
                              Va15.addElement(res5.getString(9));
                              Va16.addElement(res5.getString(10));
                              Va17.addElement(res5.getString(11));
                              Va18.addElement(res5.getString(12));
                              Va19.addElement(res5.getString(13));
                              Va20.addElement(res5.getString(14));
                              Va21.addElement(res5.getString(15));
                          }
                     }
                     res5.close();
                     ResultSet res6 = stat.executeQuery("Select Make.Make_Name,Machine.Make_Code From Make Inner Join Machine On Machine.Make_Code = Make.Make_Code Where Machine.Dept_Code = 7 Group By Machine.Make_Code,Make.Make_Name Order By 1");
                     while(res6.next())
                     {
                         VMake.addElement(res6.getString(1));
                         VMakeCode.addElement(res6.getString(2));
                         VMakeDay.addElement("0");
                         VMakeTot.addElement("0");
                     }
                     res6.close();
                     ResultSet res7 = stat.executeQuery(getMakeModelString());
                     while(res7.next())
                         organizeData(res7);
                     res7.close();
                     stat.close();
                     con.close();
             }
             catch(Exception ex)
             {
                     System.out.println("From SpinCons.java : 400 "+ex);
                     ex.printStackTrace();
             }
           try
           {
                for(int i=0;i<V2.size();i++)
                {
                    V3.setElementAt(common.getRound((String)V3.elementAt(i),0),i);
                    V4.setElementAt(common.getRound((String)V4.elementAt(i),0),i);
                    V5.setElementAt(common.getRound((String)V5.elementAt(i),0),i);
                    V6.setElementAt(common.getRound((String)V6.elementAt(i),3),i);
                    V7.setElementAt(common.getRound((String)V7.elementAt(i),3),i);
                    V8.setElementAt(common.getRound((String)V8.elementAt(i),3),i);
                    V9.setElementAt(common.getRound((String)V9.elementAt(i),3),i);
                    V10.setElementAt(common.getRound((String)V10.elementAt(i),3),i);
                    V11.setElementAt(common.getRound((String)V11.elementAt(i),3),i);
                    V12.setElementAt(common.getRound((String)V12.elementAt(i),3),i);
                    V13.setElementAt(common.getRound((String)V13.elementAt(i),3),i);
                    V14.setElementAt(common.getRound((String)V14.elementAt(i),3),i);
                    V15.setElementAt(common.getRound((String)V15.elementAt(i),3),i);
                    V16.setElementAt(common.getRound((String)V16.elementAt(i),3),i);
                    V17.setElementAt(common.getRound((String)V17.elementAt(i),3),i);

                    //Doff %
                    double dHourRun   = common.toDouble((String)V14.elementAt(i));
                    double dHourMeter = common.toDouble((String)V15.elementAt(i));
                    double dAllot     = common.toDouble((String)V18.elementAt(i));

                    V18.setElementAt(common.getRound((dHourRun-dHourMeter)*100/dAllot,3),i);
                    V19.setElementAt(common.getRound((String)V19.elementAt(i),3),i);
                    V20.setElementAt(common.getRound((String)V20.elementAt(i),3),i);
                    V21.setElementAt(common.getRound((String)V21.elementAt(i),3),i);
                    V22.setElementAt(common.getRound((String)V22.elementAt(i),3),i);
                    V23.setElementAt(common.getRound((String)V23.elementAt(i),3),i);
                    V24.setElementAt(common.getRound((String)V24.elementAt(i),3),i);
                }
           }catch(Exception ex){System.out.println(ex);}

           try
           {
                for(int i=0;i<Va1.size();i++)
                {
                    Va2.setElementAt(common.getRound((String)Va2.elementAt(i),3),i);
                    Va22.setElementAt(common.getRound((String)Va22.elementAt(i),3),i);
                    Va23.setElementAt(common.getRound((String)Va23.elementAt(i),3),i);
                    Va24.setElementAt(common.getRound((String)Va24.elementAt(i),3),i);

                    Va3.setElementAt(common.getRound((String)Va3.elementAt(i),3),i);
                    Va4.setElementAt(common.getRound((String)Va4.elementAt(i),3),i);
                    Va5.setElementAt(common.getRound((String)Va5.elementAt(i),3),i);
                    Va6.setElementAt(common.getRound((String)Va6.elementAt(i),3),i);
                    Va7.setElementAt(common.getRound((String)Va7.elementAt(i),3),i);
                    Va8.setElementAt(common.getRound((String)Va8.elementAt(i),3),i);
                    Va9.setElementAt(common.getRound((String)Va9.elementAt(i),3),i);
                    Va10.setElementAt(common.getRound((String)Va10.elementAt(i),3),i);
                    Va11.setElementAt(common.getRound((String)Va11.elementAt(i),3),i);
                    Va12.setElementAt(common.getRound((String)Va12.elementAt(i),3),i);
                    Va13.setElementAt(common.getRound((String)Va13.elementAt(i),3),i);

                    double dGPS = common.toDouble((String)Va11.elementAt(i))*1000/common.toDouble((String)Va6.elementAt(i));
                    Va14.setElementAt(common.getRound(dGPS,3),i);
                    Va15.setElementAt(common.getRound((String)Va15.elementAt(i),3),i);
                    Va16.setElementAt(common.getRound((String)Va16.elementAt(i),3),i);
                    Va17.setElementAt(common.getRound((String)Va17.elementAt(i),3),i);
                    Va18.setElementAt(common.getRound((String)Va18.elementAt(i),3),i);
                }
           }catch(Exception ex){System.out.println(ex);}
     }
     private void organizeData(ResultSet result) throws Exception
     {
          CountMakeClass cmc = null;
          int    iDate      = common.toInt(result.getString(1));
          String SCountCode = result.getString(2);
          String SCount     = result.getString(3);
          String SMakeCode  = result.getString(4);
          String SMake      = result.getString(5);
          double dGms       = result.getDouble(6);
          double dSpdl      = result.getDouble(7);

          int index = indexOf(SCountCode);
          if(index == -1)
          {
               cmc = new CountMakeClass(SCountCode,SCount,common.toInt(SEnDate));
               VMakeClass.addElement(cmc);
               index = VMakeClass.size()-1;
          }
          cmc = (CountMakeClass)VMakeClass.elementAt(index);
          cmc.setData(SMakeCode,SMake,iDate,dGms,dSpdl);
          VMakeClass.setElementAt(cmc,index);          
     }
     private int indexOf(String SCountCode)
     {
          int index = -1;
          for(int i=0;i<VMakeClass.size();i++)
          {
               CountMakeClass cmc = (CountMakeClass)VMakeClass.elementAt(i);
               if(cmc.SCountCode.equals(SCountCode))
               {
                    index=i;
                    break;
               }
          }
          return index;
     }
     public String getQString()
     {
          String QS  = " SELECT machine.mach_name, count.count_name, Sum(OETran.noofRot) AS SumOfNoRot, Avg(OETran.speed_tar) AS AvgOfspeed_tar, Avg(OETran.speed) AS AvgOfspeed, "+
                       " Sum(OETran.prod_tar) AS SumOfprod_tar, Sum(OETran.prod_exp) AS SumOfprod_exp, Sum(OETran.prod) AS SumOfprod, "+
                       " Sum(OETran.prod-OETran.Prod_tar) AS Expr1, Sum(OETran.prod-OETran.prod_Exp) AS Expr2, "+
                       " Avg(OETran.count_effy) AS AvgOfmach_effy, Avg(OETran.prod*100/decode(OETran.Prod_exp,0,1,OETran.Prod_Exp)) AS Expr3, "+
                       " Avg(OETran.prod*100/decode(OETran.prod_tar,0,1,OETran.Prod_Tar)) AS Expr4, Sum(OETran.hoursrun) AS SumOfhoursrun, "+
                       " Sum(OETran.HourMeter) AS SumOfHourMeter, Sum(OETran.stop_mt) AS SumOfstop_mt, Avg(OETran.utiloa) AS AvgOfutiloa, "+
                       " Sum(OETran.hours) AS Expr5, Sum(OETran.Rotwkd) AS SumOfRotwkd, Avg(OETran.Gms_Rot) AS AvgOfGms_Rot, "+
                       " Sum(OETran.pneumafil) AS SumOfpneumafil, Sum(OETran.pneu_tar) AS SumOfpneu_tar, "+
                       " Avg((OETran.Pneumafil-OETran.Pneu_tar)*100/decode(OETran.Pneu_tar,0,1,OETran.Pneu_Tar)) AS Expr6, Avg(OETran.pneu_per) AS AvgOfpneu_per "+
                       " FROM ((OETran INNER JOIN unit ON OETran.Unit_Code = unit.unitcode) INNER JOIN count ON OETran.count_code = count.count_code) INNER JOIN machine ON OETran.Mach_Code = machine.mach_code "+
                       " WHERE (((OETran.sp_date)>='"+SStDate+"' And (OETran.sp_date)<='"+SEnDate+"')) "+
//                        " and OETran.Prod>0"+     // NoofRot>0
//                       " and OETran.Prod_Tar>0 and OETran.Pneu_tar>0"+
                       " and Sp_Date not in(Select HDate from ProdHoliday  where leaveday = 1)"+
                       " GROUP BY machine.mach_name, count.count_name "+
                       " ORDER BY to_number(machine.mach_name) ";
          return QS;
     }

     public String getQS1()
     {
          String QS1 = " SELECT tempcount.count_name, Sum(OETran.noofRot) AS SumOfnoofRot, Sum(OETran.Rotwkd) AS SumOfRotwkd,"+
                      " Avg(Rotwkd*100/noofRot) AS Expr1, Sum(OETran.prod) AS SumOfprod,"+
                      " Sum(OETran.prod_tar) AS SumOfprod_tar, Sum(prod_tar-prod) AS Expr2, "+
                      " Avg(OETran.Gms_Rot) AS AvgOfGms_Rot, Avg(Gms_Rot*tempCount.kg30s)  AS AvgOfconv_30s,"+       
                      " Avg(Gms_Rot*tempCount.kg40s) AS AvgOfconv_40s, Sum(OETran.pneumafil) AS SumOfpneumafil,"+      
//                    " Avg(OETran.pneu_per) AS AvgOfpneu_per, "+
                      " (Sum(OETran.pneumafil)*100/decode((Sum(OETran.prod)+Sum(OETran.pneumafil)),0,1,(Sum(OETran.prod)+Sum(OETran.pneumafil)))) as AvgOfpneu_per, "+
                      " Sum(OETran.conv_30s) AS SumOfconv_30s,"+
                      " Sum(OETran.conv_40s) AS SumOfconv_40s, "+
                      " Sum(OETran.count*OETran.Rotwkd) AS SumOfCount "+
                      " FROM ((OETran INNER JOIN unit ON OETran.Unit_Code = unit.unitcode) "+
                      " Inner JOIN tempcount ON tempcount.count_code=OETran.count_code) "+
                      " INNER JOIN machine ON OETran.Mach_Code = machine.mach_code "+
                      " WHERE (((OETran.sp_date)>='"+SStDate+"' And (OETran.sp_date)<='"+SEnDate+"')) "+
//                      " and OETran.Prod>0"+     // NoofRot>0
                      " and Sp_Date not in(Select HDate from ProdHoliday where leaveday = 1)"+
                      " GROUP BY tempcount.count_name"+
                      " Order by 1";
            System.out.println(QS1);
          return QS1;
     }

     public String getQS2()
     {
          String QS2 =   " Create Table TempSpin1 as "+
                         " (SELECT tempcount.count_name, Sum(OETran.Rotwkd) AS SumOfRotwkd,Sum(OETran.prod) AS SumOfprod,sum(NoofRot) as SumOfRotAll "+
                         " FROM OETran left JOIN tempcount ON tempcount.count_code=OETran.count_code "+
                         " WHERE (OETran.shift_code=1 AND OETran.sp_date>='"+SStDate+"' And OETran.sp_date<='"+SEnDate+"')  "+
    //                     " and OETran.Prod>0"+     // NoofRot>0
                         " GROUP BY tempcount.count_name)";

          return QS2;
     }

     public String getQS3()
     {
          String QS3 =   " Create Table TempSpin2 as "+
                         " (SELECT tempcount.count_name, Sum(OETran.Rotwkd) AS SumOfRotwkd, Sum(OETran.prod) AS SumOfprod,sum(NoofRot) as SumOfRotAll "+
                         " FROM OETran left JOIN tempcount ON tempcount.count_code=OETran.count_code "+
                         " WHERE (OETran.shift_code=2 AND OETran.sp_date>='"+SStDate+"' And OETran.sp_date<='"+SEnDate+"') "+
       //                  " and OETran.Prod>0"+     // NoofRot>0
                         " GROUP BY tempcount.count_name)";

          return QS3;
     }

     public String getQS4()
     {
          String QS4 =   " Create Table TempSpin3 as "+
                         " (SELECT tempcount.count_name, Sum(OETran.Rotwkd) AS SumOfRotwkd, Sum(OETran.prod) AS SumOfprod,sum(NoofRot) as SumOfRotAll "+
                         " FROM OETran left JOIN tempcount ON tempcount.count_code=OETran.count_code "+
                         " WHERE (OETran.shift_code=3 AND OETran.sp_date>='"+SStDate+"' And OETran.sp_date<='"+SEnDate+"')  "+
       //                  " and OETran.Prod>0"+     // NoofRot>0
                         " GROUP BY tempcount.count_name)";

          return QS4;
     }

    public String getQStemp()
    {
        String QS = " Create Table TempCount as "+
                    " (Select OETran.count_code,count.count_name,count.kg30s,count.kg40s from  OETran "+  
                    " Inner Join count on count.count_code=OETran.count_code "+
                    " WHERE OETran.sp_date>='"+SStDate+"' And OETran.sp_date<='"+SEnDate+"'"+
        //            " and OETran.Prod>0"+  // comment Removed on 14.02.2012
                    " Group By count.count_name,OETran.count_code,count.kg30s,count.kg40s)";

        return QS;
    }
     public String getQSFinal()
     {
          String QS = " SELECT tempspin1.SumOfRotwkd,tempspin1.SumOfRotAll,"+
                      " tempspin1.SumOfprod, tempspin2.SumOfRotwkd,tempspin2.SumOfRotAll, "+
                      " tempspin2.SumOfprod, tempspin3.SumOfRotwkd,tempspin3.SumOfRotAll, tempspin3.SumOfprod "+
                      " FROM ((tempcount LEFT JOIN tempspin1 ON tempcount.count_name = tempspin1.count_name) "+
                      " LEFT JOIN tempspin2 ON tempcount.count_name = tempspin2.count_name) "+
                      " LEFT JOIN tempspin3 ON tempcount.count_name = tempspin3.count_name"+
                      " Order by TempCount.count_Name ";
                         
          return QS;
     }
     public String getMakeModelString()
     {

          String QS = " SELECT OETran.SP_Date,OETran.count_code, "+
                      " count.count_name, Machine.make_code, "+
                      " Make.make_name, sum(OETran.Gms_Rot*OETran.Rotwkd),sum(OETran.Rotwkd) "+
                      " FROM (OETran INNER JOIN (Machine INNER JOIN Make ON Machine.make_code = Make.make_code) ON OETran.Mach_Code = Machine.mach_code) INNER JOIN count ON OETran.count_code = count.count_code "+
                      " Where OETran.SP_Date >= '"+SStDate+"' And OETran.SP_Date <='"+SEnDate+"'"+
                      " GROUP BY OETran.SP_Date,OETran.count_code, count.count_name, Machine.make_code, Make.make_name "+
                      " Order By 1,2,3 ";
          return QS;
     }

	private Connection createConnection()
	{
		Connection conn;
		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
			conn  = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
		}catch(Exception ee)
		{
			return null;
		}
		return conn;
	}

     public void Head()
     {
          if(Lctr < 63)
             return;
          if(Pctr > 0)
          {
             try
             {
               FW.write(SLine+"\n");
             }catch(Exception ex){}
          }
          Pctr++;

          String Str1    = common.Pad("Company    : AMARJOTHI SPINNING MILLS LIMITED",80);
          String Str2    = common.Pad("Document   : OE Consolidated Report From "+common.parseDate(SStDate)+" To "+common.parseDate(SEnDate),80);
          String Str3    = common.Pad("Unit       : "+(SUnit.equals("1")?"A":(SUnit.equals("2")?"B":"S")),70);

          VHead   =  Spin2Head();

          try
          {
               FW.write(""+Str1+"\n");
               FW.write(Str2+"\n");
               FW.write(Str3+"\n\n");

               for(int i=0;i<VHead.size();i++)
               {
                    FW.write((String)VHead.elementAt(i));
               }
          }catch(Exception ex){}
          Lctr = 4+VHead.size();
          k=1;
     }
     public void Body()
     {
         String Strl = "";
         for(int i=0;i<V1.size();i++)
         {
                Strl = "";
                Strl = Strl+common.Pad((String)V1     .elementAt(i),4)+common.Space(2);
                Strl = Strl+common.Rad((String)V2   .elementAt(i),10)+common.Space(2);
                Strl = Strl+common.Rad((String)V3   .elementAt(i),6)+common.Space(2);
                Strl = Strl+common.Rad((String)V4   .elementAt(i),5)+common.Space(2);
                Strl = Strl+common.Rad((String)V5   .elementAt(i),5)+common.Space(2);
                Strl = Strl+common.Rad((String)V6   .elementAt(i),9)+common.Space(2);
                Strl = Strl+common.Rad((String)V7   .elementAt(i),9)+common.Space(2);
                Strl = Strl+common.Rad((String)V8    .elementAt(i),9)+common.Space(2);
                Strl = Strl+common.Rad((String)V9    .elementAt(i),7)+common.Space(2);
                Strl = Strl+common.Rad((String)V10   .elementAt(i),7)+common.Space(2);
                Strl = Strl+common.Rad((String)V11    .elementAt(i),5)+common.Space(2);
                Strl = Strl+common.Rad((String)V12    .elementAt(i),5)+common.Space(2);
                Strl = Strl+common.Rad((String)V13    .elementAt(i),5)+common.Space(2);
                Strl = Strl+common.Rad((String)V14    .elementAt(i),5)+common.Space(2);
                Strl = Strl+common.Rad((String)V15    .elementAt(i),5)+common.Space(2);
                Strl = Strl+common.Rad((String)V16    .elementAt(i),4)+common.Space(2);
                Strl = Strl+common.Rad((String)V17   .elementAt(i),6)+common.Space(2);
                Strl = Strl+common.Rad((String)V18   .elementAt(i),6)+common.Space(2);
                Strl = Strl+common.Rad((String)V19   .elementAt(i),7)+common.Space(2);
                Strl = Strl+common.Rad((String)V20   .elementAt(i),7)+common.Space(2);
                Strl = Strl+common.Rad((String)V21   .elementAt(i),6)+common.Space(2);
                Strl = Strl+common.Rad((String)V22   .elementAt(i),6)+common.Space(2);
                Strl = Strl+common.Rad((String)V23   .elementAt(i),5)+common.Space(2);
                Strl = Strl+common.Rad((String)V24   .elementAt(i),5);

                try
                {
                     iMacCode=common.toInt((String)V1.elementAt(i));
                     if(iMacCode!=iPMacCode && k==0)
                     {
                          if(Lctr<60)
                               FW.write(SLine); //+"\n"
                          iPMacCode=iMacCode;
                     }
                     Lctr++;    
                     FW.write(Strl+"\n"); 
                     Lctr++;
                     k=0;
                     Head();
                }catch(Exception ex){}
         }

         try
         {
               FW.write(SLine);               
         }catch(Exception ex){}

         Strl = "";

         Strl = Strl+common.Rad("Total",4)+common.Space(2);
         Strl = Strl+common.Space(10)+common.Space(2);
         Strl = Strl+common.Rad(common.getSum(V3,"S"),6)+common.Space(2);
         Strl = Strl+common.Rad(common.getSum(V4,"A"),5)+common.Space(2);
         Strl = Strl+common.Rad(common.getSum(V5,"A"),5)+common.Space(2);
         Strl = Strl+common.Rad(common.getSum(V6,"S"),9)+common.Space(2);
         Strl = Strl+common.Rad(common.getSum(V7,"S"),9)+common.Space(2);
         Strl = Strl+common.Rad(common.getSum(V8,"S"),9)+common.Space(2); //actual prod
         Strl = Strl+common.Rad(common.getSum(V9,"S"),7)+common.Space(2);
         Strl = Strl+common.Rad(common.getSum(V10,"S"),7)+common.Space(2);
         Strl = Strl+common.Rad(common.getSum(V11,"A"),5)+common.Space(2);
         Strl = Strl+common.Rad(common.getSum(V12,"A"),5)+common.Space(2);
         Strl = Strl+common.Rad(common.getSum(V13,"A"),5)+common.Space(2);
         Strl = Strl+common.Rad(common.getSum(V14,"S"),5)+common.Space(2);
         Strl = Strl+common.Rad(common.getSum(V15,"S"),5)+common.Space(2);
         Strl = Strl+common.Rad(common.getSum(V16,"S"),4)+common.Space(2);
         Strl = Strl+common.Rad(common.getSum(V17,"A"),6)+common.Space(2);
         Strl = Strl+common.Rad(common.getSum(V18,"A"),6)+common.Space(2);
         Strl = Strl+common.Rad(common.getSum(V19,"S"),7)+common.Space(2);  //wkd Rot
         double dGPSX = common.toDouble(common.getSum(V8,"S"))*1000/common.toDouble(common.getSum(V19,"S"));
         Strl = Strl+common.Rad(common.getRound(dGPSX,3),7)+common.Space(2);
         Strl = Strl+common.Rad(common.getSum(V21,"S"),6)+common.Space(2);  // pneu kgs
         Strl = Strl+common.Rad(common.getSum(V22,"S"),6)+common.Space(2);             // pneu tar
         double dPneuV = (common.toDouble(common.getSum(V22,"S"))-common.toDouble(common.getSum(V21,"S")))*100/common.toDouble(common.getSum(V22,"S"));
         Strl = Strl+common.Rad(common.getRound(dPneuV,2),5)+common.Space(2);
         double dPneuX = common.toDouble(common.getSum(V21,"S"))*100/(common.toDouble(common.getSum(V21,"S"))+common.toDouble(common.getSum(V8,"S")));
         Strl = Strl+common.Rad(common.getRound(dPneuX,2),5);

         try
         {
               FW.write(Strl+"\n");
               FW.write(SLine);               
         }catch(Exception ex){}
     }

     public void HeadAbs()
     {
          String Str1    = common.Pad("\n\nOE Countwise Abstract From "+common.parseDate(SStDate)+" To "+common.parseDate(SEnDate),80);
          VHeadAbs   =  Spin2Abs();
          try
          {
               FW.write(Str1+"\n\n");

               for(int i=0;i<VHeadAbs.size();i++)
               {
                    FW.write((String)VHeadAbs.elementAt(i));
               }
          }catch(Exception ex){}
     }

     public void BodyAbs()
     {
         String Strl = "";
         for(int i=0;i<Va1.size();i++)
         {
                Strl = "";
                Strl = Strl+common.Pad((String)Va1     .elementAt(i),10)+common.Space(1);
                Strl = Strl+common.Rad((String)Va22   .elementAt(i),10)+common.Space(2);
                Strl = Strl+common.Rad((String)Va23   .elementAt(i),10)+common.Space(2);
                Strl = Strl+common.Rad((String)Va24   .elementAt(i),10)+common.Space(2);
                Strl = Strl+common.Rad((String)Va2   .elementAt(i),10)+common.Space(2);
                Strl = Strl+common.Rad((String)Va3   .elementAt(i),10)+common.Space(2);
                Strl = Strl+common.Rad((String)Va4   .elementAt(i),10)+common.Space(2);
                Strl = Strl+common.Rad((String)Va5   .elementAt(i),10)+common.Space(2);
                Strl = Strl+common.Rad((String)Va6   .elementAt(i),10)+common.Space(2);
                Strl = Strl+common.Rad((String)Va7   .elementAt(i),5)+common.Space(2);
                Strl = Strl+common.Rad((String)Va8    .elementAt(i),10)+common.Space(2);
                Strl = Strl+common.Rad((String)Va9    .elementAt(i),10)+common.Space(2);
                Strl = Strl+common.Rad((String)Va10   .elementAt(i),10)+common.Space(2);
                Strl = Strl+common.Rad((String)Va11    .elementAt(i),10)+common.Space(2);
                Strl = Strl+common.Rad((String)Va12    .elementAt(i),10)+common.Space(2);
                Strl = Strl+common.Rad((String)Va13    .elementAt(i),7)+common.Space(2);
                Strl = Strl+common.Rad((String)Va14    .elementAt(i),7)+common.Space(2);
                Strl = Strl+common.Rad((String)Va15    .elementAt(i),9)+common.Space(2);
                Strl = Strl+common.Rad((String)Va16    .elementAt(i),9)+common.Space(2);
                Strl = Strl+common.Rad((String)Va17   .elementAt(i),6)+common.Space(2);
                Strl = Strl+common.Rad((String)Va18   .elementAt(i),6);

                try
                {
                     FW.write(Strl+"\n");
                }catch(Exception ex){}
         }
           try
           {
               FW.write(SLine2);               
           }catch(Exception ex){}

           Strl = "";

           Strl = Strl+common.Pad("Total/Avg",10)+common.Space(1);
           Strl = Strl+common.Rad(common.getSum(Va22,"S"),10)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(Va23,"S"),10)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(Va24,"S"),10)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(Va2,"S"),10)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(Va3,"S"),10)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(Va4,"S"),10)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(Va5,"S"),10)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(Va6,"S"),10)+common.Space(2);

           String SUtil1 = common.getRound(common.toDouble(common.getSum(Va6,"S"))*100/common.toDouble(common.getSum(Va2,"S")),2);
           Strl = Strl+common.Rad(SUtil1,5)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(Va8,"S"),10)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(Va9,"S"),10)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(Va10,"S"),10)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(Va11,"S"),10)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(Va12,"S"),10)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(Va13,"S"),7)+common.Space(2);

           double dRotWkd = common.toDouble(common.getSum(Va6,"S"));

           double d30sConv = common.toDouble(common.getSum(Va19,"S"));
           double d40sConv = common.toDouble(common.getSum(Va20,"S"));

           double dAvg30s  = d30sConv*1000/dRotWkd;
           double dAvg40s  = d40sConv*1000/dRotWkd;

           String SGPS1 = common.getRound(common.toDouble(common.getSum(Va11,"S"))*1000/common.toDouble(common.getSum(Va6,"S")),3);
           Strl = Strl+common.Rad(SGPS1,7)+common.Space(2);
           Strl = Strl+common.Rad(String.valueOf(dAvg30s),9)+common.Space(2); //common.getSum(Va15,"A")
           Strl = Strl+common.Rad(String.valueOf(dAvg40s),9)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(Va17,"S"),6)+common.Space(2);
           double dProdAbs = common.toDouble(common.getSum(Va11,"S"));
           double dPneuAbs = common.toDouble(common.getSum(Va17,"S"));
           System.out.println(dProdAbs+","+dPneuAbs);

           Strl = Strl+common.Rad(common.getRound(dPneuAbs*100/(dProdAbs+dPneuAbs),2),6);

           try
           {
               FW.write(Strl+"\n");
               FW.write(SLine2+"\n");
           }catch(Exception ex){}
     }
     public void FootAbs()
     {

         String SGPS1 = common.getRound(common.toDouble(common.getSum(Va11,"S"))*1000/common.toDouble(common.getSum(Va6,"S")),3);

         double dProdAbs = common.toDouble(common.getSum(Va11,"S"));
         double dPneuAbs = common.toDouble(common.getSum(Va17,"S"));

         String SUtil1 = common.getRound(common.toDouble(common.getSum(Va6,"S"))*100/common.toDouble(common.getSum(Va2,"S")),2);

         double dRotWkd = common.toDouble(common.getSum(Va6,"S"));
         double dCount   = common.toDouble(common.getSum(Va21,"S"));

         double d30sConv = common.toDouble(common.getSum(Va19,"S"));
         double d40sConv = common.toDouble(common.getSum(Va20,"S"));

         double dAvg30s  = d30sConv*1000/dRotWkd;
         double dAvg40s  = d40sConv*1000/dRotWkd;
    
         String SAvgCount = common.getRound(dCount/dRotWkd,2);

         String Str3 = "30s Converted   " + common.Space(10) + common.Rad(common.getSum(Va19,"S"),10)+common.Space(10)+"Average 30s Gms/Rot"+common.Space(7)+":"+common.Space(5)+common.Rad(String.valueOf(dAvg30s),7);    
         String Str4 = "40s Converted   " + common.Space(10) + common.Rad(common.getSum(Va20,"S"),10)+common.Space(10)+"Average 40s Gms/Rot"+common.Space(7)+":"+common.Space(5)+common.Rad(String.valueOf(dAvg40s),7);    
         String Str5 = "Average Count   " + common.Space(10) + common.Rad(SAvgCount,10)+common.Space(10)+"Average Gms/Rot Today"+common.Space(5)+":"+common.Space(5)+common.Rad(SGPS1,7);
         String Str6 = "Average Pnumafil %"+ common.Space(10) + common.Rad(common.getRound(dPneuAbs*100/(dProdAbs+dPneuAbs),2),7)+common.Space(10)+"Average Spindle Utilization"+":"+common.Space(5)+common.Rad(SUtil1,7);

         try
         {
              FW.write(Str3+"\n");
              FW.write(Str4+"\n");
              FW.write(Str5+"\n");
              FW.write(Str6+"\n");

              FW.write("<End Of Report>");

         }
         catch(Exception ex)
         {}
     }

     public Vector Spin2Head()
     {
          VHead = new Vector();

          String Str1 = common.Replicate("-",194)+"\n";
          String Str2 = common.Space(4)+common.Space(2)+common.Space(10)+common.Space(2)+common.Space(6)+common.Space(2)+
                        common.Pad("Rot Speed",12)+common.Space(2)+
                        common.Cad("Production",31)+common.Space(2)+
                        common.Cad("Variations",16)+common.Space(2)+
                        common.Cad("Efficiency",21)+common.Space(2)+
                        common.Cad("Utilization",34)+common.Space(2)+
                        common.Cad("Miscellaneous",16)+common.Space(2)+
                        common.Cad("Pneumafil",28)+"\n";

          String Str3 = common.Pad("Mach",4)+common.Space(2)+common.Pad("Count",10)+common.Space(2)+common.Pad("No Of",6)+common.Space(2)+
                        common.Replicate("-",12)+common.Space(2)+
                        common.Replicate("-",31)+common.Space(2)+
                        common.Replicate("-",16)+common.Space(2)+
                        common.Replicate("-",21)+common.Space(2)+
                        common.Replicate("-",34)+common.Space(2)+
                        common.Replicate("-",16)+common.Space(2)+
                        common.Replicate("-",28)+"\n";
                        

          String Str4 = common.Rad("No",4)+common.Space(2)+common.Space(10)+common.Space(2)+common.Rad("Rots",6)+common.Space(2)+
                        common.Rad("Target",5)+common.Space(2)+common.Rad("Actual",5)+common.Space(2)+
                        common.Rad("Target",9)+common.Space(2)+common.Rad("Expected",9)+common.Space(2)+common.Rad("Actual",9)+common.Space(2)+
                        common.Rad("Tgt Vs",7)+common.Space(2)+common.Rad("Exp Vs",7)+common.Space(2)+
                        common.Rad("Target",5)+common.Space(2)+common.Rad("Exp Vs",5)+common.Space(2)+common.Rad("Tgt Vs",5)+common.Space(2)+
                        common.Rad("Hour",5)+common.Space(2)+common.Rad("Actual",5)+common.Space(2)+common.Rad("Stop",5)+common.Space(2)+common.Rad("Net",5)+common.Space(2)+common.Rad("Doff",5)+common.Space(2)+
                        common.Rad("Wkd",7)+common.Space(2)+common.Rad("Grams/",7)+common.Space(2)+
                        common.Rad("Prod",6)+common.Space(2)+common.Rad("Target",6)+common.Space(2)+common.Rad("Varia",6)+common.Space(2)+common.Rad("Pneu",6)+"\n";

          String Str5 = common.Space(4)+common.Space(2)+common.Space(10)+common.Space(2)+common.Space(6)+common.Space(2)+
                        common.Pad(" ",5)+common.Space(2)+common.Pad(" ",5)+common.Space(2)+
                        common.Pad(" ",9)+common.Space(2)+common.Pad(" ",9)+common.Space(2)+common.Pad(" ",9)+common.Space(2)+
                        common.Pad("Actual",7)+common.Space(2)+common.Pad("Actual",7)+common.Space(2)+
                        common.Pad(" ",5)+common.Space(2)+common.Pad("Actual",5)+common.Space(2)+common.Pad("Actual",5)+common.Space(2)+
                        common.Pad("to Wrk",5)+common.Space(2)+common.Pad("Hours",5)+common.Space(2)+common.Pad("page",5)+common.Space(2)+common.Pad(" ",5)+common.Space(2)+common.Pad(" ",5)+common.Space(2)+
                        common.Pad("Rot",7)+common.Space(2)+common.Pad("Rot",7)+common.Space(2)+
                        common.Pad(" ",6)+common.Space(2)+common.Pad(" ",6)+common.Space(2)+common.Pad("tion",6)+common.Space(2)+common.Pad("mafil",6)+"\n";

          String Str6 = common.Space(4)+common.Space(2)+common.Space(10)+common.Space(2)+common.Space(6)+common.Space(2)+
                        common.Rad("RPM",5)+common.Space(2)+common.Rad("RPM",5)+common.Space(2)+
                        common.Rad("Kgs",9)+common.Space(2)+common.Rad("Kgs",9)+common.Space(2)+common.Rad("Kgs",9)+common.Space(2)+
                        common.Rad("Kgs",7)+common.Space(2)+common.Rad("Kgs",7)+common.Space(2)+
                        common.Rad("%",5)+common.Space(2)+common.Rad("%",5)+common.Space(2)+common.Rad("%",5)+common.Space(2)+
                        common.Rad("Hrs",5)+common.Space(2)+common.Rad("Hrs",5)+common.Space(2)+common.Rad("min",5)+common.Space(2)+common.Rad("%",5)+common.Space(2)+common.Rad(" ",5)+common.Space(2)+
                        common.Rad("nos",7)+common.Space(2)+common.Rad("gms",7)+common.Space(2)+
                        common.Rad("kgs",6)+common.Space(2)+common.Rad("kgs",6)+common.Space(2)+common.Rad("%",6)+common.Space(2)+common.Rad("%",6)+"\n";

          String Str7 = common.Space(4)+common.Space(2)+common.Space(10)+common.Space(2)+common.Space(6)+common.Space(2)+
                        common.Replicate("-",168)+"\n";

          VHead.addElement(Str1);
          VHead.addElement(Str2);
          VHead.addElement(Str3);
          VHead.addElement(Str4);
          VHead.addElement(Str5);
          VHead.addElement(Str7);
          VHead.addElement(Str6);
          VHead.addElement(SLine);

          return VHead;
     }

     public Vector Spin2Abs()
     {
          VHeadAbs = new Vector();

          String Str1 = common.Replicate("-",230)+"\n";
          String Str2 = common.Space(10)+common.Space(2)+
                        common.Cad("Spindle Details",82)+common.Space(2)+
                        common.Cad("Production Details",112)+common.Space(2)+
                        common.Cad("Pneumafil",14)+"\n";

          String Str3 = common.Pad("Count",10)+common.Space(2)+
                        common.Replicate("-",90)+common.Space(2)+
                        common.Replicate("-",105)+common.Space(2)+
                        common.Replicate("-",14)+"\n";

          String Str4 = common.Space(10)+common.Space(2)+
                        common.Rad("Allt I",10)+common.Space(2)+
                        common.Rad("Allt II",10)+common.Space(2)+
                        common.Rad("Allt III",10)+common.Space(2)+
                        common.Rad("Allot",10)+common.Space(2)+common.Rad("Wkd I",10)+common.Space(2)+
                        common.Rad("Wkd 2",10)+common.Space(2)+common.Rad("Wkd 3",10)+common.Space(2)+
                        common.Rad("Wkd Tot",10)+common.Space(2)+common.Rad("Util",5)+common.Space(2)+
                        common.Rad("I",10)+common.Space(2)+common.Rad("II",10)+common.Space(2)+common.Rad("III",10)+common.Space(2)+
                        common.Rad("Total",10)+common.Space(2)+common.Rad("Target",10)+common.Space(2)+
                        common.Rad("Diff",7)+common.Space(2)+common.Rad("gms/Rot",7)+common.Space(2)+
                        common.Rad("30s",9)+common.Space(2)+
                        common.Rad("40s",9)+common.Space(2)+common.Rad(" ",6)+common.Space(2)+
                        common.Rad(" ",6)+"\n";

          String Str6 = common.Space(8)+common.Space(2)+
                        common.Rad("Nos",10)+common.Space(2)+
                        common.Rad("Nos",10)+common.Space(2)+
                        common.Rad("Nos",10)+common.Space(2)+
                        common.Rad("Nos",10)+common.Space(2)+common.Rad("No",10)+common.Space(2)+
                        common.Rad("No",10)+common.Space(2)+common.Rad("No",10)+common.Space(2)+
                        common.Rad("No",10)+common.Space(2)+common.Rad("%",5)+common.Space(2)+
                        common.Rad("Kgs",10)+common.Space(2)+common.Rad("Kgs",10)+common.Space(2)+
                        common.Rad("Kgs",10)+common.Space(2)+
                        common.Rad("Kgs",10)+common.Space(2)+common.Rad("Kgs",10)+common.Space(2)+
                        common.Rad("Kgs",7)+common.Space(2)+common.Rad("gms",7)+common.Space(2)+
                        common.Rad("gms",9)+common.Space(2)+
                        common.Rad("gms",9)+common.Space(2)+common.Rad("Kgs",6)+common.Space(2)+
                        common.Rad("%",6)+"\n";

          String Str5 = common.Space(10)+common.Space(2)+
                        common.Replicate("-",218)+"\n";

          VHeadAbs.addElement(Str1);
          VHeadAbs.addElement(Str2);
          VHeadAbs.addElement(Str3);
          VHeadAbs.addElement(Str4);
          VHeadAbs.addElement(Str5);
          VHeadAbs.addElement(Str6);
          SLine2 = common.Replicate("-",230)+"\n";
          VHeadAbs.addElement(SLine2);

          return VHeadAbs;
     }

	private void setCriteria(HttpServletRequest request)
	{
		SEnDate          = request.getParameter("EndDate");
                SEnDate          = common.pureDate(SEnDate);
		SStDate          = request.getParameter("StartDate");
                SStDate          = common.pureDate(SStDate);
                SUnit            = request.getParameter("Unit");
	}
        



public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        // c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str ));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        //  c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddColumnIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iColWidth, int iLeftBorder, int iTopBorder, int iRightBorder, Font DocFont, int iBottomBorder) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColWidth);
        // c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    } 
		          
        
        
        
}


