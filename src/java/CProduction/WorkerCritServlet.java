package CProduction;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;

public class WorkerCritServlet extends HttpServlet
{
	String SUnit,SDate,SDept,SDateTo="";	
     Vector VEmpName,VEmpCode,VHrdTicketNo;
	Common common = new Common();

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);

     }
	public void doGet(HttpServletRequest request,HttpServletResponse response)
	{
		try
		{ 
			SUnit   = request.getParameter("Unit").trim();
			SDept   = request.getParameter("Dept").trim();
			SDate   = request.getParameter("Date").trim();
			SDate   = common.pureDate(SDate);
			try
			{
				SDateTo = request.getParameter("Date").trim();
				SDateTo = common.pureDate(SDateTo);
               }catch(Exception ee){}		
			if(SDateTo.length() <= 1)
				SDateTo = SDate;
			//System.out.println("WorkCritServ ->" + SUnit + ":" + SDept + ":" + SDate + ":" + SDateTo);
               setEmployees(); 
			ObjectOutputStream out = new ObjectOutputStream(response.getOutputStream());
			out.writeObject(SUnit);
			out.writeObject(SDept);
			out.writeObject(common.parseDate(SDate));
			out.writeObject(common.parseDate(SDateTo));
			out.writeObject(VEmpName);
			out.writeObject(VEmpCode);
               out.writeObject(VHrdTicketNo);

		}
		catch(Exception ee)
		{
			System.out.println("Service -> " + ee);
		}
    }
	public void doPost(HttpServletRequest request,HttpServletResponse response)
	{
		doGet(request,response);
	}

	private void setEmployees()
	{
		VEmpCode = new Vector();
		VEmpName = new Vector();
            VHrdTicketNo = new Vector();

//		String SQry = " select empcode from tempshift where DeptCode = " + SDept + 
//				    " and UnitCode = " + SUnit +" and ShiftDate >= '" + SDate + "'" +
//				    " and ShiftDate <= '" + SDateTo + "'";	
          String SQry  = " select Distinct(ticket) from " + common.getTableName(SDept) + " where " + 
			          " unit_code = " + SUnit + " and sp_date >= '" + SDate + "'" + 
			          " and sp_date <= '" + SDateTo + "'";
          
		try
		{
			Connection con = createConnection();
			Statement st   = con.createStatement();
			ResultSet rs   = st.executeQuery(SQry);
			while(rs.next())
			{
                    String SECode = rs.getString(1);

                    VEmpCode.addElement(SECode);
                    VEmpName.addElement(getTicket(SECode));
               }
               rs.close();
               st.close();
               System.out.println("In Work Crit Servlet"+VEmpName.size());

		}catch(Exception ee){System.out.println("setEmployees()->" + ee);}

//		for(int i=0;i<VEmpCode.size();i++)
//			System.out.println((String)VEmpCode.elementAt(i) + " : " + (String)VEmpName.elementAt(i));
	}

	  private String getTicket(String SEmpCode)
	  {	
		   String SRet="";
		   try
		   {			   
                  String SQry = " select empname,HrdTicketNo from hrdnew.SchemeApprentice where empcode="+SEmpCode+" union " +
                                    " select empname,HrdTicketNo from hrdnew.ActApprentice where empcode="+SEmpCode+" union  " + 
                                    " select empname,HrdTicketNo from hrdnew.ContractApprentice where empcode="+SEmpCode;
		
                    Connection conn = createConnection();
                    Statement stat  = conn.createStatement();
                    ResultSet res   = stat.executeQuery(SQry);
                    if(res.next())
                    {
                         SRet       = res.getString(1);
                         String SHrdTicket = res.getString(2);
                         VHrdTicketNo.addElement(SHrdTicket);
                    }                         
                    res.close();
//                  System.out.println(SQry + ":" + SRet);
		   }catch(Exception ee){System.out.println("getTicket->" + ee);}
		   return SRet;
      }

	private Connection createConnection()
	{
		Connection conn;
		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
			conn  = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
		}catch(Exception ee)
		{
			return null;
		}
		return conn;
	}
	
}
 