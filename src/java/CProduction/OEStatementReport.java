/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CProduction;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.sql.*;



import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;

import java.io.FileOutputStream;

/**
 *
 * @author root
 **/


public class OEStatementReport extends HttpServlet {

     HttpSession    session;
     Common common;

     String S1,S2,S3;
     String SF1,SF2,SF3,SF4,SF5,SF6,SF7,SF8,SF9,SF10;
     String SF11,SF12,SF13,SF14,SF15,SF16,SF17,SF18;
     String SM1,SM2,SM3,SM4,SM5,SM6,SM7,SM8,SM9,SM10;
     String SM11,SM12,SM13,SM14,SM15,SM16,SM17,SM18;

     String SDate,SUnit,SLine;
     String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody,tbgBody;

     FileWriter  FW;
     
      String   SFileName,SFileOpenPath,SFileWritePath;
    Document document;
    PdfPTable table;
    PdfPCell c1;
    private static Font bigbold = FontFactory.getFont("TIMES_ROMAN", 12, Font.BOLD);
    private static Font mediumbold = FontFactory.getFont("TIMES_ROMAN", 10, Font.BOLD);
    private static Font smallnormal = FontFactory.getFont("TIMES_ROMAN", 10, Font.NORMAL);
    private static Font smallbold = FontFactory.getFont("TIMES_ROMAN", 10, Font.BOLD);
    private static Font smallbold1 = FontFactory.getFont("TIMES_ROMAN", 5, Font.BOLD);
      
    int iWidth[] = {15,15,15,15,15};
    int iTotalColumns = 5;


     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
          common = new Common();

          bgColor = common.bgColor;
          bgHead  = common.bgHead;
          bgUom   = common.bgUom;
          bgBody  = common.bgBody;
          fgHead  = common.fgHead;
          fgUom   = common.fgUom;
          fgBody  = common.fgBody;
		tbgBody = common.tbgBody;
     }
     
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /*
             * TODO output your page here. You may use following sample code.
             */
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet OEStatementReport</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet OEStatementReport at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }
    }

    
    private void createPDFFile()
    {
        try 
        {
			document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(SFileWritePath));
            document.setPageSize(PageSize.A4);
            document.open();
                             
            table = new PdfPTable(iTotalColumns);
            table.setWidths(iWidth);
            table.setWidthPercentage(100);
            //table.setHeaderRows(3);
        } 
        catch (Exception ex) 
        {
            ex.printStackTrace();
            System.out.println(ex);
        }

    }

    
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
		response.setContentType("text/html");
                PrintWriter out  = response.getWriter();
		session = request.getSession(true);
		/*if(session.getValue("AppletTag")==null)
		{
			Control control = new Control();
			String SServer = control.getID("Select ProdTomHost from Server");
			response.sendRedirect(SServer + "/CProduction.SessionExpired");
			return;

		}*/
		setCriteria(request);

                
  //SFileName = "d:\\CProduction\\OEStatementReportnew1.pdf";
  
  SFileName      = "pdfreports/C-UnitOEStatementReport.pdf";
            SFileOpenPath  = "http://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/"+SFileName;
            SFileWritePath = request.getRealPath("/")+SFileName; 

            out.println("<p>");
            out.println(" <a href='"+SFileOpenPath+"' target='_blank'><b>View this PDF</b></a>");
            out.println("</p>"); 
         
                
                createPDFFile();
                
          getVariables();
          getUKG();

          out.println("<html>");
          out.println("<head>");
          out.println("<title>OE Statement Report </title>");
          out.println("</head>");
          out.println("<body bgcolor='" + common.bgBody + "'>");
        //out.println(session.getValue("AppletTag"));
          out.println("<a href='Header'>Home</a>");
          out.println(common.getUnitDateHeader("OE Production Statement (F RF 8)",SUnit,SDate));
          out.println("<div align='left'>");
          out.println("  <table border='1' width='835' height='692' >");
          
          
             AddCellIntoTable("AMARJOTHI SPINNING MILLS LIMITED", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,20f,0,0,0,0, bigbold);
	AddCellIntoTable("OE STATEMENT REPORT ON "+common.parseDate(String.valueOf(SDate)), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,20f,0,0,0,0, bigbold);
AddCellIntoTable("UNIT       : "+(SUnit.equals("1")?"A":(SUnit.equals("2")?"B":(SUnit.equals("3")?"C":"S"))), table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,20f,0,0,0,0, bigbold);      
AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, iTotalColumns,20f,0,0,0,0, bigbold);
         AddCellIntoTable("FORTHEDAY" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 0, 1,0, 0, mediumbold);
        AddCellIntoTable("OE" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2,25f, 0, 1, 0, 0, mediumbold);
        AddCellIntoTable("CONE WINDING" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2,25f, 0, 1, 0, 0, mediumbold);
         
         AddCellIntoTable("First Shift" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 0, 1, 0, 0, mediumbold);
          AddCellIntoTable(S1 , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2,25f,  0, 1, 0, 0, smallnormal);
          AddCellIntoTable("" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2,25f,  0, 1, 0, 0, smallnormal);
          AddCellIntoTable("Second Shift" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 0, mediumbold);
          AddCellIntoTable(S2 , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2,25f,  0, 0, 0, 0, smallnormal);
          AddCellIntoTable("" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2,25f,  0, 0, 0, 0, smallnormal);
          AddCellIntoTable("Third Shift" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 0, mediumbold);
          AddCellIntoTable(S3 , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2,25f,  0, 0, 0, 0, smallnormal);
          AddCellIntoTable("" , table, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2,25f,  0, 0, 0, 0, smallnormal);
            AddCellIntoTable("" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 0, 1, 0, 0, mediumbold);
          AddCellIntoTable("TODAY" , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 0, 1, 0, 2, mediumbold);
          AddCellIntoTable("FOR MONTH" , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 0, 1, 0, 2, mediumbold);
          AddCellIntoTable("TODAY" , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 0, 1, 0, 2, mediumbold);
          AddCellIntoTable("FOR MONTH" , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 0, 1, 0, 2, mediumbold);
          AddCellIntoTable("Actual Production" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 0, 1,0, 0, mediumbold);
          AddCellIntoTable(common.getRound(SF16,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 0, 1,0, 0, smallnormal);
          AddCellIntoTable(common.getRound(SM16,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 0, 1,0, 0, smallnormal);
           AddCellIntoTable("" , table, Element.ALIGN_CENTER, Element.ALIGN_RIGHT, 1,25f, 0, 1,0, 0, smallnormal);
          AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_RIGHT, 1,25f, 0, 1,0, 0, smallnormal);
          AddCellIntoTable("Target Production" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 0, 0, 0, 0, mediumbold);
          AddCellIntoTable(common.getRound(SF15,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 0, 0, 0, 0, smallnormal);
          AddCellIntoTable(common.getRound(SM15,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,0, 0, 0, 0, smallnormal);
          AddCellIntoTable("" , table, Element.ALIGN_CENTER, Element.ALIGN_RIGHT, 1,25f, 0, 0, 0, 0, smallnormal);
          AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_RIGHT, 1,25f, 0, 0, 0, 0, smallnormal); 
          AddCellIntoTable("Variation" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 0, 1, 0, 0, mediumbold);
          AddCellIntoTable(common.getRound(SF17,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,0, 1, 0, 0, smallnormal);
          AddCellIntoTable(common.getRound(SM17,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f, 0, 1, 0, 0, smallnormal);
          AddCellIntoTable("" , table, Element.ALIGN_CENTER, Element.ALIGN_RIGHT, 1,25f, 0, 1, 0, 0, smallnormal);
          AddCellIntoTable("", table, Element.ALIGN_CENTER, Element.ALIGN_RIGHT, 1,25f, 0, 1, 0, 0, smallnormal);
           AddCellIntoTable("PARTICULARS" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 0, 1, 0, 0, mediumbold);
          AddCellIntoTable("FOR THE DAY" , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f, 0, 1, 0, 0, mediumbold);
          AddCellIntoTable("FOR THE MONTH" , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f, 0, 1, 0, 0, mediumbold);
           AddCellIntoTable("Rotters To Be Worked" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 0, 1, 0, 0, mediumbold);
          AddCellIntoTable(common.getRound(SF1,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f, 0, 1, 0, 0, smallnormal);
          AddCellIntoTable(common.getRound(SM1,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f, 0, 1, 0, 0, smallnormal);
           AddCellIntoTable("Actual Rotters Worked" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 0, 0, 0, 0, mediumbold);
          AddCellIntoTable(common.getRound(SF2,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f, 0, 0, 0, 0, smallnormal);
          AddCellIntoTable(common.getRound(SM2,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f,  0, 0, 0, 0, smallnormal);
           AddCellIntoTable("Idle Rotters " , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 0, mediumbold);
          AddCellIntoTable(common.getRound(SF3,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f, 0, 0, 0, 0, smallnormal);
          AddCellIntoTable(common.getRound(SM3,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f,  0, 0, 0, 0, smallnormal);
           AddCellIntoTable("Utilization" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 0, mediumbold);
          AddCellIntoTable(common.getRound(SF4,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f,  0, 0, 0, 0, smallnormal);
//          AddCellIntoTable(common.getRound(SM4,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f,  0, 0, 0, 0, smallnormal);
          AddCellIntoTable(common.getRound((common.toDouble(SM2)*100)/common.toDouble(SM1),3) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f,  0, 0, 0, 0, smallnormal);
           AddCellIntoTable("Pneumafil Waste Kgs" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 0, mediumbold);
          AddCellIntoTable(common.getRound(SF5,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f, 0, 0, 0, 0, smallnormal);
          AddCellIntoTable(common.getRound(SM5,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f,  0, 0, 0, 0, smallnormal);
           AddCellIntoTable("Pneumafil Waste %" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 0, mediumbold);
          AddCellIntoTable(common.getRound(SF6,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f,  0, 0, 0, 0, smallnormal);
          AddCellIntoTable(common.getRound(SM6,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f,  0, 0, 0, 0, smallnormal);
           AddCellIntoTable("Average Count" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 0, mediumbold);
          AddCellIntoTable(common.getRound(SF7,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f, 0, 0, 0, 0, smallnormal);
          AddCellIntoTable(common.getRound(SM7,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f,  0, 0, 0, 0, smallnormal);
           AddCellIntoTable("Average Gms/Spindle" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 0, 0, 0, 0, mediumbold);
          AddCellIntoTable(common.getRound(SF8,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f,  0, 0, 0, 0, smallnormal);
          AddCellIntoTable(common.getRound(SM8,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f, 0, 0, 0, 0, smallnormal);
           AddCellIntoTable("30s Converted Kgs" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 0, mediumbold);
          AddCellIntoTable(common.getRound(SF9,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f,  0, 0, 0, 0, smallnormal);
          AddCellIntoTable(common.getRound(SM9,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f,  0, 0, 0, 0, smallnormal);
           AddCellIntoTable("30s Converted Gms/Spindle" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 0, mediumbold);
          AddCellIntoTable(common.getRound(SF10,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f,  0, 0, 0, 0, smallnormal);
          AddCellIntoTable(common.getRound(SM10,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f,  0, 0, 0, 0, smallnormal);
           AddCellIntoTable("40s Converted Kgs" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 0, mediumbold);
          AddCellIntoTable(common.getRound(SF11,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f,  0, 0, 0, 0, smallnormal);
          AddCellIntoTable(common.getRound(SM11,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f, 0, 0, 0, 0, smallnormal);
           AddCellIntoTable("40s Converted Gms/Spindle" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f, 0, 0, 0, 0, mediumbold);
          AddCellIntoTable(common.getRound(SF12,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f,  0, 0, 0, 0, smallnormal);
          AddCellIntoTable(common.getRound(SM12,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f,  0, 0, 0, 0, smallnormal);
           AddCellIntoTable("UKG With Web Unit" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 0, mediumbold);
          AddCellIntoTable(common.getRound(SF13,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f,  0, 0, 0, 0, smallnormal);
          AddCellIntoTable(common.getRound(SM13,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f, 0, 0, 0, 0, smallnormal);
           AddCellIntoTable("UKG Without Web Unit" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 2, mediumbold);
          AddCellIntoTable(common.getRound(SF14,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f,  0, 0, 0, 2, smallnormal);
          AddCellIntoTable(common.getRound(SM14,2) , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 2,25f,  0, 0, 0, 2, smallnormal);
        AddCellIntoTable("" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 0, mediumbold);
            AddCellIntoTable("" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 0, mediumbold);
            AddCellIntoTable("" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 0, mediumbold);
            AddCellIntoTable("" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 0, mediumbold);
            AddCellIntoTable("" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 0, mediumbold);
             AddCellIntoTable("" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 0, mediumbold);
            AddCellIntoTable("" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 0, mediumbold);
            AddCellIntoTable("" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 0, mediumbold);
            AddCellIntoTable("" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 0, mediumbold);
            AddCellIntoTable("" , table, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 0, mediumbold);
            AddCellIntoTable("Prepared By" , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 0, mediumbold);
            AddCellIntoTable("P.O." , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 0, mediumbold);
            AddCellIntoTable("S.P.O." , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 0, mediumbold);
            AddCellIntoTable("S.M." , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 0, mediumbold);
            AddCellIntoTable("D.M." , table, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 1,25f,  0, 0, 0, 0, mediumbold);
          
          
          
          out.println("    <tr>");
          out.println("      <td width='301' height='80' rowspan='2' bgcolor='"+bgHead+"' valign='middle'><b><font color='"+fgHead+"' size='4'>For The Day (in");
          out.println("        Kgs)</font></b></td>");
          out.println("      <td width='162' height='40' colspan='2' align='center' valign='middle' bgcolor='"+bgHead+"'><b><font color='"+fgHead+"' size='4'>First Shift</font></b></td>");
          out.println("      <td width='170' height='40' colspan='2' align='center' valign='middle' bgcolor='"+bgHead+"'><b><font color='"+fgHead+"' size='4'>Second Shift</font></b></td>");
          out.println("      <td width='172' height='40' colspan='2' align='center' valign='middle' bgcolor='"+bgHead+"'><b><font color='"+fgHead+"' size='4'>Third Shift</font></b></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='162' height='40' colspan='2' align='center' bgcolor='"+bgUom+"'><b><font size='4' color='"+fgHead+"'>"+S1+"</font></b></td>");
          out.println("      <td width='170' height='40' colspan='2' align='center' bgcolor='"+bgUom+"'><b><font size='4' color='"+fgHead+"'>"+S2+"</font></b></td>");
          out.println("      <td width='172' height='40' colspan='2' align='center' bgcolor='"+bgUom+"'><b><font size='4' color='"+fgHead+"'>"+S3+"</font></b></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='301' height='163' rowspan='3' bgcolor='"+bgHead+"' valign='middle'><b><font color='"+fgHead+"' size='4'>Production Details (in");
          out.println("        Kgs)</font></b></td>");
          out.println("      <td width='258' height='53' colspan='3' bgcolor='"+bgHead+"' align='center' valign='middle'><b><font color='"+fgUom+"' size='4'>For The Day</font></b></td>");
          out.println("      <td width='264' height='53' colspan='3' bgcolor='"+bgHead+"' align='center' valign='middle'><b><font color='"+fgUom+"' size='4'>Up to this Month</font></b></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='83' height='38' bgcolor='"+bgUom+"' align='center' valign='middle'><b><font color='"+fgBody+"' size='3'>Target</font></b></td>");
          out.println("      <td width='93' height='38' bgcolor='"+bgUom+"' align='center' valign='middle'><b><font color='"+fgBody+"' size='3'>Actual</font></b></td>");
          out.println("      <td width='92' height='38' bgcolor='"+bgUom+"' align='center' valign='middle'><b><font color='"+fgBody+"' size='3'>Variation</font></b></td>");
          out.println("      <td width='88' height='38' bgcolor='"+bgUom+"' valign='middle' align='center'><b><font color='"+fgBody+"' size='3'>Target</font></b></td>");
          out.println("      <td width='39' height='38' bgcolor='"+bgUom+"' valign='middle' align='center'>");
          out.println("        <p align='center'><b><font color='"+fgBody+"' size='3'>Actual</font></b></p>");
          out.println("      </td>");
          out.println("      <td width='50' height='38' bgcolor='"+bgUom+"' valign='middle' align='center'>");
          out.println("        <p align='center'><b><font color='"+fgBody+"' size='3'>Variation</font></b></p>");
          out.println("      </td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='80' height='62' bgcolor='"+bgUom+"'><b><font size='4' color='"+fgBody+"'>"+common.getRound(SF15,3)+"</font></b></td>");
          out.println("      <td width='90' height='62' bgcolor='"+bgUom+"'><b><font size='4' color='"+fgBody+"'>"+common.getRound(SF16,3)+"</font></b></td>");
          out.println("      <td width='90' height='62' bgcolor='"+bgUom+"'><b><font size='4' color='"+fgBody+"'>"+common.getRound(SF17,3)+"</font></b></td>");
          out.println("      <td width='84' height='62' bgcolor='"+bgUom+"'><b><font size='4' color='"+fgBody+"'>"+common.getRound(SM15,3)+"</font></b></td>");
          out.println("      <td width='72' height='62' bgcolor='"+bgUom+"'><b><font size='4' color='"+fgBody+"'>"+common.getRound(SM16,3)+"</font></b></td>");
          out.println("      <td width='83' height='62' bgcolor='"+bgUom+"'><b><font size='4' color='"+fgBody+"'>"+common.getRound(SM17,3)+"</font></b></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='468' height='36' colspan='3' bgcolor='"+bgHead+"' align='center'><b><font color='"+fgHead+"' size='4'>Particulars</font></b></td>");
          out.println("      <td width='178' height='36' colspan='2' bgcolor='"+bgHead+"' align='center'><b><font color='"+fgHead+"' size='4'>For The Day</font></b></td>");
          out.println("      <td width='165' height='36' colspan='2' bgcolor='"+bgHead+"' align='center'><b><font color='"+fgHead+"' size='4'>Up to The Month</font></b></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='374' height='27' colspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'><b>Rotters To Be Worked</b></font></td>");
          out.println("      <td width='90' height='27' align='right' bgcolor='"+bgUom+"' valign='middle'><font color='"+fgUom+"'>Nº</font></td>");
          out.println("      <td width='178' height='27' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SF1,3)+"</font></td>");
          out.println("      <td width='165' height='27' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SM1,3)+"</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='374' height='27' colspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'><b>Actual Rotters Worked</b></font></td>");
          out.println("      <td width='90' height='27' align='right' bgcolor='"+bgUom+"' valign='middle'><font color='"+fgUom+"'>Nº</font></td>");
          out.println("      <td width='178' height='27' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SF2,3)+"</font></td>");
          out.println("      <td width='165' height='27' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SM2,3)+"</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='374' height='27' colspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'><b>Idle Rotters</b></font></td>");
          out.println("      <td width='90' height='27' align='right' bgcolor='"+bgUom+"' valign='middle'><font color='"+fgUom+"'>Nº</font></td>");
          out.println("      <td width='178' height='27' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SF3,3)+"</font></td>");
          out.println("      <td width='165' height='27' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SM3,3)+"</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='374' height='26' colspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'><b>Utilization</b></font></td>");
          out.println("      <td width='90' height='26' align='right' bgcolor='"+bgUom+"' valign='middle'><font color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='178' height='26' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SF4,3)+"</font></td>");
          out.println("      <td width='165' height='26' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound((common.toDouble(SM2)*100)/common.toDouble(SM1),3)+"</font></td>"); // SM4
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='374' height='26' colspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'><b>Pneumafil Waste</b></font></td>");
          out.println("      <td width='90' height='26' align='right' bgcolor='"+bgUom+"' valign='middle'><font color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='178' height='26' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SF5,3)+"</font></td>");
          out.println("      <td width='165' height='26' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SM5,3)+"</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='374' height='26' colspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'><b>Pneumafil Waste</b></font></td>");
          out.println("      <td width='90' height='26' align='right' bgcolor='"+bgUom+"' valign='middle'><font color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='178' height='26' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SF6,3)+"</font></td>");
          out.println("      <td width='165' height='26' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SM6,3)+"</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='374' height='26' colspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'><b>Average Count</b></font></td>");
          out.println("      <td width='90' height='26' align='right' bgcolor='"+bgUom+"' valign='middle'>NE</td>");
          out.println("      <td width='178' height='26' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SF7,3)+"</font></td>");
          out.println("      <td width='165' height='26' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SM7,3)+"</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='374' height='26' colspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'><b>Average</b></font> </td>");
          out.println("      <td width='90' height='26' align='right' bgcolor='"+bgUom+"' valign='middle'><font color='"+fgUom+"'>gms/Rot</font></td>");
          out.println("      <td width='178' height='26' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SF8,3)+"</font></td>");
          out.println("      <td width='165' height='26' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SM8,3)+"</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='374' height='26' colspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'><b>30s Converted Production</b></font> </td>");
          out.println("      <td width='90' height='26' align='right' bgcolor='"+bgUom+"' valign='middle'><font color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='178' height='26' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SF9,3)+"</font></td>");
          out.println("      <td width='165' height='26' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SM9,3)+"</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='374' height='26' colspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'><b>30s Converted</b></font> </td>");
          out.println("      <td width='90' height='26' align='right' bgcolor='"+bgUom+"' valign='middle'><font color='"+fgUom+"'>gms/Rot</font></td>");
          out.println("      <td width='178' height='26' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SF10,3)+"</font></td>");
          out.println("      <td width='165' height='26' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SM10,3)+"</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='374' height='26' colspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'><b>40s Converted Production</b></font> </td>");
          out.println("      <td width='90' height='26' align='right' bgcolor='"+bgUom+"' valign='middle'><font color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='178' height='26' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SF11,3)+"</font></td>");
          out.println("      <td width='165' height='26' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SM11,3)+"</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='374' height='26' colspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'><b>40s Converted</b></font> </td>");
          out.println("      <td width='90' height='26' align='right' bgcolor='"+bgUom+"' valign='middle'><font color='"+fgUom+"'>gms/Rot</font></td>");
          out.println("      <td width='178' height='26' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SF12,3)+"</font></td>");
          out.println("      <td width='165' height='26' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SM12,3)+"</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='374' height='26' colspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'><b>UKG with Web Unit (30s Kgs)</b></font></td>");
          out.println("      <td width='90' height='26' align='right' bgcolor='"+bgUom+"' valign='middle'><font color='"+fgUom+"'>unit/kg</font></td>");
          out.println("      <td width='178' height='26' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SF13,3)+"</font></td>");
          out.println("      <td width='165' height='26' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SM13,3)+"</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='374' height='28' colspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'><b>UKG without Web Unit</b></font></td>");
          out.println("      <td width='90' height='28' align='right' bgcolor='"+bgUom+"' valign='middle'><font color='"+fgUom+"'>unit/kg</font></td>");
          out.println("      <td width='178' height='28' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SF14,3)+"</font></td>");
          out.println("      <td width='165' height='28' colspan='2' bgcolor='"+bgUom+"'><font color='"+fgBody+"' size='4'>"+common.getRound(SM14,3)+"</font></td>");
          out.println("    </tr>");
          out.println("  </table>");
          out.println("</div>");
          out.println("</body>");
          out.println("</html>");

		out.close();

          try
          {
               FW      = new FileWriter("/software/C-Prod-Print/Reports/spinstatc.prn");

               //FW      = new FileWriter("D:/Production/reports/spinstatc.prn");


               Head();
               Body();
               FW.close();
          }catch(Exception ex){}
      try
            {
            document.add(table);
            document.close();
            }
            catch(Exception e)		 
            {
            e.printStackTrace();
            System.out.println(e);
            }
    
    
    
    
    }

     public String getQS1()
     {
          String QS2 =   " SELECT Sum(OETran.prod) AS SumOfprod "+
                         " FROM OETran "+
                         " WHERE (OETran.shift_code=1 AND "+
                         " OETran.sp_date='"+SDate+"' AND OETran.unit_code="+SUnit+")";

          return QS2;
     }

     public String getQS2()
     {
          String QS2 =   " SELECT Sum(OETran.prod) AS SumOfprod "+
                         " FROM OETran "+
                         " WHERE (OETran.shift_code=2 AND "+
                         " OETran.sp_date='"+SDate+"' AND OETran.unit_code="+SUnit+")";

          return QS2;
     }

     public String getQS3()
     {
          String QS2 =   " SELECT Sum(OETran.prod) AS SumOfprod "+
                         " FROM OETran "+
                         " WHERE (OETran.shift_code=3 AND "+
                         " OETran.sp_date='"+SDate+"' AND OETran.unit_code="+SUnit+")";

          return QS2;
     }

     public String getQS4()
     {
          String QS1 = " SELECT Sum(OETran.noofRot) AS SumOfnoofRot, "+
                       " Sum(OETran.Rotwkd) AS SumOfRotwkd, "+
                       " Sum(noofRot-Rotwkd) AS Expr1, "+
                       " Avg(Rotwkd*100/noofRot) AS Expr2,"+
                       " Sum(OETran.pneumafil) AS SumOfpneumafil, "+
//                       " Avg(OETran.gms_Rot) AS AvgOfgms_Rot, "+
                       " (sum(Prod)*1000)/(sum(RotWkd)) as AvgOfGms_Rot,"+
                       " Sum(OETran.conv_30s) AS SumOfconv_30s, "+
                       " Sum(OETran.conv_40s) AS SumOfconv_40s, "+
                       " Sum(OETran.prod_tar) AS SumOfprod_tar, "+
                       " Sum(OETran.prod) AS SumOfprod, "+
                       " Sum(prod-prod_Tar) AS Expr6, "+
                       " Sum(OETran.count*OETran.Rotwkd) AS SumOfCount "+
                       " FROM OETran INNER JOIN unit ON OETran.Unit_Code=unit.unitcode "+
                       " WHERE substr(sp_date,1,6)= '"+SDate.substring(0,6)+"' "+
                       " and OETran.sp_Date not in(select HDate from ProdHoliday where leaveday = 1)"+
//                     " and  OETran.prod>0"+  // Updated on 22-11-2004
                       " AND OETran.sp_date <= '"+SDate+"' AND OETran.unit_code="+SUnit;

          try
          {
                  FileWriter fw = new FileWriter("Sql.txt");
                  fw.write(QS1);
                  fw.close();
          }
          catch(Exception ex)
          {
          }
          return QS1;
     }

     public String getQS5()
     {
          String QS1 = " SELECT Sum(OETran.noofRot) AS SumOfnoofRot, "+
                       " Sum(OETran.Rotwkd) AS SumOfRotwkd, "+
                       " Sum(noofRot-Rotwkd) AS Expr1, "+
                       " ((sum(Rotwkd)*100)/sum(noofRot)) AS Expr2, "+
                       " Sum(OETran.pneumafil) AS SumOfpneumafil, "+
                       " Avg(OETran.gms_Rot) AS AvgOfgms_Rot, "+
                       " Sum(OETran.conv_30s) AS SumOfconv_30s, "+
                       " Sum(OETran.conv_40s) AS SumOfconv_40s, "+
                       " Sum(OETran.prod_tar) AS SumOfprod_tar, "+
                       " Sum(OETran.prod) AS SumOfprod, "+
                       " Sum(prod-prod_Tar) AS Expr6, "+
                       " Sum(OETran.count*OETran.Rotwkd) AS SumOfCount "+
                       " FROM OETran INNER JOIN unit ON OETran.Unit_Code = unit.unitcode "+
                       " WHERE OETran.sp_date='"+SDate+"'"+
                       " and OETran.sp_Date not in(Select HDate from ProdHoliday where leaveday = 1)"+  
//                     " and OETran.Prod>0"+
                       " AND OETran.unit_code="+SUnit;

          return QS1;
     }

     public void getVariables()
     {
          S1 = "0.0";
          S2 = "0.0";
          S3 = "0.0";

          SF1   = "0.0";
          SF2   = "0.0";
          SF3   = "0.0";
          SF4   = "0.0";
          SF5   = "0.0";
          SF6   = "0.0";
          SF7   = "0.0";
          SF8   = "0.0";
          SF9   = "0.0";
          SF10   = "0.0";
          SF11   = "0.0";
          SF12   = "0.0";
          SF13   = "0.0";
          SF14   = "0.0";
          SF15   = "0.0";
          SF16   = "0.0";
          SF17   = "0.0";
          SF18   = "0.0";

          SM1   = "0.0";
          SM2   = "0.0";
          SM3   = "0.0";
          SM4   = "0.0";
          SM5   = "0.0";
          SM6   = "0.0";
          SM7   = "0.0";
          SM8   = "0.0";
          SM9   = "0.0";
          SM10   = "0.0";
          SM11   = "0.0";
          SM12   = "0.0";
          SM13   = "0.0";
          SM14   = "0.0";
          SM15   = "0.0";
          SM16   = "0.0";
          SM17   = "0.0";
          SM18   = "0.0";

             try
             {
                     Connection con = createConnection();
                     Statement stat = con.createStatement();
                     ResultSet res1 = stat.executeQuery(getQS1());
                     while(res1.next())
                          S1 = res1.getString(1);
				 ResultSet res2 = stat.executeQuery(getQS2());
                     while(res2.next())
                          S2  = res2.getString(1);
                     ResultSet res3 = stat.executeQuery(getQS3());
                     while(res3.next())
                          S3  = res3.getString(1);
                     ResultSet res4 = stat.executeQuery(getQS4());
                     while(res4.next())
                     {
                         SM1   =  res4.getString(1);
                         SM2   =  res4.getString(2);
                         SM3   =  res4.getString(3);
                         SM4   =  res4.getString(4);

                         double dMWaste = res4.getDouble(5);
                         SM5   = common.getRound(dMWaste,2);

                         SM8   =  res4.getString(6);
                         SM9   =  res4.getString(7);
                         SM11  =  res4.getString(8);
                         SM15  =  res4.getString(9);

                         double dMProd = res4.getDouble(10);
                         SM16  = common.getRound(dMProd,2);

                         SM17  =  res4.getString(11);
                         SM18  =  res4.getString(12);
                         SM6   = common.getRound(dMWaste*100/(dMWaste+dMProd),2);
                         SM10  =  common.getRound(common.toDouble(SM9)*1000/common.toDouble(SM2),2);
                         SM12  =  common.getRound(common.toDouble(SM11)*1000/common.toDouble(SM2),2);
                      }
                     ResultSet res5 = stat.executeQuery(getQS5());
                     while(res5.next())
                     {
                         SF1   =  res5.getString(1);
                         SF2   =  res5.getString(2);
                         SF3   =  res5.getString(3);
                         SF4   =  res5.getString(4);

                         double dWaste = res5.getDouble(5);
                         SF5   = common.getRound(dWaste,2);

                         SF8   =  res5.getString(6);
                         SF9   =  res5.getString(7);
                         SF11  =  res5.getString(8);
                         SF15  =  res5.getString(9);

                         double dProd = res5.getDouble(10);
                         SF16  = common.getRound(dProd,2);

                         SF17  =  res5.getString(11);
                         SF18  =  res5.getString(12);

                         SF6   = common.getRound(dWaste*100/(dWaste+dProd),2);
                         SF10  =  common.getRound(common.toDouble(SF9)*1000/common.toDouble(SF2),2);
                         SF12  =  common.getRound(common.toDouble(SF11)*1000/common.toDouble(SF2),2);
                     }
                     con.close();
             }
             catch(Exception ex)
             {
                  System.out.println("Stat(getVars) -> "+ex);
                  ex.printStackTrace();                        
             }

             SF7 = common.getRound(common.toDouble(SF18)/common.toDouble(SF2),3);
             SM7 = common.getRound(common.toDouble(SM18)/common.toDouble(SM2),3);
     }
     public String getEQ1()
     {
          String EQ1 = "SELECT tnebdet.units FROM tnebdet WHERE tnebdet.ebdate='"+SDate+"' AND tnebdet.unit_code = "+SUnit;

          return EQ1;
     }
     public String getEQ2()
     {
          String EQ1 = "SELECT sum(finalreading-inireading) FROM generatordet WHERE generatordet.gendate='"+SDate+"' AND generatordet.unit_code = "+SUnit;

          return EQ1;
     }
     public String getEQ3()
     {
          String EQ1 = "SELECT sum(tnebdet.units) FROM tnebdet WHERE substr(ebdate,5,2)='"+SDate.substring(4,6)+"' AND tnebdet.unit_code = "+SUnit;

          return EQ1;
     }
     public String getEQ4()
     {
          String EQ1 = "SELECT sum(finalreading-inireading) FROM generatordet WHERE substr(gendate,5,2)='"+SDate.substring(4,6)+"' AND generatordet.unit_code = "+SUnit;

          return EQ1;
     }

     public void getUKG()
     {
             String SF13A="0",SF13B="0";
             String SM13A="0",SM13B="0";

             try
             {
				 //amar electric	
				 Connection con = createConnection();
                     Statement stat = con.createStatement();
                     ResultSet res1 = stat.executeQuery(getEQ1());
                     while(res1.next())
                         SF13A = res1.getString(1);

                     ResultSet res2 = stat.executeQuery(getEQ2());
                     while(res2.next())
                         SF13B = res2.getString(1);

                     ResultSet res3 = stat.executeQuery(getEQ3());
                     while(res3.next())
                         SM13A = res3.getString(1);

                     ResultSet res4 = stat.executeQuery(getEQ4());
                     while(res4.next())
                         SM13B = res4.getString(1);

                     con.close();
             }
             catch(Exception ex){System.out.println("Stat(getUKG)-> "+ex);}

             SF13 = common.getRound((common.toDouble(SF13A)+common.toDouble(SF13B))/common.toDouble(SF9),3);
             SM13 = common.getRound((common.toDouble(SM13A)+common.toDouble(SM13B))/common.toDouble(SM9),3);          
     }

     public void Head()
     {
          String Str1    = common.Pad("Company    : AMARJOTHI OE MILLS LIMITED",80);
          String Str2    = common.Pad("Document   : Spinnig Statement As On "+common.parseDate(SDate),80);
          SUnit          = SUnit.equals("1")?"A":(SUnit.equals("2")?"B":"S");
          String Str3    = common.Pad("Unit       : "+SUnit,70);
          SLine          = common.Replicate("-",80);
          try
          {
               FW.write(Str1+"\n");
               FW.write(Str2+"\n");
               FW.write(Str3+"\n\n\n");
          }catch(Exception ex){}
     }

     public void Body()
     {
        String Strl = "";

        try
        {
             Strl = common.Pad("For the Day",20)+common.Space(10)+
                    common.Cad("OE",20)+common.Space(10)+
                    common.Cad("Cone Winding",20);
             FW.write(SLine+"\n");
             FW.write(Strl+"\n");
             FW.write(SLine+"\n");

             Strl = common.Pad("First Shift",20)+common.Space(10)+
                    common.Rad(common.getRound(S1,2),20)+common.Space(10)+
                    common.Pad(" ",20);
             FW.write(Strl+"\n\n");

             Strl = common.Pad("Second Shift",20)+common.Space(10)+
                    common.Rad(common.getRound(S2,2),20)+common.Space(10)+
                    common.Pad(" ",20);
             FW.write(Strl+"\n\n");

             Strl = common.Pad("Third Shift",20)+common.Space(10)+
                    common.Rad(common.getRound(S3,2),20)+common.Space(10)+
                    common.Pad(" ",20);
             FW.write(Strl+"\n");
             FW.write(SLine+"\n");

             Strl = common.Pad(" ",20)+common.Space(3)+
                    common.Rad("Today",12)+common.Space(3)+
                    common.Rad("For Month",12)+common.Space(3)+
                    common.Rad("Today",12)+common.Space(3)+
                    common.Rad("For Month",12);
             FW.write(Strl+"\n");

             Strl = common.Pad(" ",20)+common.Space(3)+
                    common.Replicate("-",12)+common.Space(3)+
                    common.Replicate("-",12)+common.Space(3)+
                    common.Replicate("-",12)+common.Space(3)+
                    common.Replicate("-",12);
             FW.write(Strl+"\n\n");

             Strl = common.Pad("Actual Production",20)+common.Space(3)+
                    common.Rad(common.getRound(SF16,2),12)+common.Space(3)+
                    common.Rad(common.getRound(SM16,2),12)+common.Space(3)+
                    common.Rad(" ",12)+common.Space(3)+
                    common.Rad(" ",12);
             FW.write(Strl+"\n\n");

             Strl = common.Pad("Target Production",20)+common.Space(3)+
                    common.Rad(common.getRound(SF15,2),12)+common.Space(3)+
                    common.Rad(common.getRound(SM15,2),12)+common.Space(3)+
                    common.Rad(" ",12)+common.Space(3)+
                    common.Rad(" ",12);
             FW.write(Strl+"\n");
             FW.write(SLine+"\n");

             Strl = common.Pad("Variation",20)+common.Space(3)+
                    common.Rad(common.getRound(SF17,2),12)+common.Space(3)+
                    common.Rad(common.getRound(SM17,2),12)+common.Space(3)+
                    common.Rad(" ",12)+common.Space(3)+
                    common.Rad(" ",12);

             String SGPS1 = common.getRound((common.toDouble(SM16)*1000)/common.toDouble(SM2),3);

             FW.write(Strl+"\n");
             FW.write(SLine+"\n");
     
                
             Strl = common.Pad("Particulars",30)+common.Space(5)+common.Rad("For the Day",17)+common.Space(5)+common.Rad("For the Month",17);
             FW.write(Strl+"\n");
             FW.write(SLine+"\n");
             Strl = common.Pad("Rotters To Be Worked",30)+common.Space(5)+common.Rad(common.getRound(SF1,2),17)+common.Space(5)+common.Rad(common.getRound(SM1,2),17);
             FW.write(Strl+"\n\n");
             Strl = common.Pad("Actual Rotters Worked",30)+common.Space(5)+common.Rad(common.getRound(SF2,2),17)+common.Space(5)+common.Rad(common.getRound(SM2,2),17);
             FW.write(Strl+"\n\n");
             Strl = common.Pad("Idle Rotters ",30)+common.Space(5)+common.Rad(common.getRound(SF3,2),17)+common.Space(5)+common.Rad(common.getRound(SM3,2),17);
             FW.write(Strl+"\n\n");
             Strl = common.Pad("Utilization",30)+common.Space(5)+common.Rad(common.getRound(SF4,2),17)+common.Space(5)+common.Rad(common.getRound((common.toDouble(SM2)*100)/common.toDouble(SM1),2),17); //SM4
             FW.write(Strl+"\n\n");
             Strl = common.Pad("Pneumafil Waste Kgs",30)+common.Space(5)+common.Rad(common.getRound(SF5,2),17)+common.Space(5)+common.Rad(common.getRound(SM5,2),17);
             FW.write(Strl+"\n\n");
             Strl = common.Pad("Pneumafil Waste %",30)+common.Space(5)+common.Rad(common.getRound(SF6,2),17)+common.Space(5)+common.Rad(common.getRound(SM6,2),17);
             FW.write(Strl+"\n\n");
             Strl = common.Pad("Average Count",30)+common.Space(5)+common.Rad(common.getRound(SF7,2),17)+common.Space(5)+common.Rad(common.getRound(SM7,2),17);
             FW.write(Strl+"\n\n");
             Strl = common.Pad("Average Gms/Spindle",30)+common.Space(5)+common.Rad(common.getRound(SF8,2),17)+common.Space(5)+common.Rad(common.getRound(SM8,3),17); //common.getRound(SM8,2)
             FW.write(Strl+"\n\n");
             Strl = common.Pad("30s Converted Kgs",30)+common.Space(5)+common.Rad(common.getRound(SF9,2),17)+common.Space(5)+common.Rad(common.getRound(SM9,2),17);
             FW.write(Strl+"\n\n");
             Strl = common.Pad("30s Converted Gms/Spindle",30)+common.Space(5)+common.Rad(common.getRound(SF10,2),17)+common.Space(5)+common.Rad(common.getRound(SM10,2),17);
             FW.write(Strl+"\n\n");
             Strl = common.Pad("40s Converted Kgs",30)+common.Space(5)+common.Rad(common.getRound(SF11,2),17)+common.Space(5)+common.Rad(common.getRound(SM11,2),17);
             FW.write(Strl+"\n\n");
             Strl = common.Pad("40s Converted Gms/Spindle",30)+common.Space(5)+common.Rad(common.getRound(SF12,2),17)+common.Space(5)+common.Rad(common.getRound(SM12,2),17);
             FW.write(Strl+"\n\n");
             Strl = common.Pad("UKG with web unit",30)+common.Space(5)+common.Rad(common.getRound(SF13,2),17)+common.Space(5)+common.Rad(common.getRound(SM13,2),17);
             FW.write(Strl+"\n\n");
             Strl = common.Pad("UKG without web unit",30)+common.Space(5)+common.Rad(common.getRound(SF14,2),17)+common.Space(5)+common.Rad(common.getRound(SM14,2),17);
             FW.write(Strl+"\n\n");
             FW.write(SLine+"\n\n\n\n\n");
             Strl = "Prepared By           P.O.               S.P.O.              S.M.            D.M.";
             FW.write(Strl);
             FW.write("   \n");
     }
     catch(Exception ex){System.out.println(ex);}
     }

	private void setCriteria(HttpServletRequest request)
	{
		SDate            = request.getParameter("Date");
                SDate            = (String)common.pureDate(SDate);
		SUnit            = request.getParameter("Unit");

		session.putValue("Unit",SUnit);
		session.putValue("Date",SDate);
		//System.out.println("SpinStat -> " + SUnit + ":" + SDate);
	}

	private Connection createConnection()
	{
		Connection conn;
		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
			conn  = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
		}catch(Exception ee)
		{
			return null;
		}
		return conn;
	}
        
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>


		  
public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        // c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str ));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        //  c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(Paragraph p1, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(p1);
        //c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddCellIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, int iLeftBorder, int iTopBorder, int iRightBorder, int iBottomBorder, Font DocFont, int iRowSpan) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    }

    public void AddColumnIntoTable(String Str, PdfPTable table, int iHorizontal, int iVertical, float fHeight, int iColWidth, int iLeftBorder, int iTopBorder, int iRightBorder, Font DocFont, int iBottomBorder) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColWidth);
        // c1.setRowspan(iRowSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
        table.addCell(c1);
    } 






}
