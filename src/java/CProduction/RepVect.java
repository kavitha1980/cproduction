package CProduction;

import java.util.*;
import java.sql.*;
public class RepVect
{
      java.sql.Connection conn  = null;
      Vector VTName,VTCode;                    // Ticket
      Vector VCCode,VCName;                    // Count
      Vector VUnit,VUnitCode;                  // Unit
      Vector VShift,VShiftCode;                // Shift
      Vector VDept,VDeptCode;                  // Dept
      Vector Vponame,VpoCode;                  // Po/Jpo
      Vector VMCode,VMName;
      
      String SUnitCode="0";
      
      Common common = new Common();
      

      RepVect(String SDeptCode)
      {
            setVectors(SDeptCode);
      }
      
      RepVect()
      {
            setVectors("01");
      }
      RepVect(String SDeptCode,String SUnitCode)
      {
            try
            {
                  this.SUnitCode = SUnitCode;
                  setVectors(SDeptCode,SUnitCode);
            }
            catch(Exception ex)
            {
                  System.out.println("@ ComVect"+ex);
            }
      }

      public void setVectors(String SDeptCode)
      {

            VCName = new Vector();
            VCCode = new Vector();

            VUnit      = new Vector();
            VUnitCode  = new Vector();

            VShift     = new Vector();
            VShiftCode = new Vector();

            VDept      = new Vector();
            VDeptCode  = new Vector();
            
            Vponame    = new Vector();
            VpoCode    = new Vector();
           
            VShift.addElement("1st Shift");
            VShift.addElement("2nd Shift");
            VShift.addElement("3rd Shift");

            VShiftCode.addElement("1");
            VShiftCode.addElement("2");
            VShiftCode.addElement("3");

           try
           {
                  
                  if(conn==null) {
                        conn = JDBCConnection.createORCDBConnection();
                  }
                  Statement stat2  = conn.createStatement();

                  ResultSet result1 = stat2.executeQuery("Select UnitName,UnitCode From Unit Order By UnitName");
                  while(result1.next())
                  {
                       VUnit.addElement(result1.getString(1));
                       VUnitCode.addElement(result1.getString(2));
                  }
                  result1.close();
                  
                  ResultSet result3 = stat2.executeQuery("Select Count_Name,Count_Code From Count Order By Count_Name");
                  while(result3.next())
                  {
                       VCName.addElement(result3.getString(1));
                       VCCode.addElement(result3.getString(2));
                  }
                  result3.close();
                  
                  ResultSet result4 = stat2.executeQuery("Select Supervisor,SupCode,Designation From Supervisor Order By 1");
                  while(result4.next())
                  {
                       Vponame.addElement(result4.getString(1));
                       VpoCode.addElement(result4.getString(2));
                  }
                  result4.close();
/*
                  ResultSet result4 = stat2.executeQuery("Select Ticket_Name,Ticket_Code From Ticket Order By 1");
                  while(result4.next())
                  {
                       VTName.addElement(result4.getString(1));
                       VTCode.addElement(result4.getString(2));
		        }
			   System.out.println("Ticket -> " + VTName.size());
 */                 
                  

                  ResultSet result5 = stat2.executeQuery("Select DeptName,ProdCode From Department Order By DeptName");
                  while(result5.next())
                  {
                       VDept.addElement(result5.getString(1));
                       VDeptCode.addElement(result5.getString(2));
                  }
                  result5.close();
                  conn.close();
           }
           catch(Exception ex) {
               
           }
      }
      public void setVectors(String SDeptCode,String SUnitCode)
      {

            VCName = new Vector();
            VCCode = new Vector();

            VUnit      = new Vector();
            VUnitCode  = new Vector();

            VShift     = new Vector();
            VShiftCode = new Vector();

            VDept      = new Vector();
            VDeptCode  = new Vector();
            
            Vponame    = new Vector();
            VpoCode    = new Vector();
            
            VMCode     = new Vector();
            VMName     = new Vector();
           
            VShift.addElement("1st Shift");
            VShift.addElement("2nd Shift");
            VShift.addElement("3rd Shift");

            VShiftCode.addElement("1");
            VShiftCode.addElement("2");
            VShiftCode.addElement("3");

           try
           {
                  
                  if(conn==null) {
                        conn = JDBCConnection.createORCDBConnection();
                  }
                  Statement stat2  = conn.createStatement();

                  ResultSet result1 = stat2.executeQuery("Select UnitName,UnitCode From Unit Order By UnitName");
                  while(result1.next())
                  {
                       VUnit.addElement(result1.getString(1));
                       VUnitCode.addElement(result1.getString(2));
                  }
                  result1.close();
                  
                  ResultSet result3 = stat2.executeQuery("Select Count_Name,Count_Code From Count Order By Count_Name");
                  while(result3.next())
                  {
                       VCName.addElement(result3.getString(1));
                       VCCode.addElement(result3.getString(2));
                  }
                  result3.close();
                  
                  ResultSet result4 = stat2.executeQuery("Select Supervisor,SupCode,Designation From Supervisor Order By 1");
                  while(result4.next())
                  {
                       Vponame.addElement(result4.getString(1));
                       VpoCode.addElement(result4.getString(2));
                  }
                  result4.close();
                  ResultSet result5 = stat2.executeQuery("Select Mach_Code,Mach_Name From Machine where Dept_Code="+SDeptCode+" and Unit_Code="+SUnitCode+" Order By 1");
                  while(result5.next())
                  {
                       VMCode.addElement(result4.getString(1));
                       VMName.addElement(result4.getString(2));
                  }
                  result5.close();
                  
/*
                  ResultSet result4 = stat2.executeQuery("Select Ticket_Name,Ticket_Code From Ticket Order By 1");
                  while(result4.next())
                  {
                       VTName.addElement(result4.getString(1));
                       VTCode.addElement(result4.getString(2));
		        }
			   System.out.println("Ticket -> " + VTName.size());
 */                 
                  

                  ResultSet result6 = stat2.executeQuery("Select DeptName,ProdCode From Department Order By DeptName");
                  while(result5.next())
                  {
                       VDept.addElement(result6.getString(1));
                       VDeptCode.addElement(result6.getString(2));
                  }
                  result6.close();
                  conn.close();
           }
           catch(Exception ex) {
               
           }
      }

	private Connection createConnection()
	{
		Connection conn;
		
                try
		{
			Class.forName("oracle.jdbc.OracleDriver");
                        conn  = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
		}catch(Exception ee) {
                    System.out.println(" conn null pointer excep");
			return null;
		}
		return conn;
	}
}
