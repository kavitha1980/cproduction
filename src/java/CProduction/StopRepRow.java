package CProduction;

import java.util.*;


public class StopRepRow
{
     String SMacCode="",SMacName="";
     Vector VStopCode,VStopMint,VStopName,VIndex;
     public StopRepRow()
     {
          SMacCode = "";
          SMacName = "";
          VStopCode = new Vector();
          VStopMint = new Vector();
          VStopName = new Vector();
     }
     public void setData(String SStopCode,String SMacCode,String SStopMint,String SStopName,String SMacName)
     {
          this.SMacCode = SMacCode;
          this.SMacName = SMacName;
          VStopCode.addElement(SStopCode);
          VStopMint.addElement(SStopMint);
          VStopName.addElement(SStopName);
     }
     public Vector getFields(Vector VHead1)
     {
          VIndex = new Vector();          
          for(int i=0;i<VHead1.size();i++)
          {
               String SStopCode = (String)VHead1.elementAt(i);
               int iindex = VStopCode.indexOf(SStopCode);
               if(iindex==-1)
                  VIndex.addElement(".");
               else
               {
                  String SStopMint = (String)VStopMint.elementAt(iindex);
                  VIndex.addElement(SStopMint);
               }   
          }
          return VIndex;
     }
}
