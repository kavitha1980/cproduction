package CProduction;

import java.io.*;
import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class MiscChairmanAReport extends HttpServlet
{
     HttpSession    session;
     Common common;

     String SDate,SLine,SServer,EWStop,MWStop,SMachs;
     String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody,tbgBody;

     Vector VParti,VStd,VAchQuery,VAch,VDeptCode,VUom,VStrl,VR,VC,VId;

     FileWriter  FW,FW2;
     FileReader  FR;

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
          common = new Common();

          bgColor = common.bgColor;
          bgHead  = common.bgHead;
          bgUom   = common.bgUom;
          bgBody  = common.bgBody;
          fgHead  = common.fgHead;
          fgUom   = common.fgUom;
          fgBody  = common.fgBody;
		tbgBody = common.tbgBody;
     }
     public void doGet(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
          response.setContentType("text/html");
          session             = request.getSession(true);
		/*if(session.getValue("AppletTag")==null)
		{
			Control control = new Control();
			String SServer = control.getID("Select ProdHost from Server");
			response.sendRedirect(SServer + "/SessionExpired");
			return;

		}*/
          //SServer             = (String)session.getValue("Server");
          PrintWriter out  = response.getWriter();
          SDate            = (request.getParameter("Date").toString()).trim();
          SDate            = common.pureDate(SDate);
		System.out.println("Ch-A : " + SDate);

          out.println("<p></p>");

          out.println("<html>");
          out.println("<head>");
          out.println("<title>Chairman Report for A - Unit</title>");
          out.println("</head>");
          out.println("<body bgcolor='" + common.bgBody + "'>");
		//out.println(session.getValue("AppletTag"));
          out.println("<a href='Header'>Home</a>");
		out.println(common.getChairmanReportHeader("Chairman Report for A - Unit",SDate));
          out.println("<form method='POST' action='MiscChairmanAReport'>");
          out.println("  <table border='1' width='898' height='1143'>");
          out.println("    <tr>");
          out.println("      <td width='172' height='44' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'><b>Department</b></font></td>");
          out.println("      <td width='42' height='44' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"' size='4'><b>UOM</b></font></td>");
          out.println("      <td width='112' height='44' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'><b>Achieved</b></font></td>");
          out.println("      <td width='113' height='44' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'><b>Standard</b></font></td>");
          out.println("      <td width='193' height='44' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'><b>Remarks</b></font></td>");
          out.println("      <td width='226' height='44' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'><b>CM Instruction</b></font></td>");
          out.println("    </tr>");

          if(!getTextFile())
               fetchData();

          for(int i=0;i<VParti.size();i++)
          {
               if((((String)VUom.elementAt(i)).trim()).equals("`"))
               {
                    out.println("    <tr>");
                    out.println("      <td width='888' height='30' colspan='6' bgcolor='"+bgUom+"'><font color='"+fgUom+"' size='4'><b>"+VParti.elementAt(i)+"</b></font></td>");
                    out.println("    </tr>");
                    continue;
               }
               out.println("    <tr>");
               out.println("      <td width='172' height='44' align='left' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'><b>"+(String)VParti.elementAt(i)+"</b></font></td>");
               out.println("      <td width='42' height='44' align='center' bgcolor='"+bgUom+"'><font color='"+fgUom+"' size='4'><b>"+(String)VUom.elementAt(i)+"</b></font></td>");
               String st = (String)VAch.elementAt(i);
               if(st.startsWith("~"))
               {
                    st = st.substring(1,st.length());
                    out.println("      <td width='112' height='44' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"' size='4'><p align='right'><input type='text' name='T"+i+"' size='10' value='"+st+"'></font></td>");
               }
               else
                    out.println("      <td width='112' height='44' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"' size='4'><b>"+(String)VAch.elementAt(i)+"</b></font></td>");

               out.println("      <td width='113' height='44' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"' size='4'><b>"+(String)VStd.elementAt(i)+"</b></font></td>");
               try
               {
                    out.println("      <td width='193' height='44' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"' size='4'><p align='center'><input type='text' name='R"+i+"' size='20' value="+(String)VR.elementAt(i)+"></font></td>");
               }
               catch(Exception ex)
               {
                    out.println("      <td width='193' height='44' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"' size='4'><p align='center'><input type='text' name='R"+i+"' size='20'></font></td>");
               }

               try
               {
                    out.println("      <td width='226' height='44' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"' size='4'><p align='center'><input type='text' name='C"+i+"' size='20' value="+(String)VC.elementAt(i)+"></font></td>");
               }
               catch(Exception ex)
               {
                    out.println("      <td width='226' height='44' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"' size='4'><p align='center'><input type='text' name='C"+i+"' size='20'></font></td>");
               }
               out.println("    </tr>");
          }
          out.println("  </table>");
          out.println("</div>");
          out.println("<blockquote>");
          out.println("<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
          out.println("<input type='submit' value='Save' name='BSave' style='font-size: 18pt; color: #003300; font-weight: bold'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
          out.println("<input type='reset' value='Cancel' name='BCancel' style='color: #FF0000; font-size: 18pt; font-style: italic; font-weight: bold'></p>");
          out.println("</blockquote>");
          out.println("</form>");
          out.println("</body>");
          out.println("</html>");
          out.close();
     }
     public void doPost(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
          response.setContentType("text/html");
          session             = request.getSession(false);
          //SServer             = (String)session.getValue("Server");
          saveData(request);
     }
     private boolean getTextFile()
     {
          try
          {
               FR = new FileReader("D:/production/acmrep/"+SDate+".txt");
          }
          catch(Exception ex)
          {
               return false;
          }
          try
          {
               setDataFromFile(FR);
               FR.close();
          }catch(Exception ex){System.out.println(ex);}
          return true;
     }
     private void fetchData()
     {
          VStd      = new Vector();
          VAch      = new Vector();
          VParti    = new Vector();
          VAchQuery = new Vector();
          VDeptCode = new Vector();
          VUom      = new Vector();

          try
          {
               Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
               Connection con = DriverManager.getConnection("jdbc:odbc:AmarProcess","","");
               Statement stat = con.createStatement();
               ResultSet res  = stat.executeQuery(getQString());
               while(res.next())
               {
                    VStd      .addElement(res.getString(1));
                    VParti    .addElement(res.getString(2));
                    VAchQuery .addElement(res.getString(3));
                    VDeptCode .addElement(res.getString(4));
                    VUom      .addElement(res.getString(5));
               }

               EWStop="";MWStop="";

               ResultSet resMa = stat.executeQuery(common.getMachines("7","1"));
               while(resMa.next())
                    SMachs = resMa.getString(1);

               ResultSet resEW = stat.executeQuery(getEBQS());
               while(resEW.next())
                    EWStop = resEW.getString(1);
               EWStop = calcStopPer(EWStop);

               ResultSet resMW = stat.executeQuery(getMWQS());
               while(resMW.next())
                    MWStop = resMW.getString(1);
               MWStop = calcStopPer(MWStop);

               for(int i=0;i<VStd.size();i++)
               {
                    String str = execAchQuery(stat,(String)VAchQuery.elementAt(i));
                    if(str=="~")
                         VAch.addElement(str);
                    else if(str=="EW")
                         VAch.addElement(EWStop);
                    else if(str=="MW")
                         VAch.addElement(MWStop);
                    else
                         VAch.addElement(common.getRound(str,2));
               }
               con.close();
          }catch(Exception ex)
          {
               System.out.println("From 116 CM1 : "+ex);
          }
     }
     private String calcStopPer(String SVal)
     {
          double dRetVal=0;
          double dMnt   = common.toDouble(SVal);
          double dHours = common.toDouble(SMachs)*24*common.getDays(SDate);
          dRetVal    = dMnt*100/(dHours*60);
          return common.getRound(dRetVal,2);
     }
     private String getQString()
     {
          String QS = " SELECT standard,particular,ach,dept_code,uom "+
                      " FROM cma ORDER BY sig";
          return QS;
     }
     public String getEBQS()
     {
          String QS = " SELECT SUM(StopTran.Stop_Mint) "+
                      " FROM StopTran INNER JOIN stopreasons ON StopTran.Stop_Code = stopreasons.STOP_CODE "+
                      " WHERE StopTran.Dept_Code = 7 AND StopTran.Unit_Code = 1 "+
                      " AND StopTran.Stop_Date<='"+SDate+"' AND StopTran.Stop_Date>='"+common.getStartDate(SDate)+"' AND "+
                      " (Stopreasons.Maint_Code='EB' OR Stopreasons.Maint_Code='EW')";
          return QS;
     }

     public String getMWQS()
     {
          String QS = " SELECT SUM(StopTran.Stop_Mint) "+
                      " FROM StopTran INNER JOIN stopreasons ON StopTran.Stop_Code = stopreasons.STOP_CODE "+
                      " WHERE StopTran.Dept_Code = 7 AND StopTran.Unit_Code = 1 "+
                      " AND StopTran.Stop_Date<='"+SDate+"' AND StopTran.Stop_Date>='"+common.getStartDate(SDate)+"' AND "+
                      " (Stopreasons.Maint_Code='MB' OR Stopreasons.Maint_Code='MW')";
          return QS;
     }

     private String execAchQuery(Statement stat,String QS)
     {
          if((QS.trim()).equals("0"))
               return "0";
          else if((QS.trim()).equals("~"))
               return "~";
          else if((QS.trim()).equals("EW"))
               return "EW";
          else if((QS.trim()).equals("MW"))
               return "MW";
          else
          {
               String retStr="";
               try
               {
                    ResultSet res  = stat.executeQuery(QS+"='"+SDate+"' and Unit_code=1");
                    while(res.next())
                         retStr = res.getString(1);
               }catch(Exception ex){System.out.println("From 132 CM1 : "+ex);}
               return retStr;
          }
     }
     private void saveData(HttpServletRequest request)
     {
          try
          {
               // if Exception in Opening the file for Writing, Return.
               FW  = new FileWriter("/software/C-Prod-Print/Reports/"+SDate+".txt");
               FW2 = new FileWriter("/software/C-Prod-Print/Reports/CMAc.prn");

               String SPrMid = "ÃÄÄÅ";
                      SPrMid = SPrMid+common.Replicate("Ä",25)+"Å";
                      SPrMid = SPrMid+common.Replicate("Ä",5)+"Å";
                      SPrMid = SPrMid+common.Replicate("Ä",10)+"Å";
                      SPrMid = SPrMid+common.Replicate("Ä",6)+"Å";
                      SPrMid = SPrMid+common.Replicate("Ä",30)+"Å";
                      SPrMid = SPrMid+common.Replicate("Ä",30)+"Ž";

               String SPrBot = "ÀÄÄÁ";
                      SPrBot = SPrBot+common.Replicate("Ä",25)+"Á";
                      SPrBot = SPrBot+common.Replicate("Ä",5)+"Á";
                      SPrBot = SPrBot+common.Replicate("Ä",10)+"Á";
                      SPrBot = SPrBot+common.Replicate("Ä",6)+"Á";
                      SPrBot = SPrBot+common.Replicate("Ä",30)+"Á";
                      SPrBot = SPrBot+common.Replicate("Ä",30)+"Ù";

               prtHead(FW2,SPrMid);
               int iCtr=0;

               for(int i=0;i<VParti.size();i++)
               {
                    String SLine = "";
                    String SPrLine = "";
                    iCtr++;
                    if((((String)VUom.elementAt(i)).trim()).equals("`"))
                    {
                         SLine   = "~|"+(String)VParti.elementAt(i);
                         SPrLine = "³E  ³"+common.Pad((String)VParti.elementAt(i),25)+"F³";
                                     SPrLine = SPrLine+common.Space(5)+"³";
                                     SPrLine = SPrLine+common.Space(10)+"³";
                                     SPrLine = SPrLine+common.Space(6)+"³";
                                     SPrLine = SPrLine+common.Space(30)+"³";
                                     SPrLine = SPrLine+common.Space(30)+"³";

                         FW.write(SLine+"\n");
                         FW2.write(SPrLine+"\n");
                         iCtr=0;
                         continue;
                    }

                    SLine = "0|"+(String)VParti.elementAt(i)+"|";
                    SLine = SLine+(String)VUom.elementAt(i)+"|";

                    SPrLine = "³"+common.Rad(""+iCtr,2)+"³"+common.Pad((String)VParti.elementAt(i),25)+"³";
                    SPrLine = SPrLine+common.Pad((String)VUom.elementAt(i),5)+"³";

                    if(((String)VAch.elementAt(i)).equals("~"))
                    {
                         SLine   = SLine+"~"+request.getParameter("T"+i)+"|";
                         SPrLine = SPrLine+common.Rad(request.getParameter("T"+i),10)+"³";
                    }
                    else
                    {
                         SLine   = SLine+(String)VAch.elementAt(i)+"|";
                         SPrLine = SPrLine+common.Rad((String)VAch.elementAt(i),10)+"³";
                    }

                    SLine = SLine+(String)VStd.elementAt(i)+"|";
                    SLine = SLine+" "+request.getParameter("R"+i)+"|";
                    SLine = SLine+" "+request.getParameter("C"+i)+"|";

                    SPrLine = SPrLine+common.Rad((String)VStd.elementAt(i),6)+"³";
                    SPrLine = SPrLine+common.Pad(request.getParameter("R"+i),30)+"³";
                    SPrLine = SPrLine+common.Pad(request.getParameter("C"+i),30)+"³";

                    FW.write(SLine+"\n");
                    FW2.write(SPrLine+"\n");
                    FW2.write(SPrMid+"\n");
               }
               FW2.write(SPrBot+"\n");

               FW.close();
               FW2.close();
          }catch(Exception ex){}
     }
     private void prtHead(FileWriter FW2,String SMidLine) throws Exception
     {
          String SPrTop = "ÚÄÄÂ";
                 SPrTop = SPrTop+common.Replicate("Ä",25)+"Â";
                 SPrTop = SPrTop+common.Replicate("Ä",5)+"Â";
                 SPrTop = SPrTop+common.Replicate("Ä",10)+"Â";
                 SPrTop = SPrTop+common.Replicate("Ä",6)+"Â";
                 SPrTop = SPrTop+common.Replicate("Ä",30)+"Â";
                 SPrTop = SPrTop+common.Replicate("Ä",30)+"¿";

          FW2.write("Chairman Report For A Unit On "+common.parseDate(SDate)+"\n\n");
          FW2.write(SPrTop+"\n");

          String Strl1 = "³No³";
                 Strl1 = Strl1+common.Cad("Particular",25)+"³";
                 Strl1 = Strl1+common.Cad("UOM",5)+"³";
                 Strl1 = Strl1+common.Cad("Achieved",10)+"³";
                 Strl1 = Strl1+common.Cad("Std",6)+"³";
                 Strl1 = Strl1+common.Cad("Remarks",30)+"³";
                 Strl1 = Strl1+common.Cad("CM Instruction",30)+"³";
          FW2.write(Strl1+"\n");
          FW2.write(SMidLine+"\n");
     }
     public void setDataFromFile(FileReader FR) throws Exception
     {
          VParti = new Vector();
          VStd   = new Vector();
          VAch   = new Vector();
          VUom   = new Vector();
          VR     = new Vector();
          VC     = new Vector();
          VStrl  = new Vector();
          VId    = new Vector();

          String str="";

          // try to start reading
          //Reader in = new FileReader(f);
          char[] buff = new char[4096];
          int nch;

          while ((nch = FR.read(buff, 0, buff.length)) != -1)
               str = new String(buff, 0, nch);

          StringTokenizer ST = new StringTokenizer(str,"\n");
          while(ST.hasMoreTokens())
               VStrl.addElement(ST.nextToken());                       

          for(int k=0;k<VStrl.size();k++)
          {
               str = (String)VStrl.elementAt(k);
               ST = new StringTokenizer(str,"|");
               while(ST.hasMoreTokens())
               {
                    String Str = ST.nextToken();
                    VId.addElement(Str);
                    if(Str.equals("~"))
                    {
                         VParti.addElement(ST.nextToken());
                         VUom.addElement("`");
                         VAch.addElement(" ");
                         VStd.addElement(" ");
                         VR.addElement(" ");
                         VC.addElement(" ");
                    }
                    else
                    {
                         VParti.addElement(ST.nextToken());
                         VUom.addElement(ST.nextToken());
                         VAch.addElement(ST.nextToken());
                         VStd.addElement(ST.nextToken());
                         VR.addElement(common.parseNull(ST.nextToken()));
                         VC.addElement(common.parseNull(ST.nextToken()));
                    }
               }
          }

     }
}

/*
Legend
------

          Parti             UOM      Ach        Std       Remark    CM Ins

1          25                 5       10        10         100       100

~|                         |     |          |          |    ...  |   ...   |

~ : Heading Line i.e. (((String)VUom.elementAt(i)).trim()).equals("`")
0 : Particulars Available

*/
