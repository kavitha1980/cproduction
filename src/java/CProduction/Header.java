/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CProduction;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author arun
 */

public class Header extends HttpServlet {

    String SPassword = "";
    
    Common common = new Common();
    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        if(request.getParameter("Password")!=null){
            SPassword = request.getParameter("Password");
        }
        
    try {
        out.println("<html>");
        out.println("<head>");
        out.println("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />");
        out.println("<title>AMARJOTHI SPINNING MILLS LIMITED</title>");
        out.println("<meta name='Author' content='Stu Nicholls' />");
        out.println("<link rel='stylesheet' type='text/css' href='css/header.css' />");
        out.println("<script src='css/stuHover.js' type='text/javascript'></script>");
        out.println("</head>");
        
        out.println("<body>");
        
        out.println("<span class='preload1'></span>");
        out.println("<span class='preload2'></span>");
        out.println("<ul id='nav'>");
        
        out.println("<li class='top'><a href='Login' id='products' class='top_link'><span class='down'>Home</span></a>");
        out.println("</li>");
        
        out.println("<li class='top'><a href='#nogo2' id='products' class='top_link'><span class='down'>Carding</span></a>");
        out.println("<ul class='sub'>");
        out.println("<li><a href='PeriodicalReportForm?id=1'><span>Periodical</span></a></li>");
        out.println("<li><a href='DailyReportForm?id=1'><span>Daily</span></a></li>");
        out.println("<li><a href='ConsCritForm?id=1'><span>Consolidated</span></a></li>");
        out.println("</ul>");
        out.println("</li>");
        
        out.println("<li class='top'><a href='#nogo2' id='products' class='top_link'><span class='down'>Drawing</span></a>");
        out.println("<ul class='sub'>");
        out.println("<li><a href='DailyReportForm?id=2'><span>Daily</span></a></li>");
        out.println("<li><a href='ConsCritForm?id=2'><span>Consolidated</span></a></li>");
        out.println("</ul>");
        out.println("</li>");
        
        out.println("<li class='top'><a href='#nogo2' id='products' class='top_link'><span class='down'>OE</span></a>");
        out.println("<ul class='sub'>");
        out.println("<li><a href='DailyReportForm?id=3'><span>Daily</span></a></li>");
        out.println("<li><a href='ConsCritForm?id=3'><span>Consolidated</span></a></li>");
        out.println("<li><a href='MonthlyReport?id=1'><span>Over All Production Report</span></a></li>");        
        if(SPassword!=null && SPassword.trim().equalsIgnoreCase("jrp")) {
        out.println("<li><a href='OEStatementForm?id=1'><span>Statement</span></a></li>");
        out.println("<li><a href='OEStatementForm?id=2'><span>Stoppages(Rotters)</span></a></li>");
        out.println("<li><a href='OEStatementForm?id=3'><span>Stoppages(Mins)</span></a></li>");
        //out.println("<li><a href='OEStatementForm?id=4'><span>OE-I</span></a></li>");
        //out.println("<li><a href='OEStatementForm?id=5'><span>OE-II</span></a></li>");
        out.println("<li><a href='OEStatementForm?id=6'><span>Production Report</span></a></li>");
        out.println("<li><a href='WorkerPerformanceCritForm?id=1'><span>Worker Performance</span></a></li>");
        }
         out.println("<li><a href='MonthWiseProductionServlet?id=14'><span>Monthly Utilization Report</span></a></li>");
        out.println("</ul>");
        out.println("</li>");
        /*
        out.println("<li class='top'><a href='#nogo2' id='products' class='top_link'><span class='down'>Worker</span></a>");
        out.println("<ul class='sub'>");
        out.println("<li><a href='WorkerAsOnCritForm?id=1'><span>As On</span></a></li>");//unit,dept,date ->WorkCritServlet
        out.println("<li><a href='WorkerPeriodCritForm?id=2'><span>Period</span></a></li>");//unit,dept,stdate,endate ->WorkCritServlet
        out.println("<li><a href='WorkerGradeCritForm?id=3'><span>Grade</span></a></li>");
        out.println("<li><a href='WorkerGradeCritForm?id=4'><span>Waste Production</span></a></li>");
        out.println("</ul>");
        out.println("</li>");
        */
        if(SPassword!=null && SPassword.trim().equalsIgnoreCase("jrp")) {
        out.println("<li class='top'><a href='#nogo2' id='products' class='top_link'><span class='down'>PO/JPO</span></a>");
        out.println("<ul class='sub'>");
        out.println("<li><a href='PoJpoform?id=1'><span>As On</span></a></li>");
        out.println("<li><a href='PoJpoform?id=2'><span>Period</span></a></li>");
        out.println("<li><a href='PoJpoform?id=3'><span>Grade</span></a></li>");
        out.println("</ul>");
        out.println("</li>");
        
        out.println("<li class='top'><a href='#nogo2' id='products' class='top_link'><span class='down'>Stoppages</span></a>");
        out.println("<ul class='sub'>");
        out.println("<li><a href='Stoppageform?id=1'><span>Shift</span></a></li>");
        out.println("<li><a href='Stoppageform?id=2'><span>Period</span></a></li>");
        out.println("<li><a href='Stoppageform?id=3'><span>OverAll</span></a></li>");
        out.println("<li><a href='Stoppageform?id=4'><span>Breakup</span></a></li>");
        out.println("</ul>");
        out.println("</li>");
        
        out.println("<li class='top'><a href='#nogo2' id='products' class='top_link'><span class='down'>Misc</span></a>");
        out.println("<ul class='sub'>");
        out.println("<li><a href='Miscform?id=1'><span>OverAll</span></a></li>");
        out.println("<li><a href='Miscform?id=2'><span>CM:A Unit</span></a></li>");
        out.println("<li><a href='Miscform?id=3'><span>CM:B Unit</span></a></li>");
        //out.println("<li><a href=''><span>Sqc</span></a></li>");
        out.println("</ul>");
        out.println("</li>");
        }
        out.println("</ul>");
        //out.println("<h1>Servlet Header at " + request.getContextPath() + "</h1>");
        out.println("</body>");
        out.println("</html>");
        } finally {            
           // out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    
    
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    
    
    
}
