/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CProduction;

import java.sql.Statement;
import java.sql.Connection;

/**
 *
 * @author arun
 */
public class JDBCConnection {

    public JDBCConnection(){
    
    }
    
    public static Connection createORCDBConnection() {
         
         Connection con = null;
         Statement stat = null;
         
     try {
          Class.forName("oracle.jdbc.OracleDriver");
          con = java.sql.DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun", "prodc","prodc");
//          con = java.sql.DriverManager.getConnection("jdbc:oracle:thin:@117.239.247.18:1530:arun", "prodc","prodc");
          stat = con.createStatement();

         }catch (java.sql.SQLException sqlEx) {
            sqlEx.printStackTrace();
         }catch (java.lang.ClassNotFoundException cNFE) {
            cNFE.printStackTrace();
         }
     
        return con;
    }
    

 public static Connection closedConnections() {
     
     Connection con = null;
     Statement stat = null;
     try {
           stat.close();
           con.close();
           stat = null;
           con = null;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return con;
    }
    
}
