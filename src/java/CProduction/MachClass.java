package CProduction;

public class MachClass
{
     int iMachCode,iShift,iDate;
     String S1,S2,S3,S4,S5,SClean;

     MachClass()
     {
          // Default Constructor
     }
     MachClass(int iMachCode,int iShift)
     {
          this.iMachCode = iMachCode;
          this.iShift    = iShift;

          S1     = "0";
          S2     = "0";
          S3     = "0";
          S4     = "0";
          S5     = "0";
          SClean = "0";
     }
     MachClass(int iMachCode,int iShift,int iDate)
     {
          this.iMachCode = iMachCode;
          this.iShift    = iShift;
          this.iDate     = iDate;

          S1     = "0";
          S2     = "0";
          S3     = "0";
          S4     = "0";
          S5     = "0";
          SClean = "0";
     }
     public void append(String SVal,int iOffRepCode)
     {
          switch (iOffRepCode)
          {
                case 1:
                      S1 = SVal;
                      break;
                case 2:
                      S2 = SVal;
                      break;   
                case 3:
                      S3 = SVal;
                      break;   
                case 4:
                      S4 = SVal;
                      break;   
                case 5:
                      S5 = SVal;
                      break;   
                case 9:
                      SClean = SVal;
                      break;   
                default:
                      break;
          }
     }
}
