package CProduction;

import java.io.*;
import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import java.io.FileOutputStream;

public class OEDailyReport extends HttpServlet
{
     HttpSession    session;
     Common common;
	
     Vector VDate,VUnit,VShift,VMachNo,VCount,VTicket,VName;
     Vector VSpeed,VTPI,VTSpeed,VHank,VNoRot;
     Vector VHourMeter,VHourRun,VHourAllot;
     Vector VHankExp,VProd,VProdExp,VProdTar;
     Vector VTarEffy,VExpAct,VTarAct,VStopMnt,VWkdRot,VGps,VPneu;
     Vector VPneuTar,VPneuVar,VPneuPer,VUtilOA,VNoDoff,VCountVal;

     Vector VAbs1,VAbs2,VAbs3,VAbs4,VAbs5,VAbs6,VAbs7,VAbs8;

     String SDate,SShift,SUnit,SSupName;
     String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody,tbgBody;

     FileWriter  FW;
     int Lctr = 70,Pctr = 0;
     String SLine;
     Vector VHead;
     boolean bFinalized;
     Control control = new Control();

     String SFile                     = "";
     String[]                         ColumnName,ColumnType,ColumnName1,ColumnType1,ColumnName2,ColumnType2,sBody;
     int[]                            iColumnWidth,iColumnWidth1,iColumnWidth2;
     private static Font catFont     = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
     private static Font redFont     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
     private static Font subFont     = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);

     private static Font bigBold     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
     private static Font bigNormal   = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);

     private static Font mediumBold  = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
     private static Font mediumNormal= new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font smallBold   = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD);
     private static Font smallNormal = new Font(Font.FontFamily.TIMES_ROMAN, 6, Font.NORMAL);

     private static Font tinyBold    = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.BOLD);
     private static Font tinyNormal  = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);

     private static Font underBold   = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.UNDERLINE);
    
     int    iWidth1[]   = {20,30,55,35,35,38,38,35,30,30,35,35,35,35,35,35,35,35,35,35,35,35,35,25,25,25,25,40};
     int    iWidth0[]   = {35,25,30,30,30,30,30,30};
     Document   document;
     PdfPTable  table1,table0;
     int        iUserCode;
     PdfWriter  writer      = null;

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
          common = new Common();

          bgColor = common.bgColor;
          bgHead  = common.bgHead;
          bgUom   = common.bgUom;
          bgBody  = common.bgBody;
          fgHead  = common.fgHead;
          fgUom   = common.fgUom;
          fgBody  = common.fgBody;
          tbgBody = common.tbgBody;
          SLine   = common.Replicate("-",229+32)+"\n";
     }
     
     
     protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

     public void doPost(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
		response.setContentType("text/html");
          PrintWriter out  = response.getWriter();
		session = request.getSession(true);
                /*
		if(session.getValue("AppletTag")==null)
		{
			String SServer = control.getID("Select ProdTomHost from Server");
			response.sendRedirect(SServer + "/AProduction.SessionExpired");
			return;

		}
                */
          setCriteria(request);
          setFinalized();
          out.println("<html>");
          out.println("<head>");
          out.println("<title>OE Production -  Daily ( NEW RF 1 ) </title>");
          out.println("</head>");
          out.println("<body bgcolor='" + common.bgBody + "'>");
          //out.println(session.getValue("AppletTag"));
          out.println(common.getDailyReportHeader("OE Production -  Daily ( NEW RF 1 )",SUnit,SShift,SDate,getReportType()));
          out.println("  <center>");
          out.println("  <table border='1' cellspacing='0' width='1700' height='1'  >");
          out.println("    <tr>");
          out.println("      <td width='60' height='144' rowspan='4' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Mah N�</font></td>");
          out.println("      <td width='60' height='144' rowspan='4' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Ticket N�</font></td>");
          out.println("      <td width='60' height='144' rowspan='4' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Sider Name</font></td>");
          out.println("      <td width='60' height='144' rowspan='4' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Count &amp;");
          out.println("        Shade</font></td>");
          out.println("      <td width='60' height='144' rowspan='4' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>N� Of Rots</font></td>");
          out.println("      <td width='177' height='41' colspan='3' align='center' valign='middle' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Process");
          out.println("        Details</font></td>");
          out.println("      <td width='297' height='39' colspan='5' align='center' valign='middle' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Production");
          out.println("        Details</font></td>");
          out.println("      <td width='177' height='39' colspan='3' align='center' valign='middle' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Efficiency</font></td>");
          out.println("      <td width='295' height='39' colspan='5' valign='middle' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Utilization</font></td>");
          out.println("      <td width='118' height='38' colspan='2' align='center' valign='middle' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Miscellaneous</font></td>");
          out.println("      <td width='236' height='38' colspan='4' align='center' valign='middle' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Pneumafil");
          out.println("        Details</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='60' height='61' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Target");
          out.println("        Speed</font></td>");
          out.println("      <td width='60' height='61' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Actual");
          out.println("        Speed</font></td>");
          out.println("      <td width='57' height='61' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>TPI</font></td>");
          out.println("      <td width='120' height='25' valign='top' rowspan='2' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Effeciency</font></td>");
          out.println("      <td width='120' height='25' valign='top' rowspan='2' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>BreaksPer1000Km</font></td>");
          out.println("      <td width='177' height='25' valign='top' colspan='3' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Production</font></td>");
          out.println("      <td width='59' height='61' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Target");
          out.println("        Effy</font></td>");
          out.println("      <td width='59' height='61' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Expect");
          out.println("        Vs Actual</font></td>");
          out.println("      <td width='59' height='61' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Target");
          out.println("        Vs Actual</font></td>");
          out.println("      <td width='59' height='61' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Hours");
          out.println("        to be worked</font></td>");
          out.println("      <td width='59' height='61' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Actual");
          out.println("        hours worked</font></td>");
          out.println("      <td width='59' height='56' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Stop");
          out.println("        page</font></td>");
          out.println("      <td width='59' height='56' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Util");
          out.println("        (net)</font></td>");
          out.println("      <td width='59' height='56' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Doff</font></td>");
          out.println("      <td width='59' height='56' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>worked");
          out.println("        Rot</font></td>");
          out.println("      <td width='59' height='56' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>gms/Rot</font></td>");
          out.println("      <td width='59' height='56' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>prod.</font></td>");
          out.println("      <td width='59' height='56' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>target</font></td>");
          out.println("      <td width='59' height='56' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Varia");
          out.println("        tion</font></td>");
          out.println("      <td width='59' height='56' valign='top' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>pneu");
          out.println("        mafil</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='59' height='30' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Expec</font></td>");
          out.println("      <td width='59' height='30' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Target</font></td>");
          out.println("      <td width='59' height='30' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Actual</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='60' height='27' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>RPM</font></td>");
          out.println("      <td width='60' height='27' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>RPM</font></td>");
          out.println("      <td width='57' height='27' align='right' bgcolor='"+bgUom+"'>&nbsp;</td>");
          out.println("      <td width='61' height='27' align='right' bgcolor='"+bgUom+"'>&nbsp;</td>");
          out.println("      <td width='59' height='27' align='right' bgcolor='"+bgUom+"'>&nbsp;</td>");
          out.println("      <td width='59' height='27' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='59' height='27' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='59' height='27' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='59' height='27' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='59' height='27' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='59' height='27' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='59' height='27' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Hrs</font></td>");
          out.println("      <td width='59' height='32' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>Min</font></td>");
          out.println("      <td width='59' height='32' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='59' height='32' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='59' height='32' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>.</font></td>");
          out.println("      <td width='59' height='32' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>gms</font></td>");
          out.println("      <td width='59' height='32' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>kgs</font></td>");
          out.println("      <td width='59' height='32' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>kgs</font></td>");
          out.println("      <td width='59' height='32' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>kgs</font></td>");
          out.println("      <td width='59' height='32' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='59' height='32' align='right' bgcolor='"+bgUom+"'><font size='4' color='"+fgUom+"'></font></td>");
          out.println("    </tr>");

          setVectorData();
          calcVectors();

          try
          {
//               FW      = new FileWriter("//software/C-Prod-Print/Reports/oedaily.prn");
               FW      = new FileWriter("D:/C-Prod-Print/oedaily.prn");
               Lctr    = 70;
               Pctr    =  0;
               Head();
               Body();
               AbsPrt();
               FW.close();
               createPDFFile();
          }catch(Exception ex){}

          for(int i=0;i<VMachNo.size();i++)
          {
               out.println("    <tr>");
               out.println("      <td width='60' height='5' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VMachNo  .elementAt(i)+"</font></td>");
               out.println("      <td width='60' height='5' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VTicket  .elementAt(i)+"</font></td>");
               out.println("      <td width='60' height='5' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VName  .elementAt(i)+"</font></td>");
               out.println("      <td width='60' height='5' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VCount  .elementAt(i)+"</font></td>");
               out.println("      <td width='60' height='5' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+common.getRound((String)VNoRot  .elementAt(i),0)+"</font></td>");
               out.println("      <td width='60' height='5' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+common.getRound((String)VTSpeed  .elementAt(i),0)+"</font></td>");
               out.println("      <td width='60' height='5' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VSpeed   .elementAt(i)+"</font></td>");
               out.println("      <td width='57' height='5' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VTPI  .elementAt(i)+"</font></td>");
               out.println("      <td width='61' height='5' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VHankExp  .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VHank  .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VProdExp  .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VProdTar  .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VProd  .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VTarEffy  .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VExpAct  .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VTarAct  .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VHourRun  .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VHourMeter  .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VStopMnt  .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VUtilOA  .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VNoDoff  .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VWkdRot  .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VGps  .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VPneu  .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VPneuTar  .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VPneuVar  .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VPneuPer  .elementAt(i)+"</font></td>");
               out.println("    </tr>");
          }
          out.println("    <tr>");
          out.println("      <td width='60' height='39' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>T O T A L</font></td>");
          out.println("      <td width='60' height='39' bgcolor='"+bgUom+"'><font color='"+fgBody+"'></font></td>");
          out.println("      <td width='60' height='39' bgcolor='"+bgUom+"'><font color='"+fgBody+"'></font></td>");
          out.println("      <td width='60' height='39' bgcolor='"+bgUom+"'><font color='"+fgBody+"'></font></td>");
          out.println("      <td width='60' height='39' align='right' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>"+common.getRound(common.getSum(VNoRot  ,"S"),0)+"</font></td>");
          out.println("      <td width='60' height='39' align='right' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>"+common.getRound(common.getSum(VTSpeed  ,"S"),0)+"</font></td>");
          out.println("      <td width='60' height='39' align='right' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>"+common.getRound(common.getSum(VSpeed   ,"A"),0)+"</font></td>");
          out.println("      <td width='57' height='39' align='right' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>"+common.getSum(VTPI  ,"A")+"</font></td>");
          out.println("      <td width='61' height='39' align='right' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>"+common.getSum(VHankExp  ,"A")+"</font></td>");
          out.println("      <td width='59' height='39' align='right' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>"+common.getSum(VHank  ,"A")+"</font></td>");
          out.println("      <td width='59' height='39' align='right' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>"+common.getSum(VProdExp  ,"S")+"</font></td>");
          out.println("      <td width='59' height='39' align='right' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>"+common.getSum(VProdTar  ,"S")+"</font></td>");
          out.println("      <td width='59' height='39' align='right' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>"+common.getSum(VProd  ,"S")+"</font></td>");
          out.println("      <td width='59' height='39' align='right' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>"+common.getSum(VTarEffy  ,"A")+"</font></td>");
          out.println("      <td width='59' height='39' align='right' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>"+common.getSum(VExpAct  ,"A")+"</font></td>");
          out.println("      <td width='59' height='39' align='right' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>"+common.getSum(VTarAct  ,"A")+"</font></td>");
          out.println("      <td width='59' height='39' align='right' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>"+common.getSum(VHourRun  ,"S")+"</font></td>");
          out.println("      <td width='59' height='39' align='right' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>"+common.getSum(VHourMeter  ,"S")+"</font></td>");
          out.println("      <td width='59' height='39' align='right' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>"+common.getSum(VStopMnt  ,"S")+"</font></td>");
          out.println("      <td width='59' height='39' align='right' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>"+common.getSum(VUtilOA  ,"A")+"</font></td>");
          out.println("      <td width='59' height='39' align='right' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>"+common.getSum(VNoDoff  ,"A")+"</font></td>");
          out.println("      <td width='59' height='39' align='right' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>"+common.getSum(VWkdRot  ,"S")+"</font></td>");
          out.println("      <td width='59' height='39' align='right' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>"+common.getSum(VGps  ,"A")+"</font></td>");
          out.println("      <td width='59' height='39' align='right' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>"+common.getSum(VPneu  ,"S")+"</font></td>");
          out.println("      <td width='59' height='39' align='right' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>"+common.getSum(VPneuTar  ,"S")+"</font></td>");
          out.println("      <td width='59' height='39' align='right' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>"+common.getSum(VPneuVar  ,"S")+"</font></td>");
          out.println("      <td width='59' height='39' align='right' bgcolor='"+bgUom+"'><font color='"+fgBody+"'>"+common.getSum(VPneuPer  ,"A")+"</font></td>");
          out.println("    </tr>");

          out.println("  </table>");
          out.println("  </center>");
          out.println("</div>");
          out.println("<p>&nbsp;</p>");
          out.println("<p>&nbsp;</p>");
          out.println("<p><b><font size='5' color='#000080'>Daily Abstract</font></b></p>");
          out.println("<div align='left'>");
          out.println("  <table border='1' cellspacing='1' width='924' height='70'>");
          out.println("    <tr>");
          out.println("      <td width='116' height='70' align='center' valign='middle' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Count</font></td>");
          out.println("      <td width='116' height='70' align='center' valign='middle' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Allotted");
          out.println("        Spindles</font></td>");
          out.println("      <td width='116' height='70' align='center' valign='middle' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Worked");
          out.println("        Spindles</font></td>");
          out.println("      <td width='116' height='70' align='center' valign='middle' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Utilization");
          out.println("        %</font></td>");
          out.println("      <td width='115' height='70' align='center' valign='middle' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Target");
          out.println("        Kgs</font></td>");
          out.println("      <td width='115' height='70' align='center' valign='middle' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Actual");
          out.println("        Kgs</font></td>");
          out.println("      <td width='115' height='70' align='center' valign='middle' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Difference");
          out.println("        Kgs</font></td>");
          out.println("      <td width='115' height='70' align='center' valign='middle' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Gms/Spindle</font></td>");
          out.println("    </tr>");

          for(int i=0;i<VAbs1.size();i++)
          {
               out.println("    <tr>");
               out.println("      <td width='116' height='70' align='center' valign='middle' bgcolor='"+bgUom+"'><font size='5' color='"+fgBody+"'>"+(String)VAbs1.elementAt(i)+"</font></td>");
               out.println("      <td width='116' height='70' align='center' valign='middle' bgcolor='"+bgUom+"'><font size='5' color='"+fgBody+"'>"+common.getRound((String)VAbs2.elementAt(i),3)+"</font></td>");
               out.println("      <td width='116' height='70' align='center' valign='middle' bgcolor='"+bgUom+"'><font size='5' color='"+fgBody+"'>"+common.getRound((String)VAbs3.elementAt(i),3)+"</font></td>");
               out.println("      <td width='116' height='70' align='center' valign='middle' bgcolor='"+bgUom+"'><font size='5' color='"+fgBody+"'>"+common.getRound((String)VAbs4.elementAt(i),3)+"</font></td>");
               out.println("      <td width='115' height='70' align='center' valign='middle' bgcolor='"+bgUom+"'><font size='5' color='"+fgBody+"'>"+common.getRound((String)VAbs5.elementAt(i),3)+"</font></td>");
               out.println("      <td width='115' height='70' align='center' valign='middle' bgcolor='"+bgUom+"'><font size='5' color='"+fgBody+"'>"+common.getRound((String)VAbs6.elementAt(i),3)+"</font></td>");
               out.println("      <td width='115' height='70' align='center' valign='middle' bgcolor='"+bgUom+"'><font size='5' color='"+fgBody+"'>"+common.getRound((String)VAbs7.elementAt(i),3)+"</font></td>");
               out.println("      <td width='115' height='70' align='center' valign='middle' bgcolor='"+bgUom+"'><font size='5' color='"+fgBody+"'>"+common.getRound((String)VAbs8.elementAt(i),3)+"</font></td>");
               out.println("    </tr>");
          }
          out.println("    <tr>");
          out.println("      <td width='116' height='70' align='center' valign='middle' bgcolor='"+bgHead+"'><font size='5' color='"+fgBody+"'>T O T A L</font></td>");
          out.println("      <td width='116' height='70' align='center' valign='middle' bgcolor='"+bgHead+"'><font size='5' color='"+fgBody+"'>"+common.getSum(VAbs2,"S")+"</font></td>");
          out.println("      <td width='116' height='70' align='center' valign='middle' bgcolor='"+bgHead+"'><font size='5' color='"+fgBody+"'>"+common.getSum(VAbs3,"S")+"</font></td>");

          double dUtil = common.toDouble(common.getSum(VAbs3,"S"))*100/common.toDouble(common.getSum(VAbs2,"S"));
          out.println("      <td width='116' height='70' align='center' valign='middle' bgcolor='"+bgHead+"'><font size='5' color='"+fgBody+"'>"+common.getRound(dUtil,3)+"</font></td>");
          out.println("      <td width='115' height='70' align='center' valign='middle' bgcolor='"+bgHead+"'><font size='5' color='"+fgBody+"'>"+common.getSum(VAbs5,"S")+"</font></td>");
          out.println("      <td width='115' height='70' align='center' valign='middle' bgcolor='"+bgHead+"'><font size='5' color='"+fgBody+"'>"+common.getSum(VAbs6,"S")+"</font></td>");
          out.println("      <td width='115' height='70' align='center' valign='middle' bgcolor='"+bgHead+"'><font size='5' color='"+fgBody+"'>"+common.getSum(VAbs7,"S")+"</font></td>");
          out.println("      <td width='115' height='70' align='center' valign='middle' bgcolor='"+bgHead+"'><font size='5' color='"+fgBody+"'>"+common.getSum(VAbs8,"A")+"</font></td>");
          out.println("    </tr>");
          out.println("  </table>");
          out.println(" <table><tr><td> <p> Average Count :</td><td>"+getAverageCount()+"</td></td></table>");
          out.println("</div>");
          out.println("</body>");
          out.println("</html>");

          out.close();
     }
     public void setVectorData()
     {
		 String SQ1="",SQ2="",SQ3="";
           VDate  = new Vector();
           VUnit  = new Vector();
           VShift = new Vector();
           VMachNo= new Vector();
           VTicket= new Vector();
           VName  = new Vector();
           VCount = new Vector();
           VCountVal= new Vector();

           VNoRot      = new Vector();
           VTSpeed      = new Vector();
           VSpeed       = new Vector();
           VTPI         = new Vector();
           VHankExp     = new Vector();
           VHank        = new Vector();
           VProdExp     = new Vector();
           VProdTar     = new Vector();
           VProd        = new Vector();
           VTarEffy     = new Vector();
           VExpAct      = new Vector();
           VTarAct      = new Vector();
           VHourRun     = new Vector();
           VHourMeter   = new Vector();
           VStopMnt     = new Vector();
           VUtilOA      = new Vector();
           VNoDoff      = new Vector();
           VWkdRot     = new Vector();
           VGps         = new Vector();
           VPneu        = new Vector();
           VPneuTar     = new Vector();
           VPneuVar     = new Vector();
           VPneuPer     = new Vector();
           VHourAllot   = new Vector();

           VAbs1 = new Vector();
           VAbs2 = new Vector();
           VAbs3 = new Vector();
           VAbs4 = new Vector();
           VAbs5 = new Vector();
           VAbs6 = new Vector();
           VAbs7 = new Vector();
           VAbs8 = new Vector();

		 if(bFinalized)
		 {
			SQ1 = getQString();
			SQ2 = getSupCd();
			SQ3 = getQSAbs();
		 }	
		 else
		 {	
		     SQ1 = getQStringTemp();
			SQ2 = getSupCdTemp();
			SQ3 = getQSAbsTemp();
		 }
             try
             {
                     Connection con = createConnection();
                     Statement stat = con.createStatement();
                     ResultSet res  = stat.executeQuery(SQ1);
                     while(res.next())
                     {
                          VDate  .addElement(res.getString(1)); 
                          VUnit  .addElement(res.getString(2));
                          VShift .addElement(res.getString(3));
                          VMachNo.addElement(res.getString(4));
                          VCount .addElement(res.getString(5));
                          VTicket.addElement(res.getString(6));
                          VName  .addElement(getTicket(res.getString(7)));

                          VNoRot      .addElement(res.getString(8));
                          VTSpeed      .addElement(res.getString(9));
                          VSpeed       .addElement(res.getString(10));
                          VTPI         .addElement(res.getString(11));
                          VHankExp     .addElement(res.getString(12));
                          VHank        .addElement(res.getString(13));
                          VProdExp     .addElement(res.getString(14));
                          VProdTar     .addElement(res.getString(15));
                          VProd        .addElement(res.getString(16));
                          VTarEffy     .addElement(res.getString(17));
                          VHourRun     .addElement(res.getString(18));
                          VHourMeter   .addElement(res.getString(19));
                          VStopMnt     .addElement(res.getString(20));
                          VUtilOA      .addElement(res.getString(21));
                          VNoDoff      .addElement(res.getString(22));
                          VWkdRot     .addElement(res.getString(23));
                          VGps         .addElement(res.getString(24));
                          VPneu        .addElement(res.getString(25));
                          VPneuTar     .addElement(res.getString(26));
                          VHourAllot   .addElement(res.getString(27));
                          VCountVal    .addElement(res.getString(28));
                     }
                     res.close(); 

                     ResultSet res1  = stat.executeQuery(SQ3);
                     while(res1.next())
                     {
                          VAbs1  .addElement(res1.getString(1));
                          VAbs2  .addElement(res1.getString(2));
                          VAbs3  .addElement(res1.getString(3));
                          VAbs4  .addElement(res1.getString(4)); 
                          VAbs5  .addElement(res1.getString(5));
                          VAbs6  .addElement(res1.getString(6));
                          VAbs7  .addElement(res1.getString(7));
                          VAbs8  .addElement(res1.getString(8)); 
                     }
                     res1.close();

                     ResultSet res3 = stat.executeQuery(SQ2);
                     while(res3.next())
                         SSupName = res3.getString(1);
                     res3.close();
                     stat.close();
                     con.close();
             }
             catch(Exception ex)
             {
                     System.out.println("From Spin.java : 285 "+ex);
             }
     }
     public void calcVectors()
     {
          for(int i=0;i<VMachNo.size();i++)
          {
                double dProd    = common.toDouble((String)VProd.elementAt(i));
                double dProdTar = common.toDouble((String)VProdTar.elementAt(i));
                double dProdExp = common.toDouble((String)VProdExp.elementAt(i));
                double dHal     = common.toDouble((String)VHourAllot.elementAt(i));
                double dPneu    = common.toDouble((String)VPneu.elementAt(i));
                double dPneuTar = common.toDouble((String)VPneuTar.elementAt(i));
                double dHrAllot = common.toDouble((String)VHourAllot.elementAt(i));
                double dHrRun   = common.toDouble((String)VHourRun.elementAt(i));
                double dHrMeter = common.toDouble((String)VHourMeter.elementAt(i));

                VExpAct.addElement(common.getRound((dProd/dProdExp*100),3));
                VTarAct.addElement(common.getRound((dProd/dProdTar*100),3));
                VPneuVar.addElement(common.getRound((dPneu-dPneuTar),3));
                VPneuPer.addElement(common.getRound((dPneu*100/(dProd+dPneu)),3));
                VNoDoff.setElementAt(common.getRound((dHrRun-dHrMeter)*100/dHrAllot,3),i);
                VTarEffy.setElementAt(common.getRound((String)VTarEffy.elementAt(i),3),i);
                VUtilOA.setElementAt(common.getRound(dHrRun*100/dHrAllot,1),i);
          }
          for(int i=0;i<VAbs1.size();i++)
          {

               double dAvgGps = (common.toDouble((String)VAbs6.elementAt(i))*1000.0/common.toDouble((String)VAbs3.elementAt(i)));

               dAvgGps  = (dAvgGps/getShiftHours())*8;

               VAbs8.setElementAt(common.getRound(dAvgGps,3),i);        //(common.toDouble((String)VAbs6.elementAt(i))*1000.0/common.toDouble((String)VAbs3.elementAt(i)))
          }
     }
     private String getAverageCount()
     {
         double dAvgCount=0;
         double dTotRotValue=0;
         for(int i=0; i<VMachNo.size(); i++)
         {
             dAvgCount=dAvgCount+(common.toDouble((String)VCountVal.elementAt(i))*common.toDouble((String)VNoRot.elementAt(i)));         
             dTotRotValue +=common.toDouble((String)VNoRot.elementAt(i));
         }
         return common.getRound(dAvgCount/dTotRotValue,2);
     }
     // modify on 23.10.2009

     private double getShiftHours()
     {

          if(common.toInt(SShift)==1)
               return 8;
          if(common.toInt(SShift)==2)
               return 8;

          return 8;
     }
     public String getQString()
     {
             String QString = " SELECT  OETran.sp_date,unit.unitname,OETran.shift_code, machine.mach_name, " +
                        " count.count_name, OETran.ticket, OETran.ticket, OETran.noofRot,  " +
                        " OETran.speed_tar, OETran.speed, OETran.tpi, OETran.Effeciency, " +
                        " OETran.BreaksPer100Km,  OETran.prod_exp, OETran.prod_tar, OETran.prod, " +
                              " OETran.count_effy, OETran.hoursrun,OETran.hourmeter,OETran.stop_mt, " +
                              " OETran.utiloa,OETran.noofdoff,OETran.Rotwkd,OETran.gms_Rot,  " +
                              " OETran.pneumafil,OETran.pneu_tar, OETran.hours,Count.Count as countVal   " +
                              " FROM (((OETran INNER JOIN unit ON OETran.unit_code = unit.unitcode) " +
                              " INNER JOIN machine ON OETran.mach_code = machine.mach_code)  " +
                              " INNER JOIN count ON OETran.count_Code = count.count_code)  " +
                              " Where OETran.sp_date='"+SDate+"' and OETran.Shift_Code = "+SShift + 
                              " and OETran.Unit_Code = " + SUnit + " ORDER by to_number(machine.mach_name)" ; 
		   return QString;
     }

     public String getQSAbs()
     {
             String QString = " SELECT count.count_name, " + 
                              " Sum(OETran.noofRot) AS Rot_alloted, " +       //Sum((OETran.noofRot*OETran.hours/8)-(OETran.idleRot*OETran.idletime/(8*60))) AS Rot_alloted
                              " Sum(OETran.Rotwkd) AS Rot_wkd, Avg(OETran.utiloa) AS uti_loa, " + 
                              " Sum(OETran.prod_tar) AS prod_tar, Sum(OETran.prod) AS prod, " + 
                              " Sum(OETran.prod-OETran.prod_tar) AS diff_kgs, Avg(OETran.gms_Rot) AS gms_Rot" + 
                              " FROM OETran INNER JOIN count ON OETran.count_code=count.count_code" + 
                              " GROUP BY count.count_name,OETran.sp_date,OETran.shift_code,OETran.unit_code" + 
                              " Having OETran.sp_date='"+SDate+"' and OETran.Shift_Code = "+SShift + 
                              " and OETran.Unit_Code = " + SUnit ;

		   return QString;
     }

     public String getSupCd()
     {
          String QS = " SELECT supervisor.supervisor FROM OETran " + 
                      " INNER JOIN supervisor ON OETran.supcode = supervisor.supcode " +
                      " GROUP BY supervisor.supervisor, OETran.Unit_Code, " +
                      " OETran.sp_date, OETran.shift_code " +
                  " HAVING (((OETran.Unit_Code)="+SUnit+") AND "+
                  " ((OETran.sp_date)='"+SDate+"') AND "+
                  " ((OETran.shift_code)="+SShift+")) ";
          return QS;
     }

     public String getQStringTemp()
     {
             String QString = " SELECT  OETran_Temp.sp_date,unit.unitname,OETran_Temp.shift_code, machine.mach_name, " +
                              " count.count_name, OETran_Temp.ticket, OETran_Temp.ticket, OETran_Temp.noofRot,  " +
                              " OETran_Temp.speed_tar, OETran_Temp.speed, OETran_Temp.tpi, OETran_Temp.Effeciency, " +
                              " OETran_Temp.BreaksPer100Km,  OETran_Temp.prod_exp, OETran_Temp.prod_tar, OETran_Temp.prod, " +
                              " OETran_Temp.count_effy, OETran_Temp.hoursrun,OETran_Temp.hourmeter,OETran_Temp.stop_mt, " +
                              " OETran_Temp.utiloa,OETran_Temp.noofdoff,OETran_Temp.Rotwkd,OETran_Temp.gms_Rot,  " +
                              " OETran_Temp.pneumafil,OETran_Temp.pneu_tar, OETran_Temp.hours,Count.Count as CountVal   " +
                              " FROM (((OETran_Temp INNER JOIN unit ON OETran_Temp.unit_code = unit.unitcode) " +
                              " INNER JOIN machine ON OETran_Temp.mach_code = machine.mach_code)  " +
                              " INNER JOIN count ON OETran_Temp.count_Code = count.count_code)  " +
                        " Where OETran_Temp.sp_date='"+SDate+"' and OETran_Temp.Shift_Code = "+SShift + 
                        " and OETran_Temp.Unit_Code = " + SUnit + " ORDER by to_number(machine.mach_name)" ; 
		   return QString;
     }

     public String getQSAbsTemp()
     {
             String QString = " SELECT count.count_name, " + 
                              " Sum(OETran_Temp.noofRot) AS Rot_alloted, " +       //Sum((OETran.noofRot*OETran.hours/8)-(OETran.idleRot*OETran.idletime/(8*60))) AS Rot_alloted
                              " Sum(OETran_Temp.Rotwkd) AS Rot_wkd, Avg(OETran_Temp.utiloa) AS uti_loa, " + 
                              " Sum(OETran_Temp.prod_tar) AS prod_tar, Sum(OETran_Temp.prod) AS prod, " + 
                              " Sum(OETran_Temp.prod-OETran_Temp.prod_tar) AS diff_kgs, Avg(OETran_Temp.gms_Rot) AS gms_Rot" + 
                              " FROM OETran_Temp INNER JOIN count ON OETran_Temp.count_code=count.count_code" + 
                              " GROUP BY count.count_name,OETran_Temp.sp_date,OETran_Temp.shift_code,OETran_Temp.unit_code" + 
                              " Having OETran_Temp.sp_date='"+SDate+"' and OETran_Temp.Shift_Code = "+SShift + 
                              " and OETran_Temp.Unit_Code = " + SUnit ;
 	        //System.out.println("\nAbs->" + QString);
		   return QString;
     }

	public String getSupCdTemp()
     {
          String QS = " SELECT supervisor.supervisor FROM OETran_Temp " + 
                      " INNER JOIN supervisor ON OETran_Temp.supcode = supervisor.supcode " +
                      " GROUP BY supervisor.supervisor, OETran_Temp.Unit_Code, " +
                      " OETran_Temp.sp_date, OETran_Temp.shift_code " +
                  " HAVING (((OETran_Temp.Unit_Code)="+SUnit+") AND "+
                  " ((OETran_Temp.sp_date)='"+SDate+"') AND "+
                  " ((OETran_Temp.shift_code)="+SShift+")) ";

          return QS;
     }


     private String getTicket(String SEmpCode)
     { 
		   String SRet="UnKnown";
		   if(SEmpCode.equals("0"))
			   return SRet;
		   try
		   {			   
			   String SQry = " select empname from SchemeApprentice where empcode="+SEmpCode+" union " +
							 " select empname from ContractApprentice where empcode="+SEmpCode;
			   Class.forName("oracle.jdbc.OracleDriver");
			   Connection conn = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
			   Statement stat  = conn.createStatement();
			   ResultSet res   = stat.executeQuery(SQry);
			   if(res.next())
				   SRet            = res.getString(1);
		   }catch(Exception ee)
		   {
			   System.out.println("getTicket->" + ee);
			   return SRet;
		   }
		   return SRet;
      }

     private String getReportType()
	{
		if(bFinalized)
			return "Finalized";
		else
			return "UnFinalized";
	}

	private void setFinalized()
	{
          String SQry = " select count(*) from OETran " + 
				    " Where sp_date='"+SDate+"' and Shift_Code = "+SShift+" and  " +
				    " Unit_Code = "+SUnit;
		try
		{
			Connection con = createConnection();
			Statement   st = con.createStatement();
			ResultSet   rs = st.executeQuery(SQry);
			rs.next();
			if(rs.getInt(1) > 0)
				bFinalized = true;
			else
				bFinalized = false;
		}catch(Exception ee){System.out.println("setFinalized->" + ee);}	
	}



	private Connection createConnection()
	{
		Connection conn;
		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
			conn  = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
		}catch(Exception ee)
		{
			return null;
		}
		return conn;
	}


    public void Head()
    {
          if(Lctr < 60)
             return;
          if(Pctr > 0)
          {
             try
             {
               FW.write(SLine+"\n");
             }catch(Exception ex){}
          }
          Pctr++;

          String Str1    = common.Pad("Company    : AMARJOTHI SPINNING MILLS LIMITED",80);
          String Str2    = common.Pad("Document   : OE Daily Report As On "+common.parseDate(SDate),80);
          String Str3    = common.Pad("Unit       : "+(SUnit.equals("1")?"A":(SUnit.equals("2")?"B":(SUnit.equals("3")?"C":"S"))),70);
          String Str4    = common.Pad("Shift      : "+(SShift.equals("1")?"Day":(SShift.equals("2")?"Half Night":"Full Night")),70);
          String Str5    = common.Pad("P.O/J.P.O  : "+SSupName,70);
          VHead = Spin1Head();
          try
          {
               FW.write(Str1+"\n");
               FW.write(Str2+"\n");
               FW.write(Str3+"\n");
               FW.write(Str4+"\n");
               FW.write(Str5+"\n\n");
               for(int i=0;i<VHead.size();i++)
               {
                    FW.write((String)VHead.elementAt(i));
               }
          }catch(Exception ex){}
          Lctr = 4+VHead.size();
     }
     public void Body()
     {
         String Strl = "";
         for(int i=0;i<VMachNo.size();i++)
         {
                Head();
                Strl = "";
                Strl = Strl+common.Pad((String)VMachNo .elementAt(i),4)+common.Space(2);
                Strl = Strl+common.Pad((String)VTicket .elementAt(i),6)+common.Space(2);
                Strl = Strl+common.Pad((String)VName   .elementAt(i),10)+common.Space(2);
                Strl = Strl+common.Pad((String)VCount  .elementAt(i),10)+common.Space(2);
                Strl = Strl+common.Rad((String)VNoRot .elementAt(i),5)+common.Space(2);
                Strl = Strl+common.Rad((String)VTSpeed .elementAt(i),5)+common.Space(2);
                Strl = Strl+common.Rad((String)VSpeed  .elementAt(i),5)+common.Space(2);
                Strl = Strl+common.Rad((String)VTPI    .elementAt(i),5)+common.Space(2);
                Strl = Strl+common.Rad((String)VHankExp.elementAt(i),9)+common.Space(2);
                Strl = Strl+common.Rad((String)VHank   .elementAt(i),9)+common.Space(2);
                Strl = Strl+common.Rad((String)VProdExp.elementAt(i),9)+common.Space(2);
                Strl = Strl+common.Rad((String)VProdTar.elementAt(i),9)+common.Space(2);
                Strl = Strl+common.Rad((String)VProd   .elementAt(i),9)+common.Space(2);
                Strl = Strl+common.Rad((String)VTarEffy.elementAt(i),5)+common.Space(2);
                Strl = Strl+common.Rad((String)VTarAct .elementAt(i),5)+common.Space(2);
                Strl = Strl+common.Rad((String)VExpAct .elementAt(i),5)+common.Space(2);
                Strl = Strl+common.Rad((String)VHourRun.elementAt(i),5)+common.Space(2);
                Strl = Strl+common.Rad((String)VHourMeter.elementAt(i),5)+common.Space(2);
                Strl = Strl+common.Rad((String)VStopMnt.elementAt(i),5)+common.Space(2);
                Strl = Strl+common.Rad((String)VUtilOA .elementAt(i),5)+common.Space(2);
                Strl = Strl+common.Rad((String)VNoDoff .elementAt(i),5)+common.Space(2);
                Strl = Strl+common.Rad((String)VWkdRot.elementAt(i),7)+common.Space(2);
                Strl = Strl+common.Rad((String)VGps    .elementAt(i),7)+common.Space(2);
                Strl = Strl+common.Rad((String)VPneu   .elementAt(i),7)+common.Space(2);
                Strl = Strl+common.Rad((String)VPneuTar.elementAt(i),7)+common.Space(2);
                Strl = Strl+common.Rad((String)VPneuVar.elementAt(i),7)+common.Space(2);
                Strl = Strl+common.Rad((String)VPneuPer.elementAt(i),7);

                try
                {
                     FW.write(Strl+"\n\n");
                     Lctr=Lctr+2;
                     Head();
                }catch(Exception ex){}
         }
         try
         {
               FW.write(SLine);               
         }catch(Exception ex){}

           Strl = "";
           Strl = Strl+common.Rad(" ",4)+common.Space(2);
           Strl = Strl+common.Rad(" ",6)+common.Space(2);
           Strl = Strl+common.Rad(" ",10)+common.Space(2);
           Strl = Strl+common.Rad(" ",10)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VNoRot ,"S"),5)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VTSpeed ,"A"),5)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VSpeed  ,"A"),5)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VTPI    ,"A"),5)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VHankExp,"A"),9)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VHank   ,"A"),9)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VProdExp,"S"),9)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VProdTar,"S"),9)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VProd   ,"S"),9)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VTarEffy,"A"),5)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VExpAct ,"A"),5)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VTarAct ,"A"),5)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VHourRun,"S"),5)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VHourMeter,"S"),5)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VStopMnt,"S"),5)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VUtilOA ,"A"),5)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VNoDoff ,"A"),5)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VWkdRot,"S"),7)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VGps    ,"A"),7)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VPneu   ,"S"),7)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VPneuTar,"S"),7)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VPneuVar,"S"),7)+common.Space(2);
           Strl = Strl+common.Rad(common.getSum(VPneuPer,"A"),7);

           try
           {
               FW.write(Strl+"\n");
               FW.write(SLine);               
           }catch(Exception ex){}
     }

     public Vector Spin1Head()
     {
          Vector VHead = new Vector();

          String Str2 = common.Space(4)+common.Space(2)+common.Space(6)+common.Space(2)+common.Space(10)+common.Space(2)+
                        common.Space(10)+common.Space(2)+common.Space(5)+common.Space(2)+
                        common.Cad("Process Details",19)+common.Space(2)+
                        common.Cad("Production",53)+common.Space(2)+
                        common.Cad("Efficiency",19)+common.Space(2)+
                        common.Cad("Utilization",33)+common.Space(2)+
                        common.Cad("Miscellaneous",16)+common.Space(2)+
                        common.Cad("Pneumafil",34)+"\n";
                        

          String Str3 = common.Space(4)+common.Space(2)+common.Space(6)+common.Space(2)+common.Space(10)+common.Space(2)+
                        common.Space(10)+common.Space(2)+common.Space(5)+common.Space(2)+
                        common.Replicate("-",19)+common.Space(2)+
                        common.Replicate("-",53)+common.Space(2)+
                        common.Replicate("-",19)+common.Space(2)+
                        common.Replicate("-",33)+common.Space(2)+
                        common.Replicate("-",16)+common.Space(2)+
                        common.Replicate("-",34)+common.Space(2)+"\n";

          String Str4 = common.Pad("No",4)+common.Space(2)+common.Pad("Ticket",6)+common.Space(2)+
                        common.Pad("Sider",10)+common.Space(2)+common.Pad("Count",10)+common.Space(2)+
                        common.Rad("No Of",5)+common.Space(2)+
                        common.Rad("Target",5)+common.Space(2)+common.Rad("Actual",5)+common.Space(2)+
                        common.Rad("TPI",5)+common.Space(2)+
                        common.Rad("Effeciency",9)+common.Space(2)+common.Rad("BreaksPer",9)+common.Space(2)+
                        common.Rad("Expected",9)+common.Space(2)+common.Rad("Target",9)+common.Space(2)+common.Rad("Actual",9)+common.Space(2)+
                        common.Rad("Tgt",5)+common.Space(2)+common.Rad("Tgt Vs",5)+common.Space(2)+
                        common.Rad("Exp Vs",5)+common.Space(2)+
                        common.Rad("Hour",5)+common.Space(2)+common.Rad("Actual",5)+common.Space(2)+
                        common.Rad("Stop",5)+common.Space(2)+common.Rad("Net",5)+common.Space(2)+
                        common.Rad("Doff",5)+common.Space(2)+
                        common.Rad("Wkd",7)+common.Space(2)+common.Rad("Grams/",7)+common.Space(2)+
                        common.Rad("Prod",7)+common.Space(2)+common.Rad("Target",7)+common.Space(2)+
                        common.Rad("Varia",7)+common.Space(2)+common.Rad("Pneu",7)+common.Space(2)+
                        common.Cad("Remarks",30)+"\n";

          String Str5 = common.Pad(" ",4)+common.Space(2)+common.Pad(" ",6)+common.Space(2)+
                        common.Pad("Name",10)+common.Space(2)+common.Pad(" ",10)+common.Space(2)+
                        common.Rad("Rot",5)+common.Space(2)+
                        common.Rad("Speed",5)+common.Space(2)+common.Rad("Speed",5)+common.Space(2)+
                        common.Rad(" ",5)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+common.Rad("1000Km",9)+common.Space(2)+
                        common.Rad("Prod",9)+common.Space(2)+common.Rad("Prod",9)+common.Space(2)+common.Rad("Prod",9)+common.Space(2)+
                        common.Rad("Effy",5)+common.Space(2)+common.Rad("Actu",5)+common.Space(2)+
                        common.Rad("Actu",5)+common.Space(2)+
                        common.Rad("To Wrk",5)+common.Space(2)+common.Rad("Hour",5)+common.Space(2)+
                        common.Rad("page",5)+common.Space(2)+common.Rad(" ",5)+common.Space(2)+
                        common.Rad(" ",5)+common.Space(2)+
                        common.Rad("Rot",7)+common.Space(2)+common.Rad("Rot",7)+common.Space(2)+
                        common.Rad(" ",7)+common.Space(2)+common.Rad("Prod",7)+common.Space(2)+
                        common.Rad("tion",7)+common.Space(2)+common.Rad(" ",7)+"\n";

          String Str6 = common.Space(4)+common.Space(2)+common.Space(6)+common.Space(2)+
                        common.Space(10)+common.Space(2)+common.Space(10)+common.Space(2)+
                        common.Rad("No",5)+common.Space(2)+
                        common.Rad("rpm",5)+common.Space(2)+common.Rad("rpm",5)+common.Space(2)+
                        common.Space(5)+common.Space(2)+
                        common.Space(9)+common.Space(2)+common.Space(9)+common.Space(2)+
                        common.Rad("Kgs",9)+common.Space(2)+common.Rad("Kgs",9)+common.Space(2)+common.Rad("Kgs",9)+common.Space(2)+
                        common.Rad("%",5)+common.Space(2)+common.Rad("%",5)+common.Space(2)+
                        common.Rad("%",5)+common.Space(2)+
                        common.Rad("Hr",5)+common.Space(2)+common.Rad("Hr",5)+common.Space(2)+
                        common.Rad("min",5)+common.Space(2)+common.Rad("%",5)+common.Space(2)+
                        common.Rad("%",5)+common.Space(2)+
                        common.Rad("No",7)+common.Space(2)+common.Rad("gms",7)+common.Space(2)+
                        common.Rad("Kgs",7)+common.Space(2)+common.Rad("Kgs",7)+common.Space(2)+
                        common.Rad("Kgs",7)+common.Space(2)+common.Rad("%",7)+"\n";

          VHead.addElement(SLine);
          VHead.addElement(Str2);
          VHead.addElement(Str3);
          VHead.addElement(Str4);
          VHead.addElement(Str5);
          VHead.addElement(Str3);
          VHead.addElement(Str6);
          VHead.addElement(SLine);

          return VHead;
     }

     public void AbsPrt()
     {
          String Strl;
          String Str1 = "\n\n\nCountwise Abstract\n\n";

          String Str2 = common.Pad("Count",10)+common.Space(2)+
                        common.Rad("Allotted",8)+common.Space(2)+
                        common.Rad("Worked",8)+common.Space(2)+
                        common.Rad("Util",6)+common.Space(2)+
                        common.Rad("Target",9)+common.Space(2)+
                        common.Rad("Actual",9)+common.Space(2)+
                        common.Rad("Diffe",9)+common.Space(2)+
                        common.Rad("Gms/",7)+"\n";

          String Str3 = common.Pad(" ",10)+common.Space(2)+
                        common.Rad("Spindles",8)+common.Space(2)+
                        common.Rad("Spindles",8)+common.Space(2)+
                        common.Rad("%",6)+common.Space(2)+
                        common.Rad("Kgs",9)+common.Space(2)+
                        common.Rad("Kgs",9)+common.Space(2)+
                        common.Rad("Kgs",9)+common.Space(2)+
                        common.Rad("Rot",7)+"\n";
     
          try
          {
               FW.write(Str1);
               FW.write(common.Replicate("-",80)+"\n");
               FW.write(Str2);
               FW.write(Str3);
               FW.write(common.Replicate("-",80)+"\n");
          }catch(Exception ex){}

          for(int i=0;i<VAbs1.size();i++)
          {
                 Strl = common.Pad((String)VAbs1.elementAt(i),10)+common.Space(2)+
                        common.Rad(common.getRound((String)VAbs2.elementAt(i),3),8)+common.Space(2)+
                        common.Rad(common.getRound((String)VAbs3.elementAt(i),3),8)+common.Space(2)+
                        common.Rad(common.getRound((String)VAbs4.elementAt(i),3),6)+common.Space(2)+
                        common.Rad(common.getRound((String)VAbs5.elementAt(i),3),9)+common.Space(2)+
                        common.Rad(common.getRound((String)VAbs6.elementAt(i),3),9)+common.Space(2)+
                        common.Rad(common.getRound((String)VAbs7.elementAt(i),3),9)+common.Space(2)+
                        common.Rad(common.getRound((String)VAbs8.elementAt(i),3),7)+"\n";

                try
                {
                    FW.write(Strl);
                }catch(Exception ex){}
          }

          double dUtil = common.toDouble(common.getSum(VAbs3,"S"))*100/common.toDouble(common.getSum(VAbs2,"S"));
            Strl = common.Pad("Total",10)+common.Space(2)+
                   common.Rad(common.getSum(VAbs2,"S"),8)+common.Space(2)+
                   common.Rad(common.getSum(VAbs3,"S"),8)+common.Space(2)+
                   common.Rad(common.getRound(dUtil,3),6)+common.Space(2)+
                   common.Rad(common.getSum(VAbs5,"S"),9)+common.Space(2)+
                   common.Rad(common.getSum(VAbs6,"S"),9)+common.Space(2)+
                   common.Rad(common.getSum(VAbs7,"S"),9)+common.Space(2)+
                   common.Rad(common.getSum(VAbs8,"A"),7)+"\n";
           try
           {
               FW.write(common.Replicate("-",80)+"\n");
               FW.write(Strl);
               FW.write(common.Replicate("-",80)+"\n");
               FW.write("Avg Count : "+getAverageCount()+"\n");
               FW.write("\n\n\n\nP.O          S.P.O.           S.M.             D.M.");
               FW.write("\n");
           }catch(Exception ex){}
     }

	private void setCriteria(HttpServletRequest request)
	{
		SDate            = request.getParameter("Date");
        SDate            = (String)common.pureDate(SDate);
        SShift           = request.getParameter("Shift");
		SUnit            = request.getParameter("Unit");

		session.putValue("Unit",SUnit);
		session.putValue("Shift",SShift);
		session.putValue("Date",SDate);

	}
        
 //PDF
        
    public void createPDFFile(){
     try{
        SFile       =   "OEDailyReportPDF.pdf";
        SFile       =   common.getPrintPath()+SFile;    
        document    =   new Document(PageSize.LEGAL.rotate());
        writer      =  PdfWriter.getInstance(document, new FileOutputStream(SFile));
        TableHeader event   = new TableHeader();
        writer      .   setPageEvent(event);
        document    .   open();
        
        table1      =   new PdfPTable(28);
        table1      .   setWidths(iWidth1);
        table1      .   setSpacingAfter(2);
        table1      .   setWidthPercentage(100);
//        table1      .   setHeaderRows(9);
        
        table0      =   new PdfPTable(8);
        table0      .   setWidths(iWidth0);
        table0      .   setSpacingAfter(2);
        table0      .   setWidthPercentage(50);
        table0      .   setHorizontalAlignment(Element.ALIGN_LEFT);
        
            setPDFHead();
            setPDFBody();
            AbsPDF();
            document.add(table1);
            document.add(table0);
            document.close();
       }
        catch (Exception e){
           e.printStackTrace();
            System.out.println("Error-->"+e);
        }
        finally{
            try{
                document.close();
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }
    }
    
    public void setPDFHead(){
        try
        {
            String sHead1       = "Company    : AMARJOTHI SPINNING MILLS LIMITED";
            String sHead2       = "Document   : OE Daily Report  "+common.parseDate(SDate);
            String sHead3       = "Unit             : "+(SUnit.equals("1")?"A":(SUnit.equals("2")?"B":(SUnit.equals("3")?"C":"S")));
            String sHead4       = "Shift            : "+(SShift.equals("1")?"Day":(SShift.equals("2")?"Half Night":"Full Night"));
            String sHead5       = "P.O/J.P.O   : "+SSupName;
            
            AddCellIntoTable(sHead1, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,28,20f,0,0,0,0, bigBold );
            AddCellIntoTable(sHead2, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,28,20f,0,0,0,0, bigBold );
            AddCellIntoTable(sHead3, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,28,20f,0,0,0,0, bigBold );
            AddCellIntoTable(sHead4, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,28,20f,0,0,0,0, bigBold );
            AddCellIntoTable(sHead5, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,28,20f,0,0,0,0, bigBold );
            
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,28,10f,0,0,0,0, tinyBold );
            
            AddHeadCellIntoTable("Mach No", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,25f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Ticket No", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,25f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Sider Name", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,25f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Count / Shade", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,25f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Noof Rot", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,25f,8,3,5,4, tinyBold );
            
            AddCellIntoTable("Process Details", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,25f,8,3,5,4, tinyBold );
            AddCellIntoTable("Production Details", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,5,25f,8,3,5,4, tinyBold);
            AddCellIntoTable("Efficiency", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,25f,8,3,5,4, tinyBold );
            AddCellIntoTable("Utilization", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,5,25f,8,3,5,4, tinyBold );
            AddCellIntoTable("Miscellaneous", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,25f,8,3,5,4, tinyBold );
            AddCellIntoTable("Pneumafil Details", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,4,25f,8,3,5,4, tinyBold );
            
//            AddHeadCellIntoTable("Actual KW", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,25f,8,3,5,4, tinyBold );
//            AddHeadCellIntoTable("UKG", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,25f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Remarks", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,25f,8,3,5,4, tinyBold );
            
            //SECOND
            
            AddHeadCellIntoTable("Target Speed", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,25f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Actual Speed", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,25f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("TPI", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,25f,8,3,5,4, tinyBold );
            
            
            AddHeadCellIntoTable("Effeciency", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,25f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("BreaksPer  1000Km", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,25f,8,3,5,4, tinyBold );
            
            AddCellIntoTable("Production Details", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,25f,8,3,5,4, tinyBold );
          
            AddHeadCellIntoTable("Target Effy", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,25f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Expect    Vs      Actual", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,25f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Target    Vs      Actual", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,25f,8,3,5,4, tinyBold );
            
            AddHeadCellIntoTable("Hours tobe worked", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,25f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Actual Hours Worked", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,25f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Stop Pages", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,25f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Util             (net)", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,25f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Doff", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,25f,8,3,5,4, tinyBold );
            
            AddHeadCellIntoTable("Worked Rot", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,25f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Gms/Rot", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,25f,8,3,5,4, tinyBold );
            
            AddHeadCellIntoTable("Prod.", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,25f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Target", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,25f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Variation", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,25f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Pneumafil", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,25f,8,3,5,4, tinyBold );
            
            // THIRD
            
            AddCellIntoTable("Expected", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,25f,8,3,5,4, tinyBold );
            AddCellIntoTable("Target", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,25f,8,3,5,4, tinyBold );
            AddCellIntoTable("Actual", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,25f,8,3,5,4, tinyBold );
         
            //4TH
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("rpm ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("rpm ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            
            for(int i=0;i<=2;i++){
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            }
          
            AddCellIntoTable("Kgs ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("Kgs ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("Kgs ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            
            AddCellIntoTable("% ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("% ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("% ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            
            AddCellIntoTable("Hrs ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("Min", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
           
            AddCellIntoTable("% ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("% ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            
            AddCellIntoTable("Gms ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("Kgs", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("Kgs ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("Kgs ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("% ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            
//            for(int i=0;i<=2;i++){
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
//            }
                   
         }
        catch(Exception Ex){
                Ex.printStackTrace();
                System.out.println("Error Head1-->"+Ex);
        }
    }
    
    public void setPDFBody(){
      try{
          
         for(int i=0;i<VMachNo.size();i++){
             
          AddCellIntoTable(common.parseNull((String)VMachNo.elementAt(i)), table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VTicket.elementAt(i)), table1, Element.ALIGN_LEFT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VName.elementAt(i)), table1, Element.ALIGN_LEFT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VCount.elementAt(i)), table1, Element.ALIGN_LEFT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VNoRot.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VTSpeed.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VSpeed.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VTPI.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    

          AddCellIntoTable(common.parseNull((String)VHankExp.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VHank.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VProdExp.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
                    
          AddCellIntoTable(common.parseNull((String)VProdTar.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VProd.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VTarEffy.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VTarAct.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VExpAct.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VHourRun.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VHourMeter.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VStopMnt.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VUtilOA.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VNoDoff.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VWkdRot.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VGps.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                 
          AddCellIntoTable(common.parseNull((String)VPneu.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VPneuTar.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VPneuVar.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VPneuPer.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
         }
          
            AddCellIntoTable("Total ", table1, Element.ALIGN_LEFT, Element.ALIGN_TOP,4,20f,5,3,8,4, smallBold );                    
            AddCellIntoTable(common.getSum(VNoRot,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getSum(VTSpeed,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getSum(VSpeed,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getSum(VTPI,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
           
            AddCellIntoTable(common.getSum(VHankExp,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getSum(VHank,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getSum(VProdExp,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getSum(VProdTar,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
              
            AddCellIntoTable(common.getSum(VProd,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getSum(VTarEffy,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getSum(VExpAct,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );     
            AddCellIntoTable(common.getSum(VTarAct,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getSum(VHourRun,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getSum(VHourMeter,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );     
            
            AddCellIntoTable(common.getSum(VStopMnt,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getSum(VUtilOA,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getSum(VNoDoff,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );     
            AddCellIntoTable(common.getSum(VWkdRot,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getSum(VGps,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getSum(VPneu,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );     
            AddCellIntoTable(common.getSum(VPneuTar,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );     
            AddCellIntoTable(common.getSum(VPneuVar,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );     
            AddCellIntoTable(common.getSum(VPneuPer,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );     
            AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );     
         
      }
      catch(Exception Ex){
                Ex.printStackTrace();
                System.out.println("Error Body1-->"+Ex);
        }
    }
    
    public void AbsPDF(){
   
        AddCellIntoTable("", table0, Element.ALIGN_LEFT, Element.ALIGN_TOP,8,40f,0,0,0,0, bigBold );     
        AddCellIntoTable("Countwise Abstract", table0, Element.ALIGN_LEFT, Element.ALIGN_TOP,8,20f,0,0,0,0, bigBold );     
        AddCellIntoTable("", table0, Element.ALIGN_LEFT, Element.ALIGN_TOP,8,20f,0,0,0,0, bigBold );     
        AddCellIntoTable("Count", table0, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,25f,5,3,8,4, smallBold );
        AddCellIntoTable("Allotted Spindles", table0, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,25f,5,3,8,4, smallBold );
        AddCellIntoTable("Worked Spindles", table0, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,25f,5,3,8,4, smallBold );
        AddCellIntoTable("Utilization %", table0, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,25f,5,3,8,4, smallBold );
        AddCellIntoTable("Target Kgs ", table0, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,25f,5,3,8,4, smallBold );
        AddCellIntoTable("Actual Kgs ", table0, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,25f,5,3,8,4, smallBold );
        AddCellIntoTable("Difference Kgs ", table0, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,25f,5,3,8,4, smallBold );
        AddCellIntoTable("Gms/Rot", table0, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,25f,5,3,8,4, smallBold );
        
        for(int i=0;i<VAbs1.size();i++) {
                AddCellIntoTable((String)VAbs1.elementAt(i), table0, Element.ALIGN_LEFT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
                AddCellIntoTable(common.getRound((String)VAbs2.elementAt(i),3), table0, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
                AddCellIntoTable(common.getRound((String)VAbs3.elementAt(i),3), table0, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
                AddCellIntoTable(common.getRound((String)VAbs4.elementAt(i),3), table0, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
                AddCellIntoTable(common.getRound((String)VAbs5.elementAt(i),3), table0, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
                AddCellIntoTable(common.getRound((String)VAbs6.elementAt(i),3), table0, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
                AddCellIntoTable(common.getRound((String)VAbs7.elementAt(i),3), table0, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
                AddCellIntoTable(common.getRound((String)VAbs8.elementAt(i),3), table0, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          }

          double dUtil = common.toDouble(common.getSum(VAbs3,"S"))*100/common.toDouble(common.getSum(VAbs2,"S"));
           AddCellIntoTable("Total", table0, Element.ALIGN_LEFT, Element.ALIGN_TOP,1,20f,5,3,8,4, smallBold );
           AddCellIntoTable(common.getSum(VAbs2,"S"), table0, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, smallBold );
           AddCellIntoTable(common.getSum(VAbs3,"S"), table0, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, smallBold );
           AddCellIntoTable(common.getRound(dUtil,3), table0, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, smallBold );
           AddCellIntoTable(common.getSum(VAbs5,"S"), table0, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, smallBold );
           AddCellIntoTable(common.getSum(VAbs6,"S"), table0, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, smallBold );
           AddCellIntoTable(common.getSum(VAbs7,"S"), table0, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, smallBold );
           AddCellIntoTable(common.getSum(VAbs8,"A"), table0, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, smallBold );
           
           AddCellIntoTable("Avg Count : "+getAverageCount(), table0, Element.ALIGN_LEFT, Element.ALIGN_TOP,8,40f,0,0,0,0, smallBold );
           
           AddCellIntoTable("P.O", table0, Element.ALIGN_LEFT, Element.ALIGN_TOP,2,60f,0,0,0,0, smallBold );
           AddCellIntoTable("S.P.O", table0, Element.ALIGN_LEFT, Element.ALIGN_TOP,2,60f,0,0,0,0, smallBold );
           AddCellIntoTable("S.M", table0, Element.ALIGN_LEFT, Element.ALIGN_TOP,2,60f,0,0,0,0, smallBold );
           AddCellIntoTable("D.M", table0, Element.ALIGN_LEFT, Element.ALIGN_TOP,2,60f,0,0,0,0,smallBold );
           
     }
    
    
    private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      } 
    
    private void AddHeadCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iRowSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
    
    private void AddCellIntoTable1(String Str,PdfPTable table,int iHorizontal,int iVertical,int iRowSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
      private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
    private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,int iRowSpan)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          //c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
    private void AddCellTable(Image image,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
          PdfPCell c1 = new PdfPCell();
		  c1.addElement(new Chunk(image, 5, -5));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

		  // image.scalePercent(90f);
      	  // paragraph.add(new Chunk(image, -1f, 1f));
      }
    private void AddCellIntoTable(Image image,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
          PdfPCell c1 = new PdfPCell();
		  //c1.addElement(new Chunk(image, 5, -5));
	  c1.addElement(image);
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

      }
     public void AddCellIntoTable2(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }
    
    class TableHeader extends PdfPageEventHelper {
        String header;
        PdfTemplate total;
        public void setHeader(String header) {
            this.header = header;
      }
    public void onOpenDocument(PdfWriter writer, Document document) {
            try{
            total = writer.getDirectContent().createTemplate(30, 16);
            }
            catch(Exception ex){
            }
      }

   public void onEndPage(PdfWriter writer, Document document) {
            PdfPTable table = new PdfPTable(3);
            try {
//                AddCellIntoTable("", table1, Element.ALIGN_LEFT, Element.ALIGN_TOP,13,18f,1,0,0,0, tinyNormal );
                table.setWidths(new int[]{24, 24, 2});
                table.setTotalWidth(520);
                table.setLockedWidth(true);
                table.getDefaultCell().setFixedHeight(20);
                table.getDefaultCell().setBorder(0);
                table.addCell(header);
                table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell("");
                table.addCell("");
                table.addCell("");
                
                table.addCell(String.format("Page %d of", writer.getPageNumber()));
                PdfPCell cell = new PdfPCell(Image.getInstance(total));
                cell.setBorder(0);
                table.addCell(cell);
                table.writeSelectedRows(0, -1, 10, 50, writer.getDirectContent());
            }
            catch(DocumentException de) {
                throw new ExceptionConverter(de);
            }
        }
        public void onCloseDocument(PdfWriter writer, Document document) {
          try{  
            ColumnText.showTextAligned(total, Element.ALIGN_LEFT,
                    new Phrase(String.valueOf(writer.getPageNumber() - 1)),
                    2, 2, 0);
             }
          catch(Exception ex){
              
          }
        }
    }       

}


