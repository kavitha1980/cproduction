package CProduction;

import java.io.*;
import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class StoppagesBreakupReport extends HttpServlet
{
     HttpSession    session;
     RepVect repVect = new RepVect();
     Common common   = new Common();

     String SDate,SShift,SUnit,SDept;

     String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody,tbgBody;

     Vector V1,V2,V3,V4,V5,V6,V7,V8,V9,V10,V11,V12,V13,V14,V15;

     FileWriter  FW;
     String SLine;
     Vector VHead;
     int Lctr=80,Pctr=0;

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);

          bgColor = common.bgColor;
          bgHead  = common.bgHead;
          bgUom   = common.bgUom;
          bgBody  = common.bgBody;
          fgHead  = common.fgHead;
          fgUom   = common.fgUom;
          fgBody  = common.fgBody;
		tbgBody = common.tbgBody;
     }

     public void doGet(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
            doPost(request,response);
     }
     public void doPost(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
          response.setContentType("text/html");
          PrintWriter out  = response.getWriter();
//		System.out.println(request.getQueryString());
		session = request.getSession(true);
		/*if(session.getValue("AppletTag")==null)
		{
			Control control = new Control();
			String SServer = control.getID("Select ProdTomHost from Server");
			response.sendRedirect(SServer + "/CProduction.SessionExpired");
			return;

                 }*/
		SDate  = request.getParameter("Date");
		SDate  = common.pureDate(SDate);
		SShift = request.getParameter("Shift").trim();
		SUnit  = request.getParameter("Unit").trim();
		SDept  = request.getParameter("Dept").trim();

		System.out.println("StopBreakup->" + SDate +":" + SUnit + ":" + SShift + ":" + SDept);

		String SDeptName = (String)repVect.VDept.elementAt(repVect.VDeptCode.indexOf(SDept));

          out.println("<html>");
          out.println("<head>");
          out.println("<title> </title>");
          out.println("</head>");
          out.println("<body bgcolor='" + common.bgBody + "'>");
		//out.println(session.getValue("AppletTag"));
          out.println("<a href='Header'>Home</a>");
	  out.println(common.getStopBreakupHeader("Utilization - Break up",SUnit,SShift,SDate,SDeptName));

          out.println("    <table border='1' width='1200' height='150'>");
          out.println("    <tr>");
          out.println("      <td width='64' height='56' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Mach No</font></td>");
          out.println("      <td width='708' height='27' colspan='5' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'><p align='center'>Stoppage (minutes)</font></td>");
          out.println("      <td width='81' height='56' rowspan='2' valign='middle' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Available Hours for Worker</font></td>");
          out.println("      <td width='81' height='56' rowspan='2' valign='middle' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Available Hours for Melange</font></td>");
          out.println("      <td width='81' height='56' rowspan='2' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Expected Hanks At Avai. Work. Hours</font></td>");
          out.println("      <td width='237' height='27' colspan='3' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'><p align='center'>Utilization</font></td>");
          out.println("      <td width='82' height='56' valign='middle' rowspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Worker Effy</font></td>");
          out.println("      <td width='243' height='27' colspan='2' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'><p align='center'>U &amp; E</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='81' height='29' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Normal</font></td>");
          out.println("      <td width='81' height='29' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Melange</font></td>");
          out.println("      <td width='81' height='29' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Total</font></td>");
          out.println("      <td width='81' height='28' valign='top' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'><font size='4'>Acce- pted</font></td>");
          out.println("      <td width='81' height='28' valign='top' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Non Acce- pted</font></td>");
          out.println("      <td width='81' height='29' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>OA</font></td>");
          out.println("      <td width='82' height='29' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Normal</font></td>");
          out.println("      <td width='82' height='29' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Melange</font></td>");
          out.println("      <td width='82' height='29' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>OA</font></td>");
          out.println("      <td width='82' height='29' align='center' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>M</font></td>");
          out.println("    </tr>");

          setVectors();
          try
          {
               FW      = new FileWriter("/software/C-Prod-Print/Reports/stop2c.prn");
               //FW      = new FileWriter("/production/Reports/stop2c.prn");
               Lctr    = 70;
               Pctr    =  0;
               Head();
               Body();
               FW.close();
          }catch(Exception ex){}

          for(int i=0; i<V1.size(); i++)
          {
               out.println("    <tr>");
               out.println("      <td width='64' height='56' bgcolor='"+tbgBody+"' align='left'><font color='"+fgBody+"' size='3'>"+(String)V1.elementAt(i)+"</font></td>");
               out.println("      <td width='81' height='56' bgcolor='"+tbgBody+"' align='right'><font color='"+fgBody+"' size='3'>"+(String)V2.elementAt(i)+"</font></td>");
               out.println("      <td width='81' height='56' bgcolor='"+tbgBody+"' align='right'><font color='"+fgBody+"' size='3'>"+(String)V3.elementAt(i)+"</font></td>");
               out.println("      <td width='81' height='56' bgcolor='"+tbgBody+"' align='right'><font color='"+fgBody+"' size='3'>"+(String)V4.elementAt(i)+"</font></td>");
               out.println("      <td width='81' height='56' bgcolor='"+tbgBody+"' align='right'><font color='"+fgBody+"' size='3'>"+(String)V5.elementAt(i)+"</font></td>");
               out.println("      <td width='81' height='56' bgcolor='"+tbgBody+"' align='right'><font color='"+fgBody+"' size='3'>"+(String)V6.elementAt(i)+"</font></td>");
               out.println("      <td width='81' height='56' bgcolor='"+tbgBody+"' align='right'><font color='"+fgBody+"' size='3'>"+(String)V7.elementAt(i)+"</font></td>");
               out.println("      <td width='81' height='56' bgcolor='"+tbgBody+"' align='right'><font color='"+fgBody+"' size='3'>"+(String)V8.elementAt(i)+"</font></td>");
               out.println("      <td width='81' height='56' bgcolor='"+tbgBody+"' align='right'><font color='"+fgBody+"' size='3'>"+(String)V9.elementAt(i)+"</font></td>");
               out.println("      <td width='81' height='56' bgcolor='"+tbgBody+"' align='right'><font color='"+fgBody+"' size='3'>"+(String)V10.elementAt(i)+"</font></td>");
               out.println("      <td width='82' height='56' bgcolor='"+tbgBody+"' align='right'><font color='"+fgBody+"' size='3'>"+(String)V11.elementAt(i)+"</font></td>");
               out.println("      <td width='82' height='56' bgcolor='"+tbgBody+"' align='right'><font color='"+fgBody+"' size='3'>"+(String)V12.elementAt(i)+"</font></td>");
               out.println("      <td width='82' height='56' bgcolor='"+tbgBody+"' align='right'><font color='"+fgBody+"' size='3'>"+(String)V13.elementAt(i)+"</font></td>");
               out.println("      <td width='82' height='56' bgcolor='"+tbgBody+"' align='right'><font color='"+fgBody+"' size='3'>"+(String)V14.elementAt(i)+"</font></td>");
               out.println("      <td width='82' height='56' bgcolor='"+tbgBody+"' align='right'><font color='"+fgBody+"' size='3'>"+(String)V15.elementAt(i)+"</font></td>");
               out.println("    </tr>");
          }
          out.println("  </table>");
          out.println("</div>");
          out.println("</body>");
          out.println("</html>");

          out.close();
     }

     public void setVectors()
     {
          V1    = new Vector();
          V2    = new Vector();
          V3    = new Vector();
          V4    = new Vector();
          V5    = new Vector();
          V6    = new Vector();
          V7    = new Vector();
          V8    = new Vector();
          V9    = new Vector();
          V10   = new Vector();
          V11   = new Vector();
          V12   = new Vector();
          V13   = new Vector();
          V14   = new Vector();
          V15   = new Vector();

          try
          {
               Connection con = createConnection();
               Statement stat = con.createStatement();
               ResultSet res1 = stat.executeQuery(getQString());
               while(res1.next())
               {
                    V1  .addElement(res1.getString(1));
                    V2  .addElement(common.getRound(res1.getString(2),2));
                    V3  .addElement(common.getRound(res1.getString(3),2));
                    V4  .addElement(common.getRound(res1.getString(4),2)); 
                    V5  .addElement(common.getRound(res1.getString(5),2));
                    V6  .addElement(common.getRound(res1.getString(6),2));
                    V7  .addElement(common.getRound(res1.getString(7),2));
                    V8  .addElement(common.getRound(res1.getString(8),2));
                    V9  .addElement(common.getRound(res1.getString(9),2));
                    V10 .addElement(common.getRound(res1.getString(10),2));
                    V11 .addElement(common.getRound(res1.getString(11),2));
                    V12 .addElement(common.getRound(res1.getString(12),2));
                    V13 .addElement(common.getRound(res1.getString(13),2));
                    V14 .addElement(common.getRound(res1.getString(14),2)); 
                    V15 .addElement(common.getRound(res1.getString(15),2));
               }
          }
          catch(Exception ex)
          {
               System.out.println("From StopBreakUp : 150 "+ex);
          }
     }

     public String getQString()
     {
          int iDept = common.toInt(SDept);
          String STable="",SStopMnt="";

          if(iDept==2)
          {
               STable = "blowtran";SStopMnt="stopmnt";
          }
          else if(iDept==3)
          {
               STable = "cardtran";SStopMnt="stopmnt";
          }
          else if(iDept==4)
          {
               STable = "combtran";SStopMnt="stopmnt";
          }
          else if(iDept==5)
          {
               STable = "drawtran";SStopMnt="stopmnt";
          }
          else if(iDept==6)
          {
               STable = "simptran";SStopMnt="stop_mt";
          }
          else if(iDept==7)
          {
               STable = "spintran";SStopMnt="stop_mt";
          }

          String QS = " SELECT Machine.mach_name,stopnu,stopmu,"+SStopMnt+","+
                      " stopacc,stopnonacc,availwork,availmu,hankexpavailwork,"+
                      " utiloa,utilnu,utilmu,workereffy,ueoa,uem "+
                      " FROM "+STable+" INNER JOIN Machine ON "+STable+".mach_code=Machine.mach_code "+
                      " WHERE ((("+STable+".unit_code)="+SUnit+") And (("+STable+".sp_date)='"+SDate+"') And (("+STable+".shift_code)="+SShift+"))";
          return QS;
     }
     public void Head()
     {
          if(Lctr < 60)
             return;
          if(Pctr > 0)
          {
             try
             {
               FW.write(SLine+"\n");
             }catch(Exception ex){}
          }
          Pctr++;

          String Str1    = common.Pad("Company    : AMARJOTHI SPINNING MILLS LIMITED",80);
          String Str2    = common.Pad("Document   : Stoppages BreakUp Report As On "+common.parseDate(SDate),80);
          String Str3    = common.Pad("Unit       : "+(SUnit.equals("1")?"A":(SUnit.equals("2")?"B":"S")),70);
          String Str4    = common.Pad("Department : "+(String)repVect.VDept.elementAt(repVect.VDeptCode.indexOf(SDept)),80);

          VHead = Stop2Head();
          try
          {
               FW.write(Str1+"\n");
               FW.write(Str2+"\n");
               FW.write(Str3+"\n");
               FW.write(Str4+"\n\n");
               for(int i=0;i<VHead.size();i++)
               {
                    FW.write((String)VHead.elementAt(i));
               }
          }catch(Exception ex){}
          Lctr = 5+VHead.size();
     }
     public void Body()
     {
         String Strl = "";
         for(int i=0;i<V1.size();i++)
         {
                Strl  = common.Pad((String)V1.elementAt(i),4)+common.Space(2)+
                         common.Rad(common.getRound((String)V2.elementAt(i),0),4)+common.Space(2)+
                         common.Rad(common.getRound((String)V3.elementAt(i),0),4)+common.Space(2)+
                         common.Rad(common.getRound((String)V4.elementAt(i),0),4)+common.Space(2)+
                         common.Rad(common.getRound((String)V5.elementAt(i),0),4)+common.Space(2)+
                         common.Rad(common.getRound((String)V6.elementAt(i),0),4)+common.Space(2)+
                         common.Rad((String)V7.elementAt(i),7)+common.Space(2)+
                         common.Rad((String)V8.elementAt(i),7)+common.Space(2)+
                         common.Rad((String)V9.elementAt(i),7)+common.Space(2)+
                         common.Rad((String)V10.elementAt(i),6)+common.Space(2)+
                         common.Rad((String)V11.elementAt(i),6)+common.Space(2)+
                         common.Rad((String)V12.elementAt(i),6)+common.Space(2)+
                         common.Rad((String)V13.elementAt(i),6)+common.Space(2)+
                         common.Rad((String)V14.elementAt(i),7)+common.Space(2)+
                         common.Rad((String)V15.elementAt(i),7);

                try
                {
                     Head();
                     FW.write(Strl+"\n\n");
                     Lctr++;
                }catch(Exception ex){}
         }
           try
           {
                FW.write(SLine+"\n");
                FW.write("<End Of Report>");
           }catch(Exception ex){}
     }

     public Vector Stop2Head()
     {
          Vector VHead = new Vector();

          String Str1  = common.Pad("Mach",4)+common.Space(2)+
                         common.Cad("Stoppage in Minutes",4+2+4+2+4+2+4+2+4)+common.Space(2)+common.Rad("Avai",7)+common.Space(2)+common.Rad("Avai",7)+common.Space(2)+
                         common.Rad("HankExp",7)+common.Space(2)+common.Cad("Utilization",6+2+6+2+6)+common.Space(2)+
                         common.Rad(" ",6)+common.Space(2)+
                         common.Cad("U & E",7+2+7);

          String Str2  = common.Pad("No",4)+common.Space(2)+
                         common.Replicate("-",4+2+4+2+4+2+4+2+4)+common.Space(2)+common.Rad("Hour",7)+common.Space(2)+common.Rad("Hour",7)+common.Space(2)+
                         common.Rad("@Avail",7)+common.Space(2)+common.Replicate("-",6+2+6+2+6)+common.Space(2)+
                         common.Rad("Worker",6)+common.Space(2)+
                         common.Replicate("-",7+2+7);

          String Str3  = common.Space(4+2)+
                         common.Rad("Norm",4)+common.Space(2)+common.Rad("Mela",4)+common.Space(2)+common.Rad("Tot",4)+common.Space(2)+
                         common.Rad("Acce",4)+common.Space(2)+common.Rad("Non",4)+common.Space(2)+common.Rad("For",7)+common.Space(2)+common.Rad("For",7)+common.Space(2)+
                         common.Rad("HourFor",7)+common.Space(2)+common.Rad("OA",6)+common.Space(2)+common.Rad("Normal",6)+common.Space(2)+common.Rad("Melange",6)+common.Space(2)+
                         common.Rad("Effy",6)+common.Space(2)+
                         common.Rad("OA",7)+common.Space(2)+common.Rad("M",7);

          String Str4  = common.Space(4+2)+
                         common.Rad("al",4)+common.Space(2)+common.Rad("nge",4)+common.Space(2)+common.Rad("al",4)+common.Space(2)+
                         common.Rad("pted",4)+common.Space(2)+common.Rad("Acc",4)+common.Space(2)+common.Rad("Worker",7)+common.Space(2)+common.Rad("Melange",7)+common.Space(2)+                         
                         common.Rad("Worker",7)+common.Space(2)+common.Rad("%",6)+common.Space(2)+common.Rad("%",6)+common.Space(2)+common.Rad(" ",6)+common.Space(2)+
                         common.Rad("%",6)+common.Space(2)+
                         common.Rad("%",7)+common.Space(2)+common.Rad("%",7);

          SLine = common.Replicate("-",111);

          Str1  = Str1+"\n";
          Str2  = Str2+"\n";
          Str3  = Str3+"\n";
          Str4  = Str4+"\n";
          SLine = SLine+"\n";

          VHead.addElement(SLine);
          VHead.addElement(Str1);
          VHead.addElement(Str2);
          VHead.addElement(Str3);
          VHead.addElement(Str4);
          VHead.addElement(SLine);

          return VHead;
     }

	private Connection createConnection()
	{
		Connection conn;
		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
			conn  = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
		}catch(Exception ee)
		{
			return null;
		}
		return conn;
	}

}
