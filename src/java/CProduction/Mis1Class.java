/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CProduction;

import java.io.FileWriter;
import java.sql.*;
import java.util.Vector;

/**
 *
 * @author root
 */
public class Mis1Class {

         String SDate,SMonStDate,SUnit;

     Vector VCountClass;
     Common common;

     FileWriter  FW;
     int Lctr = 70,Pctr = 0;
     String SLine;
     Vector VHead;

     Mis1Class(String SDate,String SUnit)
     {
          this.SDate  = SDate;
          this.SUnit  = SUnit;

          common      = new Common();

          SMonStDate  = common.getStartDate(SDate);
          VCountClass = new Vector();
          getData();
     }
     private void getData()
     {
             try
             {
                     Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
                     Connection con = DriverManager.getConnection("jdbc:odbc:AmarProcess","","");
                     Statement stat = con.createStatement();
                     ResultSet res1  = stat.executeQuery(getQString());
                     while(res1.next())
                         organizeCountClass(res1);
                     con.close();
             }
             catch(Exception ex)
             {
                     System.out.println("111 : "+ex);
                     ex.printStackTrace();
             }
     }
     private void organizeCountClass(ResultSet res1) throws Exception
     {
          String SDate       = res1.getString(1);
          String SSpeed      = res1.getString(2);
          String SSpeedTar   = res1.getString(3);
          String STPI        = res1.getString(4);
          String STPITar     = res1.getString(5);
          String SPneu       = res1.getString(6);
          String SPneuTar    = res1.getString(7);
          String SProd       = res1.getString(8);
          String SProdTar    = res1.getString(9);
          String SProdExp    = res1.getString(10);
          String SHour       = res1.getString(11);
          String SHourRun    = res1.getString(12);
          String SCount      = res1.getString(13);
          String SSpdlWkd    = res1.getString(14);
          String SCountEffy  = res1.getString(15);
          String SCountCode  = res1.getString(16);
          String SCountName  = res1.getString(17);
          String SNoOfSpdl   = res1.getString(18);

          int iindex = indexOf(SCountCode);
          if(iindex==-1)
          {
               CountClass cc = new CountClass(SCountCode,SCountName);
               cc.append(SDate,SSpeed,SSpeedTar,STPI,STPITar,SPneu,SPneuTar,SProd,SProdTar,SProdExp,SHour,SHourRun,SCount,SSpdlWkd,SCountEffy,SNoOfSpdl);
               VCountClass.addElement(cc);
          }
          else
          {
               CountClass cc = (CountClass)VCountClass.elementAt(iindex);
               cc.append(SDate,SSpeed,SSpeedTar,STPI,STPITar,SPneu,SPneuTar,SProd,SProdTar,SProdExp,SHour,SHourRun,SCount,SSpdlWkd,SCountEffy,SNoOfSpdl);
               VCountClass.setElementAt(cc,iindex);
          }
     }
     private int indexOf(String SCCode)
     {
          int index = -1;
          for(int i=0;i<VCountClass.size();i++)
          {
               CountClass cc = (CountClass)VCountClass.elementAt(i);
               if(cc.SCountCode.equals(SCCode))
               {
                    index = i;
                    break;
               }
          }
          return index;
     }
     public String getQString()
     {
             String QString = "";
             QString =   " SELECT spintran.sp_date,spintran.speed, spintran.speed_tar, "+
                         " spintran.tpi,spintran.tpi_tar,spintran.pneumafil, spintran.pneu_tar, "+
                         " spintran.prod, spintran.prod_tar, spintran.prod_exp, "+
                         " spintran.hours, spintran.hoursrun, spintran.count, "+
                         " spintran.spdlwkd,spintran.count_effy, "+
                         " spintran.count_code,count.count_name,spintran.noofspdl "+
                         " FROM spintran INNER JOIN count ON spintran.count_Code = count.count_code "+
                         " Where spintran.sp_date>='"+SMonStDate+"' and "+
                         " spintran.sp_date <= '"+SDate+"' AND "+
                         " spintran.unit_code="+SUnit;
             return QString;
     }
     public void prtBreak(FileWriter FW)
     {
          this.FW = FW;
          try
          {
               Lctr    = 70;
               Pctr    =  0;
               Head();
               Body();
          }catch(Exception ex){}
     }
     public void Head()
     {
          if(Lctr < 60)
             return;
          if(Pctr > 0)
          {
             try
             {
               FW.write(SLine+"\n");
             }catch(Exception ex){}
          }
          Pctr++;

          String Str1    = common.Pad("Company    : AMARJOTHI SPINNING MILLS LIMITED",80);
          String Str2    = common.Pad("Document   : Break-up of Factors Affecting Actual Production against Target As On "+common.parseDate(SDate),80);
          VHead = Spin1Head();
          try
          {
               FW.write(Str1+"\n");
               FW.write(Str2+"\n\n");
               for(int i=0;i<VHead.size();i++)
               {
                    FW.write((String)VHead.elementAt(i));
               }
          }catch(Exception ex){}
          Lctr = 4+VHead.size();
     }
     public void Body()
     {
         int iDate = common.toInt(SDate); 
         String Strl = "";
         double dProdTar=0,dProd=0,dSpeed=0,dSpeedU=0,dTPI=0,dTPIU=0;
         double dUtil=0,dUtilU=0,dPneu=0,dPneuU=0,dEffy=0,dEffyU=0;
         double dNet=0,dNetU=0,dProdU=0,dProdTarU=0;

         String sProdTar="",sProd="",sSpeed="",sSpeedU="",sTPI="",sTPIU="";
         String sUtil="",sUtilU="",sPneu="",sPneuU="",sEffy="",sEffyU="";
         String sNet="",sNetU="",sProdU="",sProdTarU="";

         for(int i=0;i<VCountClass.size();i++)
         {
               CountClass cc = (CountClass)VCountClass.elementAt(i);

                Strl = "";
                sProdTar  = cc.getProdTar(iDate);
                dProdTar += common.toDouble(sProdTar);
                sProdTarU = cc.getProdTar();
                dProdTarU+= common.toDouble(sProdTarU);
                sProd     = cc.getProd(iDate);
                dProd    += common.toDouble(sProd);
                sProdU    = cc.getProd();
                dProdU   += common.toDouble(sProdU);
                sSpeed    = cc.getSpeedDiff(iDate);
                dProd    += common.toDouble(sSpeed);
                sSpeedU   = cc.getSpeedDiff();
                dSpeedU  += common.toDouble(sSpeedU);
                sTPI      = cc.getTPIDiff(iDate);
                dTPI     += common.toDouble(sTPI);
                sTPIU     = cc.getTPIDiff();
                dTPIU    += common.toDouble(sTPIU);
                sUtil     = cc.getUtilDiff(iDate);
                dUtil    += common.toDouble(sUtil);
                sUtilU    = cc.getUtilDiff();
                dUtilU   += common.toDouble(sUtilU);
                sPneu     = cc.getPneuDiff(iDate);
                dPneu    += common.toDouble(sPneu);
                sPneuU    = cc.getPneuDiff();
                dPneuU   += common.toDouble(sPneuU);
                sEffy     = cc.getEffyDiff(iDate);
                dEffy    += common.toDouble(sEffy);
                sEffyU    = cc.getEffyDiff();
                dEffyU   += common.toDouble(sEffyU);
                sNet      = cc.getNetDiff(iDate);
                dNet     += common.toDouble(sNet);
                sNetU     = cc.getNetDiff();
                dNetU    += common.toDouble(sNetU);

                Strl = common.Pad(cc.SCount,15)+common.Space(2)+
                        common.Rad(sProdTar,12)+common.Space(2)+
                        common.Rad(sProd,12)+common.Space(2)+
                        common.Rad(sSpeed,10)+common.Space(2)+
                        common.Rad(sTPI,10)+common.Space(2)+
                        common.Rad(sUtil,10)+common.Space(2)+
                        common.Rad(sPneu,10)+common.Space(2)+
                        common.Rad(sEffy,10)+common.Space(2)+
                        common.Rad(sNet,10)+common.Space(2)+
                        common.Rad(sProdTarU,12)+common.Space(2)+
                        common.Rad(sProdU,12)+common.Space(2)+
                        common.Rad(sSpeedU,10)+common.Space(2)+
                        common.Rad(sTPIU,10)+common.Space(2)+
                        common.Rad(sUtilU,10)+common.Space(2)+
                        common.Rad(sPneuU,10)+common.Space(2)+
                        common.Rad(sEffyU,10)+common.Space(2)+
                        common.Rad(sNetU,10)+"\n";

                try
                {
                     FW.write(Strl);
                     Lctr++;
                     Head();
                }catch(Exception ex){}
         }
         try
         {
               FW.write(SLine);               
         }catch(Exception ex){}

           Strl = "";
           Strl = common.Pad("Total",15)+common.Space(2)+
                   common.Rad(common.getRound(dProdTar,2),12)+common.Space(2)+
                   common.Rad(common.getRound(dProd,2),12)+common.Space(2)+
                   common.Rad(common.getRound(dSpeed,2),10)+common.Space(2)+
                   common.Rad(common.getRound(dTPI,2),10)+common.Space(2)+
                   common.Rad(common.getRound(dUtil,2),10)+common.Space(2)+
                   common.Rad(common.getRound(dPneu,2),10)+common.Space(2)+
                   common.Rad(common.getRound(dEffy,2),10)+common.Space(2)+
                   common.Rad(common.getRound(dNet,2),10)+common.Space(2)+
                   common.Rad(common.getRound(dProdTarU,2),12)+common.Space(2)+
                   common.Rad(common.getRound(dProdU,2),12)+common.Space(2)+
                   common.Rad(common.getRound(dSpeedU,2),10)+common.Space(2)+
                   common.Rad(common.getRound(dTPIU,2),10)+common.Space(2)+
                   common.Rad(common.getRound(dUtilU,2),10)+common.Space(2)+
                   common.Rad(common.getRound(dPneuU,2),10)+common.Space(2)+
                   common.Rad(common.getRound(dEffyU,2),10)+common.Space(2)+
                   common.Rad(common.getRound(dNetU,2),10)+"\n";

           try
           {
               FW.write(Strl);
               FW.write(SLine);               
           }catch(Exception ex){}
     }

     public Vector Spin1Head()
     {
          Vector VHead = new Vector();

          String Str1 = common.Space(15)+common.Space(2)+
                        common.Cad("Today",98)+common.Space(2)+
                        common.Cad("Upto Date",98)+"\n";

          String Str2 = common.Space(15)+common.Space(2)+
                        common.Replicate("-",98)+common.Space(2)+
                        common.Replicate("-",98)+"\n";

          String Str3 = common.Pad("Count",15)+common.Space(2)+
                        common.Rad("Target",12)+common.Space(2)+
                        common.Rad("Actual",12)+common.Space(2)+
                        common.Rad("Speed",10)+common.Space(2)+
                        common.Rad("TPI",10)+common.Space(2)+
                        common.Rad("Util",10)+common.Space(2)+
                        common.Rad("Pneu",10)+common.Space(2)+
                        common.Rad("Effy",10)+common.Space(2)+
                        common.Rad("Total",10)+common.Space(2)+
                        common.Rad("Target",12)+common.Space(2)+
                        common.Rad("Actual",12)+common.Space(2)+
                        common.Rad("Speed",10)+common.Space(2)+
                        common.Rad("TPI",10)+common.Space(2)+
                        common.Rad("Util",10)+common.Space(2)+
                        common.Rad("Pneu",10)+common.Space(2)+
                        common.Rad("Effy",10)+common.Space(2)+
                        common.Rad("Total",10)+"\n";

          String Str5 = common.Pad(" ",15)+common.Space(2)+
                        common.Rad("kgs",12)+common.Space(2)+
                        common.Rad("kgs",12)+common.Space(2)+
                        common.Rad("kgs",10)+common.Space(2)+
                        common.Rad("kgs",10)+common.Space(2)+
                        common.Rad("kgs",10)+common.Space(2)+
                        common.Rad("kgs",10)+common.Space(2)+
                        common.Rad("kgs",10)+common.Space(2)+
                        common.Rad("kgs",10)+common.Space(2)+
                        common.Rad("kgs",12)+common.Space(2)+
                        common.Rad("kgs",12)+common.Space(2)+
                        common.Rad("kgs",10)+common.Space(2)+
                        common.Rad("kgs",10)+common.Space(2)+
                        common.Rad("kgs",10)+common.Space(2)+
                        common.Rad("kgs",10)+common.Space(2)+
                        common.Rad("kgs",10)+common.Space(2)+
                        common.Rad("kgs",10)+"\n";

          String Str4 = common.Pad(" ",15)+common.Space(2)+
                        common.Replicate("-",12)+common.Space(2)+
                        common.Replicate("-",12)+common.Space(2)+
                        common.Replicate("-",10)+common.Space(2)+
                        common.Replicate("-",10)+common.Space(2)+
                        common.Replicate("-",10)+common.Space(2)+
                        common.Replicate("-",10)+common.Space(2)+
                        common.Replicate("-",10)+common.Space(2)+
                        common.Replicate("-",10)+common.Space(2)+
                        common.Replicate("-",12)+common.Space(2)+
                        common.Replicate("-",12)+common.Space(2)+
                        common.Replicate("-",10)+common.Space(2)+
                        common.Replicate("-",10)+common.Space(2)+
                        common.Replicate("-",10)+common.Space(2)+
                        common.Replicate("-",10)+common.Space(2)+
                        common.Replicate("-",10)+common.Space(2)+
                        common.Replicate("-",10)+"\n";

          SLine = common.Replicate("-",Str1.length())+"\n";

          VHead.addElement(SLine);
          VHead.addElement(Str1);
          VHead.addElement(Str2);
          VHead.addElement(Str3);
          VHead.addElement(Str4);
          VHead.addElement(Str5);
          VHead.addElement(SLine);

          return VHead;
     }
    
}
