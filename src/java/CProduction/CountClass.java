/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CProduction;

import java.util.*;
/**
 *
 * @author root
 */
public class CountClass {

         String SCount,SCountCode;

     Vector VDate,VSpeed,VSpeedTar,VTPI,VTPITar;
     Vector VPneu,VPneuTar,VProd,VProdTar,VProdExp;
     Vector VHour,VHourRun,VCount,VSpdlWkd,VCountEffy,VNoOfSpdl;
     Common common;
     CountClass(String SCountCode,String SCount)
     {
          this.SCountCode = SCountCode;
          this.SCount     = SCount;

          common = new Common();
          VDate     = new Vector();
          VSpeed    = new Vector();
          VSpeedTar = new Vector();
          VTPI      = new Vector();
          VTPITar   = new Vector();
          VPneu     = new Vector();
          VPneuTar  = new Vector();
          VProd     = new Vector();
          VProdTar  = new Vector();
          VProdExp  = new Vector();
          VHour     = new Vector();
          VHourRun  = new Vector();
          VCount    = new Vector();
          VSpdlWkd  = new Vector();
          VCountEffy= new Vector();
          VNoOfSpdl = new Vector();
     }
     public void append(String SDate,String SSpeed,String SSpeedTar,String STPI,String STPITar,String SPneu,String SPneuTar,String SProd,String SProdTar,String SProdExp,String SHour,String SHourRun,String SCount,String SSpdlWkd,String SCountEffy,String SNoOfSpdl)
     {
          VDate     .addElement(SDate);
          VSpeed    .addElement(SSpeed);
          VSpeedTar .addElement(SSpeedTar);
          VTPI      .addElement(STPI);
          VTPITar   .addElement(STPITar);
          VPneu     .addElement(SPneu);
          VPneuTar  .addElement(SPneuTar);
          VProd     .addElement(SProd);
          VProdTar  .addElement(SProdTar);
          VProdExp  .addElement(SProdExp);
          VHour     .addElement(SHour);
          VHourRun  .addElement(SHourRun);
          VCount    .addElement(SCount);
          VSpdlWkd  .addElement(SSpdlWkd);
          VCountEffy.addElement(SCountEffy);
          VNoOfSpdl .addElement(SNoOfSpdl);
     }
     public String getSpeedDiff(int iDate)
     {
          double dProd = 0;
          for(int i=0;i<VDate.size();i++)
          {
               if(common.toInt((String)VDate.elementAt(i))!=iDate)
                    continue;
               double dProdTar  = common.toDouble((String)VProdTar.elementAt(i));

               double dNoOfSpdl = common.toDouble((String)VNoOfSpdl.elementAt(i));
               double dSpdlWkd  = common.toDouble((String)VSpdlWkd.elementAt(i));
               double dSpeedTar = common.toDouble((String)VSpeedTar.elementAt(i))/dNoOfSpdl;
               double dSpeed    = common.toDouble((String)VSpeed.elementAt(i))/dSpdlWkd;

               dProd    += (dSpeedTar-dSpeed)*dProdTar/dSpeedTar;
          }
          return common.getRound(dProd,2);
     }
     public String getSpeedDiff()
     {
          double dProd = 0;
          for(int i=0;i<VDate.size();i++)
          {
               double dProdTar  = common.toDouble((String)VProdTar.elementAt(i));
               double dNoOfSpdl = common.toDouble((String)VNoOfSpdl.elementAt(i));
               double dSpdlWkd  = common.toDouble((String)VSpdlWkd.elementAt(i));
               double dSpeedTar = common.toDouble((String)VSpeedTar.elementAt(i))/dNoOfSpdl;
               double dSpeed    = common.toDouble((String)VSpeed.elementAt(i))/dSpdlWkd;

               dProd    += (dSpeedTar-dSpeed)*dProdTar/dSpeedTar;
          }
          return common.getRound(dProd,2);
     }
     public String getTPIDiff(int iDate)
     {
          double dTPIDiff = 0;
          for(int i=0;i<VDate.size();i++)
          {
               if(common.toInt((String)VDate.elementAt(i))!=iDate)
                    continue;
               double dProdTar  = common.toDouble((String)VProdTar.elementAt(i));
               double dNoOfSpdl = common.toDouble((String)VNoOfSpdl.elementAt(i));
               double dSpdlWkd  = common.toDouble((String)VSpdlWkd.elementAt(i));
               double dTPITar   = common.toDouble((String)VTPITar.elementAt(i))/dNoOfSpdl;
               double dTPI      = common.toDouble((String)VTPI.elementAt(i))/dSpdlWkd;

               dTPIDiff    += (dTPITar-dTPI)*dProdTar/dTPITar;
          }
          return common.getRound(dTPIDiff,2);
     }
     public String getTPIDiff()
     {
          double dTPIDiff = 0;
          for(int i=0;i<VDate.size();i++)
          {
               double dProdTar  = common.toDouble((String)VProdTar.elementAt(i));
               double dNoOfSpdl = common.toDouble((String)VNoOfSpdl.elementAt(i));
               double dSpdlWkd  = common.toDouble((String)VSpdlWkd.elementAt(i));
               double dTPITar   = common.toDouble((String)VTPITar.elementAt(i))/dNoOfSpdl;
               double dTPI      = common.toDouble((String)VTPI.elementAt(i))/dSpdlWkd;

               dTPIDiff    += (dTPITar-dTPI)*dProdTar/dTPITar;
          }
          return common.getRound(dTPIDiff,2);
     }
     public String getUtilDiff(int iDate)
     {
          double dUDiff = 0;
          for(int i=0;i<VDate.size();i++)
          {
               if(common.toInt((String)VDate.elementAt(i))!=iDate)
                    continue;
               double dProdTar  = common.toDouble((String)VProdTar.elementAt(i));               
               double dSpdlWkd  = common.toDouble((String)VSpdlWkd.elementAt(i));
               double dSpdl     = common.toDouble((String)VNoOfSpdl.elementAt(i));

               double dUtil  = (dSpdl==0?0:dSpdlWkd*100/dSpdl);
               dUDiff       += (97.5-dUtil)*dProdTar/97.5;
          }
          return common.getRound(dUDiff,2);
     }
     public String getUtilDiff()
     {
          double dUDiff = 0;
          for(int i=0;i<VDate.size();i++)
          {
               double dProdTar  = common.toDouble((String)VProdTar.elementAt(i));               
               double dSpdlWkd  = common.toDouble((String)VSpdlWkd.elementAt(i));
               double dSpdl     = common.toDouble((String)VNoOfSpdl.elementAt(i));

               double dUtil  = (dSpdl==0?0:dSpdlWkd*100/dSpdl);
               dUDiff       += (97.5-dUtil)*dProdTar/97.5;
          }
          return common.getRound(dUDiff,2);
     }
     public String getPneuDiff(int iDate)
     {
          double dProd = 0;
          for(int i=0;i<VDate.size();i++)
          {
               if(common.toInt((String)VDate.elementAt(i))!=iDate)
                    continue;
               double dPneuTar = common.toDouble((String)VPneuTar.elementAt(i));
               double dPneu    = common.toDouble((String)VPneu.elementAt(i));

               dProd += dPneuTar-dPneu;
          }
          return common.getRound(dProd,2);
     }
     public String getPneuDiff()
     {
          double dProd = 0;
          for(int i=0;i<VDate.size();i++)
          {
               double dPneuTar = common.toDouble((String)VPneuTar.elementAt(i));
               double dPneu    = common.toDouble((String)VPneu.elementAt(i));

               dProd += dPneuTar-dPneu;
          }
          return common.getRound(dProd,2);
     }
     public String getEffyDiff(int iDate)
     {
          double dDiff = 0;
          dDiff += common.toDouble(getSpeedDiff(iDate));
          dDiff += common.toDouble(getTPIDiff(iDate));
          dDiff += common.toDouble(getUtilDiff(iDate));
          dDiff += common.toDouble(getPneuDiff(iDate));
          double dProd    = common.toDouble(getProd(iDate));
          double dProdTar = common.toDouble(getProdTar(iDate));
          return common.getRound(dProdTar-dProd-dDiff,2);
     }
     public String getEffyDiff()
     {
          double dDiff = 0;
          dDiff += common.toDouble(getSpeedDiff());
          dDiff += common.toDouble(getTPIDiff());
          dDiff += common.toDouble(getUtilDiff());
          dDiff += common.toDouble(getPneuDiff());
          double dProd    = common.toDouble(getProd());
          double dProdTar = common.toDouble(getProdTar());
          return common.getRound(dProdTar-dProd-dDiff,2);
     }
     public String getNetDiff()
     {
          double dDiff = 0;
          dDiff += common.toDouble(getSpeedDiff());
          dDiff += common.toDouble(getTPIDiff());
          dDiff += common.toDouble(getUtilDiff());
          dDiff += common.toDouble(getPneuDiff());
          dDiff += common.toDouble(getEffyDiff());
          return common.getRound(dDiff,2);
     }
     public String getNetDiff(int iDate)
     {
          double dDiff = 0;
          dDiff += common.toDouble(getSpeedDiff(iDate));
          dDiff += common.toDouble(getTPIDiff(iDate));
          dDiff += common.toDouble(getUtilDiff(iDate));
          dDiff += common.toDouble(getPneuDiff(iDate));
          dDiff += common.toDouble(getEffyDiff(iDate));
          return common.getRound(dDiff,2);
     }
     public String getProdTar(int iDate)
     {
          double dProdTar = 0;
          for(int i=0;i<VDate.size();i++)
          {
               if(common.toInt((String)VDate.elementAt(i))!=iDate)
                    continue;
               dProdTar += common.toDouble((String)VProdTar.elementAt(i));
          }
          return common.getRound(dProdTar,2);
     }
     public String getProdTar()
     {
          double dProdTar = 0;
          for(int i=0;i<VDate.size();i++)
               dProdTar += common.toDouble((String)VProdTar.elementAt(i));
          return common.getRound(dProdTar,2);
     }
     public String getProd(int iDate)
     {
          double dProd = 0;
          for(int i=0;i<VDate.size();i++)
          {
               if(common.toInt((String)VDate.elementAt(i))!=iDate)
                    continue;
               dProd += common.toDouble((String)VProd.elementAt(i));
          }
          return common.getRound(dProd,2);
     }
     public String getProd()
     {
          double dProd= 0;
          for(int i=0;i<VDate.size();i++)
               dProd += common.toDouble((String)VProd.elementAt(i));
          return common.getRound(dProd,2);
     }

}
