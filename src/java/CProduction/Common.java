package CProduction;

import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.io.*;
import java.sql.*;

public class Common
{
      String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody,tbgBody;

      public Common()
      {
          bgColor = "#FEF5E2";     // "#FEFCD8";
          bgHead  = "#8fbc8f";     // #9AA8D6   // "#000080";
          bgUom   = "#deb887";     //"#CCCCFF";
//          bgBody  = "#9DECFF";
		bgBody  = "#d3d3d3";
          fgHead  = "#000000";     //"#800000"   // "#00FFFF";
          fgUom   = "#000080";
          fgBody  = "#000000";
	  tbgBody = "#b0c4de";


      }

      public void setColors()
      {
           try
           {
                  Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
                  Connection conn  = DriverManager.getConnection("jdbc:odbc:AmarProcess","","");
                  Statement stat2  = conn.createStatement();

                  ResultSet result1 = stat2.executeQuery("Select bgcolor,bghead,bguom,fghead,fguom,fgbody,bgBody From color");
                  while(result1.next())
                  {
                       bgColor = result1.getString(1);
                       bgHead  = result1.getString(2);
                       bgUom   = result1.getString(3);
                       fgHead  = result1.getString(4);
                       fgUom   = result1.getString(5);
                       fgBody  = result1.getString(6);
                       bgBody  = result1.getString(7);
                  }
                  conn.close();
           }catch(Exception ex){System.out.println("getUnit :"+ex);}
      }
      public String getRound(double dAmount,int iScale)
      {
            String str="0";
            try
            {
                  java.math.BigDecimal bd1 = new java.math.BigDecimal(String.valueOf(dAmount));
                  java.math.BigDecimal bd2 = bd1.setScale(iScale,4);
                  str=bd2.toString();
            }
            catch(Exception ex)
            {
            }
            return str;
      }
      public String getRound(String SAmount,int iScale)
      {
            String str="0";
            try
            {
                  java.math.BigDecimal bd1 = new java.math.BigDecimal(SAmount);
                  java.math.BigDecimal bd2 = bd1.setScale(iScale,4);
                  str=bd2.toString();
            }
            catch(Exception ex)
            {
            }
            return str;
      }

      public String Round(String Str)
      {
         String LeftStr="",RightStr="",CurStr="";
         boolean sig=true;
         for(int i=0;i<Str.length();i++)
         {
            
            if (Str.substring(i,i+1).equals("."))
            {
               LeftStr = CurStr;
               CurStr  = "";
               sig     = false;
            }
            else
            {
               CurStr = CurStr+Str.substring(i,i+1);
            }
         }
         if (sig)
            return CurStr+".00";

         RightStr = CurStr;
         if(RightStr.length()>=2)
            RightStr = RightStr.substring(0,2);
         else if(RightStr.length()>=1)
            RightStr = RightStr.substring(0,1)+"0";
         else if(RightStr.length()==0)
            RightStr = "00";
         return LeftStr+"."+RightStr;
      }
      public String Pad(String Str,int Len)
      {
            String RetStr="";
				if(Str == null)
				{	
				 	Str = " ";
				 	Str = Str+Space(Len);
			 		return Str;
				}
            if(Str.length() > Len)
            {
                  RetStr = Str.substring(0,Len);
            }
            else
            {
                  RetStr = Str+Space(Len-Str.length());
            }
            return RetStr;
      }

      public String Rad(String Str,int Len)
      {
            Str = parseNull(Str);
            String RetStr="";
            if(Str.length() > Len)
            {
                  RetStr = Str.substring(0,Len);
            }
            else
            {
                  RetStr = Space(Len-Str.length())+Str;
            }
            return RetStr;
      }

      public String Space(int Len)
      {
            String RetStr="";
            for(int index=0;index<Len;index++)
            {
                  RetStr=RetStr+" ";
            }
            return RetStr;
      }
      public String Replicate(String Str,int Len) 
      {
            String RetStr="";
            for(int index=0;index<Len;index++)
            {
                  RetStr = RetStr+Str;
            }
            return RetStr;
      }
   public String parseNull(String str)
   {
        return str+" ";
   }
     public String Remark(String str)
     {
               String RetStr="";

               System.out.println(" String Length is   "+str.length());  

               if(str.length() <25)
                    return str;
               else if(str.length() > 25 && str.length() < 50)
               {
                     RetStr  = str.substring(0,25)+"*";
                     RetStr  = RetStr+str.substring(26,str.length()-26);
                     System.out.println("Inside Common 1 "+RetStr);
               }                     
               else if(str.length() > 50 && str.length() < 75)
               {
                     try
                     {
                          RetStr  = str.substring(0,25)+"*";
                          System.out.println("Inside Common 2 1 "+RetStr);
                          RetStr  = RetStr +str.substring(25,50)+"*";
                          System.out.println("Inside Common 2 2 "+RetStr);
                          RetStr  = RetStr +str.substring(50,str.length());
                          System.out.println("Inside Common 2 3 "+RetStr);
                     } catch(Exception e){ System.out.println(" Error : " +e); }
               }
               System.out.println(" Str :    "+str);
               System.out.println();
               System.out.println(" String  :"+ RetStr);
               return RetStr;
     }

     public String Cad(String str,int Len)
     {
               str = parseNull(str);
               if(Len < str.length())
               {
                    return Pad(str,Len);
               }
               int FirstLen = (Len-str.length())/2;
               return Pad(Space(FirstLen)+str+Space(FirstLen),Len);
     }

      public String makeString(String str,String type,boolean flag)
		{
			String formatedString="";
			int testInt;
			float testFloat;

			if(type=="S")
				formatedString="'"+str+"'";
			else if(type=="I")
			{
				try
				{
					testInt = Integer.parseInt(str);
					formatedString=str;
				}
				catch(NumberFormatException nfe)
				{
					formatedString="0";
				}
			}
			else if(type=="F")
			{
				try
				{
					testFloat= Float.parseFloat(str);
					formatedString=str;
				}
				catch(NumberFormatException nfe)
				{
					formatedString="0.0";
				}
			}
			
			if (!flag)
				formatedString=formatedString+",";
			else
      	   formatedString=formatedString+")";

         return formatedString;
   	}
     public String parseDate(String str)
     {
         String SDate="";
         str=str.trim();
         if(str.length()!=8)
            return str;
         try
         {
          SDate = str.substring(6,8)+"."+str.substring(4,6)+"."+str.substring(0,4);
         }
         catch(Exception ex)
         {
         }
         return SDate;
     }
     public String pureDate(String str)
     {
         String SDate="";
         str=str.trim();
         if(str.length()!=10)
            return str;
         try
         {
            str=str.trim();
            SDate = str.substring(6,10)+str.substring(3,5)+str.substring(0,2);
         }
         catch(Exception ex)
         {
         }
         return SDate;
     }

     public String parseZero(String str)
     {
          String SZero   = "";
          double  dZero  = 0;
          try
          {
               dZero   = Double.parseDouble(str);
          }catch(Exception ex){dZero=0;}
          SZero = String.valueOf(dZero);
          return SZero;
     }
     public double toDouble(String str)
     {
          double d=0;

          try
          {
               d = Double.parseDouble(str);       
          }
          catch(Exception ex)
          {
               d=0;
          }
          return d;
     }

     public int toInt(String str)
     {
          int d=0;

          try
          {
               d = Integer.parseInt(str);       
          }
          catch(Exception ex)
          {
               d=0;
          }
          return d;
     }
     public float toFloat(String str)
     {
          float d=0;

          try
          {
               d = Float.parseFloat(str);       
          }
          catch(Exception ex)
          {
               d=0;
          }
          return d;
     }

     //Date in Normal Form. Str1 = End Date & Str2 = Start Date
     public String getDateDiff(String str1,String str2)
     {
            int iYear1,iMonth1,iDay1;
            int iYear2,iMonth2,iDay2;

            try
            {
                  iYear1  = toInt(str1.substring(6,10))-1900;
                  iMonth1 = toInt(str1.substring(3,5))-1;
                  iDay1   = toInt(str1.substring(0,2));
            }
            catch(Exception ex)
            {
                  System.out.println(str1+"1st"+ex);
                  return "";
            }
            try
            {
                  iYear2  = toInt(str2.substring(6,10))-1900;
                  iMonth2 = toInt(str2.substring(3,5))-1;
                  iDay2   = toInt(str2.substring(0,2));
            }
            catch(Exception ex)
            {
                  System.out.println(str2+"2nd"+ex);
                  return "";
            }

            java.util.Date dt1 = new java.util.Date(iYear1,iMonth1,iDay1);
            java.util.Date dt2 = new java.util.Date(iYear2,iMonth2,iDay2);

            long num1 = dt1.getTime()-dt2.getTime();
            long diff = num1/(24*60*60*1000);
            return String.valueOf(diff);
     }
     public String getDate(String str1,long days,int Sig)
     {
            String SDay="",SMonth="",SYear="";
            int iYear1=0,iMonth1=0,iDay1=0;
            try
            {
               iYear1  = toInt(str1.substring(6,10))-1900;
               iMonth1 = toInt(str1.substring(3,5))-1;
               iDay1   = toInt(str1.substring(0,2));
            }
            catch(Exception ex)
            {
               return "0";
            }
            java.util.Date dt1 = new java.util.Date(iYear1,iMonth1,iDay1);

            long num1 = dt1.getTime();
            long num2 = (days*24*60*60*1000);
            if (Sig == -1)
                  dt1.setTime(num1-num2);
            else
                  dt1.setTime(num1+num2);

            int iDay   = dt1.getDate();
            int iMonth = dt1.getMonth()+1;
            int iYear  = dt1.getYear()+1900;

            SDay   = (iDay<10 ?"0"+iDay:""+iDay);
            SMonth = (iMonth<10 ?"0"+iMonth:""+iMonth);
            SYear  = ""+iYear;
            return SDay+"."+SMonth+"."+SYear;
     }
     public String getSQ(String str,Vector VVector,String SChar)
     {
                String QS=" ";
                String sF=(SChar=="N"?"0":"'");
                String sL=(SChar=="N"?"":"'");
                for(int i=0;i<VVector.size();i++)
                        QS=QS+str+"="+sF+(String)VVector.elementAt(i)+sL+" Or ";
                try
                {
                    QS = QS.substring(0,QS.length()-4);
                    QS = " And "+"("+QS+")";
                }
                catch(Exception ex)
                {
                    QS = "";
                }
                return QS;
     }
     public String getSQ(String str,String STokenable,String SChar)
     {
                Vector VVector = new Vector();
                StringTokenizer ST = new StringTokenizer(STokenable,",");
                while(ST.hasMoreTokens())
                {
                        VVector.addElement(ST.nextToken());
                }
                return getSQ(str,VVector,SChar);
     }
     public String getSQ(String str1,String str2,JTextField TValue,String SChar)
     {
            String QS = "";
            String SOp = (SChar == "G"?">=":"<=");
            QS = str1+SOp+str2+"("+str2+TValue.getText()+"/100)";
            return " and "+QS;
     }
     public void warn(JLayeredPane DeskTop,StatusPanel SPanel)
     {
          String SFileName  = "Errorc.prn";
          JInternalFrame jf = new JInternalFrame(SFileName);
          Notepad        np = new Notepad(new File(SFileName),SPanel);
          jf.getContentPane().add("Center",np);
          jf.show();
          jf.setBounds(80,100,550,350);
          jf.setClosable(true);
          jf.setMaximizable(true);
          jf.setTitle("Omissions");
          try
          {
               DeskTop.add(jf);
               jf.moveToFront();
               jf.setSelected(true);
               np.editor.setForeground(Color.white);
               np.editor.setBackground(Color.red);
               np.editor.setFont(new Font("monospaced", Font.PLAIN, 18));
               np.editor.setEditable(false);
               DeskTop.repaint();
          }
          catch(java.beans.PropertyVetoException ex){}
     }

      // SOp - operation : "S" - SUM; "A" - Average
      public String getSum(Vector Vx,String SOp)
      {
            double dAmount=0.0;   
            for(int i=0;i<Vx.size();i++)
            {
                dAmount = dAmount+toDouble(getRound((String)Vx.elementAt(i),2));
            }
            if(SOp.equals("A"))
            {
              dAmount = dAmount/Vx.size();
            }
            return getRound(dAmount,3);
      }
     public int indexOf(Vector vect,String str)
     {
           int iindex=-1;
           str = str.trim();
           for(int i=0;i<vect.size();i++)
           {
                  String str1 = (String)vect.elementAt(i);
                  str1 = str1.trim();
                  if(str1.startsWith(str))
                        return i;
           }
           return iindex;
     }

     //Report format for Consolidated Production Reports
     public Vector ConsHead()
     {
          Vector VHead = new Vector();

          String Str1 = Replicate("-",199)+"\n";
          String Str2 = Space(4)+Space(2)+Cad("Hank Produced",22)+Space(2)+
                        Cad("Production",31)+Space(2)+Cad("Actual Efficiency",22)+Space(2)+
                        Cad("Target Vs Actual Variation",31)+Space(2)+
                        Cad("Expected Vs Actual Variation",31)+Space(2)+
                        Cad("Worked Hours",22)+Space(2)+Cad("Utilization",22)+"\n";

          String Str3 = Space(4)+Space(2)+Replicate("-",193)+"\n";
          String Str4 = Pad("Mach",4)+Space(2)+
                        Rad("I",6)+Space(2)+Rad("II",6)+Space(2)+Rad("III",6)+Space(2)+
                        Rad("I",9)+Space(2)+Rad("II",9)+Space(2)+Rad("III",9)+Space(2)+
                        Rad("I",6)+Space(2)+Rad("II",6)+Space(2)+Rad("III",6)+Space(2)+
                        Rad("I",9)+Space(2)+Rad("II",9)+Space(2)+Rad("III",9)+Space(2)+
                        Rad("I",9)+Space(2)+Rad("II",9)+Space(2)+Rad("III",9)+Space(2)+
                        Rad("I",6)+Space(2)+Rad("II",6)+Space(2)+Rad("III",6)+Space(2)+
                        Rad("I",6)+Space(2)+Rad("II",6)+Space(2)+Rad("III",6)+"\n";

          String Str5 = Pad("No.",4)+Space(2)+
                        Rad(" ",6)+Space(2)+Rad(" ",6)+Space(2)+Rad(" ",6)+Space(2)+
                        Rad("Kgs",9)+Space(2)+Rad("Kgs",9)+Space(2)+Rad("Kgs",9)+Space(2)+
                        Rad("%",6)+Space(2)+Rad("%",6)+Space(2)+Rad("%",6)+Space(2)+
                        Rad("Kgs",9)+Space(2)+Rad("Kgs",9)+Space(2)+Rad("Kgs",9)+Space(2)+
                        Rad("Kgs",9)+Space(2)+Rad("Kgs",9)+Space(2)+Rad("Kgs",9)+Space(2)+
                        Rad("Hrs",6)+Space(2)+Rad("Hrs",6)+Space(2)+Rad("Hrs",6)+Space(2)+
                        Rad("%",6)+Space(2)+Rad("%",6)+Space(2)+Rad("%",6)+"\n";

          VHead.addElement(Str1);
          VHead.addElement(Str2);
          VHead.addElement(Str3);
          VHead.addElement(Str4);
          VHead.addElement(Str3);
          VHead.addElement(Str5);
          VHead.addElement(Str1);

          return VHead;
     }
     public String validString(ResultSet res,int iCol)
     {
          try
          {
               return res.getString(iCol);
          }
          catch(Exception ex)
          {
               return "0";
          }
     }
     public String getStartDate(String SDate)
     {
           return (SDate.substring(0,6)+"01");
     }

     //To return no of days from the begining of the month
     public int getDays(String SDate)
     {
          return toInt(SDate.substring(6,8));    
     }

     //to return no of machines in a dept
     public String getMachines(String SDept,String SUnit)
     {
          String retStr = "SELECT count(*) FROM machine WHERE Unit_Code = "+SUnit+" AND Dept_Code = "+SDept;
          return retStr;
     }

     public String getOraDSN()
     {
          //return SOraDSN;
        return "jdbc:oracle:thin:@172.16.2.28:1521:arun";        
//         return "jdbc:oracle:thin:@117.239.247.18:1530:arun";    
     }

	 //oracle oracle user name
         public String getOraUserName()
	 {
		  //return SUserName;
            return "Prodc";
	 }

	 //oracle oracle Password
         public String getOraPassword()
	 {
		  //return SPassword;
            return "prodc";
	 }
     public int isLeap(int iYear)
     {
          double dYear = iYear;
          if(Math.IEEEremainder(dYear,100.0)==0)
               return 1;
          else if(Math.IEEEremainder(dYear,4.0)==0)
               return 1;
          else
               return 0;
     }

     public String getMonthDays(int iMonth)
     {
            String retStr = "";
            String SDay;
            int    iYear = toInt(String.valueOf(iMonth).substring(0,4));
            switch (iMonth)
            {
                  case 1:
                        SDay="31";
                        break;   
                  case 2:
                        if(isLeap(iYear)==1)
                           SDay="29";
                        else
                           SDay="28";
                        break;   
                  case 3:
                        SDay="31";
                        break;   
                  case 4:
                        SDay="30";
                        break;   
                  case 5:
                        SDay="31";
                        break;   
                  case 6:
                        SDay="30";
                        break;   
                  case 7:
                        SDay="31";
                        break;   
                  case 8:
                        SDay="31";
                        break;   
                  case 9:
                        SDay="30";
                        break;   
                  case 10:
                        SDay="31";
                        break;   
                  case 11:
                        SDay="30";
                        break;   
                  case 12:
                        SDay="31";
                        break;
                  default:
                        SDay="30";
                        break;
            }
            return (SDay);
     }

	public String getTableName(String st)
	{
		int i = Integer.parseInt(st);
		if(i==2)
			return "BlowTran";
		if(i==3)
			return "CardTran";
		if(i==4)
			return "CombTran";
		if(i==5)
			return "DrawTran";
		if(i==6)
			return "SimpTran";
		if(i==7)
			return "SpinTran";
		//if(i==9)
		return "SlapTran";


	}

	public String getUnit(String SUnit)
	{
		int i = Integer.parseInt(SUnit);
		if(i==1)
			return "A-Unit";
		if(i==2)
			return "B-Unit";
		if(i==3)
			return "C-Unit";
		return "General";
	}

	public String getShift(String SShift)
	{
		int i = Integer.parseInt(SShift);
		if(i==1)
			return "1st Shift";
		if(i==2)
			return "2nd Shift";
		if(i==3)
			return "3rd Shift";
		return "X Shift";
	}
	
	public String getDailyReportHeader(String STitle,String SUnit,String SShift,String SDate)
	{			
	   return "<p align='center' style='COLOR: maroon; FONT-SIZE: large; FONT-WEIGHT: bold'>" + STitle + "</p>" +
		     "<p align='left' style='FONT-SIZE: small; FONT-WEIGHT: bold'>" +
		     "<font color='mediumblue'>Unit&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple> " + getUnit(SUnit) + "</font>&nbsp;&nbsp;&nbsp;&nbsp;" +
		     "<font color='mediumblue'>Shift&nbsp;&nbsp;:&nbsp;</font><font color=purple>" + getShift(SShift) + "</font>&nbsp;&nbsp;&nbsp;&nbsp;" +
		     "<font color='mediumblue'>Date&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple>" + parseDate(SDate) + "</font>" + 
		     "</p>";
	}

	public String getDailyReportHeader(String STitle,String SUnit,String SShift,String SDate,String RepType)
	{			
	   return "<p align='center' style='COLOR: maroon; FONT-SIZE: large; FONT-WEIGHT: bold'>" + STitle + "</p>" +
		     "<p align='left' style='FONT-SIZE: small; FONT-WEIGHT: bold'>" +
		     "<font color='mediumblue'>Unit&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple> " + getUnit(SUnit) + "</font>&nbsp;&nbsp;&nbsp;&nbsp;" +
		     "<font color='mediumblue'>Shift&nbsp;&nbsp;:&nbsp;</font><font color=purple>" + getShift(SShift) + "</font>&nbsp;&nbsp;&nbsp;&nbsp;" +
		     "<font color='mediumblue'>Date&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple>" + parseDate(SDate) + "</font>" + 
		     "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + 
			"<font color='mediumblue'><u>" + RepType + " </u></font>" + 
		     "</p>";
	}

	public String getStopBreakupHeader(String STitle,String SUnit,String SShift,String SDate,String SDept)
	{			
	   return "<p align='center' style='COLOR: maroon; FONT-SIZE: large; FONT-WEIGHT: bold'>" + STitle + "</p>" +
		     "<p align='left' style='FONT-SIZE: small; FONT-WEIGHT: bold'>" +
		     "<font color='mediumblue'>Department&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple>" + SDept + "</font>&nbsp;&nbsp;&nbsp;&nbsp;" + 
	  	     "<font color='mediumblue'>Unit&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple> " + getUnit(SUnit) + "</font>&nbsp;&nbsp;&nbsp;&nbsp;" +
		     "<font color='mediumblue'>Shift&nbsp;&nbsp;:&nbsp;</font><font color=purple>" + getShift(SShift) + "</font>&nbsp;&nbsp;&nbsp;&nbsp;" +
		     "<font color='mediumblue'>Date&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple>" + parseDate(SDate) + "</font>" + 
		     "</p>";
	}

	public String getStopReportHeader(String STitle,String SUnit,String SShift,String SDate,String SDept)
	{			
	   return "<p align='center' style='COLOR: maroon; FONT-SIZE: large; FONT-WEIGHT: bold'>" + STitle + "</p>" +
		     "<p align='left' style='FONT-SIZE: small; FONT-WEIGHT: bold'>" +
		     "<font color='mediumblue'>Department&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple>" + SDept + "</font>&nbsp;&nbsp;&nbsp;&nbsp;" + 
		     "<font color='mediumblue'>Unit&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple> " + getUnit(SUnit) + "</font>&nbsp;&nbsp;&nbsp;&nbsp;" +
		     "<font color='mediumblue'>Shift&nbsp;&nbsp;:&nbsp;</font><font color=purple>" + getShift(SShift) + "</font>&nbsp;&nbsp;&nbsp;&nbsp;" +
		     "<font color='mediumblue'>Date&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple>" + parseDate(SDate) + "</font>&nbsp;&nbsp;&nbsp;&nbsp;" + 

		     "</p>";
	}

	public String getStopPeriodReportHeader(String STitle,String SUnit,String SStDate,String SEnDate,String SDept)
	{			
	   return "<p align='center' style='COLOR: maroon; FONT-SIZE: large; FONT-WEIGHT: bold'>" + STitle + "</p>" +
		     "<p align='left' style='FONT-SIZE: small; FONT-WEIGHT: bold'>" +
		     "<font color='mediumblue'>Department&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple>" + SDept + "</font>&nbsp;&nbsp;&nbsp;&nbsp;" + 
		     "<font color='mediumblue'>Unit&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple> " + getUnit(SUnit) + "</font>&nbsp;&nbsp;&nbsp;&nbsp;" +
		     "<font color='mediumblue'>Start Date&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple>" + parseDate(SStDate) + "</font>&nbsp;&nbsp;&nbsp;&nbsp;" + 
		     "<font color='mediumblue'>End Date&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple>" + parseDate(SEnDate) + "</font>" + 
		     "</p>";
	}


	public String getUnitDateHeader(String STitle,String SUnit,String SDate)
	{			
	   return "<p align='center' style='COLOR: maroon; FONT-SIZE: large; FONT-WEIGHT: bold'>" + STitle + "</p>" +
		     "<p align='left' style='FONT-SIZE: small; FONT-WEIGHT: bold'>" +
		     "<font color='mediumblue'>Unit&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple> " + getUnit(SUnit) + "</font>&nbsp;&nbsp;&nbsp;&nbsp;" +
		     "<font color='mediumblue'>Date&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple>" + parseDate(SDate) + "</font>" + 
		     "</p>";
	}

	public String getConsReportHeader(String STitle,String SUnit,String SStDate,String SEnDate)
	{			
	   return "<p align='center' style='COLOR: maroon; FONT-SIZE: large; FONT-WEIGHT: bold'>" + STitle + "</p>" +
		     "<p align='left' style='FONT-SIZE: small; FONT-WEIGHT: bold'>" +
		     "<font color='mediumblue'>Unit&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple> " + getUnit(SUnit) + "</font>&nbsp;&nbsp;&nbsp;&nbsp;" +
		     "<font color='mediumblue'>Start Date&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple>" + parseDate(SStDate) + "</font>&nbsp;&nbsp;&nbsp;&nbsp;" + 
		     "<font color='mediumblue'>End Date&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple>" + parseDate(SEnDate) + "</font>" + 
		     "</p>";
	}

	public String getChairmanReportHeader(String STitle,String SDate)
	{			
	   return "<p align='center' style='COLOR: maroon; FONT-SIZE: large; FONT-WEIGHT: bold'>" + STitle + "</p>" +
		     "<p align='left' style='FONT-SIZE: small; FONT-WEIGHT: bold'>" +
		     "<font color='mediumblue'>Date&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple>" + parseDate(SDate) + "</font>" + 
		     "</p>";
	}
    public int getPreviousMonth(int iMonth)
    {
          String SMonth = null;
          SMonth = String.valueOf(iMonth);
          int iYear = toInt(SMonth.substring(0,4));
          String SPreMonth = String.valueOf(iMonth-1);
          if(SPreMonth.substring(4,6).equals("00"))
               SPreMonth=String.valueOf(iYear-1)+"12";
           return toInt(SPreMonth);
    }
public String getMonthEndDate1(String SDate)
     {
            String retStr = "";
            int    iMonth = toInt(SDate.substring(4,6));
            int    iYear  = toInt(SDate.substring(0,4));
            String SDay;
            switch (iMonth)
            {
                  case 1:
                        SDay="31";
                        break;   
                  case 2:
                        if(isLeap2(iYear))
                           SDay="29";
                        else
                           SDay="28";
                        break;   
                  case 3:
                        SDay="31";
                        break;   
                  case 4:
                        SDay="30";
                        break;   
                  case 5:
                        SDay="31";
                        break;   
                  case 6:
                        SDay="30";
                        break;   
                  case 7:
                        SDay="31";
                        break;   
                  case 8:
                        SDay="31";
                        break;   
                  case 9:
                        SDay="30";
                        break;   
                  case 10:
                        SDay="31";
                        break;   
                  case 11:
                        SDay="30";
                        break;   
                  case 12:
                        SDay="31";
                        break;
                  default:
                        SDay="30";
                        break;
            }
            return (SDate.substring(0,6)+SDay);
     }
public boolean isLeap2(int iYear){
 
      double dYear = iYear;
      if(Math.IEEEremainder(dYear,100.0)==0)
         return true;
      else if(Math.IEEEremainder(dYear,4.0)==0)
         return true;
      else
         return false;
   }
// From Server
     public int getNextDate(int int1)
     {

          int iDate=0;

          String SQry="";
          SQry= "Select to_char(to_Date('"+int1+"','YYYYMMDD')+1,'YYYYMMDD')  from dual";

          try
          {
            Class.forName("oracle.jdbc.OracleDriver");
            Connection   theConnection = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","system","hammer");
//            Connection   theConnection = DriverManager.getConnection("jdbc:oracle:thin:@117.239.247.18:1530:arun","system","hammer");
               
			Statement theStatement = theConnection.createStatement();
            ResultSet theResult = theStatement.executeQuery(SQry);
            if(theResult.next())
                    iDate= theResult.getInt(1);
            theResult.close();
            theStatement.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               return 0;
          }
          return iDate;
     }
     
      public int getNextMonth(int iMonth)
   {
      iMonth = toInt(String.valueOf(iMonth).substring(0,6));
      String SMonth = String.valueOf(iMonth);
      String STmp = "";
      int iYear = toInt(SMonth.substring(0,4));
      int iNextMonth = iMonth+1;
      if(String.valueOf(iNextMonth).substring(4,6).equals("13"))
         STmp=String.valueOf(iYear+1)+"01";
      else
         STmp=String.valueOf(iNextMonth);
      return toInt(STmp);
   }
      public java.util.List getMonthList(int iStartMonth,int iEndMonth)
    {
      java.util.List ls = new java.util.ArrayList();
        ls.add(iStartMonth);
      while(iStartMonth<iEndMonth)
      {
         int    iMonth = getNextMonth(iStartMonth);
         String smonth    = String.valueOf(iMonth);
         ls.add(smonth);
         iStartMonth=iMonth;
      }
      return ls;
    }
      
       public int getMonths(int iStartMonth,int iEndMonth)
   {
      int iCntr=0;

      while(iStartMonth<=iEndMonth)
      {
         iCntr++;
         int iMonth = getNextMonth(iStartMonth);
         iStartMonth=iMonth;
      }
      return iCntr;
    }
    public String getPrintPath(){
	
           // System.out.println("Inside Common");
		String SFilePath="";
		String workingdir = System.getProperty("user.dir");
		String sfile = "One.txt";
		File fileName = new File(workingdir,sfile);
		File[] root = fileName.listRoots();
                
		String OSName = System.getProperty("os.name");
		String SActFile="";
		for (int i=0;i<root.length ;i++ ){
			String SCurFile = root[i].getAbsolutePath();
			if(OSName.startsWith("Lin")){
				SActFile = SCurFile+"software"+"C-Prod-Print"+System.getProperty("file.separator");
				break;
			}
                        else{
				if(SCurFile.startsWith("D")){
					SActFile = SCurFile+"C-Prod-Print"+System.getProperty("file.separator");
					break;
				}
			}
		}
                SFilePath = SActFile+System.getProperty("file.separator");
//                System.out.println("ActFile-->"+SActFile);
//                System.out.println("Print File Path-->"+SFilePath);
		return SFilePath;
                
	 }
      
}
