
package CProduction;

public class OEOverAllProductionClass {
    String SSpinDate,SSmxSpDate,DrgSpDate,SBobbinStockDate,SPackedDate,SOEPackedDate,OESpinDate,CdgSpDate,SAcProdDate,SCheeseStockDate;
    String SStopDate,SStopCode="",SStopMachCode="";
    double dSpinProd,dCount,dSpdlWkd,dTarVsAct,dUtil,dAvgGps,dHoursRun,dHoursMeter,dActualKw,dAllotHour,dNoOfSpdl,dGmsSpdl,
           dSpgPackedProd,dOEPackedProd,dPneumafilWaste,dStopMint,dCheeseStock;
    int    iOEStatus,iStopShift=0;
    // For OE
    double OEProd,OECount,OERotWkd,OETarVsAct,OEUtil,OEAvgGps,OEHoursRun,OEHoursMeter,OEActualKw,OEAllotHour,OENoOfRot,OEGmsSpdl,OESmxUe,OEPneumafilWaste;
    
    double DrgUe,CdgUe; 
	double dAcProduction,dAcHardWaste;              
    public OEOverAllProductionClass()
    {
        
    }

    public double getdNoOfSpdl() {
        return dNoOfSpdl;
    }

    public void setdNoOfSpdl(double dNoOfSpdl) {
        this.dNoOfSpdl = dNoOfSpdl;
    }

    public String getSSpinDate() {
        return SSpinDate;
    }

    public void setSSpinDate(String SSpinDate) {
        this.SSpinDate = SSpinDate;
    }

    public double getdActualKw() {
        return dActualKw;
    }

    public void setdActualKw(double dActualKw) {
        this.dActualKw = dActualKw;
    }

    public double getdAllotHour() {
        return dAllotHour;
    }

    public void setdAllotHour(double dAllotHour) {
        this.dAllotHour = dAllotHour;
    }

    public double getdAvgGps() {
        return dAvgGps;
    }

    public void setdAvgGps(double dAvgGps) {
        this.dAvgGps = dAvgGps;
    }

    public double getdCount() {
        return dCount;
    }

    public void setdCount(double dCount) {
        this.dCount = dCount;
    }

    public double getdHoursMeter() {
        return dHoursMeter;
    }

    public void setdHoursMeter(double dHoursMeter) {
        this.dHoursMeter = dHoursMeter;
    }

    public double getdHoursRun() {
        return dHoursRun;
    }

    public void setdHoursRun(double dHoursRun) {
        this.dHoursRun = dHoursRun;
    }

    public double getdSpdlWkd() {
        return dSpdlWkd;
    }

    public void setdSpdlWkd(double dSpdlWkd) {
        this.dSpdlWkd = dSpdlWkd;
    }

    public double getdSpinProd() {
        return dSpinProd;
    }

    public void setdSpinProd(double dSpinProd) {
        this.dSpinProd = dSpinProd;
    }

    public double getdTarVsAct() {
        return dTarVsAct;
    }

    public void setdTarVsAct(double dTarVsAct) {
        this.dTarVsAct = dTarVsAct;
    }

    public double getdUtil() {
        return dUtil;
    }

    public void setdUtil(double dUtil) {
        this.dUtil = dUtil;
    }

    public double getdGmsSpdl() {
        return dGmsSpdl;
    }

    public void setdGmsSpdl(double dGmsSpdl) {
        this.dGmsSpdl = dGmsSpdl;
    }


    public String getPackedDate() {
        return SPackedDate;
    }

    public void setPackedDate(String SPackedDate) {
        this.SPackedDate = SPackedDate;
    }

    public double getPackedProd() {
        return dSpgPackedProd;
    }

    public void setPackedProd(double dSpgPackedProd) {
        this.dSpgPackedProd = dSpgPackedProd;
    }

    public int getOEStatus() {
        return iOEStatus;
    }

    public void setOEStatus(int iOEStatus) {
        this.iOEStatus = iOEStatus;
    }

    public String getOEPackedDate() {
        return SOEPackedDate;
    }

    public void setOEPackedDate(String SOEPackedDate) {
        this.SOEPackedDate = SOEPackedDate;
    }

    public String getSPackedDate() {
        return SPackedDate;
    }

    public void setSPackedDate(String SPackedDate) {
        this.SPackedDate = SPackedDate;
    }

    public double getOEPackedProd() {
        return dOEPackedProd;
    }

    public void setOEPackedProd(double dOEPackedProd) {
        this.dOEPackedProd = dOEPackedProd;
    }

    public double getdOEPackedProd() {
        return dOEPackedProd;
    }

    public void setdOEPackedProd(double dOEPackedProd) {
        this.dOEPackedProd = dOEPackedProd;
    }

    public int getiOEStatus() {
        return iOEStatus;
    }

    public void setiOEStatus(int iOEStatus) {
        this.iOEStatus = iOEStatus;
    }

    public String getSOEPackedDate() {
        return SOEPackedDate;
    }

    public void setSOEPackedDate(String SOEPackedDate) {
        this.SOEPackedDate = SOEPackedDate;
    }

    public double getdPneumafilWaste() {
        return dPneumafilWaste;
    }

    public void setdPneumafilWaste(double dPneumafilWaste) {
        this.dPneumafilWaste = dPneumafilWaste;
    }

    public String getStopCode() {
        return SStopCode;
    }

    public void setStopCode(String SStopCode) {
        this.SStopCode = SStopCode;
    }

    public String getStopDate() {
        return SStopDate;
    }

    public void setStopDate(String SStopDate) {
        this.SStopDate = SStopDate;
    }

    public String getStopMachCode() {
        return SStopMachCode;
    }

    public void setStopMachCode(String SStopMachCode) {
        this.SStopMachCode = SStopMachCode;
    }

    public int getStopShift() {
        return iStopShift;
    }

    public void setStopShift(int iStopShift) {
        this.iStopShift = iStopShift;
    }

    public double getStopMint() {
        return dStopMint;
    }

    public void setStopMint(double dStopMint) {
        this.dStopMint = dStopMint;
    }
    
    // For OE Prod

    public double getOEActualKw() {
        return OEActualKw;
    }

    public void setOEActualKw(double OEActualKw) {
        this.OEActualKw = OEActualKw;
    }

    public double getOEAllotHour() {
        return OEAllotHour;
    }

    public void setOEAllotHour(double OEAllotHour) {
        this.OEAllotHour = OEAllotHour;
    }

    public double getOEAvgGps() {
        return OEAvgGps;
    }

    public void setOEAvgGps(double OEAvgGps) {
        this.OEAvgGps = OEAvgGps;
    }

    public double getOECount() {
        return OECount;
    }

    public void setOECount(double OECount) {
        this.OECount = OECount;
    }

    public double getOEGmsSpdl() {
        return OEGmsSpdl;
    }

    public void setOEGmsSpdl(double OEGmsSpdl) {
        this.OEGmsSpdl = OEGmsSpdl;
    }

    public double getOEHoursMeter() {
        return OEHoursMeter;
    }

    public void setOEHoursMeter(double OEHoursMeter) {
        this.OEHoursMeter = OEHoursMeter;
    }

    public double getOEHoursRun() {
        return OEHoursRun;
    }

    public void setOEHoursRun(double OEHoursRun) {
        this.OEHoursRun = OEHoursRun;
    }

    public double getOENoOfRot() {
        return OENoOfRot;
    }

    public void setOENoOfRot(double OENoOfRot) {
        this.OENoOfRot = OENoOfRot;
    }

    public double getOEProd() {
        return OEProd;
    }

    public void setOEProd(double OEProd) {
        this.OEProd = OEProd;
    }

    public double getOERotWkd() {
        return OERotWkd;
    }

    public void setOERotWkd(double OERotWkd) {
        this.OERotWkd = OERotWkd;
    }

    public double getOESmxUe() {
        return OESmxUe;
    }

    public void setOESmxUe(double OESmxUe) {
        this.OESmxUe = OESmxUe;
    }

    public double getOETarVsAct() {
        return OETarVsAct;
    }

    public void setOETarVsAct(double OETarVsAct) {
        this.OETarVsAct = OETarVsAct;
    }

    public double getOEUtil() {
        return OEUtil;
    }

    public void setOEUtil(double OEUtil) {
        this.OEUtil = OEUtil;
    }

    public double getOEPneumafilWaste() {
        return OEPneumafilWaste;
    }

    public void setOEPneumafilWaste(double OEPneumafilWaste) {
        this.OEPneumafilWaste = OEPneumafilWaste;
    }

    public String getOESpinDate() {
        return OESpinDate;
    }

    public void setOESpinDate(String OESpinDate) {
        this.OESpinDate = OESpinDate;
    }


    public double getCdgUe() {
        return CdgUe;
    }

    public void setCdgUe(double CdgUe) {
        this.CdgUe = CdgUe;
    }

    

    public double getDrgUe() {
        return DrgUe;
    }

    public void setDrgUe(double DrgUe) {
        this.DrgUe = DrgUe;
    }

    public String getCdgSpDate() {
        return CdgSpDate;
    }

    public void setCdgSpDate(String CdgSpDate) {
        this.CdgSpDate = CdgSpDate;
    }

    public String getDrgSpDate() {
        return DrgSpDate;
    }

    public void setDrgSpDate(String DrgSpDate) {
        this.DrgSpDate = DrgSpDate;
    }
    public double getdAcHardWaste() {
        return dAcHardWaste;
    }

    public void setdAcHardWaste(double dAcHardWaste) {
        this.dAcHardWaste = dAcHardWaste;
    }

    public double getdAcProduction() {
        return dAcProduction;
    }

    public void setdAcProduction(double dAcProduction) {
        this.dAcProduction = dAcProduction;
    }
    public String getSAcProdDate() {
        return SAcProdDate;
    }

    public void setSAcProdDate(String SAcProdDate) {
        this.SAcProdDate = SAcProdDate;
    }       

    public String getSCheeseStockDate() {
        return SCheeseStockDate;
    }

    public void setSCheeseStockDate(String SCheeseStockDate) {
        this.SCheeseStockDate = SCheeseStockDate;
    }
    public double getdCheeseStock() {
        return dCheeseStock;
    }

    public void setdCheeseStock(double dCheeseStock) {
        this.dCheeseStock = dCheeseStock;
    }
    
}
