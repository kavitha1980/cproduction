package CProduction;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.net.*;

public class WorkerPeriodCritPanelFinal extends JPanel 
{
	JLabel  LUnit;
	JLabel  LDept;
	JLabel  LDate;
	JLabel  LDateTo;
	JComboBox JCEmp;
	String SURL;
	int iUnit,iDept;
	String SDate,SDateTo;
	Vector VEmpName,VEmpCode;
	

	public WorkerPeriodCritPanelFinal(String SURL,String STitle,int iUnit,int iDept,String SDate,String SDateTo,Vector VEmpName,Vector VEmpCode)
	{
		this.SURL     = SURL;
		this.iUnit    = iUnit;
		this.iDept    = iDept;
		this.SDate    = SDate;
		this.SDateTo  = SDateTo;
		this.VEmpName = VEmpName;
		this.VEmpCode = VEmpCode;

		createComponents();
		setLayouts();
		addComponents();
		setPresets();
	}

	private void setLayouts()
	{
		setLayout(new GridLayout(5,2,5,10));
		setPreferredSize(new Dimension(200,150));
	}


	private void createComponents()
	{
		LUnit  = new JLabel();
		LDept  = new JLabel();
		LDate  = new JLabel();
		LDateTo= new JLabel();
		JCEmp  = new JComboBox(VEmpName);
	}

	private void addComponents()
	{
		add(new JLabel("Unit"));
		add(LUnit);
		add(new JLabel("Department"));
		add(LDept);
		add(new JLabel("Start Date"));
		add(LDate);
		add(new JLabel("End Date"));
		add(LDateTo);
		add(new JLabel("Sider"));
		add(JCEmp);
	}

	private void setPresets()
	{
		if(iUnit == 1)
			LUnit.setText("A-Unit");
		if(iUnit == 2)
			LUnit.setText("B-Unit");
		if(iUnit == 3)
			LUnit.setText("General");
		if(iUnit == 4)
			LUnit.setText("C-Unit");

		if(iDept == 2 )
			LDept.setText("BlowRoom");
		if(iDept == 3 )
			LDept.setText("Carding");
		if(iDept == 9 )
			LDept.setText("SL/RL");
		if(iDept == 4 )
			LDept.setText("Comber");
		if(iDept == 5 )
			LDept.setText("Drawing");
		if(iDept == 6 )
			LDept.setText("Simplex");
		if(iDept == 7 )
			LDept.setText("Spinning");

		LDate.setText(SDate);
		LDateTo.setText(SDateTo);
	}



	public URL getURL() throws Exception
	{
		URL theURL = new URL(SURL + getQueryString());
		return theURL;
	}

	public String getQueryString()
	{
		String QS = "?";
		QS = QS + getUnitQS();
		QS = QS + "&" + getDateQS();
		QS = QS + "&" + getDateToQS();
		QS = QS + "&" + getDeptQS();
		QS = QS + "&" + getDeptNameQS();
		QS = QS + "&" + getEmpCodeQS();
		QS = QS + "&" + getEmpNameQS();
		return QS;
	}

	private String getUnitQS()
	{
		return "Unit=" + iUnit;
	}

	private String getDateQS()
	{
		return "Date=" + SDate;
	}
	private String getDateToQS()
	{
		return "DateTo=" + SDateTo;
	}
	private String getDeptQS()
	{
		return "Dept=" + iDept;
	}
	private String getDeptNameQS()
	{
		return "DeptName=" + LDept.getText();
	}

	private String getEmpCodeQS()
	{
		return "EmpCode=" + (String)VEmpCode.elementAt(JCEmp.getSelectedIndex());
//		return "EmpCode=20604";
	}

	private String getEmpNameQS()
	{
          return "EmpName=" + (String)VEmpName.elementAt(JCEmp.getSelectedIndex());
//		return "EmpName=Selvi.P";
	}
}
