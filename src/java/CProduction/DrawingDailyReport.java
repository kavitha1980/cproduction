package CProduction;

import java.io.*;
import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;

public class DrawingDailyReport extends HttpServlet
{
HttpSession    session;
     Common common;

     Vector VDate,VUnit,VShift,VMachNo,VCount,VTicket,VName;
     Vector VOrderNo,VShade,VBreak,VSpeed,VSlHank,VHank,VBreaksPer100Km;
     Vector VHourMeter,VHourRun,VDelivery;
     Vector VHankExp,VHankTar,VProd,VProdExp,VProdTar;
     Vector VStopAcc,VStopNonAcc,VWorkerEffy,VProdLoss,VActualEffy;
     Vector VUEM,VUEOA,VEffyAvai,VHourAllot;
     Vector VNoRunOut,VNoPersons,VRoTime,VExcess;
     Vector VActualKW;

     double dBProd,dBProdExp,dBProdTar,dBEffy,dBHourRun,dBUEOA;
     double dFProd,dFProdExp,dFProdTar,dFEffy,dFHourRun,dFUEOA;

     String SDate,SShift,SUnit,SSupName,SUtilTar,SWorkTar;
     String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody,tbgBody;
     
       String SFile                     = "";
     String[]                         ColumnName,ColumnType,ColumnName1,ColumnType1,ColumnName2,ColumnType2,sBody;
     int[]                            iColumnWidth,iColumnWidth1,iColumnWidth2;

     private static Font catFont     = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
     private static Font redFont     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
     private static Font subFont     = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);

     private static Font bigBold     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
     private static Font bigNormal   = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);

     private static Font mediumBold  = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
     private static Font mediumNormal= new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font smallBold   = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD);
     private static Font smallNormal = new Font(Font.FontFamily.TIMES_ROMAN, 6, Font.NORMAL);

     private static Font tinyBold    = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.BOLD);
     private static Font tinyNormal  = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.NORMAL);

     private static Font underBold   = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.UNDERLINE);
    
     int    iWidth1[]   = {20,25,35,40,25,25,20,20,20,20,20,20,20,20,20,30,30,30,35,25,25,28,28,25,25,25,25,20,20,20};
     int    iWidth0[]   = {35,35};
     Document   document;
     PdfPTable  table1,table0;
     int        iUserCode;
     PdfWriter  writer      = null;

     FileWriter  FW;
     int Lctr = 70,Pctr = 0;
     String SLine;
     Vector VHead;
     boolean bFinalized;
     Control control = new Control();

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
          common = new Common();

          bgColor = common.bgColor;
          bgHead  = common.bgHead;
          bgUom   = common.bgUom;
          bgBody  = common.bgBody;
          fgHead  = common.fgHead;
          fgUom   = common.fgUom;
          fgBody  = common.fgBody;
          tbgBody = common.tbgBody;
          SLine   = common.Replicate("-",258)+"\n";
     }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
        
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        doPost(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);

		response.setContentType("text/html");
          PrintWriter out  = response.getWriter();
		session = request.getSession(true);
		/*if(session.getValue("AppletTag")==null)
		{
			String SServer = control.getID("Select ProdHost from Server");
			response.sendRedirect(SServer + "/SessionExpired");
			return;

		}*/
                
		setCriteria(request);
		setFinalized();
        out.println("<html><head><title>AMARJOTHI SPINNING MILLS LIMITED</title>");
        out.println("<link rel='stylesheet' type='text/css' href='css/header.css' />");
        out.println("<script src='css/stuHover.js' type='text/javascript'></script>");
        out.println("</head><body bgcolor='white' text='"+ fgBody+" '>");
          
          out.println("<p><a href='ProductionServlet'>Home</a></p>");
          //out.println(session.getValue("AppletTag"));
          out.println(common.getDailyReportHeader("Drawing Production Report (F DRG 1)",SUnit,SShift,SDate,getReportType()));
          out.println("  <center>");
          out.println("  <table border='1' cellspacing='0' width='1700' height='1'  >");
          out.println("    <tr>");
          out.println("      <td width='44' height='142' rowspan='4' valign='top' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Mach Nº</font></td>");
          out.println("      <td width='46' height='142' rowspan='4' valign='top' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Count</font></td>");
          out.println("      <td width='47' height='142' rowspan='4' valign='top' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Breaker/");
          out.println("        Finisher</font></td>");
          out.println("      <td width='47' height='142' rowspan='4' valign='top' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Tenter Nº</font></td>");
          out.println("      <td width='47' height='142' rowspan='4' valign='top' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Tenter Name</font></td>");
          out.println("      <td width='47' height='104' rowspan='3' valign='top' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Delivery&nbsp;");
          out.println("        Speed</font></td>");
          out.println("      <td width='52' height='104' rowspan='3' valign='top' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Delivery");
          out.println("        One/Two</font></td>");
          out.println("      <td width='44' height='104' rowspan='3' valign='top' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Sliver Hank</font></td>");
          out.println("      <td width='209' height='36' colspan='4' valign='middle' align='center' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>SC");
          out.println("        Time</font></td>");
          out.println("      <td width='427' height='34' colspan='8' valign='middle' align='center' bgcolor='"+bgHead+"'  >");
          out.println("       <font color='"+fgHead+"' size='4'>");
          out.println("       Production Details</font></td>");
          out.println("      <td width='211' height='34' colspan='4' valign='middle' align='center' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Utilization</font></td>");
          out.println("      <td width='231' height='34' colspan='4' valign='top' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Worker Efficiency</font></td>");
          out.println("      <td width='44' height='142' rowspan='4' valign='top' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Actual KW</font></td>");
          out.println("      <td width='46' height='142' rowspan='4' valign='top' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>UKG</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='50' height='53' valign='top' rowspan='2' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Nº of SC</font></td>");
          out.println("      <td width='49' height='53' valign='top' rowspan='2' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Nº of person</font></td>");
          out.println("      <td width='47' height='53' valign='top' rowspan='2' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Actual</font></td>");
          out.println("      <td width='45' height='53' valign='top' rowspan='2' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Breaks Per 100</font></td>");
          out.println("      <td width='164' height='21' valign='top' colspan='3' align='center' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Hank</font></td>");
          out.println("      <td width='205' height='21' valign='top' colspan='4' align='center' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Production</font></td>");
          out.println("      <td width='46' height='44' valign='top' rowspan='2' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Actual Effy</font></td>");
          out.println("      <td width='51' height='53' valign='top' rowspan='2' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Hours to be worked</font></td>");
          out.println("      <td width='56' height='53' valign='top' rowspan='2' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Hour Meter Reading</font></td>");
          out.println("      <td width='42' height='53' valign='top' rowspan='2' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>UxE (OA)</font></td>");
          out.println("      <td width='44' height='53' valign='top' rowspan='2' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>UxE");
          out.println("        (MU)</font></td>");
          out.println("      <td width='43' height='53' valign='top' rowspan='2' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Effy Avai lable</font></td>");
          out.println("      <td width='52' height='53' valign='top' rowspan='2' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Worker Effy</font></td>");
          out.println("      <td width='59' height='53' valign='top' rowspan='2' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Accep Stoppage</font></td>");
          out.println("      <td width='59' height='53' valign='top' rowspan='2' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Non Acc Stoppage</font></td>");
          
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='58' height='23' valign='top' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Expected</font></td>");
          out.println("      <td width='47' height='23' valign='top' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Target</font></td>");
          out.println("      <td width='47' height='23' valign='top' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Actual</font></td>");
          out.println("      <td width='58' height='23' valign='top' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Expected</font></td>");
          out.println("      <td width='46' height='23' valign='top' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Target</font></td>");
          out.println("      <td width='45' height='23' valign='top' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Actual</font></td>");
          out.println("      <td width='38' height='23' valign='top' bgcolor='"+bgHead+"'  ><font color='"+fgHead+"' size='4'>Prod Loss</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='47' height='31' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgUom+"'>m/Min</font></td>");
          out.println("      <td width='52' height='31' valign='top' bgcolor='"+bgUom+"' align='right'  ></td>");
          out.println("      <td width='44' height='31' valign='top' bgcolor='"+bgUom+"' align='right'  ></td>");
          out.println("      <td width='50' height='31' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgUom+"'>Nº</font></td>");
          out.println("      <td width='49' height='31' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgUom+"'>Nº</font></td>");
          out.println("      <td width='47' height='31' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgUom+"'>Min</font></td>");
          out.println("      <td width='45' height='31' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgUom+"'>Km</font></td>");
          out.println("      <td width='58' height='31' valign='top' bgcolor='"+bgUom+"' align='right'  ></td>");
          out.println("      <td width='47' height='31' valign='top' bgcolor='"+bgUom+"' align='right'  ></td>");
          out.println("      <td width='47' height='31' valign='top' bgcolor='"+bgUom+"' align='right'  ></td>");
          out.println("      <td width='58' height='31' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='46' height='31' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='45' height='31' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='38' height='31' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgUom+"'>Kgs</font></td>");
          out.println("      <td width='46' height='31' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='51' height='31' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgUom+"'>Hr</font></td>");
          out.println("      <td width='56' height='31' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgUom+"'>Hr</font></td>");
          out.println("      <td width='42' height='31' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='44' height='31' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='43' height='31' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='52' height='31' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='59' height='31' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgUom+"'>Hr</font></td>");
          out.println("      <td width='59' height='31' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgUom+"'>Hr</font></td>");
          out.println("    </tr>");
/*
          out.println("    <tr>");
          out.println("      <td width='959' height='45' valign='middle' bgcolor='"+bgHead+"' colspan='18'  >");
          out.println("        <p align='left'><b><font color='#FFFF00' size='4'>Target</font></b></p>");
          out.println("      </td>");
          out.println("      <td width='46' height='45' valign='top' bgcolor='"+bgHead+"'  >&nbsp;</td>");
          out.println("      <td width='113' height='45' valign='top' bgcolor='"+bgHead+"' colspan='2'  >&nbsp;</td>");
          out.println("      <td width='42' height='45' valign='top' bgcolor='"+bgHead+"'  >&nbsp;</td>");
          out.println("      <td width='44' height='45' valign='top' bgcolor='"+bgHead+"'  >&nbsp;</td>");
          out.println("      <td width='231' height='45' valign='top' bgcolor='"+bgHead+"' colspan='5'  >&nbsp;</td>");
          out.println("    </tr>");
*/
          setVectorData();
          calcVectors();

          try
          {
//               FW      = new FileWriter("/software/C-Prod-Print/Reports/drawc.prn");
              FW      = new FileWriter("D:/C-Prod-Print/drawc.prn");
               //FW      = new FileWriter("d:/drawbA.prn");
               Lctr    = 70;
               Pctr    =  0;
               Head();
               Body();
               FW.close();
               createPDFFile();
          }catch(Exception ex){}

          for(int i=0;i<VMachNo.size();i++)
          {
               out.println("    <tr>");
               out.println("      <td width='44' height='5' valign='top' bgcolor='"+tbgBody+"'  ><font  color='"+fgBody+"'>"+(String)VMachNo .elementAt(i)+"</font></td>");
               out.println("      <td width='46' height='5' valign='top' bgcolor='"+tbgBody+"'  ><font  color='"+fgBody+"'>"+(String)VCount .elementAt(i)+"</font></td>");
               out.println("      <td width='47' height='5' valign='top' bgcolor='"+tbgBody+"'  ><font  color='"+fgBody+"'>"+(String)VBreak .elementAt(i)+"</font></td>");
               out.println("      <td width='47' height='5' valign='top' bgcolor='"+tbgBody+"'  ><font  color='"+fgBody+"'>"+(String)VTicket .elementAt(i)+"</font></td>");
               out.println("      <td width='47' height='5' valign='top' bgcolor='"+tbgBody+"'  ><font  color='"+fgBody+"'>"+(String)VName .elementAt(i)+"</font></td>");
               out.println("      <td width='47' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'  ><font  color='"+fgBody+"'>"+(String)VSpeed .elementAt(i)+"</font></td>");
               out.println("      <td width='52' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'  ><font  color='"+fgBody+"'>"+(String)VDelivery .elementAt(i)+"</font></td>");
               out.println("      <td width='44' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'  ><font  color='"+fgBody+"'>"+(String)VSlHank .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VNoRunOut.elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VNoPersons.elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VRoTime.elementAt(i)+"</font></td>");
               out.println("      <td width='45' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'  ><font  color='"+fgBody+"'>"+(String)VBreaksPer100Km.elementAt(i)+"</font></td>");
               out.println("      <td width='58' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'  ><font  color='"+fgBody+"'>"+(String)VHankExp .elementAt(i)+"</font></td>");
               out.println("      <td width='47' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'  ><font  color='"+fgBody+"'>"+(String)VHankTar .elementAt(i)+"</font></td>");
               out.println("      <td width='47' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'  ><font  color='"+fgBody+"'>"+(String)VHank .elementAt(i)+"</font></td>");
               out.println("      <td width='58' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'  ><font  color='"+fgBody+"'>"+(String)VProdExp .elementAt(i)+"</font></td>");
               out.println("      <td width='46' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'  ><font  color='"+fgBody+"'>"+(String)VProdTar .elementAt(i)+"</font></td>");
               out.println("      <td width='45' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'  ><font  color='"+fgBody+"'>"+(String)VProd .elementAt(i)+"</font></td>");
               out.println("      <td width='38' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'  ><font  color='"+fgBody+"'>"+(String)VProdLoss .elementAt(i)+"</font></td>");
               out.println("      <td width='46' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'  ><font  color='"+fgBody+"'>"+(String)VActualEffy .elementAt(i)+"</font></td>");
               out.println("      <td width='51' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'  ><font  color='"+fgBody+"'>"+(String)VHourRun .elementAt(i)+"</font></td>");
               out.println("      <td width='56' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'  ><font  color='"+fgBody+"'>"+(String)VHourMeter .elementAt(i)+"</font></td>");
               out.println("      <td width='42' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'  ><font  color='"+fgBody+"'>"+(String)VUEOA .elementAt(i)+"</font></td>");
               out.println("      <td width='44' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'  ><font  color='"+fgBody+"'>"+(String)VUEM .elementAt(i)+"</font></td>");
               out.println("      <td width='43' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'  ><font  color='"+fgBody+"'>"+(String)VEffyAvai .elementAt(i)+"</font></td>");
               out.println("      <td width='52' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'  ><font  color='"+fgBody+"'>"+(String)VWorkerEffy .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'  ><font  color='"+fgBody+"'>"+(String)VStopAcc .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'  ><font  color='"+fgBody+"'>"+(String)VStopNonAcc .elementAt(i)+"</font></td>");
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>"+(String)VActualKW .elementAt(i)+"</font></td>");
               
               double dProduction  = common.toDouble((String)VProd .elementAt(i));
               double dWorkedHours = common.toDouble((String)VHourRun .elementAt(i));
               double dActualKW    = common.toDouble((String)VActualKW .elementAt(i));
               double dUKG         = (dWorkedHours*dActualKW)/dProduction;
               
               out.println("      <td width='59' height='5' valign='top' bgcolor='"+tbgBody+"' align='right'><font   color='"+fgBody+"'>"+common.getRound(dUKG,3)+"</font></td>");
               out.println("    </tr>");
          }

          out.println("    <tr>");
          out.println("      <td width='44' height='38' valign='top' bgcolor='"+bgUom+"'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='46' height='38' valign='top' bgcolor='"+bgUom+"'  ><font size='4' color='"+fgBody+"'>TOTAL/Avg.</font></td>");
          out.println("      <td width='47' height='38' valign='top' bgcolor='"+bgUom+"'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='47' height='38' valign='top' bgcolor='"+bgUom+"'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='47' height='38' valign='top' bgcolor='"+bgUom+"'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='47' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getSum(VSpeed,"A")+"</font></td>");
          out.println("      <td width='52' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='44' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getSum(VSlHank,"A")+"</font></td>");
          out.println("      <td width='50' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+"-"+"</font></td>");
          out.println("      <td width='49' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+"-"+"</font></td>");
          out.println("      <td width='47' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+"-"+"</font></td>");
          out.println("      <td width='58' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getSum(VBreaksPer100Km ,"S")+"</font></td>");
          out.println("      <td width='58' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getSum(VHankExp ,"S")+"</font></td>");
          out.println("      <td width='47' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getSum(VHankTar ,"S")+"</font></td>");
          out.println("      <td width='47' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getSum(VHank ,"S")+"</font></td>");
          out.println("      <td width='58' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getSum(VProdExp ,"S")+"</font></td>");
          out.println("      <td width='46' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getSum(VProdTar ,"S")+"</font></td>");
          out.println("      <td width='45' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getSum(VProd ,"S")+"</font></td>");
          out.println("      <td width='38' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getSum(VProdLoss ,"S")+"</font></td>");
          out.println("      <td width='46' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getSum(VActualEffy ,"A")+"</font></td>");
          out.println("      <td width='51' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getSum(VHourRun ,"S")+"</font></td>");
          out.println("      <td width='56' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getSum(VHourMeter ,"S")+"</font></td>");
          out.println("      <td width='42' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getSum(VUEOA ,"A")+"</font></td>");
          out.println("      <td width='44' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getSum(VUEM ,"A")+"</font></td>");
          out.println("      <td width='43' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getSum(VEffyAvai ,"A")+"</font></td>");
          out.println("      <td width='52' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getSum(VWorkerEffy ,"A")+"</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getSum(VStopAcc ,"S")+"</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getSum(VStopNonAcc ,"S")+"</font></td>");
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getSum(VActualKW ,"A")+"</font></td>");
          
          double dTotProduction  = common.toDouble(common.getSum(VProd ,"S"));
          double dTotWorkedHours = common.toDouble(common.getSum(VHourRun ,"S"));
          double dTotActualKW    = common.toDouble(common.getSum(VActualKW ,"A"));
          double dTotUKG         = (dTotWorkedHours*dTotActualKW)/dTotProduction;
               
          out.println("      <td width='59' height='5' valign='top' bgcolor='"+bgUom+"' align='right'><font  size='4' color='"+fgBody+"'>"+common.getRound(dTotUKG ,3)+"</font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='44' height='38' valign='top' bgcolor='"+bgUom+"'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='46' height='38' valign='top' bgcolor='"+bgUom+"'  ><font size='4' color='"+fgBody+"'>Breaker</font></td>");
          out.println("      <td width='47' height='38' valign='top' bgcolor='"+bgUom+"'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='47' height='38' valign='top' bgcolor='"+bgUom+"'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='47' height='38' valign='top' bgcolor='"+bgUom+"'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='47' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='52' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='44' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='50' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+"-"+"</font></td>");
          out.println("      <td width='49' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+"-"+"</font></td>");
          out.println("      <td width='47' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+"-"+"</font></td>");
          out.println("      <td width='45' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+"-"+"</font></td>");
          out.println("      <td width='58' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='47' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='47' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='58' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getRound(dBProdExp ,3)+"</font></td>");
          out.println("      <td width='46' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getRound(dBProdTar ,3)+"</font></td>");
          out.println("      <td width='45' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getRound(dBProd ,3)+"</font></td>");
          out.println("      <td width='38' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='46' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getRound(dBEffy ,3)+"</font></td>");
          out.println("      <td width='51' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getRound(dBHourRun ,3)+"</font></td>");
          out.println("      <td width='56' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='42' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getRound(dBUEOA ,3)+"</font></td>");
          out.println("      <td width='44' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='43' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='52' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("    </tr>");

          out.println("    <tr>");
          out.println("      <td width='44' height='38' valign='top' bgcolor='"+bgUom+"'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='46' height='38' valign='top' bgcolor='"+bgUom+"'  ><font size='4' color='"+fgBody+"'>Finisher</font></td>");
          out.println("      <td width='47' height='38' valign='top' bgcolor='"+bgUom+"'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='47' height='38' valign='top' bgcolor='"+bgUom+"'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='47' height='38' valign='top' bgcolor='"+bgUom+"'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='47' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='52' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='44' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='50' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+"-"+"</font></td>");
          out.println("      <td width='49' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+"-"+"</font></td>");
          out.println("      <td width='47' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+"-"+"</font></td>");
          out.println("      <td width='45' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+"-"+"</font></td>");
          out.println("      <td width='58' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='47' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='47' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='58' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getRound(dFProdExp ,3)+"</font></td>");
          out.println("      <td width='46' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getRound(dFProdTar ,3)+"</font></td>");
          out.println("      <td width='45' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getRound(dFProd ,3)+"</font></td>");
          out.println("      <td width='38' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='46' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getRound(dFEffy ,3)+"</font></td>");
          out.println("      <td width='51' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getRound(dFHourRun ,3)+"</font></td>");
          out.println("      <td width='56' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='42' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>"+common.getRound(dFUEOA ,3)+"</font></td>");
          out.println("      <td width='44' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='43' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='52' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("      <td width='59' height='1' valign='top' bgcolor='"+bgUom+"' align='right'  ><font size='4' color='"+fgBody+"'>.</font></td>");
          out.println("    </tr>");

          out.println("  </table></center>");
          out.println("</div>");
          out.println("");
          out.println("<p>&nbsp;</p>");
          out.println("<p align='center'>&nbsp;</p>");
          out.println("<p>&nbsp;</p>");
          out.println("<p>&nbsp;</p>");
          out.println("</body>");
          out.println("");
          out.println("</html>");
          out.println("");

          out.close();
     }
     public void setVectorData()
     {
		 String SQ1="",SQ2="";		
           VDate  = new Vector();
           VUnit  = new Vector();
           VShift = new Vector();
           VMachNo= new Vector();
           VCount = new Vector();
           VTicket= new Vector();
           VName  = new Vector();

           VBreak       = new Vector();
           VSpeed       = new Vector();
           VDelivery    = new Vector();
           VSlHank      = new Vector();
           VHankExp     = new Vector();
           VHankTar     = new Vector();
           VHank        = new Vector();
           VProdExp     = new Vector();
           VProdTar     = new Vector();
           VProd        = new Vector();
           VProdLoss    = new Vector();
           VActualEffy  = new Vector();
           VHourRun     = new Vector();
           VHourMeter   = new Vector();
           VUEOA        = new Vector();
           VUEM         = new Vector();
           VEffyAvai    = new Vector();
           VWorkerEffy  = new Vector();
           VStopAcc     = new Vector();
           VStopNonAcc  = new Vector();
           VHourAllot   = new Vector();
           VNoRunOut    = new Vector();
           VNoPersons   = new Vector();
           VRoTime      = new Vector();
           VExcess      = new Vector();
           VActualKW    = new Vector();
           VBreaksPer100Km=  new Vector();
		 if(bFinalized)
		 {
			SQ1 = getQString();
			SQ2 = getSupCd();
		 }	
		 else
		 {	
		     SQ1 = getQStringTemp();
			SQ2 = getSupCdTemp();
		 }

             try
             {
                     Connection con = createConnection();
                     Statement stat = con.createStatement();
                     ResultSet res  = stat.executeQuery(SQ1);
                     while(res.next())
                     {
                          int iExcess=0;

                          VDate  .addElement(res.getString(1));
                          VUnit  .addElement(res.getString(2));
                          VShift .addElement(res.getString(3));
                          VMachNo.addElement(res.getString(4));
                          VCount .addElement(res.getString(5));
                          VTicket.addElement(res.getString(6));
                          VName  .addElement(getTicket(res.getString(7),con));

                          int iBreak = res.getInt(8);
                          if(iBreak == 1)
                               VBreak       .addElement("Breaker");
                          else
                               VBreak       .addElement("Finisher");

                          VSpeed       .addElement(res.getString(9));

                          int iDelivery = res.getInt(10);
                          if(iDelivery == 1)
                               VDelivery       .addElement("One");
                          else
                               VDelivery       .addElement("Two");

                          VSlHank      .addElement(res.getString(11));
                          VHankExp     .addElement(res.getString(12));
                          VHankTar     .addElement(res.getString(13));
                          VHank        .addElement(res.getString(14));
                          VProdExp     .addElement(res.getString(15));
                          VProdTar     .addElement(res.getString(16));
                          VProd        .addElement(res.getString(17));
                          VHourRun     .addElement(res.getString(18));
                          VHourMeter   .addElement(res.getString(19));
                          VUEOA        .addElement(res.getString(20));
                          VUEM         .addElement(res.getString(21));
                          VWorkerEffy  .addElement(res.getString(22));
                          VStopAcc     .addElement(res.getString(23));
                          VStopNonAcc  .addElement(res.getString(24));
                          VHourAllot   .addElement(res.getString(25));
                          VNoRunOut    .addElement(res.getString(26));
                          VNoPersons   .addElement(res.getString(27));
                          VRoTime      .addElement(res.getString(28));
                          
                          int iRoTime = res.getInt(29);
                          int iRoTimeAc=res.getInt(30);
                          if(iRoTime>30)
                            iExcess = iExcess+(iRoTime-30);
                          if(iRoTimeAc>60)
                            iExcess = iExcess+(iRoTimeAc-30);
                          VExcess.addElement(String.valueOf(iExcess));
                          
                          VActualKW    .addElement(res.getString(31));
                          VBreaksPer100Km.addElement(res.getString(32));
                     }
                     ResultSet res3 = stat.executeQuery(SQ2);
                     while(res3.next())
                         SSupName = res3.getString(1);
                     ResultSet res4 = stat.executeQuery(getTarget());
                     while(res4.next())
                     {
                         SWorkTar = res4.getString(1);
                         SUtilTar = res4.getString(2);
                     }
                     res4.close();
                     res3.close();
                     res.close();
                     stat.close();
                     con.close();
             }
             catch(Exception ex)
             {
                     System.out.println("From Card.java : 252 "+ex);
             }
     }
     public void calcVectors()
     {
               double dBreak=0;
               double dFinish=0;

                dBProd       = 0;
                dBProdExp    = 0;
                dBProdTar    = 0;
                dBEffy       = 0;
                dBHourRun    = 0;
                dBUEOA       = 0;
                dFProd       = 0;
                dFProdExp    = 0;
                dFProdTar    = 0;
                dFEffy       = 0;
                dFHourRun    = 0;
                dFUEOA       = 0;

               for(int i=0;i<VMachNo.size();i++)
               {
                     double dProd    = common.toDouble((String)VProd.elementAt(i));
                     double dProdTar = common.toDouble((String)VProdTar.elementAt(i));
                     double dProdExp = common.toDouble((String)VProdExp.elementAt(i));
                     double dHal     = common.toDouble((String)VHourAllot.elementAt(i));
                     double dStopAcc = common.toDouble((String)VStopAcc.elementAt(i))/60;
                     VProdLoss    .addElement((String)common.getRound(dProd-dProdTar,3));
                     VActualEffy  .addElement((String)common.getRound((dProd/dProdExp*100),3));
                     VEffyAvai    .addElement((String)common.getRound(((dHal-dStopAcc)/dHal*100),3));

                     if(((String)VBreak.elementAt(i)).startsWith("Breaker"))
                     {
                          dBProd       = dBProd+dProd;
                          dBProdExp    = dBProdExp+dProdExp;
                          dBProdTar    = dBProdTar+dProdTar;
                          dBEffy       = dBEffy+(dBProd*100/dBProdExp);
                          dBHourRun    = dBHourRun+common.toDouble((String)VHourRun.elementAt(i));
                          dBUEOA       = dBUEOA+common.toDouble((String)VUEOA.elementAt(i));
                          
                          dBreak++;
                     }
                     else
                     {
                          dFProd       = dFProd+dProd;
                          dFProdExp    = dFProdExp+dProdExp;
                          dFProdTar    = dFProdTar+dProdTar;
                          dFEffy       = dFEffy+(dFProd*100/dFProdExp);
                          dFHourRun    = dFHourRun+common.toDouble((String)VHourRun.elementAt(i));
                          dFUEOA       = dFUEOA+common.toDouble((String)VUEOA.elementAt(i));
                          dFinish++;
                     }
               }
               dBEffy=dBEffy/dBreak;
               dBUEOA=dBUEOA/dBreak;
               dFEffy=dFEffy/dFinish;
               dFUEOA=dFUEOA/dFinish;
     }
     public String getTarget()
     {
          String QS = " Select work_effy,ue_effy from Target " + 
			       " where dept_code=5 and unit_code = "+SUnit;
          return QS;
     }
     public String getQString()
     {
		String QString = " SELECT  drawtran.sp_date,unit.unitname,drawtran.shift_code, machine.mach_name, " +
					  " count.count_name, drawtran.ticket, drawtran.ticket, drawtran.break_final," +
					  " drawtran.doffer_m_min, drawtran.delivery,drawtran.sliverhank, drawtran.hank_exp," +
					  " drawtran.hank_tar,drawtran.hank,  drawtran.prod_exp, drawtran.prod_tar, " +
					  " drawtran.prod,drawtran.hourrun,drawtran.hourmeter,drawtran.ueoa," +
					  " drawtran.uem,drawtran.workereffy,drawtran.stopacc,drawtran.stopnonacc," +
					  " drawtran.houralloted,drawtran.noofro+drawtran.noofroac," +
                                          " drawtran.noofpersons+drawtran.noofpersonsac,drawtran.rotime+drawtran.rotimeac,"+
                                          " drawtran.rotime,drawtran.rotimeac,decode(machine.ActualKW,null,0,machine.ActualKW),drawTran.BreaksPer100Km"+
					  " FROM ((((drawtran Left JOIN unit ON drawtran.unit_code = unit.unitcode) " +
					  " Left JOIN machine ON drawtran.mach_code = machine.mach_code) " +
					  " Left JOIN count ON drawtran.count_Code = count.count_code) " +
					  " Left JOIN SHADE ON drawtran.shade_code = SHADE.shade_code) " +
					  " Where DrawTran.sp_date='"+SDate+"' and DrawTran.Shift_Code = "+SShift + 
					  " and DrawTran.Unit_Code = "+SUnit+" ORDER by to_number(machine.mach_name)" ;
		//System.out.println(QString);
          return QString;
     }

	public String getSupCd()
     {
		String QS = " SELECT supervisor.supervisor FROM DrawTran " + 
 				  " INNER JOIN supervisor ON DrawTran.supcode = supervisor.supcode " +
				  " GROUP BY supervisor.supervisor, DrawTran.Unit_Code, " +
				  " DrawTran.sp_date, DrawTran.shift_code " +
                      " HAVING (((DrawTran.Unit_Code)="+SUnit+") AND "+
                      " ((DrawTran.sp_date)='"+SDate+"') AND "+
                      " ((DrawTran.shift_code)="+SShift+")) ";
		//System.out.println(QS);
          return QS;
     }
     public String getQStringTemp()
     {
		String QString = " SELECT  DrawTran_Temp.sp_date,unit.unitname,DrawTran_Temp.shift_code, machine.mach_name, " +
					  " count.count_name, DrawTran_Temp.ticket, DrawTran_Temp.ticket, DrawTran_Temp.break_final," +
					  " DrawTran_Temp.doffer_m_min, DrawTran_Temp.delivery,DrawTran_Temp.sliverhank, DrawTran_Temp.hank_exp," +
					  " DrawTran_Temp.hank_tar,DrawTran_Temp.hank,  DrawTran_Temp.prod_exp, DrawTran_Temp.prod_tar, " +
					  " DrawTran_Temp.prod,DrawTran_Temp.hourrun,DrawTran_Temp.hourmeter,DrawTran_Temp.ueoa," +
					  " DrawTran_Temp.uem,DrawTran_Temp.workereffy,DrawTran_Temp.stopacc,DrawTran_Temp.stopnonacc," +
					  " DrawTran_Temp.houralloted,DrawTran_Temp.noofro+DrawTran_Temp.noofroac," +
                                          " DrawTran_Temp.noofpersons+DrawTran_Temp.noofpersonsac,DrawTran_Temp.rotime+DrawTran_Temp.rotimeac,   " +
                                          " drawtran_Temp.rotime,drawtran_Temp.rotimeac,decode(machine.ActualKW,null,0,machine.ActualKW),drawTran_Temp.BreaksPer100Km"+
					  " FROM ((((DrawTran_Temp Left JOIN unit ON DrawTran_Temp.unit_code = unit.unitcode) " +
					  " Left JOIN machine ON DrawTran_Temp.mach_code = machine.mach_code) " +
					  " Left JOIN count ON DrawTran_Temp.count_Code = count.count_code) " +
					  " Left JOIN SHADE ON DrawTran_Temp.shade_code = SHADE.shade_code) " +
					  " Where DrawTran_Temp.sp_date='"+SDate+"' and DrawTran_Temp.Shift_Code = "+SShift + 
					  " and DrawTran_Temp.Unit_Code = "+SUnit+" ORDER by to_number(machine.mach_name)" ;
		//System.out.println(QString);
          return QString;
     }

	public String getSupCdTemp()
     {
		String QS = " SELECT supervisor.supervisor FROM DrawTran_Temp " + 
 				  " INNER JOIN supervisor ON DrawTran_Temp.supcode = supervisor.supcode " +
				  " GROUP BY supervisor.supervisor, DrawTran_Temp.Unit_Code, " +
				  " DrawTran_Temp.sp_date, DrawTran_Temp.shift_code " +
                      " HAVING (((DrawTran_Temp.Unit_Code)="+SUnit+") AND "+
                      " ((DrawTran_Temp.sp_date)='"+SDate+"') AND "+
                      " ((DrawTran_Temp.shift_code)="+SShift+")) ";
		//System.out.println(QS);
          return QS;
     }

	  private String getTicket(String SEmpCode,Connection conn)
	  {	
		   String SRet="UnKnown";
		   if(SEmpCode.equals("0"))
			   return SRet;
		   try
		   {			   
			   String SQry = " select empname from schemeapprentice where empcode="+SEmpCode+" union " +
							 " select empname from actapprentice where empcode="+SEmpCode+" union  " + 
							 " select empname from contractapprentice where empcode="+SEmpCode;
			  // Class.forName("oracle.jdbc.OracleDriver");
			  // Connection conn = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
			   Statement stat  = conn.createStatement();
			   ResultSet res   = stat.executeQuery(SQry);
			   if(res.next())
				   SRet            = res.getString(1);
                           res.close();
                           stat.close();
//			   System.out.println(SQry + ":" + SRet);
		   }catch(Exception ee)
		   {
			   System.out.println("getTicket->" + ee);
			   return SRet;
		   }
		   return SRet;
      }

     private String getReportType()
	{
		if(bFinalized)
			return "Finalized";
		else
			return "UnFinalized";
	}

	private void setFinalized()
	{
		String SQry = " select count(*) from DrawTran " + 
				    " Where sp_date='"+SDate+"' and Shift_Code = "+SShift+" and  " +
				    " Unit_Code = "+SUnit;
		try
		{
			Connection con = createConnection();
			Statement   st = con.createStatement();
			ResultSet   rs = st.executeQuery(SQry);
			rs.next();
			if(rs.getInt(1) > 0)
				bFinalized = true;
			else
				bFinalized = false;

                        rs.close();
                        st.close();
		}catch(Exception ee){System.out.println("setFinalized->" + ee);}	
	}



	private Connection createConnection()
	{
		Connection conn;
		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
			conn  = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
		}catch(Exception ee)
		{
			return null;
		}
		return conn;
	}

     public void Head()
     {
          if(Lctr < 60)
             return;
          if(Pctr > 0)
          {
             try
             {
               FW.write(SLine+"\n");
             }catch(Exception ex){}
          }
          Pctr++;

          String Str1    = common.Pad("Company    : AMARJOTHI SPINNING MILLS LIMITED",80);
          String Str2    = common.Pad("Document   : Drawing Daily Report On "+common.parseDate(SDate),80);
          String Str3    = common.Pad("Unit       : "+(SUnit.equals("1")?"A":(SUnit.equals("2")?"B":(SUnit.equals("3")?"C":"S"))),70);
          String Str4    = common.Pad("Shift      : "+(SShift.equals("1")?"Day":(SShift.equals("2")?"Half Night":"Full Night")),70);
          String Str5    = common.Pad("P.O/J.P.O  : "+SSupName,70);
          VHead = Spin1Head();
          try
          {
               FW.write(""+Str1+"\n");
               FW.write(Str2+"\n");
               FW.write(Str3+"\n");
               FW.write(Str4+"\n");
               FW.write(Str5+"\n\n");
               for(int i=0;i<VHead.size();i++)
               {
                    FW.write((String)VHead.elementAt(i));
               }
          }catch(Exception ex){}
          Lctr = 4+VHead.size();
     }
     public void Body()
     {
         String Strl = "";
         for(int i=0;i<VMachNo.size();i++)
         {
                Strl  = common.Pad((String)VMachNo.elementAt(i),4)+common.Space(2)+
                        common.Pad((String)VTicket.elementAt(i),6)+common.Space(2)+
                        common.Pad((String)VName.elementAt(i),10)+common.Space(2)+
                        common.Pad((String)VCount.elementAt(i),10)+common.Space(2)+
                        common.Pad((String)VBreak.elementAt(i),5)+common.Space(2)+
                        common.Pad((String)VSpeed.elementAt(i),7)+common.Space(2)+
                        common.Rad((String)VDelivery.elementAt(i),3)+common.Space(2)+
                        common.Rad((String)VSlHank.elementAt(i),7)+common.Space(2)+
                        common.Rad((String)VNoRunOut.elementAt(i),3)+common.Space(2)+
                        common.Rad((String)VNoPersons.elementAt(i),3)+common.Space(2)+
                        common.Rad((String)VRoTime.elementAt(i),4)+common.Space(2)+
                        common.Rad((String)VBreaksPer100Km.elementAt(i),4)+common.Space(2)+
                        common.Rad((String)VHankExp.elementAt(i),9)+common.Space(2)+
                        common.Rad((String)VHankTar.elementAt(i),9)+common.Space(2)+
                        common.Rad((String)VHank.elementAt(i),9)+common.Space(2)+
                        common.Rad((String)VProdExp.elementAt(i),9)+common.Space(2)+
                        common.Rad((String)VProdTar.elementAt(i),9)+common.Space(2)+
                        common.Rad((String)VProd.elementAt(i),9)+common.Space(2)+
                        common.Rad((String)VProdLoss.elementAt(i),9)+common.Space(2)+
                        common.Rad((String)VActualEffy.elementAt(i),7)+common.Space(2)+
                        common.Rad((String)VHourRun.elementAt(i),5)+common.Space(2)+
                        common.Rad((String)VHourMeter.elementAt(i),5)+common.Space(2)+
                        common.Rad((String)VUEOA.elementAt(i),6)+common.Space(2)+
                        common.Rad((String)VUEM.elementAt(i),6)+common.Space(2)+
                        common.Rad((String)VEffyAvai.elementAt(i),6)+common.Space(2)+
                        common.Rad((String)VWorkerEffy.elementAt(i),6)+common.Space(2)+
                        common.Rad((String)VStopAcc.elementAt(i),6)+common.Space(2)+
                        common.Rad((String)VStopNonAcc.elementAt(i),6)+common.Space(2)+
                        common.Rad((String)VActualKW.elementAt(i),10)+common.Space(2);

                        double dProduction  = common.toDouble((String)VProd .elementAt(i));
                        double dWorkedHours = common.toDouble((String)VHourRun .elementAt(i));
                        double dActualKW    = common.toDouble((String)VActualKW .elementAt(i));
                        double dUKG         = (dWorkedHours*dActualKW)/dProduction;
                        
                        Strl = Strl+ common.Rad(common.getRound(dUKG,3),8)+"\n";

                try
                {
                     FW.write(Strl);
                     Lctr++;
                     Head();
                }catch(Exception ex){}
         }

           try
           {
               FW.write(SLine);                   
           }catch(Exception ex){}

                Strl  = common.Pad(" ",4)+common.Space(2)+common.Pad(" ",6)+common.Space(2)+
                        common.Pad("Breaker",10)+common.Space(2)+
                        common.Pad(" ",10)+common.Space(2)+common.Rad(" ",5)+common.Space(2)+
                        common.Rad(" ",7)+common.Space(2)+common.Rad(" ",3)+common.Space(2)+
                        common.Rad(" ",7)+common.Space(2)+common.Rad(" ",3)+common.Space(2)+
                        common.Rad(" ",3)+common.Space(2)+common.Rad(" ",4)+common.Space(2)+
                        common.Rad(common.getSum(VBreaksPer100Km ,"S"),4)+common.Space(2)+common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(common.getRound(dBProdExp,3),9)+common.Space(2)+
                        common.Rad(common.getRound(dBProdTar,3),9)+common.Space(2)+
                        common.Rad(common.getRound(dBProd,3),9)+common.Space(2)+
                        common.Rad(common.getRound(dBProd-dBProdTar,3),9)+common.Space(2)+
                        common.Rad(common.getRound(dBEffy,3),7)+common.Space(2)+
                        common.Rad(common.getRound(dBHourRun,1),5)+common.Space(2)+
                        common.Rad(" ",5)+common.Space(2)+
                        common.Rad(common.getRound(dBUEOA,1),6)+common.Space(2)+
                        common.Rad(" ",6)+common.Space(2)+
                        common.Rad(" ",6)+common.Space(2)+
                        common.Rad(" ",6)+common.Space(2)+
                        common.Rad(" ",6)+common.Space(2)+
                        common.Rad(" ",6)+common.Space(2)+
                        common.Rad(" ",10)+common.Space(2)+
                        common.Rad(" ",8)+"\n";

           try
           {
               FW.write(Strl);
           }catch(Exception ex){System.out.println("DR : 555 "+ex);}

                Strl  = common.Pad(" ",4)+common.Space(2)+common.Pad(" ",6)+common.Space(2)+
                        common.Pad("Finisher",10)+common.Space(2)+
                        common.Pad(" ",10)+common.Space(2)+common.Rad(" ",5)+common.Space(2)+
                        common.Rad(" ",7)+common.Space(2)+common.Rad(" ",3)+common.Space(2)+
                        common.Rad(" ",7)+common.Space(2)+common.Rad(" ",3)+common.Space(2)+
                        common.Rad(" ",3)+common.Space(2)+common.Rad(" ",4)+common.Space(2)+
                        common.Rad(" ",4)+common.Space(2)+common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(common.getRound(dFProdExp,3),9)+common.Space(2)+
                        common.Rad(common.getRound(dFProdTar,3),9)+common.Space(2)+
                        common.Rad(common.getRound(dFProd,3),9)+common.Space(2)+
                        common.Rad(common.getRound(dFProd-dFProdTar,3),9)+common.Space(2)+
                        common.Rad(common.getRound(dFEffy,3),7)+common.Space(2)+
                        common.Rad(common.getRound(dFHourRun,1),5)+common.Space(2)+
                        common.Rad(" ",5)+common.Space(2)+
                        common.Rad(common.getRound(dFUEOA,1),6)+common.Space(2)+
                        common.Rad(" ",6)+common.Space(2)+
                        common.Rad(" ",6)+common.Space(2)+
                        common.Rad(" ",6)+common.Space(2)+
                        common.Rad(" ",6)+common.Space(2)+
                        common.Rad(" ",6)+common.Space(2)+
                        common.Rad(" ",10)+common.Space(2)+
                        common.Rad(" ",8)+"\n";
           try
           {
               FW.write(Strl);
           }catch(Exception ex){System.out.println("DR : 583 "+ex);}

                Strl  = common.Pad(" ",4)+common.Space(2)+
                        common.Pad(" ",6)+common.Space(2)+
                        common.Pad("Total/Avg",10)+common.Space(2)+
                        common.Pad(" ",10)+common.Space(2)+
                        common.Rad(" ",5)+common.Space(2)+
                        common.Rad(common.getSum(VSpeed,"A"),7)+common.Space(2)+
                        common.Rad(" ",3)+common.Space(2)+
                        common.Rad(common.getSum(VSlHank,"A"),7)+common.Space(2)+
                        common.Rad(" ",3)+common.Space(2)+
                        common.Rad(" ",3)+common.Space(2)+
                        common.Rad(" ",4)+common.Space(2)+
                        common.Rad(" ",4)+common.Space(2)+
                        common.Rad(common.getSum(VHankExp,"S"),9)+common.Space(2)+
                        common.Rad(common.getSum(VHankTar,"S"),9)+common.Space(2)+
                        common.Rad(common.getSum(VHank,"S"),9)+common.Space(2)+
                        common.Rad(common.getSum(VProdExp,"S"),9)+common.Space(2)+
                        common.Rad(common.getSum(VProdTar,"S"),9)+common.Space(2)+
                        common.Rad(common.getSum(VProd,"S"),9)+common.Space(2)+
                        common.Rad(common.getSum(VProdLoss,"S"),9)+common.Space(2)+
                        common.Rad(common.getSum(VActualEffy,"A"),7)+common.Space(2)+
                        common.Rad(common.getSum(VHourRun,"S"),5)+common.Space(2)+
                        common.Rad(common.getSum(VHourMeter,"S"),5)+common.Space(2)+
                        common.Rad(common.getSum(VUEOA,"A"),6)+common.Space(2)+
                        common.Rad(common.getSum(VUEM,"A"),6)+common.Space(2)+
                        common.Rad(common.getSum(VEffyAvai,"A"),6)+common.Space(2)+
                        common.Rad(common.getSum(VWorkerEffy,"A"),6)+common.Space(2)+
                        common.Rad(common.getSum(VStopAcc,"S"),6)+common.Space(2)+
                        common.Rad(common.getSum(VStopNonAcc,"S"),6)+common.Space(2)+
                        common.Rad(common.getSum(VActualKW,"A"),10)+common.Space(2);

                        double dTotProduction  = common.toDouble(common.getSum(VProd,"S"));
                        double dTotWorkedHours = common.toDouble(common.getSum(VHourRun,"S"));
                        double dTotActualKW    = common.toDouble(common.getSum(VActualKW,"A"));
                        double dTotUKG         = (dTotWorkedHours*dTotActualKW)/dTotProduction;
                        
                        Strl = Strl+ common.Rad(common.getRound(dTotUKG,3),8)+"\n";
           try
           {
               FW.write(Strl);
               FW.write(SLine+"\n\n");
               FW.write("\n\n\n\nP.O          S.P.O.           S.M.             D.M.\n");
               FW.write("<End Of Report>");
           }catch(Exception ex){}
     }

     public Vector Spin1Head()
     {
          Vector VHead = new Vector();

          String Str2 = common.Space(4)+common.Space(2)+
                        common.Space(6)+common.Space(2)+
                        common.Space(10)+common.Space(2)+
                        common.Space(10)+common.Space(2)+
                        common.Space(5)+common.Space(2)+
                        common.Space(7)+common.Space(2)+
                        common.Space(3)+common.Space(2)+
                        common.Space(7)+common.Space(2)+
                        common.Cad("Shade Change",20)+common.Space(2)+
                        common.Cad("Production Details",84)+common.Space(2)+
                        common.Cad("Utilization",28)+common.Space(2)+
                        common.Cad("Worker Effy",30)+common.Space(2)+
                        common.Cad("ActualKW",10)+common.Space(2)+
                        common.Cad("UKG",8)+"\n";

          String Str3 = common.Pad("Mach",4)+common.Space(2)+
                        common.Pad("Sider",6)+common.Space(2)+
                        common.Pad("Sider",10)+common.Space(2)+
                        common.Pad("Count",10)+common.Space(2)+
                        common.Pad("Break",5)+common.Space(2)+
                        common.Pad("Deliv",7)+common.Space(2)+
                        common.Pad("Del",3)+common.Space(2)+
                        common.Pad(" ",7)+common.Space(2)+
                        common.Replicate("-",20)+common.Space(2)+
                        common.Replicate("-",84)+common.Space(2)+
                        common.Replicate("-",28)+common.Space(2)+
                        common.Replicate("-",30)+common.Space(2)+
                        common.Replicate("-",10)+common.Space(2)+
                        common.Replicate("-",8)+"\n";

          String Str4 = common.Pad("No",4)+common.Space(2)+
                        common.Pad("No",6)+common.Space(2)+
                        common.Pad("Name",10)+common.Space(2)+
                        common.Pad("Shade",10)+common.Space(2)+
                        common.Pad("Finish",5)+common.Space(2)+
                        common.Pad("Speed",7)+common.Space(2)+
                        common.Rad("1/2",3)+common.Space(2)+
                        common.Rad("Hank",7)+common.Space(2)+
                        common.Rad("No",3)+common.Space(2)+
                        common.Rad("Per",3)+common.Space(2)+
                        common.Rad("Actual",4)+common.Space(2)+
                        common.Rad("Breaks",4)+common.Space(2)+
                        common.Rad("Exp Hank",9)+common.Space(2)+
                        common.Rad("Tar Hank",9)+common.Space(2)+
                        common.Rad("Act Hank",9)+common.Space(2)+
                        common.Rad("Exp Prod",9)+common.Space(2)+
                        common.Rad("Tar Prod",9)+common.Space(2)+
                        common.Rad("Act Prod",9)+common.Space(2)+
                        common.Rad("Prod Loss",9)+common.Space(2)+
                        common.Rad("ActEffy",7)+common.Space(2)+
                        common.Rad("Hour",5)+common.Space(2)+
                        common.Rad("Hour",5)+common.Space(2)+
                        common.Rad("UxE",6)+common.Space(2)+
                        common.Rad("UxE",6)+common.Space(2)+
                        common.Rad("Effy",6)+common.Space(2)+
                        common.Rad("Worker",6)+common.Space(2)+
                        common.Rad("Acc",6)+common.Space(2)+
                        common.Rad("NonAcc",6)+common.Space(2)+
                        common.Rad(" ",10)+common.Space(2)+
                        common.Rad(" ",8)+"\n";
          

          String Str5 = common.Pad(" ",4)+common.Space(2)+
                        common.Pad(" ",6)+common.Space(2)+
                        common.Pad(" ",10)+common.Space(2)+
                        common.Pad(" ",10)+common.Space(2)+
                        common.Pad(" ",5)+common.Space(2)+
                        common.Pad(" ",7)+common.Space(2)+
                        common.Rad(" ",3)+common.Space(2)+
                        common.Rad(" ",7)+common.Space(2)+
                        common.Rad(" ",3)+common.Space(2)+
                        common.Rad("son",3)+common.Space(2)+
                        common.Rad(" ",4)+common.Space(2)+
                        common.Rad("Per100",4)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad("A - T",9)+common.Space(2)+
                        common.Rad("A / E",7)+common.Space(2)+
                        common.Rad("toWork",5)+common.Space(2)+
                        common.Rad("Meter",5)+common.Space(2)+
                        common.Rad("(OA)",6)+common.Space(2)+
                        common.Rad("(ME)",6)+common.Space(2)+
                        common.Rad("Avail",6)+common.Space(2)+
                        common.Rad("Effy",6)+common.Space(2)+
                        common.Rad("Stop",6)+common.Space(2)+
                        common.Rad("Stop",6)+common.Space(2)+
                        common.Rad(" ",10)+common.Space(2)+
                        common.Rad(" ",8)+"\n";

          String Str6 = common.Pad(" ",4)+common.Space(2)+
                        common.Pad(" ",6)+common.Space(2)+
                        common.Pad(" ",10)+common.Space(2)+
                        common.Pad(" ",10)+common.Space(2)+
                        common.Pad(" ",5)+common.Space(2)+
                        common.Pad("mt/min",7)+common.Space(2)+
                        common.Rad(" ",3)+common.Space(2)+
                        common.Rad(" ",7)+common.Space(2)+
                        common.Rad(" ",3)+common.Space(2)+
                        common.Rad(" ",3)+common.Space(2)+
                        common.Rad("min",4)+common.Space(2)+
                        common.Rad("Km",4)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad("Kgs",9)+common.Space(2)+
                        common.Rad("Kgs",9)+common.Space(2)+
                        common.Rad("Kgs",9)+common.Space(2)+
                        common.Rad("Kgs",9)+common.Space(2)+
                        common.Rad("85%",7)+common.Space(2)+
                        common.Rad("Hr",5)+common.Space(2)+
                        common.Rad("Hr",5)+common.Space(2)+
                        common.Rad("70%",6)+common.Space(2)+
                        common.Rad("85%",6)+common.Space(2)+
                        common.Rad("%",6)+common.Space(2)+
                        common.Rad("%",6)+common.Space(2)+
                        common.Rad("min",6)+common.Space(2)+
                        common.Rad("min",6)+common.Space(2)+
                        common.Rad(" ",10)+common.Space(2)+
                        common.Rad(" ",8)+"\n";

          String StrL = common.Space(4)+common.Space(2)+
                        common.Space(6)+common.Space(2)+
                        common.Space(10)+common.Space(2)+
                        common.Space(10)+common.Space(2)+
                        common.Space(5)+common.Space(2)+
                        common.Replicate("-",7)+common.Space(2)+
                        common.Replicate("-",3)+common.Space(2)+
                        common.Replicate("-",7)+common.Space(2)+
                        common.Replicate("-",20)+common.Space(2)+
                        common.Replicate("-",84)+common.Space(2)+
                        common.Replicate("-",28)+common.Space(2)+
                        common.Replicate("-",30)+common.Space(2)+
                        common.Replicate("-",10)+common.Space(2)+
                        common.Replicate("-",8)+"\n";

          String STar = common.Pad("Target",4+2+6)+common.Space(2)+
                        common.Pad(" ",10)+common.Space(2)+
                        common.Pad(" ",10)+common.Space(2)+
                        common.Pad(" ",5)+common.Space(2)+
                        common.Pad(" ",7)+common.Space(2)+
                        common.Rad(" ",3)+common.Space(2)+
                        common.Rad(" ",7)+common.Space(2)+
                        common.Rad(" ",3)+common.Space(2)+
                        common.Rad(" ",3)+common.Space(2)+
                        common.Rad(" ",4)+common.Space(2)+
                        common.Rad(" ",4)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",9)+common.Space(2)+
                        common.Rad(" ",7)+common.Space(2)+
                        common.Rad(" ",5)+common.Space(2)+
                        common.Rad(" ",5)+common.Space(2)+
                        common.Rad(SUtilTar,6)+common.Space(2)+
                        common.Rad(" ",6)+common.Space(2)+
                        common.Rad(" ",6)+common.Space(2)+
                        common.Rad(SWorkTar,6)+common.Space(2)+
                        common.Rad(" ",6)+common.Space(2)+
                        common.Rad(" ",6)+common.Space(2)+
                        common.Rad(" ",10)+common.Space(2)+
                        common.Rad(" ",8)+"\n";

          VHead.addElement(SLine);
          VHead.addElement(Str2);
          VHead.addElement(Str3);
          VHead.addElement(Str4);
          VHead.addElement(Str5);
          VHead.addElement(StrL);
          VHead.addElement(Str6);
          VHead.addElement(SLine);
          VHead.addElement(STar);
          VHead.addElement(SLine);

          return VHead;
     }

	private void setCriteria(HttpServletRequest request)
	{
		SDate            = request.getParameter("Date");
          SDate            = (String)common.pureDate(SDate);
          SShift           = request.getParameter("Shift");
		SUnit            = request.getParameter("Unit");

		session.putValue("Unit",SUnit);
		session.putValue("Shift",SShift);
		session.putValue("Date",SDate);

		//System.out.println("Draw -> " + SUnit + ":" + SShift + ":" + SDate);
	}

    //PDF
        
    public void createPDFFile(){
     try{
         
                
        SFile       =   "DrawingDailyReportPDF.pdf";
        SFile       =   common.getPrintPath()+SFile;    
        document    =   new Document(PageSize.A4.rotate());
        writer      =  PdfWriter.getInstance(document, new FileOutputStream(SFile));
        TableHeader event   = new TableHeader();
        writer      .   setPageEvent(event);
        document    .   open();

        table1      =   new PdfPTable(30);
        table1      .   setWidths(iWidth1);
        table1      .   setSpacingAfter(2);
        table1      .   setWidthPercentage(100);
        table1      .   setHeaderRows(9);
        
        table0      =   new PdfPTable(2);
        table0      .   setWidths(iWidth0);
        table0      .   setSpacingAfter(2);
        table0      .   setWidthPercentage(50);
        table0      .   setHorizontalAlignment(Element.ALIGN_LEFT);
        

            setPDFHead();
            setPDFBody();
            document.add(table1);
            document.add(table0);
            document.close();
      
       }
        catch (Exception e){
           e.printStackTrace();
            System.out.println("Error-->"+e);
        }
        finally{
            try{
                document.close();
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }
    }
    
    public void setPDFHead(){
        try
        {
            String sHead1       = "Company    : AMARJOTHI SPINNING MILLS LIMITED";
            String sHead2       = "Document   : Drawing Daily Report On "+common.parseDate(SDate);
            String sHead3       = "Unit             : "+(SUnit.equals("1")?"A":(SUnit.equals("2")?"B":(SUnit.equals("3")?"C":"S")));
            String sHead4       = "Shift            : "+(SShift.equals("1")?"Day":(SShift.equals("2")?"Half Night":"Full Night"));
            String sHead5       = "P.O/J.P.O   : "+SSupName;
        
            AddCellIntoTable(sHead1, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,30,20f,0,0,0,0, bigBold );
            AddCellIntoTable(sHead2, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,30,20f,0,0,0,0, bigBold );
            AddCellIntoTable(sHead3, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,30,20f,0,0,0,0, bigBold );
            AddCellIntoTable(sHead4, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,30,20f,0,0,0,0, bigBold );
            AddCellIntoTable(sHead5, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,30,20f,0,0,0,0, bigBold );
           
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,30,10f,0,0,0,0, tinyBold );
            
            AddHeadCellIntoTable("Mach No", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Sider No", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Sider Name", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Count   /   Shade", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Break Finish", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Deliver Speed", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Del 1/2", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Silver Hank", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,20f,8,3,5,4, tinyBold );
           
            AddCellIntoTable("Shade Change", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,4,20f,8,3,5,4, tinyBold );
            AddCellIntoTable("Production Details", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,8,20f,8,3,5,4, tinyBold );
            AddCellIntoTable("Utilization", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,4,20f,8,3,5,4, tinyBold );
            AddCellIntoTable("Worker Effy", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,4,20f,8,3,5,4, tinyBold );
            
            AddHeadCellIntoTable("Actual KW", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("UKG", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,20f,8,3,5,4, tinyBold );
            
            //SECOND
            
            AddHeadCellIntoTable("Noof SC", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Noof Person", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Actual", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Ex / Sh", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,20f,8,3,5,4, tinyBold );
            
            AddCellIntoTable("Hank", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,20f,8,3,5,4, tinyBold );
            AddCellIntoTable("Production ", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,4,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Actual Eff A/E", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,20f,8,3,5,4, tinyBold );
            
            AddHeadCellIntoTable("Hours tobe worked", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Hour Meter Reading", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("UxE (OA)", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("UxE (MU)", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,20f,8,3,5,4, tinyBold );
            
            AddHeadCellIntoTable("Effy Available", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Worker Effy", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Accep Stoppage", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,20f,8,3,5,4, tinyBold );
            AddHeadCellIntoTable("Non Acc Stopage", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,2,20f,8,3,5,4, tinyBold );
            // THIRD
            AddCellIntoTable("Expected", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,20f,8,3,5,4, tinyBold );
            AddCellIntoTable("Target", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,20f,8,3,5,4, tinyBold );
            AddCellIntoTable("Actual", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,20f,8,3,5,4, tinyBold );
            
            AddCellIntoTable("Expected", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,20f,8,3,5,4, tinyBold );
            AddCellIntoTable("Target", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,20f,8,3,5,4, tinyBold );
            AddCellIntoTable("Actual", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,20f,8,3,5,4, tinyBold );
            AddCellIntoTable("Prod Loss      A-T ", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,20f,8,3,5,4, tinyBold );
//            AddCellIntoTable("A-E ", table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,20f,8,3,5,4, tinyBold );

            //4TH
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("mt/min ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );

            for(int i=0;i<=3;i++){
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            }
            
            AddCellIntoTable("min", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("min", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            
            for(int i=0;i<=2;i++){
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            }
            AddCellIntoTable("Kgs ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("Kgs ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("Kgs ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("Kgs ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
           
            AddCellIntoTable("100% ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("Hrs ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("Hrs ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("80% ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("85% ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("% ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("95% ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            
            AddCellIntoTable("min", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            AddCellIntoTable("min ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            
            for(int i=0;i<=1;i++){
            AddCellIntoTable(" ", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,15f,8,3,5,4, tinyBold );
            }
                   
         }
        catch(Exception Ex){
                Ex.printStackTrace();
                System.out.println("Error Head1-->"+Ex);
        }
    }
    
    public void setPDFBody(){
      try{
          for(int i=0;i<VMachNo.size();i++){
          
          AddCellIntoTable(common.parseNull((String)VMachNo.elementAt(i)), table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VTicket.elementAt(i)), table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VName.elementAt(i)), table1, Element.ALIGN_LEFT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VCount.elementAt(i)), table1, Element.ALIGN_LEFT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
        
          AddCellIntoTable(common.parseNull((String)VBreak.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VSpeed.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VDelivery.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VSlHank.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
         
          AddCellIntoTable(common.parseNull((String)VNoRunOut.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VNoPersons.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VRoTime.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(" ", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          
          AddCellIntoTable(common.parseNull((String)VHankExp.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VHankTar.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VHank.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VProdExp.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VProdTar.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          
          AddCellIntoTable(common.parseNull((String)VProd.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VProdLoss.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VActualEffy.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VHourRun.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VHourMeter.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VUEOA.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VUEM.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                 
          AddCellIntoTable(common.parseNull((String)VEffyAvai.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          
          AddCellIntoTable(common.parseNull((String)VWorkerEffy.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VStopAcc.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          AddCellIntoTable(common.parseNull((String)VStopNonAcc.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
          
          AddCellIntoTable(common.parseNull((String)VActualKW.elementAt(i)), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                    
                        double dProduction  = common.toDouble((String)VProd .elementAt(i));
                        double dWorkedHours = common.toDouble((String)VHourRun .elementAt(i));
                        double dActualKW    = common.toDouble((String)VActualKW .elementAt(i));
                        double dUKG         = (dWorkedHours*dActualKW)/dProduction;
          AddCellIntoTable(common.getRound(dUKG,3), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyNormal );                                            
         }
          
            AddCellIntoTable(" Breaker", table1, Element.ALIGN_LEFT, Element.ALIGN_TOP,15,20f,5,3,8,4, smallBold );                    
            AddCellIntoTable(common.getRound(dBProdExp,3), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getRound(dBProdTar,3), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getRound(dBProd,3), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getRound(dBProd-dBProdTar,3), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getRound(dBEffy,3), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getRound(dBHourRun,3), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getRound(dBUEOA,1), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );
            for(int a=0;a<=6;a++){
             AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            }
            
            AddCellIntoTable(" Finisher", table1, Element.ALIGN_LEFT, Element.ALIGN_TOP,15,20f,5,3,8,4, smallBold );                    
            AddCellIntoTable(common.getRound(dFProdExp,3), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getRound(dFProdTar,3), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getRound(dFProd,3), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getRound(dFProd-dFProdTar,3), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getRound(dFEffy,3), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getRound(dFHourRun,3), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getRound(dFUEOA,1), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );
            for(int a=0;a<=6;a++){
             AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            }
            
            AddCellIntoTable(" Total / Avg", table1, Element.ALIGN_LEFT, Element.ALIGN_TOP,5,20f,5,3,8,4, smallBold );                    
//            AddCellIntoTable(" ", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
//            AddCellIntoTable(" ", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getSum(VSpeed,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable(common.getSum(VSlHank,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    

            AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                    
            
            AddCellIntoTable(common.getSum(VHankExp,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VHankTar,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VHank,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VProdExp,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VProdTar,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VProd,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VProdLoss,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            
            AddCellIntoTable(common.getSum(VActualEffy,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VHourRun,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VHourMeter,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VUEOA,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VUEM,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VEffyAvai,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VWorkerEffy,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VStopAcc,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            AddCellIntoTable(common.getSum(VStopNonAcc,"S"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
            
            AddCellIntoTable(common.getSum(VActualKW,"A"), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                            
                    double dTotProduction  = common.toDouble(common.getSum(VProd,"S"));
                    double dTotWorkedHours = common.toDouble(common.getSum(VHourRun,"S"));
                    double dTotActualKW    = common.toDouble(common.getSum(VActualKW,"A"));
                    double dTotUKG         = (dTotWorkedHours*dTotActualKW)/dTotProduction;
            AddCellIntoTable(common.getRound(dTotUKG,3), table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,1,20f,5,3,8,4, tinyBold );                                                                
                        
          
            AddCellIntoTable(" ", table1, Element.ALIGN_RIGHT, Element.ALIGN_TOP,30,50f,0,0,0,0, tinyBold );   
            
            AddCellIntoTable("P.O ", table1, Element.ALIGN_LEFT, Element.ALIGN_TOP,3,20f,0,0,0,0, smallBold );   
            AddCellIntoTable("S.P.O ", table1, Element.ALIGN_LEFT, Element.ALIGN_TOP,3,20f,0,0,0,0, smallBold );   
            AddCellIntoTable("S.M ", table1, Element.ALIGN_LEFT, Element.ALIGN_TOP,3,20f,0,0,0,0, smallBold );   
            AddCellIntoTable("D.M ", table1, Element.ALIGN_LEFT, Element.ALIGN_TOP,3,20f,0,0,0,0, smallBold );   
            AddCellIntoTable(" ", table1, Element.ALIGN_LEFT, Element.ALIGN_TOP,18,20f,0,0,0,0, tinyBold );   
        }
      catch(Exception Ex){
                Ex.printStackTrace();
                System.out.println("Error Body1-->"+Ex);
        }
    }
    
    private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      } 
    
    private void AddHeadCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iRowSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
    
    private void AddCellIntoTable1(String Str,PdfPTable table,int iHorizontal,int iVertical,int iRowSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
      private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
      private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,int iRowSpan)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          //c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
      private void AddCellTable(Image image,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
          PdfPCell c1 = new PdfPCell();
		  c1.addElement(new Chunk(image, 5, -5));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

		  // image.scalePercent(90f);
      	  // paragraph.add(new Chunk(image, -1f, 1f));
      }
       private void AddCellIntoTable(Image image,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
          PdfPCell c1 = new PdfPCell();
		  //c1.addElement(new Chunk(image, 5, -5));
	  c1.addElement(image);
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

      }
     public void AddCellIntoTable2(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }
    
     class TableHeader extends PdfPageEventHelper {
        String header;
        PdfTemplate total;
        public void setHeader(String header) {
            this.header = header;
      }
      public void onOpenDocument(PdfWriter writer, Document document) {
            try{
            total = writer.getDirectContent().createTemplate(30, 16);
            }
            catch(Exception ex){
            }
      }

        public void onEndPage(PdfWriter writer, Document document) {
            PdfPTable table = new PdfPTable(3);
            try {
//                AddCellIntoTable("", table1, Element.ALIGN_LEFT, Element.ALIGN_TOP,13,18f,1,0,0,0, tinyNormal );
                table.setWidths(new int[]{24, 24, 2});
                table.setTotalWidth(520);
                table.setLockedWidth(true);
                table.getDefaultCell().setFixedHeight(20);
                table.getDefaultCell().setBorder(0);
                table.addCell(header);
                table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell("");
                table.addCell("");
                table.addCell("");
                
                table.addCell(String.format("Page %d of", writer.getPageNumber()));
                PdfPCell cell = new PdfPCell(Image.getInstance(total));
                cell.setBorder(0);
                table.addCell(cell);
                table.writeSelectedRows(0, -1, 10, 50, writer.getDirectContent());
            }
            catch(DocumentException de) {
                throw new ExceptionConverter(de);
            }
        }
        public void onCloseDocument(PdfWriter writer, Document document) {
          try{  
            ColumnText.showTextAligned(total, Element.ALIGN_LEFT,
                    new Phrase(String.valueOf(writer.getPageNumber() - 1)),
                    2, 2, 0);
             }
          catch(Exception ex){
              
          }
        }
    }
  
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
