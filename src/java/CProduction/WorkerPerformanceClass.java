package CProduction;

import java.util.*;
import java.sql.*;
public class WorkerPerformanceClass
{
    
      java.sql.Connection conn  = null;
      Vector VEmpName,VEmpCode,VMonthYear,VMonthYearCode,VHRDTicketNo;
    
      String SUnitCode="0";
	 Common common = new Common();

      WorkerPerformanceClass()
      {
            setVectors("5");
      }
      public void setVectors(String SDeptCode)
      {
           VEmpName         = new Vector();
           VEmpCode         = new Vector();
           VMonthYear       = new Vector();
           VMonthYearCode   = new Vector();
           VHRDTicketNo     = new Vector();

           StringBuffer sb  = new StringBuffer();
                        sb.append(" select Distinct SchemeApprentice.EmpName, SchemeApprentice.EmpCode, SchemeApprentice.HRDTicketNo ");
                        sb.append(" from SchemeApprentice ");
                        sb.append(" where SchemeApprentice.DeptCode = 5 ");
                        sb.append(" Union All ");
                        sb.append(" select Distinct ContractApprentice.EmpName, ContractApprentice.EmpCode, ContractApprentice.HRDTicketNo ");
                        sb.append(" from ContractApprentice ");
                        sb.append(" where ContractApprentice.DeptCode = 5 ");
           
           String Count = "";
                
           try
           {
                  if(conn==null) {
                        conn = JDBCConnection.createORCDBConnection();
                  }
                  Statement stat2  = conn.createStatement();

                  ResultSet result1 = stat2.executeQuery(sb.toString());
                  
                  while(result1.next())
                  {
                      Count  = result1.getString(1);
                      
                      VEmpName.addElement(result1.getString(1));
                      VEmpCode.addElement(result1.getString(2));
                      VHRDTicketNo.addElement(result1.getString(3));
                  }
                  System.out.println("Count Value :"+Count);
                  conn.close();
           }
           catch(Exception ex) {
               
           }
      }

	private Connection createConnection()
	{
		Connection conn;
		
                try
		{
                  Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
                  conn  = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","Prod_A_0405","aunitprod");
		}catch(Exception ee)
		{
			return null;
		}
		return conn;
	}
}
