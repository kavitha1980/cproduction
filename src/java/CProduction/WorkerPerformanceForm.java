package CProduction;

import java.io.*;
import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;


public class WorkerPerformanceForm extends HttpServlet
{
     HttpSession    session;
     Common common;
     java.sql.Connection con = null;
     String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody,tbgBody;
     String SHRDTicketNo,SMonth,SYear;
     Vector VEmpName,VWorkedDate,VCount,VWastePercent,VMachCode;
     
        
     FileWriter  FW;
     int Lctr = 70,Pctr = 0;
     String SLine;
     Vector VHead;
     boolean bFinalized;
     Control control = new Control();
     int    iCount  = 0;
        
     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
          common = new Common();
            
          bgColor = common.bgColor;
          bgHead  = common.bgHead;
          bgUom   = common.bgUom;
          bgBody  = common.bgBody;
          fgHead  = common.fgHead;
          fgUom   = common.fgUom;
          fgBody  = common.fgBody;
          tbgBody = common.tbgBody;
          SLine   = common.Replicate("-",236)+"\n";
     }
     public void doGet(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
            doPost(request,response);
     }
     public void doPost(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
		response.setContentType("text/html");
          PrintWriter out  = response.getWriter();
		session = request.getSession(true);
                
		/*if(session.getValue("AppletTag")==null)
		{
			String SServer = control.getID("Select ProdHost from Server");
			response.sendRedirect(SServer + "/SessionExpired");
			return;

		}*/
		getData(request);
                setVectorData();
           if(iCount>0)     {
           for(int x=0;x<VEmpName.size();x++) {
               
             System.out.println("Emp name:"+VEmpName.elementAt(x));
             System.out.println("Date:"+common.parseNull((String)VWorkedDate.elementAt(x)).substring(6));
             System.out.println("count:"+VCount.elementAt(x));
             System.out.println("waste %:"+VWastePercent.elementAt(x));
             System.out.println("mach name:"+VMachCode.elementAt(x));
           }
           }
                
          int machdata=0;     
          
          out.println("<html>");
          out.println("<head>");
          out.println("<title>Test Production Report </title>");
          out.println("</head>");
          out.println("<body bgcolor='" + common.bgBody + "'>");
          out.println("<a href='Header'>Home</a>");
          out.println("  <table border='1' cellspacing='0' width='1600' height='1''>");
          out.println("    <tr>");
          out.println("      <td width='59' height='82'  valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Month</font></td>");
          out.println("      <td width='59' height='82'  valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Aspects</font></td>");
        for(int i=1;i<=31;i++){
          out.println("      <td width='59' height='82'  valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>"+i+"</font></td>");
        }      
          out.println("    </tr>");
          
          out.println("    <tr>");
          out.println("      <td width='59' height='69' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'></font></td>");
          out.println("      <td width='59' height='69' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Mach No</font></td>");
    for(int i=1;i<=31;i++){   
       if(iCount>0) {
        String date = common.parseNull((String)VWorkedDate.elementAt(machdata)).substring(6);
        String ivalue=""+i;
        if(date.trim().equals(ivalue.trim())) { 
         out.println("<td width='59' height='69' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>"+VMachCode.elementAt(machdata)+"</font></td>");
         machdata++;
        }else{
          out.println("      <td width='59' height='69' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>&nbsp;&nbsp;</font></td>");
        }
       }else{
          out.println("      <td width='59' height='69' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>&nbsp;&nbsp;</font></td>");
       }
    }
          out.println("    </tr>");
    
    int countdata=0;
          
          out.println("    <tr>");
          out.println("      <td width='59' height='69' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'></font></td>");
          out.println("      <td width='59' height='69' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Count</font></td>");
    for(int i=1;i<=31;i++){          
        if(iCount>0) {
        String date = common.parseNull((String)VWorkedDate.elementAt(countdata)).substring(6);
        String ivalue=""+i;
        if(date.trim().equals(ivalue.trim())) { 
         out.println("      <td width='59' height='69' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>"+VCount.elementAt(countdata)+"</font></td>");
         countdata++;
        }else{
          out.println("      <td width='59' height='69' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>&nbsp;&nbsp;</font></td>");
        }
       }else{
          out.println("      <td width='59' height='69' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>&nbsp;&nbsp;</font></td>");
       }
        
    }
          out.println("    </tr>");
          
    int wastedata=0;
    
          out.println("    <tr>");
          out.println("      <td width='59' height='69' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'></font></td>");
          out.println("      <td width='59' height='69' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Waste %</font></td>");
    for(int i=1;i<=31;i++){          
        if(iCount>0) {
        String date = common.parseNull((String)VWorkedDate.elementAt(wastedata)).substring(6);
        String ivalue=""+i;
        if(date.trim().equals(ivalue.trim())) { 
         out.println("      <td width='59' height='69' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>"+VWastePercent.elementAt(wastedata)+"</font></td>");
         wastedata++;
        }else{
          out.println("      <td width='59' height='69' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>&nbsp;&nbsp;</font></td>");
        }
       }else{
          out.println("      <td width='59' height='69' valign='top' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>&nbsp;&nbsp;</font></td>");
       }
        
    }
          out.println("    </tr>");
          
          out.println("  </table>");
          out.println("</body>");
          out.println("</html>");
          out.close();

          try
          {
               FW      = new FileWriter("/WorkerPerformanceReport.prn");
               //FW      = new FileWriter("/software/C-Prod-Print/Reports/WorkerPerformanceReport.prn");
//             FW      = new FileWriter("//home/arun/reports/drawc.prn");

               Lctr    = 70;
               Pctr    =  0;
               //Head();
               //Body();
               FW.close();
          }catch(Exception ex)
          {
                System.out.println(ex);
                ex.printStackTrace();
          }


          out.println("</body>");
          out.println("");
          out.println("</html>");
          out.println("");

          out.close();
     }
     
     public void setVectorData()
     {
         VEmpName       = new Vector();
         VWorkedDate    = new Vector();
         VCount         = new Vector();
         VWastePercent  = new Vector();
         VMachCode      = new Vector();       
         
         
         StringBuffer	sb	= new StringBuffer();

			sb.append(" select SchemeApprentice.EMPNAME, SpinTran.SP_Date, SpinTran.count, sum(SpinTran.PNEUMAFIL) as WastePercent, ");
			sb.append(" Machine.MACH_CODE ");
			sb.append(" from  SpinTran ");
			sb.append(" inner join SchemeApprentice on SpinTran.TICKET = SchemeApprentice.EmpCode ");
			sb.append(" inner join Department on SchemeApprentice.DeptCode = Department.DeptCode ");
			sb.append(" inner join Machine on Machine.Mach_Code = SpinTran.Mach_Code ");
			sb.append(" where SpinTran.Sp_date  like '"+SYear+""+getMonthValue()+"%' ");
			//sb.append(" where SpinTran.Sp_date = 20121120 ");
			sb.append(" and SchemeApprentice.DeptCode = 5 and Department.ProdCode = 7 ");
			//sb.append(" and SpinTran.TICKET = '"+SHRDTicketNo+"' ");
			sb.append(" and SpinTran.TICKET = 173505 ");
			sb.append(" and SpinTran.Shift_Code=1 ");
			sb.append(" group by SchemeApprentice.EmpName, SpinTran.Sp_Date, SpinTran.count, Machine.Mach_Code  ");
			sb.append(" order by SP_DATE, Mach_Code ");
         
                        System.out.println("Query Data : "+SHRDTicketNo+","+getMonthValue()+","+SYear);
                        System.out.println("Query:"+sb.toString());
                        
                  try{
                     if(con==null){
                        con = JDBCConnection.createORCDBConnection();
                     }
                     java.sql.Statement stat = con.createStatement();
                     java.sql.ResultSet res  = stat.executeQuery(sb.toString());
                     while(res.next())
                     {
                         iCount++;
                         //iCount         =res.getInt(1);
                         
                         System.out.println("count:"+iCount);
                         
                         VEmpName       .addElement(res.getString(1));
                         VWorkedDate    .addElement(res.getString(2));
                         VCount         .addElement(res.getString(3));
                         VWastePercent  .addElement(res.getString(4));
                         VMachCode      .addElement(res.getString(5));
                     }
                     System.out.println("Count :"+iCount+","+VEmpName.size());
                     
             }
             catch(Exception ex) {
                     ex.printStackTrace();
             }
     }
	private Connection createConnection()
	{
		Connection conn;
		Statement  stat;
                try
		{
                  Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
                  conn  = DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","Prod_A_0405","aunitprod");
                  stat =conn.createStatement();
		}catch(Exception ee)
		{
			return null;
		}
		return conn;
	}
	private void getData(HttpServletRequest request)
	{
		SHRDTicketNo     = request.getParameter("TicketNo");
                SMonth           = request.getParameter("Month");
		SYear            = request.getParameter("Year");

                System.out.println("From getData : "+SHRDTicketNo+","+SMonth+","+SYear);
	}
        
        private int getMonthValue(){
          int mval=0;
                if(SMonth.trim().equals("Jan")){ mval=1;}
                if(SMonth.trim().equals("Feb")){ mval=2;}
                if(SMonth.trim().equals("Mar")){ mval=3;}
                if(SMonth.trim().equals("Apr")){ mval=4;}
                if(SMonth.trim().equals("May")){ mval=5;}
                if(SMonth.trim().equals("Jun")){ mval=6;}
                if(SMonth.trim().equals("Jul")){ mval=7;}
                if(SMonth.trim().equals("Aug")){ mval=8;}
                if(SMonth.trim().equals("Sep")){ mval=9;}
                if(SMonth.trim().equals("Oct")){ mval=10;}
                if(SMonth.trim().equals("Nov")){ mval=11;}
                if(SMonth.trim().equals("Dec")){ mval=12;}
        
          return mval;
        }

}
