package CProduction;

import java.io.*;
import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
public class OffEffy extends HttpServlet
{
     HttpSession    session;

     Common common   = new Common();
	Control control = new Control();
     String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody,tbgBody;
     String SStDate,SUnit,SDept,SOffCode,SOfficer,SDesig;
     String SDeptCode,SUnitCode;

     Vector VDept,VDeptCode,VDesig;

     OfficerClass OC;

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);

          bgColor = common.bgColor;
          bgHead  = common.bgHead;
          bgUom   = common.bgUom;
          bgBody  = common.bgBody;
          fgHead  = common.fgHead;
          fgUom   = common.fgUom;
          fgBody  = common.fgBody;
		tbgBody = common.tbgBody;
     }
     public void doGet(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
          response.setContentType("text/html");
          PrintWriter out  = response.getWriter();
          SUnitCode        = (request.getParameter("Unit")).trim();
          SStDate          = common.pureDate((request.getParameter("Date")).trim());
          SDeptCode        = (request.getParameter("Dept")).trim();
          SDept            = (request.getParameter("Dept")).trim();
          //SDept            = (request.getParameter("DeptName")).trim();
          SOfficer         = (request.getParameter("EmpName")).trim();
          SOffCode         = (request.getParameter("EmpName")).trim();
          //SOffCode         = (request.getParameter("EmpCode")).trim();
          SDesig           = "PO";
          //SDesig           = (request.getParameter("EmpDesig")).trim();
          SUnit            = common.toInt(SUnitCode)==1?"A":(common.toInt(SUnitCode)==2?"B":"S");
          
		
		//System.out.print("OffEffy->" + SUnit + ":" + SDept + ":" + SStDate + ":");
		//System.out.println(SOfficer + ":" + SOffCode + ":" + SDesig);
		session = request.getSession(true);
		/*if(session.getValue("AppletTag")==null)
		{
			String SServer = control.getID("Select ProdHost from Server");
			response.sendRedirect(SServer + "/SessionExpired");
			return;

		}*/

		if(common.toInt(SDeptCode)==7)
          {
          }
          else
          {
               OC = null;
               OC = new OfficerClass(SOffCode,SOfficer,SDesig,SUnitCode,SDeptCode,SDept,SStDate);
               OC.prtWorkRep();
               toHTML(out);
          }

     }
     private void toHTML(PrintWriter out)
     {
          out.println("<html>");
          out.println("<body bgcolor='"+bgBody+"' alink='#0000FF'>");
          out.println("<a href='Header'>Home</a>");
		//out.println(session.getValue("AppletTag"));
          out.println("<p align='center'><font size='5' color='maroon'><b>Officer wise Production Report</b></font></p>");
		out.println("<p align='left' style='FONT-SIZE: small; FONT-WEIGHT: bold'>");
		out.println("<font color='mediumblue'>Officer&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple> "+SOfficer+" : "+SOffCode+" : "+SDesig+"</font><br><br>");
		out.println("<font color='mediumblue'>Unit&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple> " + SUnit + "</font>&nbsp;&nbsp;&nbsp;&nbsp;");
		out.println("<font color='mediumblue'>Department&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple> " + SDept + "</font>&nbsp;&nbsp;&nbsp;&nbsp;");
		out.println("<font color='mediumblue'>Date&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple>" + common.parseDate(SStDate) + "</font>&nbsp;&nbsp;&nbsp;&nbsp;"); 
		out.println("<br><br>");

//          out.println("<p align='center'><font size='4' color='maroon'><b>Efficiency Of : "+SWorker+"</b></font></p>");
//          out.println("<p align='center'><font size='4' color='maroon'><b>Unit          : "+SUnit+"  Department : "+SDept+"</b></font></p>");
//          out.println("<p align='center'><font size='4' color='maroon'><b>Period        : "+common.parseDate(SStDate)+"</b></font></p>");
		
		out.println("<div align='left'>");
          out.println("  <table border='1' width='1200' height='1'>");
          out.println("    <tr>");
          out.println("      <td width='35' height='94' rowspan='4' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Date</font></td>");
          out.println("      <td width='35' height='94' rowspan='4' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Shift</font></td>");
          out.println("      <td width='35' height='94' rowspan='4' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Mach No</font></td>");
          out.println("      <td width='35' height='94' rowspan='4' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Count</font></td>");
          out.println("      <td width='35' height='94' rowspan='4' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Shade</font></td>");
          out.println("      <td width='70' height='22' colspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Worker</font></td>");
          out.println("      <td width='70' height='23' colspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Production</font></td>");
          out.println("      <td width='104' height='23' colspan='3' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Efficiency</font></td>");
          out.println("      <td width='272' height='20' colspan='7' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Excess Stoppage Details</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='35' height='26' rowspan='3' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>No</font></td>");
          out.println("      <td width='35' height='26' rowspan='3' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Name</font></td>");
          out.println("      <td width='35' height='24' rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Actual</font></td>");
          out.println("      <td width='35' height='24' rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Loss</font></td>");
          out.println("      <td width='35' height='24' rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>UxE OA</font></td>");
          out.println("      <td width='35' height='24' rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>UxE M</font></td>");
          out.println("      <td width='34' height='24' rowspan='2' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Worker</font></td>");
          out.println("      <td width='136' height='17' colspan='5' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Officer's Response</font></td>");
          out.println("      <td width='136' height='19' colspan='3' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Worker's Response</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='34' height='4' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>"+OC.SName[0]+"</font></td>");
          out.println("      <td width='34' height='4' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>"+OC.SName[1]+"</font></td>");
          out.println("      <td width='34' height='4' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>"+OC.SName[2]+"</font></td>");
          out.println("      <td width='34' height='4' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>"+OC.SName[3]+"</font></td>");
          out.println("      <td width='34' height='4' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>"+OC.SName[4]+"</font></td>");
          out.println("      <td width='34' height='7' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>SC With AC</font></td>");
          out.println("      <td width='34' height='7' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>SC Without AC</font></td>");
          out.println("      <td width='34' height='7' align='center' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Shift AC</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='35' height='1' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>Kg</font></td>");
          out.println("      <td width='35' height='1' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>Kg</font></td>");
          out.println("      <td width='35' height='1' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='35' height='1' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='34' height='1' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='34' height='1' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>Min</font></td>");
          out.println("      <td width='34' height='1' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>Min</font></td>");
          out.println("      <td width='34' height='1' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>Min</font></td>");
          out.println("      <td width='34' height='1' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>Min</font></td>");
          out.println("      <td width='34' height='1' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>   </font></td>");
          out.println("      <td width='34' height='1' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>Man Min</font></td>");
          out.println("      <td width='34' height='1' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>Man Min</font></td>");
          out.println("      <td width='34' height='1' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>Min</font></td>");
          out.println("    </tr>");
          for(int i=0;i<OC.VMach.size();i++)
          {
               out.println("    <tr>");
               out.println("      <td width='35' height='54' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+common.parseDate(SStDate)+"</font></td>");
               out.println("      <td width='35' height='54' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)OC.VShift.elementAt(i)+"</font></td>");
               out.println("      <td width='35' height='54' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)OC.VMach.elementAt(i)+"</font></td>");
               out.println("      <td width='35' height='54' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)OC.VCount.elementAt(i)+"</font></td>");
               out.println("      <td width='35' height='54' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>&nbsp;</font></td>");
               out.println("      <td width='35' height='1' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)OC.VWorkCode.elementAt(i)+"</font></td>");
               out.println("      <td width='35' height='1' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)OC.VWork.elementAt(i)+"</font></td>");
               out.println("      <td width='35' height='1' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)OC.VProd.elementAt(i)+"</font></td>");
               out.println("      <td width='35' height='1' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)OC.VLoss.elementAt(i)+"</font></td>");
               out.println("      <td width='35' height='1' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)OC.VUeOA.elementAt(i)+"</font></td>");
               out.println("      <td width='35' height='1' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)OC.VUeM.elementAt(i)+"</font></td>");
               out.println("      <td width='34' height='1' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)OC.VWorkEffy.elementAt(i)+"</font></td>");

               int iMCode = common.toInt((String)OC.VMachCode.elementAt(i));
               int iShift = common.toInt((String)OC.VShift.elementAt(i));

               MachClass MC = (MachClass)OC.VMachClass.elementAt(OC.indexOf(iMCode,iShift));

               out.println("      <td width='34' height='1' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+MC.S1+"</font></td>");
               out.println("      <td width='34' height='1' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+MC.S2+"</font></td>");
               out.println("      <td width='34' height='1' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+MC.S3+"</font></td>");
               out.println("      <td width='34' height='1' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+MC.S4+"</font></td>");
               out.println("      <td width='34' height='1' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'></font></td>");
               out.println("      <td width='34' height='1' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)OC.VAC.elementAt(i)+"</font></td>");
               out.println("      <td width='34' height='1' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)OC.VWithoutAC.elementAt(i)+"</font></td>");
               out.println("      <td width='34' height='1' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+MC.SClean+"</font></td>");
               out.println("    </tr>");
          }
          out.println("  </table>");
          out.println("</div>");
          out.println("</body>");
          out.println("</html>");

          out.close();

     }
     private String parseDesig(String str)
     {
          StringTokenizer ST = new StringTokenizer(str,"~");
          while(ST.hasMoreTokens())
               str=ST.nextToken();

          StringTokenizer ST1 = new StringTokenizer(str,"$");
          while(ST1.hasMoreTokens())
               str=ST1.nextToken();

          return str;
     }
     private String parseAcCode(String str)
     {
          StringTokenizer ST = new StringTokenizer(str,"~");
          while(ST.hasMoreTokens())
               str=ST.nextToken();

          StringTokenizer ST1 = new StringTokenizer(str,"$");
          while(ST1.hasMoreTokens())
          {
               str=ST1.nextToken();
               break;
          }
          return str;
     }

     private String parseAcName(String str)
     {
          StringTokenizer ST = new StringTokenizer(str,"~");
          while(ST.hasMoreTokens())
          {
               str=ST.nextToken();
               break;
          }
          return str;
     }
}
