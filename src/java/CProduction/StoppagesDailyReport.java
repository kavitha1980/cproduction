package CProduction;

import java.io.*;
import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class StoppagesDailyReport extends HttpServlet
{
     HttpSession    session;
     RepVect repVect = new RepVect();
     Common common   = new Common();

     String SDate,SShift,SUnit,SDept;

     Vector VHead,VHead1,VRow;
     Vector VStopCode,VMachCode,VStopMint,VStopName,VMacName;
     Vector VMCode,VProdMnt;

     StopRepRow SR;
     String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody,tbgBody;

     FileWriter  FW;
     String SLine,SHead1,SHead2,SHead3;
     int iindex=0;
     double dProdMnt=0;

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);

          bgColor = common.bgColor;
          bgHead  = common.bgHead;
          bgUom   = common.bgUom;
          bgBody  = common.bgBody;
          fgHead  = common.fgHead;
          fgUom   = common.fgUom;
          fgBody  = common.fgBody;
		tbgBody = common.tbgBody;
     }

     public void doGet(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
            doPost(request,response);
     }
     public void doPost(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
          response.setContentType("text/html");
          PrintWriter out  = response.getWriter();
//		System.out.println(request.getQueryString());
		session = request.getSession(true);
		/*if(session.getValue("AppletTag")==null)
		{
			Control control = new Control();
			String SServer = control.getID("Select ProdTomHost from Server");
			response.sendRedirect(SServer + "/CProduction.SessionExpired");
			return;

		}*/		
		SDate  = request.getParameter("Date");
		SDate  = common.pureDate(SDate);
		SShift = request.getParameter("Shift").trim();
		SUnit  = request.getParameter("Unit").trim();
		SDept  = request.getParameter("Dept").trim();

		System.out.println("StopDaily->" + SDept +":"+ SUnit +":"+ SShift + ":" + SDate);

          SLine= common.Replicate("-",5);
          SHead1=common.Pad("Mac",3)+common.Space(2);
          SHead2=common.Pad("No",3)+common.Space(2);
          SHead3=common.Pad(" ",3)+common.Space(2);

          setVectors();
          setProdVectors();

		String SDeptName = (String)repVect.VDept.elementAt(repVect.VDeptCode.indexOf(SDept));
		String STitle = "Machine Wise Stoppages" + " - "+ SDeptName;

          out.println("<html>");
          out.println("<head>");
          out.println("<title> </title>");
          out.println("</head>");
          out.println("<body bgcolor='" + common.bgBody + "'>");
		//out.println(session.getValue("AppletTag"));
          out.println("<a href='Header'>Home</a>");
		out.println(common.getStopReportHeader(STitle,SUnit,SShift,SDate,SDeptName));
		out.println("<center><br><br>");
          out.println("  <table border='1' width='417' height='40'>");
          out.println("    <tr>");
          out.println("      <td width='114' height='40' rowspan='2' bgcolor='"+bgHead+"'><font color='"+fgHead+"' size='4'>Machine");
          out.println("        Name</font></td>");

          for(int i=0;i<VHead.size();i++)
          {
               out.println("      <td width='122' height='17' colspan='2' bgcolor='"+bgHead+"' align='center'><font color='"+fgHead+"' size='4'>"+(String)VHead.elementAt(i)+"</font></td>");
               SHead1 = SHead1+common.Pad((String)VHead.elementAt(i),15)+common.Space(2);
               SHead2 = SHead2+common.Replicate("-",15)+common.Space(2);
               SLine  = SLine+common.Replicate("-",17);
          }

          out.println("    </tr>");
          out.println("    <tr>");
          for(int i=0;i<VHead1.size();i++)
          {
               out.println("      <td width='48' height='11' bgcolor='"+bgUom+"' align='right'><font color='"+fgUom+"'>Time</font></td>");
               out.println("      <td width='68' height='11' bgcolor='"+bgUom+"' align='right'><font color='"+fgUom+"'>Kgs</font></td>");
               SHead3 = SHead3+common.Rad("mnt",6)+common.Space(2)+common.Rad("Kgs",7)+common.Space(2);
          }
          out.println("    </tr>");

          try
          {
               FW      = new FileWriter("/software/C-Prod-Print/Reports/stopc.prn");
               //FW      = new FileWriter("/production/Reports/stopc.prn");
               FW.write("Amarjothi Spinning Mills Ltd, Nambiyur\n");
               FW.write("Stoppages Report - "+common.parseDate(SDate)+" - Shift : "+SShift+"\n");
               FW.write("Department - "+repVect.VDept.elementAt(repVect.VDeptCode.indexOf(SDept))+"\n\n");
               FW.write(SLine+"\n");
               FW.write(SHead1+"\n");
               FW.write(SHead2+"\n");
               FW.write(SHead3+"\n");
               FW.write(SLine+"\n");
          }catch(Exception ex){}

          for(int i=0;i<VRow.size();i++)
          {
               StopRepRow SR   = (StopRepRow)VRow.elementAt(i);
               String SMacName = SR.SMacName;

               iindex   = VMCode.indexOf(common.getRound(SR.SMacCode,0));
               try
               {
                    if(iindex<0)
                         dProdMnt = 0;
                    else 
                         dProdMnt = common.toDouble((String)VProdMnt.elementAt(iindex));
               }catch(Exception ex){System.out.println(ex);}

               out.println("    <tr>");
               out.println("      <td width='114' height='40' align='center' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+SMacName+"</font></td>");

               try
               {
                    FW.write(common.Pad(SMacName,3)+common.Space(2));
               }catch(Exception ex){}

               Vector VFields  = SR.getFields(VHead1);
               for(int j=0;j<VFields.size();j++)
               {
                    out.println("      <td width='48' height='40' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VFields.elementAt(j)+"</font></td>");
                    out.println("      <td width='68' height='40' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+common.getRound(dProdMnt*common.toDouble((String)VFields.elementAt(j)),2)+"</font></td>");
                    try
                    {
                         FW.write(common.Rad((String)VFields.elementAt(j),6)+common.Space(2)+common.Rad(common.getRound(dProdMnt*common.toDouble((String)VFields.elementAt(j)),2),7)+common.Space(2));
                    }catch(Exception ex){}
               }
               out.println("    </tr>");
               try
               {
                    FW.write("\n");
               }catch(Exception ex){}
          }

          try
          {
               FW.write(SLine+"\n");
               FW.close();
          }catch(Exception ex){}

          out.println("  </table></center>");
          out.println("</div>");
          out.println("</body>");
          out.println("</html>");
          out.close();
     }
     public void setVectors()
     {
          VStopCode  = new Vector();
          VMachCode  = new Vector();
          VStopMint  = new Vector();
          VStopName  = new Vector();
          VMacName   = new Vector();
          VRow       = new Vector();
          try
          {
                  Connection con = createConnection();
                  Statement stat = con.createStatement();
                  ResultSet res  = stat.executeQuery(getQString());
                  while(res.next())
                  {
                         VStopCode.addElement(res.getString(1));
                         VMachCode.addElement(res.getString(2));
                         VStopMint.addElement(res.getString(3));
                         VStopName.addElement(res.getString(4));
                         VMacName.addElement(res.getString(5));
                  }
                  con.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
               return;
          }
          VRow = new Vector();
          for(int i=0;i<VStopCode.size();i++)
          {
               organize((String)VStopCode.elementAt(i),
                        (String)VMachCode.elementAt(i),
                        (String)VStopMint.elementAt(i),
                        (String)VStopName.elementAt(i),
                        (String)VMacName.elementAt(i));
          }
          setHead();
     }
     public String getQString()
     {
               String QS = " SELECT StopTran.Stop_Code, StopTran.Mach_Code, Sum(StopTran.Stop_Mint) AS SumOfStop_Mint, " + 
				       " StopReasons.Stop_Name, Machine.Mach_Name "+
                           " FROM (StopTran INNER JOIN machine ON StopTran.Mach_Code=machine.mach_code) " + 
				       " Inner Join StopReasons On StopReasons.Stop_Code = StopTran.Stop_Code "+
                           " GROUP BY StopTran.Stop_Code, StopTran.Mach_Code, StopTran.Stop_Date, " + 
				       " StopTran.Stop_Shift, StopTran.Unit_Code, StopTran.Dept_Code,"  +
				       " StopReasons.Stop_Name,Machine.Mach_Name "+
                           " HAVING (((StopTran.Stop_Date)='"+SDate+"') AND ((StopTran.Stop_Shift)="+SShift+
				       ") AND ((StopTran.Unit_Code)="+SUnit+") AND " + 
				       " ((StopTran.Dept_Code)="+SDept+")) Order By 2,1";
               return QS;
     }          
     public void organize(String SStopCode,String SMacCode,String SStopMint,String SStopName,String SMacName)
     {
          int iindex = -1;
          for(int i=0;i<VRow.size();i++)
          {
               StopRepRow SR = (StopRepRow)VRow.elementAt(i);
               if(SR.SMacCode.equals(SMacCode))
               {
                    SR.VStopCode.addElement(SStopCode);
                    SR.VStopName.addElement(SStopName);
                    SR.VStopMint.addElement(SStopMint);
                    VRow.setElementAt(SR,i);
                    iindex = i;
                    break;
               }
          }
          if(iindex == -1)
          {
               StopRepRow SR = new StopRepRow();
               SR.setData(SStopCode,SMacCode,SStopMint,SStopName,SMacName);
               VRow.addElement(SR);
          }
     }
     public void setHead()
     {
          VHead1 = new Vector();
          VHead  = new Vector();
          for(int i=0;i<VStopCode.size();i++)
          {
               String SStopCode = (String)VStopCode.elementAt(i);
               String SStopName = (String)VStopName.elementAt(i);
               int iindex = VHead1.indexOf(SStopCode);
               if(iindex == -1)
               {
                    VHead1.addElement(SStopCode);
                    VHead.addElement(SStopName);
               }   
          }
     }
     public String getProdString()
     {
          String QS="";
          if(SDept.equals("2"))
          {
               QS = " SELECT blowtran.Mach_Code, prod/(hourrun*60) AS Expr1 "+
                    " FROM blowtran "+
                    " WHERE (((blowtran.Unit_Code)="+SUnit+") AND "+
                    " ((blowtran.shift_code)="+SShift+") AND ((blowtran.sp_date)='"+SDate+"') AND hourrun>0)";
          }
          else if(SDept.equals("3"))
          {
               QS = " SELECT cardtran.Mach_Code, prod/(hourrun*60) AS Expr1 "+
                    " FROM cardtran "+
                    " WHERE (((cardtran.Unit_Code)="+SUnit+") AND "+
                    " ((cardtran.shift_code)="+SShift+") AND ((cardtran.sp_date)='"+SDate+"') AND hourrun>0)";
          }
          else if(SDept.equals("5"))
          {
               QS = " SELECT drawtran.Mach_Code, prod/(hourrun*60) AS Expr1 "+
                    " FROM drawtran "+
                    " WHERE (((drawtran.Unit_Code)="+SUnit+") AND "+
                    " ((drawtran.shift_code)="+SShift+") AND ((drawtran.sp_date)='"+SDate+"') AND hourrun>0)";
          }
          else if(SDept.equals("6"))
          {
               QS = " SELECT simptran.Mach_Code, prod/(hoursrun*60) AS Expr1 "+
                    " FROM simptran "+
                    " WHERE (((simptran.Unit_Code)="+SUnit+") AND "+
                    " ((simptran.shift_code)="+SShift+") AND ((simptran.sp_date)='"+SDate+"') AND hoursrun>0)";
          }
          else if(SDept.equals("7"))
          {
               QS = " SELECT spintran.Mach_Code, prod/(hoursrun*60) AS Expr1 "+
                    " FROM spintran "+
                    " WHERE (((spintran.Unit_Code)="+SUnit+") AND "+
                    " ((spintran.shift_code)="+SShift+") AND ((spintran.sp_date)='"+SDate+"') AND hoursrun>0)";
          }
          return QS;
     }
     public void setProdVectors()
     {
          String QS = getProdString();
          VProdMnt = new Vector();
          VMCode   = new Vector();
          try
          {
                  Connection con = createConnection();
                  Statement stat = con.createStatement();
                  ResultSet res  = stat.executeQuery(QS);
                  while(res.next())
                  {
                         VMCode  .addElement(res.getString(1));
                         VProdMnt.addElement(res.getString(2));
                  }
                  con.close();
          }
          catch(Exception ex)
          {
               //System.out.println(QS);
               //System.out.println("setProdVectors"+ex);
          }
     }

	private Connection createConnection()
	{
		Connection conn;
		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
			conn  = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
		}catch(Exception ee)
		{
			return null;
		}
		return conn;
	}

}


