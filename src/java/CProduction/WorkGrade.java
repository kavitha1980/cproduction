package CProduction;

import java.io.*;
import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
public class WorkGrade extends HttpServlet
{
     HttpSession    session;

     Common common   = new Common();

     String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody,tbgBody;
     String SStDate,SEnDate,SUnit;
     String SUnitCode;

     Vector VDeptCode,VSCAC,VSC,VShiftAC,VEffy;

     Vector VWorkGradeClass;

     Vector VDept,VCode,VName,VGrade,VEffyRep,VSCRep,VSCPerRep,VSCACRep,VSCACPerRep;
     Vector VShift,VShiftPer,VOther,VOtherPer,VGCode;

     FileWriter  FW;
     int Lctr = 70,Pctr = 0;
     int iDeptSig = 0,iGradeSig=0;
     String SLine;
     Vector VHead;

     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);

          bgColor = common.bgColor;
          bgHead  = common.bgHead;
          bgUom   = common.bgUom;
          bgBody  = common.bgBody;
          fgHead  = common.fgHead;
          fgUom   = common.fgUom;
          fgBody  = common.fgBody;
		tbgBody = common.tbgBody;
     }
    public void doGet(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
            doPost(request,response);
     }
     public void doPost(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
		try
		{
		response.setContentType("text/html");
          PrintWriter out  = response.getWriter();
		session = request.getSession(true);
	/*	if(session.getValue("AppletTag")==null)
		{
			Control control = new Control();
			String SServer = control.getID("Select ProdTomHost from Server");
			response.sendRedirect(SServer + "/CProduction.SessionExpired");
			return;
	}*/
		SUnitCode = (request.getParameter("Unit")).trim();
          SStDate   = common.pureDate((request.getParameter("StDate")).trim());
		SEnDate   = common.pureDate((request.getParameter("EnDate")).trim());
          SUnit     = common.toInt(SUnitCode)==1?"A":(common.toInt(SUnitCode)==2?"B":"S");          

		System.out.println("Work Grade->" + SUnit + ":" + SStDate + ":" + SEnDate);

          setVectors();
          fillVectors();

		try
          {
               FW      = new FileWriter("D:/production/reports/grade1c.prn");
               Lctr    = 70;
               Pctr    =  0;
               Head();
               Body();
               FW.close();
          }catch(Exception ex){}

          out.println("<html>");
          out.println("<body bgcolor='"+bgBody+"' alink='#0000FF'>");
		out.println(session.getValue("AppletTag"));
          out.println("<p align='center'><font size='5' color='maroon'><b>All department Worker Efficiency Grade Report</b></font></p>");
		out.println("<p align='left' style='FONT-SIZE: small; FONT-WEIGHT: bold'>");
		out.println("<font color='mediumblue'>Unit&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple> " + SUnit + "</font>&nbsp;&nbsp;&nbsp;&nbsp;");
		out.println("<font color='mediumblue'>Start Date&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple>" + common.parseDate(SStDate) + "</font>&nbsp;&nbsp;&nbsp;&nbsp;"); 
		out.println("<font color='mediumblue'>End Date&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple>" + common.parseDate(SEnDate) + "</font>"); 
		out.println("<br><br>");
          out.println("<div align='left'>");
          out.println("  <table border='1' width='800' height='42'>");
          out.println("    <tr>");
          out.println("      <td width='64' height='111' rowspan='3' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Department</font></td>");
          out.println("      <td width='72' height='111' rowspan='3' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Grade</font></td>");
          out.println("      <td width='90' height='111' rowspan='3' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Worker Name</font></td>");
          out.println("      <td width='66' height='111' rowspan='3' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Ticket No</font></td>");
          out.println("      <td width='62' height='111' rowspan='3' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Worker Effy</font></td>");
          out.println("      <td width='252' height='26' colspan='6' bgcolor='"+bgHead+"' align='center'><font color='"+fgHead+"'>Excess Effy Loss Due To</font></td>");
          out.println("      <td width='82' height='104' colspan='2' rowspan='2' bgcolor='"+bgHead+"'><font color='"+fgHead+"'>Other Losses</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='84' height='37' colspan='2' bgcolor='"+bgHead+"' align='center' valign='top'><font color='"+fgHead+"'>Shade Change With Air Cleaning</font></td>");
          out.println("      <td width='84' height='37' colspan='2' bgcolor='"+bgHead+"' align='center' valign='top'><font color='"+fgHead+"'>Shade Change Without Air Cleaning</font></td>");
          out.println("      <td width='84' height='37' colspan='2' bgcolor='"+bgHead+"' align='center' valign='top'><font color='"+fgHead+"'>Shift End Air Cleaning</font></td>");
          out.println("    </tr>");
          out.println("    <tr>");
          out.println("      <td width='42' height='31' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>Min</font></td>");
          out.println("      <td width='42' height='31' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='42' height='31' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>Min</font></td>");
          out.println("      <td width='42' height='31' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='42' height='31' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>Min</font></td>");
          out.println("      <td width='42' height='31' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>%</font></td>");
          out.println("      <td width='41' height='25' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>Min</font></td>");
          out.println("      <td width='41' height='25' align='right' bgcolor='"+bgUom+"'><font color='"+fgUom+"'>%</font></td>");
          out.println("    </tr>");
          for(int i=0;i<VDept.size();i++)
          {
               out.println("    <tr>");
               out.println("      <td width='64' height='42' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VDept.elementAt(i)+"</font></td>");
               out.println("      <td width='72' height='42' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VGrade.elementAt(i)+"</font></td>");
               out.println("      <td width='90' height='42' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VName.elementAt(i)+"</font></td>");
               out.println("      <td width='66' height='42' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VCode.elementAt(i)+"</font></td>");
               out.println("      <td width='62' height='42' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VEffyRep.elementAt(i)+"</font></td>");
               out.println("      <td width='42' height='1' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VSCACRep.elementAt(i)+"</font></td>");
               out.println("      <td width='42' height='1' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VSCACPerRep.elementAt(i)+"</font></td>");
               out.println("      <td width='42' height='1' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VSCRep.elementAt(i)+"</font></td>");
               out.println("      <td width='42' height='1' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VSCPerRep.elementAt(i)+"</font></td>");
               out.println("      <td width='42' height='1' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VShift.elementAt(i)+"</font></td>");
               out.println("      <td width='41' height='1' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VShiftPer.elementAt(i)+"</font></td>");
               out.println("      <td width='41' height='1' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VOther.elementAt(i)+"</font></td>");
               out.println("      <td width='42' height='1' align='right' bgcolor='"+tbgBody+"'><font color='"+fgBody+"'>"+(String)VOtherPer.elementAt(i)+"</font></td>");
               out.println("    </tr>");
          }
          out.println("  </table>");
          out.println("</div>");
          out.println("</body>");
          out.println("</html>");

          out.close();
		}catch(Exception ee)
		{
			System.out.println(ee);
		}


     }
     private void setVectors()
     {
          VWorkGradeClass = new Vector();
          VDeptCode       = new Vector();
          VSCAC           = new Vector();
          VSC             = new Vector();
          VShiftAC        = new Vector();
          VEffy           = new Vector();

          try
          {
                 Connection con = createConnection();
                 Statement stat = con.createStatement();
                 ResultSet res0 = stat.executeQuery(getTargets());
                 while(res0.next())
                 {
                    VDeptCode .addElement(res0.getString(1));
                    VEffy     .addElement(res0.getString(2));
                    VSC       .addElement(res0.getString(3));
                    VSCAC     .addElement(res0.getString(4));
                    VShiftAC  .addElement("0");
                 }
                 ResultSet res0X = stat.executeQuery(getShiftStd());
                 while(res0X.next())
                 {
                    String SCode = res0X.getString(1);
                    int iIndex   = VDeptCode.indexOf(SCode);
                    String SStd  = res0X.getString(2);
                    if(iIndex>=0)
                         VShiftAC  .setElementAt(SStd,iIndex);
                 }  
                 ResultSet res1 = stat.executeQuery(getBlowQS1());
                 while(res1.next())
                    organizeGradeClass1(res1,"2");
                 ResultSet res2  = stat.executeQuery(getBlowQS2());
                 while(res2.next())
                    organizeGradeClass2(res2,"2");
                 ResultSet res3 = stat.executeQuery(getCardQS1());
                 while(res3.next())
                    organizeGradeClass1(res3,"3");
                 ResultSet res4  = stat.executeQuery(getCardQS2());
                 while(res4.next())
                    organizeGradeClass2(res4,"3");
                 if(common.toInt(SUnitCode)==2)
                 {
                      ResultSet res5 = stat.executeQuery(getSLapQS1());
                      while(res5.next())
                         organizeGradeClass1(res5,"9");
                      ResultSet res6  = stat.executeQuery(getSLapQS2());
                      while(res6.next())
                         organizeGradeClass2(res6,"9");
                      ResultSet res7 = stat.executeQuery(getCombQS1());
                      while(res7.next())
                         organizeGradeClass1(res7,"4");
                      ResultSet res8  = stat.executeQuery(getCombQS2());
                      while(res8.next())
                         organizeGradeClass2(res8,"4");
                 }
                 ResultSet res9 = stat.executeQuery(getDrawQS1());
                 while(res9.next())
                    organizeGradeClass1(res9,"5");
                 ResultSet res10  = stat.executeQuery(getDrawQS2());
                 while(res10.next())
                    organizeGradeClass2(res10,"5");
                 ResultSet res11 = stat.executeQuery(getSimpQS1());
                 while(res11.next())
                    organizeGradeClass1(res11,"6");
                 ResultSet res12  = stat.executeQuery(getSimpQS2());
                 while(res12.next())
                    organizeGradeClass2(res12,"6");
                 con.close();
          }
          catch(Exception ex)
          {
                 System.out.println("111 : "+ex);
                 ex.printStackTrace();
          }
     }
     private void organizeGradeClass1(ResultSet res,String SDept) throws Exception
     {
          String SCode   = res.getString(1);
          String SName   = getTicket(res.getString(2));

          double dEffy   = res.getDouble(3);
          int    iAC     = res.getInt(4);
          int    iNoAC   = res.getInt(5);
          int    iTime   = res.getInt(6);
          int    iNoTime = res.getInt(7);
          String SStop   = res.getString(8);

          double dSCStd      = common.toDouble((String)VSC.elementAt(VDeptCode.indexOf("2")));
          double dSCACStd    = common.toDouble((String)VSCAC.elementAt(VDeptCode.indexOf("2")));
          double dShiftACStd = common.toDouble((String)VShiftAC.elementAt(VDeptCode.indexOf("2")));
          double dEffyStd    = common.toDouble((String)VEffy.elementAt(VDeptCode.indexOf("2")));

          double dSCAC = iNoAC==0?0:(dSCACStd-(iAC/iNoAC));     //SC with AC minutes - Std
          double dSC   = iNoTime==0?0:(dSCStd-(iTime/iNoTime)); //SC minutes - Std

          double dSCACPer = dSCAC==0?0:dSCAC*100/dSCACStd;
          double dSCPer   = dSC==0?0:dSC*100/dSCStd;

          int iindex = indexOf(SCode,SDept);
          if(iindex==-1)
          {
               WorkGradeClass wgc = new WorkGradeClass(SCode,SName,SDept);
               wgc.setStds(dSCStd,dSCACStd,dShiftACStd,dEffyStd);
               wgc.append(dEffy,dSC,dSCPer,dSCAC,dSCACPer,SStop);
               VWorkGradeClass.addElement(wgc);
          }
          else
          {
               WorkGradeClass wgc = (WorkGradeClass)VWorkGradeClass.elementAt(iindex);
               wgc.append(dEffy,dSC,dSCPer,dSCAC,dSCACPer,SStop);
               VWorkGradeClass.setElementAt(wgc,iindex);
          }
     }
     private void organizeGradeClass2(ResultSet res,String SDept) throws Exception
     {
          String SCode   = res.getString(1);
          String SName   = getTicket(res.getString(2));
          double dStop   = res.getDouble(3);
          int    iCtr    = res.getInt(4);

          double dSCStd      = common.toDouble((String)VSC.elementAt(VDeptCode.indexOf("2")));
          double dSCACStd    = common.toDouble((String)VSCAC.elementAt(VDeptCode.indexOf("2")));
          double dShiftACStd = common.toDouble((String)VShiftAC.elementAt(VDeptCode.indexOf("2")));
          double dEffyStd    = common.toDouble((String)VEffy.elementAt(VDeptCode.indexOf("2")));

          int iindex = indexOf(SCode,SDept);
          if(iindex==-1)
          {
               WorkGradeClass wgc = new WorkGradeClass(SCode,SName,SDept);
               wgc.setStds(dSCStd,dSCACStd,dShiftACStd,dEffyStd);
               wgc.setShiftAC(dStop,iCtr);
               VWorkGradeClass.addElement(wgc);
          }
          else
          {
               WorkGradeClass wgc = (WorkGradeClass)VWorkGradeClass.elementAt(iindex);
               wgc.setShiftAC(dStop,iCtr);
               VWorkGradeClass.setElementAt(wgc,iindex);
          }
     }

     private int indexOf(String SCCode,String SDept)
     {
          int index = -1;
          for(int i=0;i<VWorkGradeClass.size();i++)
          {
               WorkGradeClass wgc = (WorkGradeClass)VWorkGradeClass.elementAt(i);
               if(wgc.SCode.equals(SCCode) && wgc.SDept.equals(SDept))
               {
                    index = i;
                    break;
               }
          }
          return index;
     }
     private String getTargets()
     {
          String QS="";
          QS = " SELECT Target.dept_code, Target.work_effy, "+
               " Target.RunOutStd, Target.RunOutAcStd "+
               " FROM Target where unit_code="+SUnitCode+" order by 1";
         return QS;
     }
     private String getShiftStd()
     {
          //Stop_Code=30 stands for ShiftEnd Air Cleaning
          String QS = "";
          QS = " SELECT Stoppage.dept_code,Stoppage.MillStd "+
               " FROM Stoppage where stop_code=30 order by 2";
          return QS;
     }
     private String getBlowQS1()
     {
          String QS = "";
          // Blowroom
          QS = " SELECT blowtran.ticket, blowtran.ticket, blowtran.workereffy, "+
               " blowtran.rotimeac, blowtran.noofpersonsac, blowtran.rotime, "+
               " blowtran.noofpersons,blowtran.stopmnt "+
               " FROM blowtran "+
               " WHERE blowtran.unit_code="+SUnitCode+
               " And blowtran.sp_date>='"+SStDate+"'"+
               " And blowtran.sp_date<='"+SEnDate+"' And blowtran.ticket>0 "+
               " ORDER BY blowtran.workereffy DESC";

          return QS;
     }
     private String getBlowQS2()
     {
          String QS = "";
          //Blowroom
          QS = " SELECT blowtran.ticket, blowtran.ticket, "+
               " Sum(StopTran.Stop_Mint) AS SumOfStop_Mint,"+
               " Count(StopTran.Stop_Mint) AS CountOfStop_Mint "+
               " FROM BlowTran INNER JOIN StopTran ON (blowtran.sp_date=StopTran.Stop_Date) AND "+
               " (blowtran.shift_code=StopTran.Stop_Shift) AND "+
               " (blowtran.unit_code=StopTran.Unit_Code) AND "+
               " (blowtran.mach_code=StopTran.mach_Code) "+
               " WHERE StopTran.Stop_Code=30 And StopTran.Dept_Code=2 "+
               " And StopTran.unit_Code="+SUnitCode+
               " And StopTran.Stop_Mint > 0 "+
               " AND blowtran.ticket > 0 "+
               " And blowtran.sp_date>='"+SStDate+"' And blowtran.sp_date<='"+SEnDate+"'"+
               " GROUP BY blowtran.ticket";

          return QS;
     }
     
     private String getCardQS1()
     {
          String QS = "";
          QS = " SELECT CardTran.ticket, CardTran.ticket, CardTran.workereffy, "+
               " CardTran.rotimeac, CardTran.noofpersonsac, CardTran.rotime, "+
               " CardTran.noofpersons,Cardtran.stopmnt "+
               " FROM CardTran "+
               " WHERE CardTran.unit_code="+SUnitCode+
               " And CardTran.sp_date>='"+SStDate+"'"+
               " And CardTran.sp_date<='"+SEnDate+"' And CardTran.ticket>0 "+
               " ORDER BY CardTran.workereffy DESC";
          return QS;
     }
     private String getCardQS2()
     {
          String QS = "";
          QS = " SELECT CardTran.ticket,CardTran.ticket, "+
               " Sum(StopTran.Stop_Mint) AS SumOfStop_Mint "+
               " FROM CardTran INNER JOIN StopTran ON (CardTran.sp_date=StopTran.Stop_Date) AND "+
               " (CardTran.shift_code=StopTran.Stop_Shift) AND "+
               " (CardTran.unit_code=StopTran.Unit_Code) "+
               " AND (cardtran.mach_code=StopTran.mach_Code) "+
               " WHERE StopTran.Stop_Code=30 And StopTran.Dept_Code=2 "+
               " And StopTran.unit_Code="+SUnitCode+
               " And StopTran.Stop_Mint > 0 "+
               " AND cardtran.ticket > 0 "+
               " And CardTran.sp_date>='"+SStDate+"' And CardTran.sp_date<='"+SEnDate+"'"+
               " GROUP BY CardTran.ticket";
          return QS;
     }
     private String getSLapQS1()
     {
          String QS = "";
          QS = " SELECT SLaptran.ticket, SLaptran.ticket, SLaptran.workereffy, "+
               " SLaptran.rotimeac, SLaptran.noofpersonsac, SLaptran.rotime, "+
               " SLaptran.noofpersons,SLaptran.stopmnt "+
               " FROM SLaptran  "+
               " WHERE SLaptran.unit_code="+SUnitCode+
               " And SLaptran.sp_date>='"+SStDate+"'"+
               " And SLaptran.sp_date<='"+SEnDate+"' And SLaptran.ticket>0 "+
               " ORDER BY SLaptran.workereffy DESC";
          return QS;
     }
     private String getSLapQS2()
     {
          String QS = "";
          QS = " SELECT SLaptran.ticket, SLaptran.ticket, "+
               " Sum(StopTran.Stop_Mint) AS SumOfStop_Mint "+
               " FROM SlapTran INNER JOIN StopTran ON (SLaptran.sp_date=StopTran.Stop_Date) AND "+
               " (SLaptran.shift_code=StopTran.Stop_Shift) AND "+
               " (SLaptran.unit_code=StopTran.Unit_Code) "+
               " ANd (SLaptran.mach_code=StopTran.mach_Code) "+
               " WHERE StopTran.Stop_Code=30 And StopTran.Dept_Code=2 "+
               " And StopTran.unit_Code="+SUnitCode+
               " And StopTran.Stop_Mint > 0 "+
               " AND SLaptran.ticket > 0 "+
               " And SLaptran.sp_date>='"+SStDate+"' And SLaptran.sp_date<='"+SEnDate+"'"+
               " GROUP BY SLaptran.ticket";
          return QS;
     }
     private String getCombQS1()
     {
          String QS = "";
          QS = " SELECT Combtran.ticket, Combtran.ticket, Combtran.workereffy, "+
               " Combtran.rotimeac, Combtran.noofpersonsac, Combtran.rotime, "+
               " Combtran.noofpersons,combtran.stopmnt "+
               " FROM Combtran  "+
               " WHERE Combtran.unit_code="+SUnitCode+
               " And Combtran.sp_date>='"+SStDate+"'"+
               " And Combtran.sp_date<='"+SEnDate+"' And Combtran.ticket>0 "+
               " ORDER BY Combtran.workereffy DESC";
          return QS;
     }
     private String getCombQS2()
     {
          String QS = "";
          QS = " SELECT Combtran.ticket, Combtran.ticket, "+
               " Sum(StopTran.Stop_Mint) AS SumOfStop_Mint "+
               " FROM CombTran INNER JOIN StopTran ON (Combtran.sp_date=StopTran.Stop_Date) AND "+
               " (Combtran.shift_code=StopTran.Stop_Shift) AND "+
               " (Combtran.unit_code=StopTran.Unit_Code) "+
               " AND (Combtran.mach_code=StopTran.mach_Code) "+
               " WHERE StopTran.Stop_Code=30 And StopTran.Dept_Code=2 "+
               " And StopTran.unit_Code="+SUnitCode+
               " And StopTran.Stop_Mint > 0 "+
               " AND Combtran.ticket > 0 "+
               " And Combtran.sp_date>='"+SStDate+"' And Combtran.sp_date<='"+SEnDate+"'"+
               " GROUP BY Combtran.ticket";
          return QS;
     }
     private String getDrawQS1()
     {
          String QS = "";
          QS = " SELECT Drawtran.ticket, Drawtran.ticket, Drawtran.workereffy, "+
               " Drawtran.rotimeac, Drawtran.noofpersonsac, Drawtran.rotime, "+
               " Drawtran.noofpersons,Drawtran.stopmnt "+
               " FROM Drawtran  "+
               " WHERE Drawtran.unit_code="+SUnitCode+
               " And Drawtran.sp_date>='"+SStDate+"'"+
               " And Drawtran.sp_date<='"+SEnDate+"' And Drawtran.ticket>0 "+
               " ORDER BY Drawtran.workereffy DESC";
          return QS;
     }
     private String getDrawQS2()
     {
          String QS = "";
          QS = " SELECT Drawtran.ticket,Drawtran.ticket, "+
               " Sum(StopTran.Stop_Mint) AS SumOfStop_Mint "+
               " FROM DrawTran  INNER JOIN StopTran ON (Drawtran.sp_date=StopTran.Stop_Date) AND "+
               " (Drawtran.shift_code=StopTran.Stop_Shift) AND "+
               " (Drawtran.unit_code=StopTran.Unit_Code) "+
               " AND (Drawtran.mach_code=StopTran.mach_Code) "+
               " WHERE StopTran.Stop_Code=30 And StopTran.Dept_Code=2 "+
               " And StopTran.unit_Code="+SUnitCode+
               " And StopTran.Stop_Mint > 0 "+
               " AND Drawtran.ticket > 0 "+
               " And Drawtran.sp_date>='"+SStDate+"' And Drawtran.sp_date<='"+SEnDate+"'"+
               " GROUP BY Drawtran.ticket";
          return QS;
     }
     private String getSimpQS1()
     {
          String QS = "";
          QS = " SELECT Simptran.ticket, Simptran.ticket, Simptran.workereffy, "+
               " Simptran.rotimeac, Simptran.noofpersonsac, Simptran.rotime, "+
               " Simptran.noofpersons,SimpTran.stop_mt "+
               " FROM Simptran  "+
               " WHERE Simptran.unit_code="+SUnitCode+
               " And Simptran.sp_date>='"+SStDate+"'"+
               " And Simptran.sp_date<='"+SEnDate+"' And Simptran.ticket>0 "+
               " ORDER BY Simptran.workereffy DESC";
          return QS;
     }
     private String getSimpQS2()
     {
          String QS = "";
          QS = " SELECT Simptran.ticket, Simptran.ticket, "+
               " Sum(StopTran.Stop_Mint) AS SumOfStop_Mint "+
               " FROM SimpTran INNER JOIN StopTran ON (Simptran.sp_date=StopTran.Stop_Date) AND "+
               " (Simptran.shift_code=StopTran.Stop_Shift) AND "+
               " (Simptran.unit_code=StopTran.Unit_Code) "+
               " ANd (Simptran.mach_code=StopTran.mach_Code) "+
               " WHERE StopTran.Stop_Code=30 And StopTran.Dept_Code=2 "+
               " And StopTran.unit_Code="+SUnitCode+
               " And StopTran.Stop_Mint > 0 "+
               " AND Simptran.ticket > 0 "+
               " And Simptran.sp_date>='"+SStDate+"' And Simptran.sp_date<='"+SEnDate+"'"+
               " GROUP BY Simptran.ticket";
          return QS;
     }
     private String getDept(String SCode)
     {
          String SDept = "";
          if(SCode.equals("2"))
               SDept = "Blowroom";
          else if(SCode.equals("3"))
               SDept = "Carding";               
          else if(SCode.equals("4"))
               SDept = "Comber";
          else if(SCode.equals("5"))
               SDept = "Drawing";               
          else if(SCode.equals("6"))
               SDept = "Simplex";
          else if(SCode.equals("9"))
               SDept = "SLap/R Lap";               
          return SDept;
     }
     private String getGrade(double dEffy)
     {
          String SGrade = "";
          if(dEffy < 70)
               SGrade = "E :< 70";
          else if(dEffy >= 70 && dEffy <= 75)
               SGrade = "D :70-75";
          else if(dEffy > 75 && dEffy <= 80)
               SGrade = "C :76-80";
          else if(dEffy > 80 && dEffy <= 85)
               SGrade = "B :81-85";
          else if(dEffy > 85)
               SGrade = "A :> 85";
          return SGrade;
     }
     private String getGradeCode(double dEffy)
     {
          String SGrade = "";
          if(dEffy < 70)
               SGrade = "5";
          else if(dEffy >= 70 && dEffy <= 75)
               SGrade = "4";
          else if(dEffy > 75 && dEffy <= 80)
               SGrade = "3";
          else if(dEffy > 80 && dEffy <= 85)
               SGrade = "2";
          else if(dEffy > 85)
               SGrade = "1";
          return SGrade;
     }

     private void fillVectors()
     {
          VDept         = new Vector();
          VCode         = new Vector();
          VName         = new Vector();
          VGrade        = new Vector();
          VEffyRep      = new Vector();
          VSCRep        = new Vector();
          VSCPerRep     = new Vector();
          VSCACRep      = new Vector();
          VSCACPerRep   = new Vector();
          VShift        = new Vector();
          VShiftPer     = new Vector();
          VOther        = new Vector();
          VOtherPer     = new Vector();
          VGCode        = new Vector();

          try
          {
                 Connection con = createConnection();
                 Statement stat = con.createStatement();
                 try{stat.execute("truncate table tempgrade");}catch(Exception ex){}
                 String QS = "";
                 for(int i=0;i<VWorkGradeClass.size();i++)
                 {
                    WorkGradeClass wgc = (WorkGradeClass)VWorkGradeClass.elementAt(i);                    
                    QS  = " Insert Into tempgrade (DeptCode,Department,WorkCode,Worker,";
                    QS += " Grade,Effy,SCMin,SCPer,SCACMin,SCACPer,ShiftMin,ShiftPer,OtherMin,OtherPer,GradeCode) Values (";
                    QS += wgc.SDept+",'"+getDept(wgc.SDept)+"',"+wgc.SCode+",'";
                    QS += wgc.SName+"','"+getGrade(wgc.getEffy())+"',";
                    QS += wgc.getEffy()+","+wgc.getSC()+",";
                    QS += wgc.getSCPer()+","+wgc.getSCAC()+","+wgc.getSCACPer()+",";
                    QS += wgc.dShiftAC+","+wgc.dShiftACPer+","+wgc.getOther()+",";
                    QS += wgc.getOtherPer()+","+getGradeCode(wgc.getEffy())+")";
				
                    stat.execute(QS);
                 }
                 ResultSet res99 = stat.executeQuery(getQS());
                 while(res99.next())
                 {
                    VDept         .addElement(res99.getString(1));
                    VName         .addElement(res99.getString(2));
                    VGrade        .addElement(res99.getString(3));
                    VEffyRep      .addElement(common.getRound(res99.getString(4),2));
                    VSCRep        .addElement(common.getRound(res99.getString(5),0));
                    VSCPerRep     .addElement(common.getRound(res99.getString(6),2));
                    VSCACRep      .addElement(common.getRound(res99.getString(7),0));
                    VSCACPerRep   .addElement(common.getRound(res99.getString(8),2));
                    VShift        .addElement(common.getRound(res99.getString(9),0));
                    VShiftPer     .addElement(common.getRound(res99.getString(10),2));
                    VOther        .addElement(common.getRound(res99.getString(11),0));
                    VOtherPer     .addElement(common.getRound(res99.getString(12),2));
                    VCode         .addElement(res99.getString(13));
                    VGCode        .addElement(res99.getString(14));
                 }
                 con.close();
          }catch(Exception ex)
          {
               System.out.println(ex);
               ex.printStackTrace();
          }
     }
     private String getQS()
     {
          String QS = "";
          QS = " Select Department,Worker,Grade,Effy,SCMin,SCPer,SCACMin, "+
               " SCACPer,ShiftMin,ShiftPer,OtherMin,OtherPer,WorkCode,GradeCode "+
               " From TempGrade Order By 1,4 desc";
          return QS;
     }
     public void Head()
     {
          if(Lctr < 60)
             return;
          if(Pctr > 0)
          {
             try
             {
               FW.write(SLine+"\n");
             }catch(Exception ex){}
          }
          Pctr++;

          String Str1    = common.Pad("Company    : AMARJOTHI SPINNING MILLS LIMITED",80);
          String Str2    = common.Pad("Document   : Worker Grade during "+common.parseDate(SStDate)+" & "+common.parseDate(SEnDate),80);
          String Str3    = common.Pad("Unit       : "+SUnit,70);
          VHead = Stop1Head();
          try
          {
               FW.write(Str1+"\n");
               FW.write(Str2+"\n");
               FW.write(Str3+"\n\n");
               for(int i=0;i<VHead.size();i++)
               {
                    FW.write((String)VHead.elementAt(i));
               }
          }catch(Exception ex){}
          Lctr = 5+VHead.size();
          iGradeSig=0;iDeptSig=0;
     }
     public void Body()
     {
         String Strl = "",SPreDept="",SPreGrade="";
         iDeptSig = 0;iGradeSig=0;
         for(int i=0;i<VDept.size();i++)
         {
                if(SPreDept.equals((String)VDept.elementAt(i)))
                    iDeptSig=1;
                else
                    iDeptSig=0;

                if(SPreGrade.equals((String)VGrade.elementAt(i)))
                    iGradeSig=1;
                else
                {
                    SPreGrade = (String)VGrade.elementAt(i);
                    iGradeSig=0;
                }
                if(iGradeSig==0 || iDeptSig==0)
                    Strl = common.Pad((String)VGrade.elementAt(i),10)+common.Space(2);
                else 
                    Strl  = common.Pad(" ",10)+common.Space(2);

                Strl += common.Pad((String)VCode.elementAt(i),8)+common.Space(2)+
                      common.Pad((String)VName.elementAt(i),15)+common.Space(2)+
                      common.Rad((String)VEffyRep.elementAt(i),6)+common.Space(2)+
                      common.Rad((String)VSCACRep.elementAt(i),6)+common.Space(2)+
                      common.Rad((String)VSCACPerRep.elementAt(i),6)+common.Space(2)+
                      common.Rad((String)VSCRep.elementAt(i),6)+common.Space(2)+
                      common.Rad((String)VSCPerRep.elementAt(i),6)+common.Space(2)+
                      common.Rad((String)VShift.elementAt(i),6)+common.Space(2)+
                      common.Rad((String)VShiftPer.elementAt(i),6)+common.Space(2)+
                      common.Rad((String)VOther.elementAt(i),6)+common.Space(2)+
                      common.Rad((String)VOtherPer.elementAt(i),6);

                try
                {
                     Head();

                     if(iDeptSig==0)
                     {
                         if(Lctr>13)
                              FW.write(SLine);
                         FW.write((String)VDept.elementAt(i)+"\n\n");
                         Lctr+=3;
                         SPreDept  = (String)VDept.elementAt(i);
                         iDeptSig=1;
                     }
                     FW.write(Strl+"\n");
                     Lctr++;
                }catch(Exception ex){}
         }

         try
         {
              FW.write(SLine);
              FW.write("<End Of Report>");
         }catch(Exception ex){}
     }

     public Vector Stop1Head()
     {
          Vector VHead = new Vector();

          String Str1  = common.Pad("Grade",10)+common.Space(2)+
                      common.Pad("Worker",8+2+15)+common.Space(2)+
                      common.Rad("Effi-",6)+common.Space(2)+
                      common.Cad("Excess Effy Loss Due To",46)+common.Space(2)+
                      common.Cad("Other Loss",14);

          String Str2  = common.Pad(" ",10)+common.Space(2)+
                      common.Pad("Code",8)+common.Space(2)+
                      common.Pad("Name",15)+common.Space(2)+
                      common.Rad("ciency",6)+common.Space(2)+
                      common.Replicate("-",46)+common.Space(2)+
                      common.Replicate("-",14);

          String Str3  = common.Pad(" ",10)+common.Space(2)+
                      common.Pad(" ",8)+common.Space(2)+
                      common.Pad(" ",15)+common.Space(2)+
                      common.Rad(" ",6)+common.Space(2)+
                      common.Cad("SC With",14)+common.Space(2)+
                      common.Cad("SC Without",14)+common.Space(2)+
                      common.Cad("Shift End",14)+common.Space(2)+
                      common.Rad(" ",14);

          String Str4  = common.Pad(" ",10)+common.Space(2)+
                      common.Pad(" ",8)+common.Space(2)+
                      common.Pad(" ",15)+common.Space(2)+
                      common.Rad(" ",6)+common.Space(2)+
                      common.Cad("Air Cleaning",14)+common.Space(2)+
                      common.Cad("Air Cleaning",14)+common.Space(2)+
                      common.Cad("Air Cleaning",14)+common.Space(2)+
                      common.Rad(" ",14);

          String Str5  = common.Pad(" ",10)+common.Space(2)+
                      common.Pad(" ",8)+common.Space(2)+
                      common.Pad(" ",15)+common.Space(2)+
                      common.Rad(" ",6)+common.Space(2)+
                      common.Replicate("-",14)+common.Space(2)+
                      common.Replicate("-",14)+common.Space(2)+
                      common.Replicate("-",14)+common.Space(2)+
                      common.Replicate("-",14);

          String Str6  = common.Pad(" ",10)+common.Space(2)+
                      common.Pad(" ",8)+common.Space(2)+
                      common.Pad(" ",15)+common.Space(2)+
                      common.Rad(" ",6)+common.Space(2)+
                      common.Rad("Min",6)+common.Space(2)+
                      common.Rad("%",6)+common.Space(2)+
                      common.Rad("Min",6)+common.Space(2)+
                      common.Rad("%",6)+common.Space(2)+
                      common.Rad("Min",6)+common.Space(2)+
                      common.Rad("%",6)+common.Space(2)+
                      common.Rad("Min",6)+common.Space(2)+
                      common.Rad("%",6);

          SLine = common.Replicate("-",Str1.length())+"\n";

          VHead.addElement(SLine);
          VHead.addElement(Str1+"\n");
          VHead.addElement(Str2+"\n");
          VHead.addElement(Str3+"\n");
          VHead.addElement(Str4+"\n");
          VHead.addElement(Str5+"\n");
          VHead.addElement(Str6+"\n");
          VHead.addElement(SLine);

          return VHead;
     }

	  private String getTicket(String SEmpCode)
	  {	
		   String SRet="UnKnown";
		   if(SEmpCode.equals("0"))
			   return SRet;
		   try
		   {			   
			   String SQry = " select empname from hrdnew.SchemeApprentice where empcode="+SEmpCode+" union " +
							 " select empname from hrdnew.ActApprentice where empcode="+SEmpCode+" union  " + 
							 " select empname from hrdnew.ContractApprentice where empcode="+SEmpCode;
			   Class.forName("oracle.jdbc.OracleDriver");
			   Connection conn = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
			   Statement stat  = conn.createStatement();
			   ResultSet res   = stat.executeQuery(SQry);
			   if(res.next())
				   SRet            = res.getString(1);
		   }catch(Exception ee)
		   {
			   System.out.println("getTicket->" + ee);
			   return SRet;
		   }
		   return SRet;
      }



	private Connection createConnection()
	{
		Connection conn;
		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
			conn  = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
		}catch(Exception ee)
		{
			return null;
		}
		return conn;
	}

}

