package CProduction; 

import java.io.*;
import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class OEProductionReport extends HttpServlet
{
     HttpSession    session;
     Common common;
	
     String SOnDate,SUnit;
     String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody,tbgBody;

     FileWriter  FW;
     int Lctr = 70,Pctr = 0;
     String SLine;
     double   dDayTotalProd,dDayTotalTarget,dTotalUptoProd,dTotalUptoTarget,
              dTotDiff,dTotTodayGMSperSPL,dTotToday30s;
     
     double  dTargetPerDay,dLastAvgCnt,dLastUtilPer,dLastWatePer;
     double  dUptoAvgCnt,dUptoWastePer,dOnDateAvgCnt,dOnDateWastePer,
             dUptoUtilPer,dOnDateUtilPer;

     Control control = new Control();
     Vector  VCountObj,VHead,VCount,VActualSpeed,VHoliday;
     double  dTotRotWkd;

     int iMonth;
     public void init(ServletConfig config) throws ServletException
     {
          super.init(config);
          common = new Common();

          bgColor = common.bgColor;
          bgHead  = common.bgHead;
          bgUom   = common.bgUom;
          bgBody  = common.bgBody;
          fgHead  = common.fgHead;
          fgUom   = common.fgUom;
          fgBody  = common.fgBody;
          tbgBody = common.tbgBody;
          SLine   = common.Replicate("�",120)+"\n";
     }
     public void doGet(HttpServletRequest request, HttpServletResponse response)
                         throws ServletException, IOException
     {
          doPost(request,response);
     }
     public void doPost(HttpServletRequest request, HttpServletResponse response)
                        throws ServletException, IOException
     {
		response.setContentType("text/html");
          PrintWriter out  = response.getWriter();
		session = request.getSession(true);
		/*if(session.getValue("AppletTag")==null)
		{n
			String SServer = control.getID("Select ProdHost from Server");
			response.sendRedirect(SServer + "/SessionExpired");
			return;
		}*/
          dDayTotalProd=0;
          dTotalUptoProd=0;
          dTotalUptoTarget=0;
          dDayTotalTarget=0;
          dTotDiff=0;
          dTotTodayGMSperSPL=0;
          dTotToday30s=0;

          setCriteria(request);
          setData();
          out.println("<html>");
          out.println("<head>");
          out.println("<title>"+SUnit +" OE Production Report   </title>");
          out.println("</head>");
          //out.println(session.getValue("AppletTag"));
          out.println("<body bgcolor='" + common.bgBody + "'>");
          out.println(common.getUnitDateHeader("OE Production Report",SUnit,SOnDate));
          out.println("  <table border='1' cellspacing='0'>");
          out.println("    <tr>");
          out.println("      <td  height='44' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Count</font></td>");
          out.println("      <td  height='44' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Actual Spl Speed</font></td>");
          out.println("      <td  height='44' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Target Spl Speed</font></td>");
          out.println("      <td  height='44' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>On Date Production</font></td>");
          out.println("      <td  height='44' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>UptoDate Production</font></td>");
          out.println("      <td  height='44' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>On Date Target</font></td>");
          out.println("      <td  height='44' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>UptoDate Target</font></td>");
          out.println("      <td  width='12' height='44' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Diff</font></td>");
          out.println("      <td  height='44' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>Today GMS/Rot</font></td>");
          out.println("      <td  height='44' bgcolor='"+bgHead+"'><font size='4' color='"+fgHead+"'>OnDate 30sGMS/Rot</font></td>");
          out.println("</tr>");
          int j=0;
          for(int i=0; i<VCountObj.size(); i++)
          {

               CountObj countObj   = (CountObj)VCountObj.elementAt(i);

               double dActualSpeed  = getActualSpeed(countObj.getCountCode());

//             double dActualSpeed = countObj.getDaySpeed()/3;
               double dTargetSpeed = countObj.getDayTargetSpeed();
               double dDayProd     = countObj.dDayProd;
               double dDayTargetProd = countObj.dDayTargetProd;
               double dDayConv30     = countObj.getDayConv30();
               double dDiff        = dDayProd-dDayTargetProd;

			out.println("    <tr>");
               out.println("      <td   bgcolor='"+tbgBody+"'  ><font   color='"+fgBody+"'>"+countObj.SCount+"</font></td>");
               if(dDayProd>0)
               {
                    out.println("      <td   bgcolor='"+tbgBody+"'  ><font   color='"+fgBody+"'>"+common.getRound(String.valueOf(dActualSpeed),0)+"</font></td>");
                    out.println("      <td   bgcolor='"+tbgBody+"'  ><font   color='"+fgBody+"'>"+common.getRound(String.valueOf(dTargetSpeed),0)+"</font></td>");
               }
               else
               {
                    out.println("      <td   bgcolor='"+tbgBody+"'  ><font   color='"+fgBody+"'>&nbsp;</font></td>");
                    out.println("      <td   bgcolor='"+tbgBody+"'  ><font   color='"+fgBody+"'>&nbsp;</font></td>");
               }
               out.println("      <td   bgcolor='"+tbgBody+"'  ><font   color='"+fgBody+"'>"+common.getRound(String.valueOf(dDayProd),2)+"</font></td>");
               out.println("      <td   bgcolor='"+tbgBody+"'  ><font   color='"+fgBody+"'>"+common.getRound(String.valueOf(countObj.dUptoProd),2)+"</font></td>");
               out.println("      <td   bgcolor='"+tbgBody+"'  ><font   color='"+fgBody+"'>"+common.getRound(String.valueOf(dDayTargetProd),2)+"</font></td>");
               out.println("      <td   bgcolor='"+tbgBody+"'  ><font   color='"+fgBody+"'>"+common.getRound(String.valueOf(countObj.dUptoTargetProd),2)+"</font></td>");
               out.println("      <td   bgcolor='"+tbgBody+"'  ><font   color='"+fgBody+"'>"+common.getRound(String.valueOf(dDiff),2)+"</font></td>");
               out.println("      <td   bgcolor='"+tbgBody+"'  ><font   color='"+fgBody+"'>"+common.getRound(String.valueOf(countObj.getDayGPS()),2)+"</font></td>");
               out.println("      <td   bgcolor='"+tbgBody+"'  ><font   color='"+fgBody+"'>"+common.getRound(String.valueOf(countObj.getDayConv30()),2)+"</font></td>");
               out.println("</tr>");

               dDayTotalProd+=dDayProd;
               dDayTotalTarget+=dDayTargetProd;
               dTotalUptoProd+=countObj.dUptoProd;
               dTotalUptoTarget+=countObj.dUptoTargetProd;
               dTotDiff+=dDiff;
               dTotTodayGMSperSPL+=countObj.getDayGPS();
               dTotToday30s=dTotToday30s+dDayConv30;
               if(countObj.getDayGPS()>0 && dDayConv30>0)
                    j=j+1;
          }
          dTotTodayGMSperSPL = (dTotTodayGMSperSPL/j);
          dTotToday30s       = (dTotToday30s/j); 
          out.println("<tr  bgcolor='"+bgUom+"'>");
          out.println("<td >Total/Avg</td>");
          out.println("<td></td>");
          out.println("<td></td>");
          out.println("<td>"+common.getRound(String.valueOf(dDayTotalProd),2)+"</td>");
          out.println("<td>"+common.getRound(String.valueOf(dTotalUptoProd),2)+"</td>");
          out.println("<td>"+common.getRound(String.valueOf(dDayTotalTarget),2)+"</td>");
          out.println("<td>"+common.getRound(String.valueOf(dTotalUptoTarget),2)+"</td>");
          out.println("<td>"+common.getRound(String.valueOf(dTotDiff),2)+"</td>");
          out.println("<td>"+common.getRound(String.valueOf(dTotTodayGMSperSPL),2)+"</td>");
          out.println("<td>"+common.getRound(String.valueOf(dTotToday30s),2)+"</td>");
          out.println("</tr>");
         
          out.println(" </table></center>");
          out.println("</div>");
          out.println("</body>");
          out.println("</html>");
          out.close();
          try
          {
               if(common.toInt(SUnit)==1)
                      FW      = new FileWriter("//software/C-Prod-Print/OEProductionc.prn");
               else
                      FW      = new FileWriter("//software/C-Prod-Print/OEProductionc.prn");                
               Lctr    = 70;
               Pctr    =  0;
               Head();
               Body();
               Foot();
               FW.close();
          }catch(Exception ex){}
     }
          
     private void setData()
     {
          VCountObj = new Vector();
          VCount    = new Vector();
          VActualSpeed =new Vector();
          VHoliday  = new Vector();

          try
          {
               Connection con = createConnection();
               Statement stat = con.createStatement();
               try{stat.execute("Drop Table TempProd");}catch(Exception ex){}
               stat.execute(TempProdQS());   
               ResultSet res  = stat.executeQuery(getTargetQS());
               while(res.next())
                    organizeTarget(res);
               res.close();
               res  = stat.executeQuery(getProdQS());
               while(res.next())
                    organizeProd(res);
               res.close();
               res = stat.executeQuery(getActTarQS());      //get Actual and Target  from TempProd
               while(res.next())
                    organizeSpeed(res);
               res.close();
               res = stat.executeQuery(getLastMonth());
               while(res.next())
                    organizeLast(res);
               res.close();
               res = stat.executeQuery(getOnDate());
               while(res.next())
                    organizeOnDate(res);
               res.close();
               res = stat.executeQuery(getUptoDate());
               while(res.next())
                    organizeUptoDate(res);
               res.close();
               res = stat.executeQuery("select * from ProdHoliday");
               while(res.next())
               {
                    VHoliday.addElement(res.getString(1));
               }
               res.close();
               stat.close();
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     private String getTargetQS()
     {
          String QS = " Select TargetPerDay from SpinTarget"+ // from Common TableSpace
                      " where UnitCode="+SUnit;         
          return QS;
     }
     private String TempProdQS()
     {
          String QS =" Create table TempProd as "+
                     " (select count_Code ,Avg(Speed) as AvgSpeed ,"+
                     " sum(NoofRot) as TotNoofRot "+
                     " from OETran "+
                     " where Sp_Date="+SOnDate+
                     " group by Mach_Code,Count_Code)";
          return QS;
     }
     private String getLastMonth()
     {
          String QS =" Select sum(OEtran.count*OETran.RotWkd)/Sum(OETran.RotWkd) as AvgCount,"+
                     " Avg(Rotwkd*100/noofRot) AS Util, "+
                     " (sum(OEtran.pneumafil)*100)/(sum(OETran.pneumafil)+sum(OETran.Prod)) as PneuPer  from"+
                     " OETran where substr(sp_date,1,6)="+common.getPreviousMonth(common.toInt(SOnDate.substring(0,6)));
//                     " and OETran.prod>0";
          return    QS;
     }
     private String getOnDate()
     {
//        String QS =" Select Avg(OETran.count) as AvgCount,"+
          String QS =" Select sum(OETran.count*OETran.RotWkd)/Sum(OETran.RotWkd) as AvgCount,"+
                     " Avg(Rotwkd*100/noofRot) AS Util, "+ 
                     " (sum(OETran.pneumafil)*100)/(sum(OETran.pneumafil)+sum(OETran.Prod)) as PneuPer ,sum(RotWkd) as RotWkd from"+
                     " OETran where sp_date="+SOnDate;
//                     " and OETran.prod>0";

          return    QS;
     }
     private String getUptoDate()
     {
//        String QS =" Select Avg(OETran.count) as AvgCount,"+
         String QS =" Select sum(OETran.count*OETran.RotWkd)/Sum(OETran.RotWkd) as AvgCount,"+
                     " Avg(Rotwkd*100/noofRot) AS Util, "+
                     " (sum(OETran.pneumafil)*100)/(sum(OETran.pneumafil)+sum(OETran.Prod)) as PneuPer  from"+
                     " OETran "+
                     " where Sp_Date>="+common.getStartDate(SOnDate) +
                     " And Sp_date<= "+SOnDate;
//                     " and OETran.prod>0";
          return    QS;
     }

     private String getActTarQS()
     {
          String QS =" Select Count_Code,sum(AvgSpeed*TotNoofRot)/sum(TotNoofRot)"+
                     " as ActualSpeed"+
                     " from TempProd"+
                     " group by Count_Code";
          return QS;
     }
     private String getMonthQS()
     {
          String QS1 = " SELECT Sum(OETran.prod_tar) AS SumOfprod_tar "+
                       " FROM OETran INNER JOIN unit ON OETran.Unit_Code=unit.unitcode "+
                       " WHERE substr(sp_date,1,6)= '"+SOnDate.substring(0,6)+"' "+
//                       " and  OETran.prod>0"+  // Updated on 23-11-2004
                       " and OETran.unit_code="+SUnit;
          return QS1;          
     }
     private String getProdQS()
     {
          String QS = " Select OETran.count_Code,OETran.Sp_Date,"+
                    " Count.Count_Name,Count.kg30s, "+
                    " sum(RotWkd) as RotWkd,"+
                    " sum(Prod) as Prod , sum(Prod_Tar) as ProdTarget,"+
                    " sum(Speed) as Speed ,Avg(Speed_Tar) as TargetSpeed "+
                    " from OETran "+
                    " Inner join count on "+
                    " Count.Count_Code = OETran.Count_Code "+
                    " where Sp_Date>="+common.getStartDate(SOnDate) +
                    " And Sp_date<= "+SOnDate +
                    " and OETran.Unit_Code="+SUnit +
//                    " and OETran.Prod>0"+
                    " Group by OETran.Count_Code,OETran.Sp_Date,"+
                    " Count.Count_Name,count.kg30s";

                 QS = " Select * from ("+ QS + ") Order by Count_Code";

          return QS;
     }
     private void organizeLast(ResultSet theResult) throws Exception
     {
          dLastAvgCnt    = theResult.getDouble(1);   
          dLastUtilPer   = theResult.getDouble(2);   
          dLastWatePer   = theResult.getDouble(3);   
     }
     private void organizeUptoDate(ResultSet theResult) throws Exception
     {
          dUptoAvgCnt    = theResult.getDouble(1);
          dUptoUtilPer   = theResult.getDouble(2);
          dUptoWastePer  = theResult.getDouble(3);
     }
     private void organizeOnDate(ResultSet theResult) throws Exception
     {
          dOnDateAvgCnt     = theResult.getDouble(1);
          dOnDateUtilPer    = theResult.getDouble(2);    
          dOnDateWastePer   = theResult.getDouble(3);
          dTotRotWkd    = theResult.getDouble(4);
     }
     private void organizeTarget(ResultSet theResult) throws Exception
     {
          dTargetPerDay = theResult.getDouble(1);
     }
     private void organizeSpeed(ResultSet theResult) throws Exception
     {
          int       iCountCode     = theResult.getInt(1);
          double    dActualSpeed   = theResult.getDouble(2);

          VCount.addElement(String.valueOf(iCountCode));
          VActualSpeed.addElement(String.valueOf(dActualSpeed));
     }
     private void organizeProd(ResultSet theResult) throws Exception
     {
          String SCountCode  = theResult.getString(1);
          int iDate          = theResult.getInt(2);
          String SCount      = theResult.getString(3);
          double dKg30s      = theResult.getDouble(4);
          double dRotWkd    = theResult.getDouble(5);
          double dProd       = theResult.getDouble(6);
          double dTargetProd = theResult.getDouble(7);
          double dSpeed      = theResult.getDouble(8);
          double dTargetSpeed= theResult.getDouble(9);

          int    iOnDate     = common.toInt(SOnDate);

          int iIndex  = indexOf(SCountCode);

          if(iIndex==-1)
          {
               CountObj countobj = new CountObj(SCountCode,SCount,dKg30s,iOnDate);
               countobj.setProduction(dProd,dTargetProd,iDate);
               countobj.setSpeed(dSpeed,dTargetSpeed,dRotWkd,iDate);
               VCountObj.addElement(countobj);
          }
          else
          {
               CountObj countobj = (CountObj)VCountObj.elementAt(iIndex);
               countobj.setProduction(dProd,dTargetProd,iDate);
               countobj.setSpeed(dSpeed,dTargetSpeed,dRotWkd,iDate);
               VCountObj.setElementAt(countobj,iIndex);
          }
     }
     private int indexOf(String SCCode)
     {
          int index = -1;
          for(int i=0;i<VCountObj.size();i++)
          {
               CountObj cobj = (CountObj)VCountObj.elementAt(i);
               if(cobj.SCountCode.equals(SCCode))
               {
                    index = i;
                    break;
               }
          }
          return index;
     }
     private void setCriteria(HttpServletRequest request)
	{
          SOnDate    = request.getParameter("Date");
          SOnDate    = (String)common.pureDate(SOnDate);
          SUnit      = request.getParameter("Unit");

		session.putValue("Unit",SUnit);
          session.putValue("Date",SOnDate);

          iMonth = common.toInt(SOnDate.substring(0,6));
	}
     private Connection createConnection()
	{
		Connection conn;
		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
			conn  = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
		}catch(Exception ee)
		{
			return null;
		}
		return conn;
     }
     public void Head()
     {
          if(Lctr < 60)
             return;
          if(Pctr > 0)
          {
             try
             {
               FW.write(SLine+"\n");
             }catch(Exception ex){}
          }
          Pctr++;

          String Str1    = common.Pad("Company    : AMARJOTHI OE MILLS LIMITED",80);
          String Str2    = common.Pad("Document   : OE Production Report  On "+common.parseDate(SOnDate),80);
          String Str3    = common.Pad("Unit       : "+(SUnit.equals("1")?"A":(SUnit.equals("2")?"B":"S")),70);
          VHead = Spin1Head();
          try
          {
               FW.write(""+Str1+"\n");
               FW.write(Str2+"\n");
               FW.write(Str3+"\n\n");

               double dMonthProdTarget  = dTargetPerDay*common.toInt(common.getMonthDays(iMonth));
               FW.write(common.Pad("Target For theMonth : ",21)+common.Rad(common.getRound(String.valueOf(dMonthProdTarget),2),12)+
                         common.Space(10)+"Breakage Target\n");
               FW.write(common.Pad("Target Per Day      : ",21)+common.Rad(common.getRound(String.valueOf(dTargetPerDay),2),12)+
                         common.Space(10)+"OE\n");
               FW.write(common.Pad("Utilization         : ",21)+common.Rad("94%",12)+"\n");
               FW.write("g");
               for(int i=0;i<VHead.size();i++)
               {
                    FW.write((String)VHead.elementAt(i));
               }
          }catch(Exception ex){}
          Lctr = 4+VHead.size();
     }
     public Vector Spin1Head()
     {
          Vector VHead = new Vector();

          String Str3 = common.Pad("Count",12)+common.Space(2)+
                        common.Pad("Actual Rot",10)+common.Space(2)+
                        common.Pad("Target Rot",10)+common.Space(2)+
                        common.Pad("On Date",10)+common.Space(2)+
                        common.Pad("Upto Date",10)+common.Space(2)+
                        common.Pad("On Date",10)+common.Space(2)+
                        common.Pad("Upto Date",10)+common.Space(4)+
                        common.Pad("Diff",10)+common.Space(2)+
                        common.Pad("Today",10)+common.Space(2)+
                        common.Pad("On Date",10)+common.Space(2)+"\n";

          String Str4 = common.Space(12)+common.Space(4)+
                        common.Pad("Speed",10)+common.Space(2)+
                        common.Pad("Speed",10)+common.Space(2)+
                        common.Pad("Prod",10)+common.Space(2)+                        
                        common.Pad("Prod",10)+common.Space(2)+
                        common.Pad("Target",10)+common.Space(2)+
                        common.Pad("Target",10)+common.Space(2)+
                        common.Space(10)+common.Space(2)+
                        common.Pad("GMS/Rot",10)+common.Space(2)+
                        common.Pad("30s GMS/Rot",10)+common.Space(2)+"\n";

          VHead.addElement(SLine);
          VHead.addElement(Str3);
          VHead.addElement(Str4);
          VHead.addElement(SLine);

          return VHead;
     }
     private  void  Body()
     {

          for(int i=0; i<VCountObj.size(); i++)
          {

               CountObj countObj   = (CountObj)VCountObj.elementAt(i);

               double dActualSpeed  = getActualSpeed(countObj.getCountCode());

               double dDayProd     = countObj.dDayProd;
               double dDayTargetProd = countObj.dDayTargetProd;
//             double dActualSpeed = countObj.getDaySpeed()/3;
               double dTargetSpeed = countObj.getDayTargetSpeed();

               double dDiff        = dDayProd-dDayTargetProd;

               String Str1 = common.Pad(countObj.SCount,10);
               String Str2 = dDayProd>0?common.Rad(common.getRound(String.valueOf(dActualSpeed),2),12):common.Space(12);
               String Str3 = dDayProd>0?common.Rad(common.getRound(String.valueOf(dTargetSpeed),2),12):common.Space(12);
               String Str4 = common.Rad(common.getRound(String.valueOf(dDayProd),2),12);
               String Str5 = common.Rad(common.getRound(String.valueOf(countObj.dUptoProd),2),12);
               String Str6 = common.Rad(common.getRound(String.valueOf(dDayTargetProd),2),12);
               String Str7 = common.Rad(common.getRound(String.valueOf(countObj.dUptoTargetProd),2),12);
               String Str8 = common.Rad(common.getRound(String.valueOf(dDiff),2),12);
               String Str9 = common.Rad(common.getRound(String.valueOf(countObj.getDayGPS()),2),12);
               String Str10 = common.Rad(common.getRound(String.valueOf(countObj.getDayConv30()),2),12);

               String Strl = Str1+Str2+Str3+Str4+Str5+Str6+Str7+Str8+Str9+Str10;

                try
                {
                     FW.write(Strl+"\n");
                     Lctr=Lctr+1;
                     Head();
                }catch(Exception ex){}
           }
     }
     private void Foot()
     {
          try
          {
               dTotTodayGMSperSPL=(dDayTotalProd*1000)/dTotRotWkd;

               FW.write(SLine);
               FW.write(common.Space(10)+common.Space(12)+common.Space(12)+
                        common.Rad(common.getRound(String.valueOf(dDayTotalProd),2),12)+
                        common.Rad(common.getRound(String.valueOf(dTotalUptoProd),2),12)+
                        common.Rad(common.getRound(String.valueOf(dDayTotalTarget),2),12)+
                        common.Rad(common.getRound(String.valueOf(dTotalUptoTarget),2),12)+
                        common.Rad(common.getRound(String.valueOf(dTotDiff),2),12)+
                        common.Rad(common.getRound(String.valueOf(dTotTodayGMSperSPL),2),12)+
                        common.Rad(common.getRound(String.valueOf(dTotToday30s),2),12)+"\n");
               FW.write(SLine+"\n");

               FW.write(common.Replicate("�",64)+common.Space(10)+common.Replicate("�",50)+"\n");
               FW.write(common.Pad("Descr.",10)+common.Pad("Last M.Ach",15)+
                        common.Pad("On Date",10)+common.Pad("Up-to-Date",15)+common.Pad("Tar.For Month",14)+
                        common.Space(10)+
                        common.Pad("Dept",10)+common.Pad("Last Achieve",15)+
                        common.Pad("Target",12)+common.Pad("Upto.Ach",10)+"\n");

               String SUtil="99.00";

               if(common.toInt(SUnit)==1)
                      SUtil="99.3";

               FW.write(common.Replicate("�",64)+common.Space(10)+common.Replicate("�",50)+"\n");

               FW.write(common.Pad("Avg Count",10)+common.Pad(common.getRound(dLastAvgCnt,2),15)+
                        common.Pad(common.getRound(dOnDateAvgCnt,2),10)+common.Pad(common.getRound(dUptoAvgCnt,2),15)+common.Pad("30s",14)+
                        common.Space(10)+
                        common.Pad("Spg",10)+common.Pad("",15)+
                        common.Pad("",12)+common.Pad("",10)+"\n");

               FW.write(common.Pad("Util.",10)+common.Pad(common.getRound(dLastUtilPer,2),15)+
                        common.Pad(common.getRound(dOnDateUtilPer,2),10)+common.Pad(common.getRound(dUptoUtilPer,2),15)+common.Pad(SUtil,14)+
                        common.Space(10)+
                        common.Pad("Dept",10)+common.Pad("Mech.Idle",15)+
                        common.Pad("M.Idle Tar",12)+common.Pad("Bobbin Idle",10)+"\n");

               FW.write(common.Pad("Waste%",10)+common.Pad(common.getRound(dLastWatePer,2),15)+
                        common.Pad(common.getRound(dOnDateWastePer,2),10)+
                        common.Pad(common.getRound(dUptoWastePer,2),15)+common.Pad("1.5",14)+
                        common.Space(10)+
                        common.Pad("Spg",10)+common.Pad("",15)+
                        common.Pad("",12)+common.Pad("",10)+"\n\n");

               int iDays = common.toInt(SOnDate.substring(6,8));
               System.out.println(iDays+","+SOnDate+","+SOnDate.substring(6,8));

               for(int i=0; i<VHoliday.size(); i++)
               {
                    int iHdate = common.toInt((String)VHoliday.elementAt(i));
                    int iMonth = common.toInt(((String)VHoliday.elementAt(i)).substring(0,6));
                    System.out.println(iHdate);
                    if(common.toInt(SOnDate)==iHdate)
                         iDays = iDays-1;
                    if(common.toInt(SOnDate)>iHdate)
                    {
                         if(iMonth==(common.toInt(SOnDate.substring(0,6))))
                              iDays = iDays-1;
                    }
               }
               System.out.println(iDays);
               FW.write(common.Pad("Target UptoDate               : ",32)+common.Rad(common.getRound(String.valueOf(dTotalUptoTarget),2),12)+"\n");
               FW.write(common.Pad("Achieved Uptodate             : ",32)+common.Rad(common.getRound(String.valueOf(dTotalUptoProd),2),12)+"\n");
               FW.write(common.Pad("Avg Prod/Day                  : ",32)+common.Rad(common.getRound(String.valueOf(dTotalUptoProd/iDays),2),12)+"\n");
          }
          catch(Exception ex)
          {
               System.out.println(ex);
          }
     }
     private double getActualSpeed(String SCountCode)
     {
          int iIndex = VCount.indexOf(SCountCode);
          if(iIndex==-1)
               return 0;
          return common.toDouble((String)VActualSpeed.elementAt(iIndex));
     }
}
