package CProduction;

import java.util.*;

public class WorkGradeClass
{
     String SCode,SName,SDept;

     Vector VSCAC,VSC,VStop;
     Vector VSCACPer,VSCPer,VEffy;

     double dShiftAC,dShiftACPer,dShiftACNet;
     Common common;

     // dSCStd      = Shade Change Without Air Cleaning Standard
     // dSCACStd    = Shade Change With Air Cleaning Standard
     // dShiftACStd = Shift End Air Cleaning Standard. StopCode=30
     double dSCStd=0,dSCACStd=0,dShiftACStd=0,dEffyStd=0;

     WorkGradeClass()
     {
     }
     WorkGradeClass(String SCode,String SName,String SDept)
     {
          this.SCode = SCode;
          this.SName = SName;
          this.SDept = SDept;

          common     = new Common();

          VStop        = new Vector();
          VSCAC        = new Vector();
          VSC          = new Vector();

          VSCACPer     = new Vector();
          VSCPer       = new Vector();
          VEffy        = new Vector();

          dShiftAC     = 0;
          dShiftACPer  = 0;
          dShiftACNet  = 0;
     }
     public void setStds(double dSCStd,double dSCACStd,double dShiftACStd,double dEffyStd)
     {
          this.dSCStd      = dSCStd;
          this.dSCACStd    = dSCACStd;
          this.dShiftACStd = dShiftACStd;
          this.dEffyStd    = dEffyStd;
     }
     public void append(double dEffy,double dSC,double dSCPer,double dSCAC,double dSCACPer,String SStop)
     {
          VEffy   .addElement(common.getRound(dEffy,2));
          VSC     .addElement(common.getRound(dSC,0));
          VSCPer  .addElement(common.getRound(dSCPer,2));
          VSCAC   .addElement(common.getRound(dSCAC,0));
          VSCACPer.addElement(common.getRound(dSCACPer,2));
          VStop   .addElement(SStop);
     }
     public void setShiftAC(double dStop,int iCtr)
     {
          this.dShiftACNet = dStop;
          this.dShiftAC    = dStop-(iCtr*dShiftACStd);
          this.dShiftACStd = iCtr*dShiftACStd;
          this.dShiftACPer = dShiftAC*100/dShiftACStd;
     }
     public double getEffy()
     {
          return common.toDouble(common.getSum(VEffy,"A"));
     }
     public double getSC()
     {
          return common.toDouble(common.getSum(VSC,"S"));
     }
     public double getSCAC()
     {
          return common.toDouble(common.getSum(VSCAC,"S"));
     }
     public double getSCPer()
     {
          return common.toDouble(common.getSum(VSCPer,"A"));
     }
     public double getSCACPer()
     {
          return common.toDouble(common.getSum(VSCACPer,"A"));
     }
     public double getOther()
     {
          int iSize     = VSC.size();
          double dSum   = common.toDouble(common.getSum(VStop,"S"));
          double dSC    = getSC();
          double dSCAC  = getSCAC();
          double dOther = dSum-dSC-dSCAC-dShiftACNet;
          //System.out.println(dSum+"  "+iSize+"  "+dSC+"  "+dSCAC+"  "+dShiftACNet+"  "+dOther);
          return dOther;
     }
     private int getNonZero(Vector Vx)
     {
          int iCtr = 0;
          for(int i=0;i<Vx.size();i++)
          {
               if(common.toDouble((String)Vx.elementAt(i))>0)
                    iCtr++;
          }
          return iCtr;
     }
     public double getOtherPer()
     {
          double dSum   = common.toDouble(common.getSum(VStop,"S"));
          double dOther = getOther();
          if(dSum==0)
               return 0;
          return dOther*100/dSum;
     }
}

