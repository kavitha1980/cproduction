package CProduction;

// Used By SpinningProductionReport Prepared On 03-08-2004

public class CountObj
{
     String SCount,SCountCode;
     double dConv30;
     int    iOnDate;

     double dDayProd,dDayTargetProd;
     double dUptoProd,dUptoTargetProd;

     double dDaySpeed,dDayTargetSpeed;
     double dUptoSpeed,dUptoTargetSpeed;
    
     double dDaySpdls,dUptoSpdls;

     public CountObj(String SCountCode,String SCount,double dConv30,int iOnDate)
     {
          this.SCountCode  = SCountCode;
          this.SCount      = SCount;
          this.dConv30     = dConv30;
          this.iOnDate     = iOnDate;
     }
     
     public void setProduction(double dProd,double dTargetProd,int iDate)
     {
          if(iOnDate==iDate)
          {
               dDayProd        = dProd;
               dDayTargetProd  = dTargetProd;
          }
          dUptoProd       = dUptoProd+dProd;
          dUptoTargetProd = dUptoTargetProd+dTargetProd;
     }
     public String getCountCode()
     {
          return  SCountCode;         
     }
     public void setSpeed(double dSpeed,double dTargetSpeed,double dSpdls,int iDate)
     {
          if(iOnDate == iDate)
          {
               dDaySpeed        = dDaySpeed +(dSpdls*dSpeed);
               dDayTargetSpeed  = dDayTargetSpeed+(dSpdls*dTargetSpeed);
               dDaySpdls        = dDaySpdls+dSpdls;
          }
          dUptoSpdls       = dUptoSpdls+dSpdls;
          dUptoSpeed       = dUptoSpeed+(dSpdls*dSpeed);
          dUptoTargetSpeed = dUptoTargetSpeed+(dSpdls*dTargetSpeed);
     }
     public double getUptoSpeed()
     {
          if(dUptoSpdls == 0)
              return 0;
          
          return dUptoSpeed/dUptoSpdls;
     }
     public double getUptoTargetSpeed()
     {
          if(dUptoSpdls == 0)
              return 0;
          
          return dUptoTargetSpeed/dUptoSpdls;
     }

     public double getDaySpeed()
     {
          if(dDaySpdls == 0)
              return 0;
          
          return dDaySpeed/dDaySpdls;
     }
     public double getDayTargetSpeed()
     {
          if(dDaySpdls == 0)
              return 0;
          
          return dDayTargetSpeed/dDaySpdls;
     }

     public double getUptoGPS()
     {
          return (dUptoProd*1000)/dUptoSpdls;
     }
     public double getUptoConv30()
     {
          return getUptoGPS()*dConv30;
     }

     public double getDayGPS()
     {
          if(dDaySpdls==0)
               return 0;
          return (dDayProd*1000)/dDaySpdls;
     }
     public double getDayConv30()
     {
          return (getDayGPS()*dConv30);
     }

}
