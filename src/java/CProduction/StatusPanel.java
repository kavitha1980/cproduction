package CProduction;

import javax.swing.*;
import java.awt.*;
public class StatusPanel extends JPanel
{
     JLabel LLabel,MLabel,RLabel;
     JPanel MPanel;
     public StatusPanel()
     {
          LLabel = new JLabel(" ",JLabel.LEFT);
          RLabel = new JLabel("Developed By FIPL for Amarjothi Spinning Mills Ltd",new ImageIcon("Fipl2.gif"),JLabel.LEFT);
          MLabel = new JLabel(" ",JLabel.LEFT);
          MPanel = new JPanel(true);

          setLayout(new GridLayout(1,4,1,1));

          add(LLabel);
          add(MPanel);
          add(MLabel);
          add(RLabel);
     }
}
