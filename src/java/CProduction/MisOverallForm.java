/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CProduction;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Vector;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author root
 */
public class MisOverallForm extends HttpServlet {

     HttpSession    session;
     Control        control;
     Common         common;
     Vector         VSCode,VSName,VSPass;
     String         SServer;
     String         bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody;
     RepVect repvect;
     
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            repvect  = new RepVect();
           SServer   ="MiscOverallReport";
           
          out.println("<html><head><title>AMARJOTHI SPINNING MILLS LIMITED</title>");
          out.println("<link rel='stylesheet' type='text/css' href='css/header.css' />");
          out.println("<script src='css/stuHover.js' type='text/javascript'></script>");
          out.println("</head><body bgcolor='white' text='"+ fgBody+  " '>");
        
         RequestDispatcher  dispatch = request.getRequestDispatcher("/ProductionServlet");
                            dispatch . include(request, response);
            
          out.println("<form accept-charset='UTF-8' name=flogin method='POST' action='"+SServer+"'>");
          out.println("<center>");
	  out.println("<p><FONT face=Verdana size=3 color=black>");
	  out.println("<STRONG>Amar Jothi Spinning Mills Ltd</STRONG></FONT></P>");
	  out.println("</center><center>");
	  out.println("<P><STRONG><FONT face=Verdana size=5 color=navy>");
	  out.println(" MIS - Overall Performance </FONT></STRONG>");
	  out.println("</center><br><br><center>");
	  out.println("</CENTER><br><br>");
	
          out.println("<center>");
          out.println("<font face=Verdana size='3' color='"+fgBody+"'><b>Unit</b>&nbsp;&nbsp;&nbsp;&nbsp;</font></td>");
          out.println(" <select size='1' name='Unit' style='HEIGHT: 22px; WIDTH: 110px' > ");
          
          for(int i=0;i<repvect.VUnitCode.size();i++)
          out.println("<option value="+(String)repvect.VUnitCode.elementAt(i)+">"+(String)repvect.VUnit.elementAt(i)+"</option>");
          out.println(" </select>");
		out.println("</center><br>");
		

            out.println("<center>");
            out.println("<font face=Verdana size='3' color='"+fgBody+"'><b>Start Date</b>&nbsp;&nbsp;&nbsp;&nbsp;</font></td>");
            out.println("<input type='text' name='Date' size='15' value='" + getCurrentDate() + "'>");
            out.println("</center><br><br>");
            out.println("<center>");
            
            out.println("<center>");
            out.println("<font face=Verdana size='3' color='"+fgBody+"'><b>End Date</b>&nbsp;&nbsp;&nbsp;&nbsp;</font></td>");
            out.println("<input type='text' name='DateTo' size='15' value='" + getCurrentDate() + "'>");
            out.println("</center><br><br>");
            out.println("<center>");
                
          out.println("<input type='submit' value='Submit' name='B1' style='font-family: Book Antiqua; font-size: 10pt;  text-transform: uppercase; font-weight: bold;' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
          out.println("</center></form></body></html>");
          out.close();

        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>



    private String getCurrentDate()
	{
		String SDate;		
		Date dt = new Date();
		int iDay   = dt.getDate();
		int iMonth = dt.getMonth()+1;
		int iYear  = dt.getYear()+1900;
		if(iDay < 10)
		 SDate = "0"+iDay;
		else
		 SDate = ""+iDay;
		if(iMonth < 10)
		 SDate = SDate + "." + "0"+iMonth;
		else
		 SDate = SDate + "." + iMonth;
		SDate = SDate + "." + iYear;
		return SDate;
	}



}
