package CProduction;

import java.util.*;
import java.sql.*;
import javax.swing.*;
import java.io.*;

public class OffPeriodClass
{
     // ResCode 2 : Responsibility is for Worker;

     String SOffCode,SOfficer,SDesig,SUnitCode,SDeptCode,SStDate,SEnDate,SDept;

     double dClgTar,dClgACTar,dEffyTar;

     Vector VMach,VMachCode,VShift,VCount,VWork,VWorkCode,VMachClass;
     Vector VProd,VLoss,VUeOA,VUeM,VWorkEffy;
     Vector VAC,VWithoutAC,VDate;

     String SCode[],SName[];  //Stoppage Codes and Names

     FileWriter  FW;
     int iLctr = 70,iPctr = 0;
     String SLine;

     Common common = new Common();

     OffPeriodClass()
     {
          // Default Constructor
     }
     OffPeriodClass(String SOffCode,String SOfficer,String SDesig,String SUnitCode,String SDeptCode,String SDept,String SStDate,String SEnDate)
     {
          this.SOffCode  = SOffCode;
          this.SOfficer  = SOfficer;
          this.SDesig    = SDesig;
          this.SUnitCode = SUnitCode;
          this.SDeptCode = SDeptCode;
          this.SDept     = SDept;
          this.SStDate   = SStDate;
          this.SEnDate   = SEnDate;

          setVectors();
          fillCodeNames();
     }
     private void setVectors()
     {
          VMach       = new Vector();
          VDate       = new Vector();
          VShift      = new Vector();
          VCount      = new Vector();
          VWork       = new Vector();  
          VWorkCode   = new Vector();
          VProd       = new Vector();
          VLoss       = new Vector();
          VUeOA       = new Vector();
          VUeM        = new Vector();
          VWorkEffy   = new Vector();
          VAC         = new Vector();
          VWithoutAC  = new Vector();
          VMachClass  = new Vector();
          VMachCode   = new Vector();

          try
          {
               Connection con = createConnection();
               Statement stat = con.createStatement();
               ResultSet res  = stat.executeQuery(getTarQS());
               while(res.next())
               {
                    dEffyTar  = res.getDouble(1);
                    dClgTar   = res.getDouble(2);
                    dClgACTar = res.getDouble(3);
               }
               ResultSet res2 = stat.executeQuery(getQS());
               while(res2.next())
               {
                    String SMach = res2.getString(1);
                    VMach       .addElement(SMach);
                    int iShift = res2.getInt(2);
                    VShift      .addElement(""+iShift);
                    VCount      .addElement(" - ");
                    VWorkCode   .addElement(res2.getString(3));
                    VWork       .addElement(getTicket(res2.getString(4)));
                    VProd       .addElement(res2.getString(5));
                    VLoss       .addElement(res2.getString(6));
                    VUeOA       .addElement(res2.getString(7));
                    VUeM        .addElement(res2.getString(8));
                    VWorkEffy   .addElement(res2.getString(9));
                    double dPer = res2.getDouble(10);
                    double dMin = res2.getDouble(11);
                    if(dPer>1)
                         dMin = dMin/dPer;
                    VWithoutAC  .addElement(common.getRound(dMin,2));
                    double dPer1 = res2.getDouble(12);
                    double dMin1 = res2.getDouble(13);
                    if(dPer1>1)
                         dMin1 = dMin1/dPer1;
                    VAC         .addElement(common.getRound(dMin1,2));
                    int iMachCode = res2.getInt(14);
                    VMachCode   .addElement(""+iMachCode);
                    int iDate = res2.getInt(15);
                    VDate       .addElement(""+iDate);
                    VMachClass  .addElement(new MachClass(iMachCode,iShift,iDate));
               }
               ResultSet res3 = stat.executeQuery(getOCQS());
               while(res3.next())
               {
                    organizeMachClass(res3);
               }
          }catch(Exception ex){System.out.println("WC 999 : "+ex);}
     }
     private void organizeMachClass(ResultSet res3) throws Exception
     {
          int iMachCode= res3.getInt(1);
          int iDate    = res3.getInt(2);
          int iShift   = res3.getInt(3);
          String SVal  = res3.getString(4);
          int iRepCode = res3.getInt(5);

          int iIndex = indexOf(iMachCode,iDate,iShift);
          if(iIndex == -1)
          {
               MachClass MC = new MachClass(iMachCode,iShift,iDate);
               MC.append(SVal,iRepCode);
               VMachClass.addElement(MC);
          }
          else
          {
               MachClass MC = (MachClass)VMachClass.elementAt(iIndex);
               MC.append(SVal,iRepCode);
               VMachClass.setElementAt(MC,iIndex);
          }
     }
     public int indexOf(int iMachCode,int iDate,int iShift)
     {
          int iIndex = -1;
          for(int i=0;i<VMachClass.size();i++)
          {
               MachClass MC = (MachClass)VMachClass.elementAt(i);
               if(MC.iMachCode==iMachCode && MC.iShift==iShift && MC.iDate==iDate)
               {
                    iIndex = i;
                    break;
               }
          }
          return iIndex;
     }

     private String getTableName()
     {
          int iCode = common.toInt(SDeptCode);
          if(iCode == 2)
               return "BlowTran";
          else if(iCode == 3)
               return "CardTran";
          else if(iCode == 4)
               return "CombTran";
          else if(iCode == 5)
               return "DrawTran";
          else if(iCode == 6)
               return "SimpTran";
          return "SLapTran";
     }
     private void fillCodeNames()
     {
          SCode = new String[5];
          SName = new String[5];

          int iCode = common.toInt(SDeptCode);
          if(iCode == 2)
          {
               SCode[0] = "25";
               SName[0] = "LapRod Shortage   ";
               SCode[1] = "10";
               SName[1] = "Absenteeism       ";
               SCode[2] = "26";
               SName[2] = "Want Of Mixing    ";
               SCode[3] = "28";
               SName[3] = "Wheel Changes     ";
               SCode[4] = "0";
               SName[4] = "                  ";
          }
          else if(iCode == 3)
          {
               SCode[0] = "33";
               SName[0] = "Lap Shortage      ";
               SCode[1] = "10";
               SName[1] = "Absenteeism       ";
               SCode[2] = "18";
               SName[2] = "Can Shortage      ";
               SCode[3] = "28";
               SName[3] = "Wheel Changes     ";
               SCode[4] = "0";
               SName[4] = "                  ";
          }
          else if(iCode == 4)
          {
               SCode[0] = "33";
               SName[0] = "Lap Shortage      ";
               SCode[1] = "10";
               SName[1] = "Absenteeism       ";
               SCode[2] = "8";
               SName[2] = "Can Shortage      ";
               SCode[3] = "28";
               SName[3] = "Wheel Changes     ";
               SCode[4] = "0";
               SName[4] = "                  ";
          }
          else if(iCode == 5)
          {
               SCode[0] = "8";
               SName[0] = "Empties Shortage  ";
               SCode[1] = "10";
               SName[1] = "Absenteeism       ";
               SCode[2] = "34";
               SName[2] = "Card Can Shortage ";
               SCode[3] = "28";
               SName[3] = "Wheel Changes     ";
               SCode[4] = "35";
               SName[4] = "Wrap Wheel Changes";
          }
          else if(iCode == 6)
          {
               SCode[0] = "18";
               SName[0] = "Can Shortage      ";
               SCode[1] = "10";
               SName[1] = "Absenteeism       ";
               SCode[2] = "11";
               SName[2] = "Want Of Bobbin    ";
               SCode[3] = "28";
               SName[3] = "Wheel Changes     ";
               SCode[4] = "0";
               SName[4] = "                  ";
          }
          else if(iCode == 9)
          {
               SCode[0] = "33";
               SName[0] = "Lap Spool Shortage";
               SCode[1] = "10";
               SName[1] = "Absenteeism       ";
               SCode[2] = "8";
               SName[2] = "Can Shortage      ";
               SCode[3] = "28";
               SName[3] = "Wheel Changes     ";
               SCode[4] = "0";
               SName[4] = "                  ";
          }
     }
     private String getQS()
     {
          String STable = getTableName();

          String QS="";
             QS = " SELECT Machine.mach_name, "+STable+".shift_code, "+STable+".ticket, "+
                    STable+".ticket, Sum("+STable+".prod) AS SumOfprod, "+
                  " Sum("+STable+".prod_tar-"+STable+".prod) AS Expr1, "+
                  " Avg("+STable+".ueoa) AS AvgOfueoa, Avg("+STable+".uem) AS AvgOfuem, "+
                  " Avg("+STable+".workereffy) AS AvgOfworkereffy, "+
                  " Sum("+STable+".noofpersons) AS SumOfnoofpersons, "+
                  " Sum("+STable+".rotime) AS SumOfrotime, "+
                  " Sum("+STable+".noofpersonsac) AS SumOfnoofpersonsac, "+
                  " Sum("+STable+".rotimeac) AS SumOfrotimeac, "+STable+".Mach_Code, "+
                    STable+".sp_date "+ 
                  " FROM ("+STable+" INNER JOIN Machine ON "+STable+".Mach_Code=Machine.mach_code) "+
                  " INNER JOIN supervisor ON "+STable+".supcode=supervisor.supcode "+
                  " WHERE "+STable+".supcode="+SOffCode+
                  " And "+STable+".sp_date>='"+SStDate+"'"+
                  " And "+STable+".sp_date<='"+SEnDate+"'"+
                  " And "+STable+".Unit_Code="+SUnitCode+ 
                  " GROUP BY Machine.mach_name, "+STable+".shift_code,"+STable+".ticket, "+
                  STable+".Mach_Code, "+STable+".sp_date ";
		   if(STable.equals("SLapTran"))
			   QS = QS + " ORDER BY 15,2, 3, val(Machine.mach_name) ";
		   else
			   QS = QS + " ORDER BY 15,2, 3, to_number(Machine.mach_name) ";
          return QS;
     }
     private String getOCQS()
     {
          String QS="";

            QS =  " SELECT StopTran.Mach_Code, StopTran.Stop_Date,StopTran.Stop_Shift, "+
                  " Sum(StopTran.Stop_Mint) AS SumOfStop_Mint, Stop2.OffRepCode "+
                  " FROM Stop2 INNER JOIN StopTran ON Stop2.stop_code=StopTran.Stop_Code "+
                  " WHERE StopTran.Dept_Code="+SDeptCode+
                  " And StopTran.Unit_Code="+SUnitCode+
                  " And StopTran.Stop_Date>='"+SStDate+"'"+
                  " And StopTran.Stop_Date<='"+SEnDate+"'"+
                  " GROUP BY StopTran.Mach_Code, StopTran.Stop_Date,StopTran.Stop_Shift, stop2.offrepcode "+
                  " ORDER BY 1, 2, 4";

          return QS;
     }
     private String getStop2QS()
     {
          String QS="";

          QS = " create table Stop2 as "+
               " (SELECT stopreasons.stop_name, Stoppage.stop_code, "+
               " Stoppage.dept_code, Stoppage.OffRepCode "+
               " FROM stopreasons INNER JOIN Stoppage ON stopreasons.STOP_CODE = Stoppage.stop_code "+
               " WHERE Stoppage.dept_code="+SDeptCode+" AND Stoppage.OffRepCode>0 "+
               " ORDER BY Stoppage.OffRepCode)";

          return QS;
     }
     private String getTarQS()
     {
          String QS = " Select work_effy,runoutstd,runoutacstd "+
                      " from Target where dept_code="+SDeptCode+
                      " and unit_code = "+SUnitCode;
          return QS;
     }
     public void prtWorkRep()
     {
          try
          {
               FW       = new FileWriter("/production/Reports/poperiodc.prn");
               iLctr    = 70;
               iPctr    =  0;
               Head();
               Body();
               FW.close();
          }catch(Exception ex){System.out.println("111 : "+ex);ex.printStackTrace();}
     }
     private void Head()
     {
          if(iLctr < 60)
             return;
          if(iPctr > 0)
          {
             try
             {
               FW.write(SLine+"\n");
             }catch(Exception ex){System.out.println(ex);}
          }
          iPctr++;

          String Str1    = common.Pad("Company    : AMARJOTHI SPINNING MILLS LIMITED",80);
          String Str2    = common.Pad("Document   : "+SDept+" Officer Effy Report "+common.parseDate(SStDate)+" - "+common.parseDate(SEnDate),80);
          String Str3    = common.Pad("Unit       : "+(SUnitCode.equals("1")?"A":(SUnitCode.equals("2")?"B":"S")),70);
          String Str4    = common.Pad("Officer    : "+SOfficer+"   : No : "+SOffCode+" : Designation : "+SDesig,80);

          String SHead1  = common.Pad(" ",10)+common.Space(2)+
                           common.Pad(" ",3)+common.Space(2)+
                           common.Pad(" ",3)+common.Space(2)+
                           common.Pad(" ",6)+common.Space(2)+
                           common.Pad(" ",6)+common.Space(2)+
                           common.Cad("Worker",6+2+20)+common.Space(2)+
                           common.Cad("Production",9+2+9)+common.Space(2)+
                           common.Cad("Efficiency",6+2+6+2+6)+common.Space(2)+
                           common.Cad("Officer's Reponse",7+2+7+2+7+2+7+2+7)+common.Space(2)+
                           common.Cad("Worker's Reponse",7+2+7+2+7);

          String SHead2  = common.Pad(" ",10)+common.Space(2)+
                           common.Pad(" ",3)+common.Space(2)+
                           common.Pad(" ",3)+common.Space(2)+
                           common.Pad(" ",6)+common.Space(2)+
                           common.Pad(" ",6)+common.Space(2)+
                           common.Replicate("-",6+2+20)+common.Space(2)+
                           common.Replicate("-",9+2+9)+common.Space(2)+
                           common.Replicate("-",6+2+6+2+6)+common.Space(2)+
                           common.Replicate("-",7+2+7+2+7+2+7+2+7)+common.Space(2)+
                           common.Replicate("-",7+2+7+2+7);

          String SHead3  = common.Pad("Date",10)+common.Space(2)+
                           common.Pad("Sht",3)+common.Space(2)+
                           common.Pad("Mac",3)+common.Space(2)+
                           common.Pad("Count",6)+common.Space(2)+
                           common.Pad("Shade",6)+common.Space(2)+
                           common.Rad("No",6)+common.Space(2)+
                           common.Rad("Name",20)+common.Space(2)+
                           common.Rad("Actual",9)+common.Space(2)+
                           common.Rad("Prod",9)+common.Space(2)+
                           common.Rad("UxE OA",6)+common.Space(2)+
                           common.Rad("UxE M",6)+common.Space(2)+
                           common.Rad("Work",6)+common.Space(2)+
                           common.Rad(SName[0].substring(0,7),7)+common.Space(2)+
                           common.Rad(SName[1].substring(0,7),7)+common.Space(2)+
                           common.Rad(SName[2].substring(0,7),7)+common.Space(2)+
                           common.Rad(SName[3].substring(0,7),7)+common.Space(2)+
                           common.Rad(SName[4].substring(0,7),7)+common.Space(2)+
                           common.Rad("SC",7)+common.Space(2)+
                           common.Rad("SC with",7)+common.Space(2)+
                           common.Rad("Shift",7);

          String SHead4  = common.Pad(" ",10)+common.Space(2)+
                           common.Pad(" ",3)+common.Space(2)+
                           common.Pad(" ",3)+common.Space(2)+
                           common.Pad(" ",6)+common.Space(2)+
                           common.Pad(" ",6)+common.Space(2)+
                           common.Rad(" ",6)+common.Space(2)+
                           common.Rad(" ",20)+common.Space(2)+
                           common.Rad("Prod",9)+common.Space(2)+
                           common.Rad("Loss",9)+common.Space(2)+
                           common.Rad(" ",6)+common.Space(2)+
                           common.Rad(" ",6)+common.Space(2)+
                           common.Rad("Effy",6)+common.Space(2)+
                           common.Rad(SName[0].substring(7,14),7)+common.Space(2)+
                           common.Rad(SName[1].substring(7,14),7)+common.Space(2)+
                           common.Rad(SName[2].substring(7,14),7)+common.Space(2)+
                           common.Rad(SName[3].substring(7,14),7)+common.Space(2)+
                           common.Rad(SName[4].substring(7,14),7)+common.Space(2)+
                           common.Rad("With AC",7)+common.Space(2)+
                           common.Rad("out AC",7)+common.Space(2)+
                           common.Rad("end AC",7);

          String SHead5  = common.Pad(" ",10)+common.Space(2)+
                           common.Pad(" ",3)+common.Space(2)+
                           common.Pad(" ",3)+common.Space(2)+
                           common.Pad(" ",6)+common.Space(2)+
                           common.Pad(" ",6)+common.Space(2)+
                           common.Rad(" ",6)+common.Space(2)+
                           common.Rad(" ",20)+common.Space(2)+
                           common.Rad("Kg",9)+common.Space(2)+
                           common.Rad("Kg",9)+common.Space(2)+
                           common.Rad("%",6)+common.Space(2)+
                           common.Rad("%",6)+common.Space(2)+
                           common.Rad("%",6)+common.Space(2)+
                           common.Rad("Min",7)+common.Space(2)+
                           common.Rad("Min",7)+common.Space(2)+
                           common.Rad("Min",7)+common.Space(2)+
                           common.Rad("Min",7)+common.Space(2)+
                           common.Rad("Min",7)+common.Space(2)+
                           common.Rad("Man Min",7)+common.Space(2)+
                           common.Rad("Man Min",7)+common.Space(2)+
                           common.Rad("Man Min",7);

          String SHead6  = common.Pad("Target",10)+common.Space(2)+
                           common.Pad(" ",3)+common.Space(2)+
                           common.Pad(" ",3)+common.Space(2)+
                           common.Pad(" ",6)+common.Space(2)+
                           common.Pad(" ",6)+common.Space(2)+
                           common.Rad(" ",6)+common.Space(2)+
                           common.Rad(" ",20)+common.Space(2)+
                           common.Rad(" ",9)+common.Space(2)+
                           common.Rad(" ",9)+common.Space(2)+
                           common.Rad(" ",6)+common.Space(2)+
                           common.Rad(" ",6)+common.Space(2)+
                           common.Rad(common.getRound(dEffyTar,2),6)+common.Space(2)+
                           common.Rad(" ",7)+common.Space(2)+
                           common.Rad(" ",7)+common.Space(2)+
                           common.Rad(" ",7)+common.Space(2)+
                           common.Rad(" ",7)+common.Space(2)+
                           common.Rad(" ",7)+common.Space(2)+
                           common.Rad(common.getRound(dClgTar,2),7)+common.Space(2)+
                           common.Rad(common.getRound(dClgACTar,2),7)+common.Space(2)+
                           common.Rad(" ",7);

          SLine = common.Replicate("-",SHead6.length())+"\n";

          try              
          {
               FW.write(""+Str1+"\n");
               FW.write(Str2+"\n");
               FW.write(Str3+"\n");
               FW.write(Str4+"\n\n");

               FW.write(SLine);
               FW.write(SHead1+"\n");
               FW.write(SHead2+"\n");
               FW.write(SHead3+"\n");
               FW.write(SHead4+"\n");
               FW.write(SHead5+"\n");
               FW.write(SLine);
               FW.write(SHead6+"\n");
               FW.write(SLine);
          }catch(Exception ex){System.out.println("222 : "+ex);}
          iLctr = 14;
     }
     private void Body() throws Exception
     {
          String Strl = "";

          for(int i=0;i<VMach.size();i++)
          {
               Strl = common.Pad(common.parseDate((String)VDate.elementAt(i)),10)+common.Space(2)+
                      common.Pad((String)VShift.elementAt(i),3)+common.Space(2)+
                      common.Pad((String)VMach.elementAt(i),3)+common.Space(2)+
                      common.Pad((String)VCount.elementAt(i),6)+common.Space(2)+
                      common.Pad("-",6)+common.Space(2)+
                      common.Rad((String)VWorkCode.elementAt(i),6)+common.Space(2)+
                      common.Rad((String)VWork.elementAt(i),20)+common.Space(2)+
                      common.Rad((String)VProd.elementAt(i),9)+common.Space(2)+
                      common.Rad((String)VLoss.elementAt(i),9)+common.Space(2)+
                      common.Rad((String)VUeOA.elementAt(i),6)+common.Space(2)+
                      common.Rad((String)VUeM.elementAt(i),6)+common.Space(2)+
                      common.Rad((String)VWorkEffy.elementAt(i),6)+common.Space(2);

               int iMCode = common.toInt((String)VMachCode.elementAt(i));
               int iDate  = common.toInt((String)VDate.elementAt(i));
               int iShift = common.toInt((String)VShift.elementAt(i));

               MachClass MC = (MachClass)VMachClass.elementAt(indexOf(iMCode,iDate,iShift));

               Strl+= common.Rad(MC.S1,7)+common.Space(2)+
                      common.Rad(MC.S2,7)+common.Space(2)+
                      common.Rad(MC.S3,7)+common.Space(2)+
                      common.Rad(MC.S4,7)+common.Space(2)+
                      common.Rad(MC.S5,7)+common.Space(2)+
                      common.Rad((String)VAC.elementAt(i),7)+common.Space(2)+
                      common.Rad((String)VWithoutAC.elementAt(i),7)+common.Space(2)+
                      common.Rad(MC.SClean,7);

               Head();
               try
               {
                  FW.write(Strl+"\n");
               }catch(Exception ex){System.out.println("333 : "+ex);}
               iLctr++;
          }
          Strl = common.Pad("Total/Avg",10)+common.Space(2)+
                 common.Pad(" ",3)+common.Space(2)+
                 common.Pad(" ",3)+common.Space(2)+
                 common.Pad(" ",6)+common.Space(2)+
                 common.Pad(" ",6)+common.Space(2)+
                 common.Rad(" ",6)+common.Space(2)+
                 common.Rad(" ",20)+common.Space(2)+
                 common.Rad(common.getSum(VProd,"S"),9)+common.Space(2)+
                 common.Rad(common.getSum(VLoss,"S"),9)+common.Space(2)+
                 common.Rad(common.getSum(VUeOA,"A"),6)+common.Space(2)+
                 common.Rad(common.getSum(VUeM,"A"),6)+common.Space(2)+
                 common.Rad(common.getSum(VWorkEffy,"A"),6)+common.Space(2);

          double d1=0,d2=0,d3=0,d4=0,d5=0,d9=0;
          for(int i=0;i<VMachClass.size();i++)
          {
               MachClass MC = (MachClass)VMachClass.elementAt(i);
               d1 += common.toDouble(MC.S1);
               d2 += common.toDouble(MC.S2);
               d3 += common.toDouble(MC.S3);
               d4 += common.toDouble(MC.S4);
               d5 += common.toDouble(MC.S5);
               d9 += common.toDouble(MC.SClean);
          }
          Strl+= common.Rad(common.getRound(d1,0),7)+common.Space(2)+
                 common.Rad(common.getRound(d2,0),7)+common.Space(2)+
                 common.Rad(common.getRound(d3,0),7)+common.Space(2)+
                 common.Rad(common.getRound(d4,0),7)+common.Space(2)+
                 common.Rad(common.getRound(d5,0),7)+common.Space(2)+
                 common.Rad(common.getSum(VAC,"S"),7)+common.Space(2)+
                 common.Rad(common.getSum(VWithoutAC,"S"),7)+common.Space(2)+
                 common.Rad(common.getRound(d9,0),7);

          try
          {
                FW.write(SLine);
                FW.write(Strl+"\n");
                FW.write(SLine);
                FW.write("\n\n\n\nP.O          S.P.O.           S.M.             D.M.\n");
                FW.write("<End Of Report>");
          }catch(Exception ex){System.out.println("444 : "+ex);}
     }

	  private String getTicket(String SEmpCode)
	  {	
		   String SRet="@";
		   try
		   {			   
			   String SQry = " select empname from hrdnew.SchemeApprentice where empcode="+SEmpCode+" union " +
							 " select empname from hrdnew.ActApprentice where empcode="+SEmpCode+" union  " + 
							 " select empname from hrdnew.ContractApprentice where empcode="+SEmpCode;
                           Connection conn = null;
			   Class.forName("oracle.jdbc.OracleDriver");
                           if(conn==null){
                                    conn = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
                           }
			   Statement stat  = conn.createStatement();
			   ResultSet res   = stat.executeQuery(SQry);
			   if(res.next())
				   SRet            = res.getString(1);
//			   System.out.println(SQry + ":" + SRet);
		   }catch(Exception ee)
		   {       ee.printStackTrace();
			   System.out.println("getTicket->" + ee);
			   return SRet;
		   }
		   return SRet;
      }


	private Connection createConnection()
	{
		Connection conn;
		try
		{
			Class.forName("oracle.jdbc.OracleDriver");
			conn  = DriverManager.getConnection(common.getOraDSN(),common.getOraUserName(),common.getOraPassword());
		}catch(Exception ee)
		{
			return null;
		}
		return conn;
	}

}
